package de.ugoe.cs.swe.T3Q;

import java.util.List;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.validation.FeatureBasedDiagnostic;

import com.google.common.collect.Lists;

public class TTCN3Dependency {
	private final Resource resource;
	private final List<String> messages;
	private final List<String> output;
	
	public TTCN3Dependency(final Resource resource) {
		this.resource = resource;
		this.messages  = Lists.newArrayList();
		this.output  = Lists.newArrayList();
	}

	public Resource getResource() {
		return resource;
	}

	public List<String> getOutput() {
		return output;
	}
	public List<String> getMessages() {
		return messages;
	}
}
