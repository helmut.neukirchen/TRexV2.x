@echo off

set JAVA_CMD=java -Xmx3g -Xss512m -jar .\t3q.jar

if "%1" == "--echo" (
	echo %JAVA_CMD%
) else (
	%JAVA_CMD% %*
)
