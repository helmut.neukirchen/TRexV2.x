/******************************************************************************/
// AUTOMATICALLY GENERATED MODULE - DO NOT MODIFY
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-04 18:24:57 +0100 (Fri, 04 Mar 2016) $
// $Rev: 15510 $
/******************************************************************************/

module IMS_CommonParameters {

  import from IP_ASP_TypeDefs all;
  
  //****************************************************************************
  // PIXIT Parameters
  //----------------------------------------------------------------------------
  modulepar charstring                  px_IMS_AssociatedTelUri     := "tel:+331234567";
                                                                                        // @desc  Set as record 3 in EFIMPU as defined in TS 34.229-1[5]
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_IMS_CalleeContactUri     := "sip:User-B-Contact@3gpp.org";
                                                                                        // @desc  URI provided by the remote side (i.e. by SS) to be used by the UE as contact address in further SIP signalling of the dialog
                                                                                        // NOTE: in general this URI shall be different than the one in px_CalleeUri
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_IMS_CalleeUri            := "sip:User-B@3gpp.org";
                                                                                        // @desc  URI of Callee, sent by the UE in INVITE (MO call establishment) to address the remote UE
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar IPsec_CipheringAlgorithm_Type px_IMS_CiphAlgo_Def         := des_ede3_cbc;  // @desc  Ciphering Algorithm
                                                                                        // Supported values:
                                                                                        // des_ede3_cbc,
                                                                                        // aes_cbc,
                                                                                        // nociph
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_IMS_HomeDomainName       := "3gpp.org";      // @desc  Home Domain Name.
                                                                                        // When using an ISIM it is set to the same value as EFDOMAIN.
                                                                                        // When not using ISIM just USIM the home domain name is derived from px_IMSI (preceded by 'sip:')
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar IPsec_IntegrityAlgorithm_Type px_IMS_IPSecAlgorithm       := hmac_sha_1_96; // @desc  Integrity Algorithm
                                                                                        // Supported values:
                                                                                        // hmac_md5_96,
                                                                                        // hmac_sha_1_96
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_IMS_Private_UserId       := "privateuser@3gpp.org";
                                                                                        // @desc  Private User Identity.When usingan ISIM this is set to the same value as EFIMPI.When ISIM is not used just USIM the private user identity is derived from px_IMSI.
                                                                                        //
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_IMS_PublicUserIdentity1  := "sip:PublicId1@3gpp.org";
                                                                                        // @desc  Public User Identity. It is set to the same value as the first record in EFIMPU.
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_IMS_PublicUserIdentity2  := "sip:PublicId2@3gpp.org";
                                                                                        // @desc  Public User Identity. It is set to the same value as the second record in EFIMPU.
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_IMS_PublicUserIdentity3  := "sip:PublicId3@3gpp.org";
                                                                                        // @desc  Public User Identity. It is set to the same value as the third record in EFIMPU.
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_IMS_Scscf                := "scscf.3gpp.org";
                                                                                        // @desc  S-CSCF fully qualified domain name that resolves to the IP address of SS
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar boolean                     px_UEwithISIM               := true;            // @desc  true - UE has ISIM; false - UE has USIM only
                                                                                        // Reference to other Spec: 34.229-3
  
  
  //****************************************************************************
  // PICS Parameters
  //----------------------------------------------------------------------------
  modulepar boolean                     pc_IMS_Deregistration       := false;           // @desc  UE has the method that supports IMS deregistration
                                                                                        // Reference: 34.229-2 Table A.12/39
  
  modulepar boolean                     pc_IMS_GIBA_Sec             := false;           // @desc  UE supports GIBA security only
                                                                                        // Reference: 34.229-2 Table A.6a/1
  
  modulepar boolean                     pc_IMS_GRUUsInSIP           := false;           // @desc  UE supports obtaining and using GRUUs in the Session Initiation Protocol (SIP)
                                                                                        // Reference: 34.229-2 Table A.4/53
  
  modulepar boolean                     pc_IMS_Geolocation          := false;           // @desc  UE is capable of obtaining location information
                                                                                        // Reference: 34.229-2 Table A.12/27
  
  modulepar boolean                     pc_IMS_Sec                  := false;           // @desc  UE supports full IMS security
                                                                                        // Reference: 34.229-2 Table A.6a/2
  
  modulepar boolean                     pc_IMS_SessionIdSupported   := false;           // @desc  UE supports session id
                                                                                        // Reference: 34.229-2 Table A.12/30
  
  modulepar boolean                     pc_IMS_Video_FeatureTag     := false;           // @desc  UE indicates video media feature tag in REGISTER and INVITE request
                                                                                        // Reference: 34.229-2 Table A.12/32
  
  modulepar boolean                     pc_InitiateSession          := false;           // @desc  Initiating a session
                                                                                        // Reference: 34.229-2 Table A.4/2B
  
  modulepar boolean                     pc_MTSI_Speech              := false;           // @desc  MTSI media - Speech
                                                                                        // Reference: 34.229-2 Table A.15/1
  
  modulepar boolean                     pc_MTSI_Video               := false;           // @desc   MTSI media - Video
                                                                                        // Reference: 34.229-2 Table A.15/3
  
  modulepar boolean                     pc_MultimediaTelephonyService := false;         // @desc  Multimedia telephony service participant
                                                                                        // Reference: 34.229-2 Table A.3A/50
  
  modulepar boolean                     pc_SMS_MT                   := false;           // @desc  UE supports SM-over-IP receiver
                                                                                        // Reference: 34.229-2 Table A.3A/62
  
  modulepar boolean                     pc_TMPI_forGBA              := false;           // @desc  UE uses TMPI in HTTP request for GBA
                                                                                        // Reference: 34.229-2 Table A.12/44
  
  
}
