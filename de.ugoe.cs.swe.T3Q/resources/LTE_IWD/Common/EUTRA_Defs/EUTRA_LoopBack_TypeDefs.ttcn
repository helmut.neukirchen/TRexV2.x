/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-05 20:19:01 +0100 (Sat, 05 Mar 2016) $
// $Rev: 15516 $
/******************************************************************************/
module EUTRA_LoopBack_TypeDefs {
  import from CommonDefs all;
  import from NAS_CommonTypeDefs all;

  //----------------------------------------------------------------------------
  // General Info Elements (36.509 )
  //----------------------------------------------------------------------------

  type Octet_Type       UE_TestLoopMode_Type;
  type Octet_Type       UE_TestLoopModeB_LB_Setup_Type;
  type Octet_Type       UE_PositioningTechnology_Type;
  type O4_Type          MBMS_PacketCounterValue_Type;
  const integer tsc_MAX_ModeA_LB_Entities := 8;
  const integer tsc_MAX_ModeD_Monitor_Entities := 400; // 36.509 cl 7.2
  const integer tsc_MAX_ModeE_Monitor_Entities := 16;   // 36.509 cl 7.2

  //****************************************************************************
  // Test Loop PROTOCOL DEFINITIONS: PDUs
  //----------------------------------------------------------------------------
  // Special conformance testing functions for User Equipment messages (36.509 cl. 6)
  //----------------------------------------------------------------------------

  // =============================================================================
  // Close UE Test Loop
  // 3G TS 36.509 cl. 6.1
  // Direction:     SS to UE
  // -----------------------------------------------------------------------------

  type record CLOSE_UE_TEST_LOOP {
    SkipIndicator                       skipIndicator,                          /*    M V 1/2    */
    ProtocolDiscriminator               protocolDiscriminator,                  /*    M V 1/2    */
    MessageType                         messageType,                            /*    M V 1      */
    UE_TestLoopMode_Type                ueTestLoopMode,                         /*    M V 1      */
    UE_TestLoopModeA_LB_Setup_Type      ueTestLoopModeA_LB_Setup optional,      /*    C LV 1-25  */
    /* present if ueTestLoopMode ='00'F */
    UE_TestLoopModeB_LB_Setup_Type      ueTestLoopModeB_LB_Setup optional,       /*    C V 1      */
    /* present if ueTestLoopMode ='01'F */
    /* represents IP PDU delay time 0..255 seconds */
    UE_TestLoopModeC_LB_Setup_Type      ueTestLoopModeC_LB_Setup optional,       /*    C V 3      */
    //@sic eMBMS sic@
   /* present if ueTestLoopMode ='02'H */
    // @sic R5-155362 D2D sic@
    UE_TestLoopModeD_LB_Setup_Type      ueTestLoopModeD_LB_Setup optional,      /*    C LV  3-803 */
    /* This IE is mandatory present if the IE "UE test loop mode" is set to UE test loop Mode D. Else it shall be absent. */
    // @sic R5-155362 D2D sic@
    UE_TestLoopModeE_LB_Setup_Type      ueTestLoopModeE_LB_Setup optional       /*    C LV  2-18 */
    /* This IE is mandatory present if the IE "UE test loop mode" is set to UE test loop Mode E. Else it shall be absent. */

   };
  // =============================================================================
  // Structured Type Definition
  // *** Comments:
  // UE Test Loop Mode A LB Setup  36.509 cl. 6.1
  // *** additional Notes:
  // The maximum number of LB entities in the LB setup list is less than or equal to 5.
  // -----------------------------------------------------------------------------
  type record UE_TestLoopModeA_LB_Setup_Type {
    O1_Type                             iel,
    LB_SetupDRB_IE_List_Type            lbSetupDrbList optional
  };
// =============================================================================
  // Structured Type Definition
  // *** Comments:
  // UE Test Loop Mode C LB Setup  36.509 cl. 6.1
  // =============================================================================
   type record UE_TestLoopModeC_LB_Setup_Type {
    B8_Type                            mBSFN_AreaId,  //MBSFN area Identity  0.. 255 Binary coded
    B4_Type                            reservedB1,
    B4_Type                            pMCHIdentity, // MCH identity 0.. 14 (binary coded,
    B3_Type                            reservedB2,
    B5_Type                            logChId //Logical channel identity 0..28 (binary coded,
  };

  // =============================================================================
  // LB Setup DRB IE
  // 3G TS 36.509 cl. 6.1
  // -----------------------------------------------------------------------------
  type record LB_SetupDRB_IE_Type {
    B16_Type                            uplinkPdcpSduSize,
    B3_Type                             reserved,
    B5_Type                             drbIdentity
  };

  type record length (1..tsc_MAX_ModeA_LB_Entities)of LB_SetupDRB_IE_Type LB_SetupDRB_IE_List_Type;

    // =============================================================================
  // Structured Type Definition
  // *** Comments:
  // UE Test Loop Mode D LB Setup  36.509 cl. 6.1
  // =============================================================================
   type record UE_TestLoopModeD_LB_Setup_Type {
    B7_Type                            reserved,
    B1_Type                            discAnnounceOrMonitor,   // D0 - Discovery Announce or Monitor
    B16_Type                           discMonitorListLength,   // Length of UE test loop mode D monitor list in bytes
    DiscMonitorList_Type               dicMonitorList optional
  };
  
  type record length (1..tsc_MAX_ModeD_Monitor_Entities) of DiscMonitor_Type DiscMonitorList_Type;  // Monitor list
  
  type record DiscMonitor_Type {
    //ProSe App Code (LSBs) #n to monitor
    B1_Type             proSeAppCodeLSB_A7,
    B1_Type             proSeAppCodeLSB_A6,
    B1_Type             proSeAppCodeLSB_A5,
    B1_Type             proSeAppCodeLSB_A4,
    B1_Type             proSeAppCodeLSB_A3,
    B1_Type             proSeAppCodeLSB_A2,
    B1_Type             proSeAppCodeLSB_A1,
    B1_Type             proSeAppCodeLSB_A0,
    B7_Type             reserved,
    B1_Type             proSeAppCodeLSB_A9
  }
   
  // =============================================================================
  // Structured Type Definition
  // *** Comments:
  // UE Test Loop Mode E LB Setup  36.509 cl. 6.1
  // =============================================================================
   type record UE_TestLoopModeE_LB_Setup_Type {
    B7_Type                            reserved,
    B1_Type                            commTransmitOrReceive,   // Communication Transmit or Receive
    B8_Type                            commMonitorListLength,   // Length of UE test loop mode D monitor list in bytes
    CommMonitorList_Type               commMonitorList optional
  };
  
  type record length (1..tsc_MAX_ModeE_Monitor_Entities) of CommMonitor_Type CommMonitorList_Type;
  type B8_Type CommMonitor_Type;

  // =============================================================================
  // Close UE Test Loop Complete
  // 3G TS 36.509 cl. 6.2
  // Direction:     UE to SS
  // -----------------------------------------------------------------------------
  type record CLOSE_UE_TEST_LOOP_COMPLETE {
    SkipIndicator                       skipIndicator,                  /*    M V 1/2    */
    ProtocolDiscriminator               protocolDiscriminator,          /*    M V 1/2    */
    MessageType                         messageType                     /*    M V 1      */
  };

  // =============================================================================
  // OPEN UE Test Loop
  // 3G TS 36.509 cl. 6.3
  // Direction:     SS  to UE
  // -----------------------------------------------------------------------------
  type record OPEN_UE_TEST_LOOP {
    SkipIndicator                       skipIndicator,                  /*    M V 1/2    */
    ProtocolDiscriminator               protocolDiscriminator,          /*    M V 1/2    */
    MessageType                         messageType                     /*    M V 1      */
  };

  // =============================================================================
  // OPEN UE Test Loop Complete
  // 3G TS 36.509 cl. 6.4
  // Direction:     UE to SS
  // -----------------------------------------------------------------------------
  type record OPEN_UE_TEST_LOOP_COMPLETE {
    SkipIndicator                       skipIndicator,                  /*    M V 1/2    */
    ProtocolDiscriminator               protocolDiscriminator,          /*    M V 1/2    */
    MessageType                         messageType                     /*    M V 1      */
  };

  // =============================================================================
  // Activate RB Test Mode
  // 3G TS 36.509 cl. 6.5
  // Direction:     SS  to UE
  // -----------------------------------------------------------------------------
  type record ACTIVATE_TEST_MODE {
    SkipIndicator                       skipIndicator,                  /*    M V 1/2    */
    ProtocolDiscriminator               protocolDiscriminator,          /*    M V 1/2    */
    MessageType                         messageType,                    /*    M V 1      */
    UE_TestLoopMode_Type                ueTestLoopMode                  /*    M V 1      */
  };

  // =============================================================================
  // Activate RB Test Mode Complete
  // 3G TS 36.509 cl. 6.6
  // Direction:     UE to SS
  // -----------------------------------------------------------------------------
  type record ACTIVATE_TEST_MODE_COMPLETE {
    SkipIndicator                       skipIndicator,                  /*    M V 1/2    */
    ProtocolDiscriminator               protocolDiscriminator,          /*    M V 1/2    */
    MessageType                         messageType                     /*    M V 1      */
  };

  // =============================================================================
  // Deactivate RB Test Mode
  // 3G TS 36.509 cl. 6.7
  // Direction:     SS  to UE
  // -----------------------------------------------------------------------------
  type record DEACTIVATE_TEST_MODE {
    SkipIndicator                       skipIndicator,                  /*    M V 1/2    */
    ProtocolDiscriminator               protocolDiscriminator,          /*    M V 1/2    */
    MessageType                         messageType                     /*    M V 1      */
  };

  // =============================================================================
  // Deactivate RB Test Mode Complete
  // 3G TS 36.509 cl. 6.8
  // Direction:     UE to SS
  // -----------------------------------------------------------------------------
   type record DEACTIVATE_TEST_MODE_COMPLETE {
     SkipIndicator                       skipIndicator,                 /*    M V 1/2    */
     ProtocolDiscriminator               protocolDiscriminator,         /*    M V 1/2    */
     MessageType                         messageType                    /*    M V 1      */
   };

   // =============================================================================
  // UE TEST LOOP MODE C MBMS PACKET COUNTER REQUEST
  // 3G TS 36.509 cl. 6.10
  // Direction:    SS to UE
  // -----------------------------------------------------------------------------
  type record UE_TESTLOOPMODEC_MBMS_PACKETCOUNTERREQUEST {
    SkipIndicator                       skipIndicator,                  /*    M V 1/2    */
    ProtocolDiscriminator               protocolDiscriminator,          /*    M V 1/2    */
    MessageType                         messageType                     /*    M V 1      */
  };

  // =============================================================================
  // UE TEST LOOP MODE C MBMS PACKET COUNTER RESPONSE
  // 3G TS 36.509 cl. 6.11
  // Direction:     UE to SS
  // -----------------------------------------------------------------------------
  type record UE_TESTLOOPMODEC_MBMS_PACKETCOUNTERRESPONSE {
    SkipIndicator                       skipIndicator,                  /*    M V 1/2    */
    ProtocolDiscriminator               protocolDiscriminator,          /*    M V 1/2    */
    MessageType                         messageType,                    /*    M V 1      */
    MBMS_PacketCounterValue_Type        MBMS_PacketCounterValue          /*   M V 4      */
  };

  // =============================================================================
  // UE TEST LOOP PROSE PACKET COUNTER REQUEST
  // 3G TS 36.509 cl. 6.13
  // Direction:    SS to UE
  // -----------------------------------------------------------------------------
  type record UE_TESTLOOP_PROSE_PACKETCOUNTERREQUEST {
    SkipIndicator                       skipIndicator,                  /*    M V 1/2    */
    ProtocolDiscriminator               protocolDiscriminator,          /*    M V 1/2    */
    MessageType                         messageType                     /*    M V 1      */
  };

  // =============================================================================
  // UE TEST LOOP PROSE PACKET COUNTER RESPONSE
  // 3G TS 36.509 cl. 6.14
  // Direction:     UE to SS
  // -----------------------------------------------------------------------------
  type record UE_TESTLOOP_PROSE_PACKETCOUNTERRESPONSE {
    SkipIndicator                           skipIndicator,                  /*    M V 1/2    */
    ProtocolDiscriminator                   protocolDiscriminator,          /*    M V 1/2    */
    MessageType                             messageType,                    /*    M V 1      */
    ProSeDirectDiscPacketCounter_Type       proSeDirectDiscPacketCounter optional, /* CV-ModeD  V 4 * ND + 1 */
    ProSeDirectCommPSCCH_PacketCounter_Type proSeDirectCommPSCCH_PacketCounter optional, /* CV-ModeE V 4 * NC + 1 */
    ProSeDirectCommSTCH_PacketCounter_Type  proSeDirectCommSTCH_PacketCounter optional   /* CV-ModeE V 4* NC + 1 */
  };

  type record length(1..tsc_MAX_ModeD_Monitor_Entities) of O4_Type ProSeDirectDiscPacketCounter_Type;
  type record length(1..tsc_MAX_ModeE_Monitor_Entities) of O4_Type ProSeDirectCommPSCCH_PacketCounter_Type;
  type record length(1..tsc_MAX_ModeE_Monitor_Entities) of O4_Type ProSeDirectCommSTCH_PacketCounter_Type;
  
} with { encode "NAS Types"}
