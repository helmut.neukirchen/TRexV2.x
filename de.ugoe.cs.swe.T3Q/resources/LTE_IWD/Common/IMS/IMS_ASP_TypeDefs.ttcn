/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2015-06-26 20:08:06 +0200 (Fri, 26 Jun 2015) $
// $Rev: 13970 $
/******************************************************************************/

module IMS_ASP_TypeDefs {
  import from CommonDefs all;
  import from LibSip_SIPTypesAndValues all;
  import from IP_ASP_TypeDefs all;

  //****************************************************************************
  // Data ASPs
  //****************************************************************************

  type enumerated IMS_SecurityContextEnum_Type {unprotected, protectedContext1, protectedContext2};    /* @sic R5-145732: support of second security context sic@ */

  type record IMS_RoutingInfo_Type {
    InternetProtocol_Type               Protocol,              // UDP or TCP
    IMS_SecurityContextEnum_Type        Security optional,     // protected or unprotected (in DL omit when IP PTC shall decide what to do)
    IP_AddrInfo_Type                    UE_Address optional,   // sent by the IP PTC when there is an initial request on unprotected connection
    IP_AddrInfo_Type                    NW_Address optional    // sent by the IP PTC when there is an initial request on unprotected connection
  };

  type RequestUnion IMS_Request_Type  with { encode "SIPCodec"}; // Alias for "RequestUnion" as defined in LibSip_SIPTypesAndValues
  type Response     IMS_Response_Type with { encode "SIPCodec"}; // Alias for "Response" as defined in LibSip_SIPTypesAndValues

  type record IMS_DATA_REQ {
    IMS_RoutingInfo_Type        RoutingInfo,
    IMS_Request_Type            Request
  };

  type record IMS_DATA_RSP {
    IMS_RoutingInfo_Type        RoutingInfo,
    IMS_Response_Type           Response
  };

  //****************************************************************************
  // Configuration ASPs and common definitions
  //****************************************************************************

  type record IMS_UnprotectedPorts_Type {
    PortNumber_Type     Port_us,                // UE side: 5060 per default; may be set to other value by initial request (REGISTER) by the UE
    PortNumber_Type     Port_ps                 // network side: 5060 (without choice)
  };

  type record IMS_ProtectedPorts_Type {
    PortNumber_Type     Port_us,                // UE side: Server
    PortNumber_Type     Port_uc,                // UE side: Client
    PortNumber_Type     Port_ps,                // network side: Server
    PortNumber_Type     Port_pc                 // network side: Client
  };

  type record IMS_SPIs_Type {
    IPsec_SPI_Type      SPI_us,                 // SPI at UE side: assigned by the UE
    IPsec_SPI_Type      SPI_uc,                 // SPI at UE side: assigned by the UE
    IPsec_SPI_Type      SPI_ps  optional,       // SPI at network side: to be assigned by TTCN
    IPsec_SPI_Type      SPI_pc  optional        // SPI at network side: to be assigned by TTCN
  };

  type record IMS_SecurityInfo_Type {
    IMS_ProtectedPorts_Type             ProtectedPorts,
    IMS_SPIs_Type                       SPIs,
    IPsec_IntegrityAlgorithm_Type       IntegrityAlgorithm,
    IPsec_CipheringAlgorithm_Type       CipheringAlgorithm
  };

  type record IMS_RegistrationInfo_Type {
    IP_AddrInfo_Type                    NW_Address,             // network address of the chosen IMS server (e.g. IPv4, IPv6)
    IP_AddrInfo_Type                    UE_Address,             // UE address as used for security protected connections
    IMS_SecurityInfo_Type               SecurityInfo  optional  // omit in case of GIBA
  };

  type record IMS_PortsAndSecurityConfigReq_Type {
    PortNumber_Type                     UnprotectedPort_us  optional,   // 5060 per default
    IMS_RegistrationInfo_Type           RegistrationInfo    optional    /* @sic R5s150268 change 3: optional sic@ */
  };

  type Null_Type IMS_PortsAndSecurityConfigCnf_Type;                    /* @sic R5s130266 change 1.3, 4, 5 - MCC160 Implementation: sic@
                                                                           SPIs and protected ports are fully controlled by the IMS PTC
                                                                           => it is not necessary anymore to return IMS_ProtectedPorts_Type, IMS_SPIs_Type to the IMS PTC */

  type union IMS_CONFIG_REQ {
    IPsec_SecurityKeys_Type             InstallKey,
    IMS_PortsAndSecurityConfigReq_Type  PortsAndSecurityConfig,
    Null_Type                           SecurityRelease,
    Null_Type                           RegInfoRelease,                 /* @sic R5s140123 change 9 sic@ */
    Null_Type                           CloseTCP                        /* @sic R5s130988 change 1 - MCC160 implementation sic@ */
  };

  type union IMS_CONFIG_CNF {
    Null_Type                           InstallKey,
    IMS_PortsAndSecurityConfigCnf_Type  PortsAndSecurityConfig,
    Null_Type                           SecurityRelease,
    Null_Type                           RegInfoRelease,                 /* @sic R5s140123 change 9 sic@ */
    Null_Type                           CloseTCP                        /* @sic R5s130988 change 1 - MCC160 implementation sic@ */
  };

  //****************************************************************************
  // Port definitions
  //****************************************************************************

  type port IMS_IP_CTRL_PORT message {          // Control port at the IMS PTC to configure IP for IMS
    out IMS_CONFIG_REQ;
    in  IMS_CONFIG_CNF;
  };

  type port IP_IMS_CTRL_PORT message {          // Control port at the IP PTC to get configuration from IMS
    out IMS_CONFIG_CNF;
    in  IMS_CONFIG_REQ;
  };

  type port IMS_IP_CLIENT_PORT message {        // IMS client: send requests, receive response
    out IMS_DATA_REQ;
    in  IMS_DATA_RSP;
  };

  type port IP_IMS_CLIENT_PORT message {        // counter part for the IMS client at the IP PTC: receive requests, send response
    out IMS_DATA_RSP;
    in  IMS_DATA_REQ;
  };

  type port IMS_IP_SERVER_PORT message {        // IMS server: send response, receive requests
    out IMS_DATA_RSP;
    in  IMS_DATA_REQ;
  };

  type port IP_IMS_SERVER_PORT message {        // counter part for the IMS server at the IP PTC: receive response, send requests
    out IMS_DATA_REQ;
    in  IMS_DATA_RSP;
  };

}
