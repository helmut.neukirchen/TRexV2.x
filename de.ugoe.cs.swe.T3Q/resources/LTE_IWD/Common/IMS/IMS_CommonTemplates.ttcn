/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-09 22:17:57 +0100 (Wed, 09 Mar 2016) $
// $Rev: 15678 $
/******************************************************************************/

module IMS_CommonTemplates {

  import from CommonDefs all;
  import from LibSip_Common all;

  /*
   * @desc      concat two semicolon param lists
   * @param     p_SemicolonParam_List1
   * @param     p_SemicolonParam_List2
   * @return    template (value) SemicolonParam_List
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_SemicolonParam_List_Concat_TX(template (omit) SemicolonParam_List p_SemicolonParam_List1,
                                           template (value) SemicolonParam_List p_SemicolonParam_List2) return template (value) SemicolonParam_List
  {
    var template (value) SemicolonParam_List v_SemicolonParam_List;
    var integer i := 0;
    var integer k;

    if (isvalue(p_SemicolonParam_List1)) {       /* @sic R5s140580 change 3 sic@ */
      v_SemicolonParam_List := valueof(p_SemicolonParam_List1);
      i := lengthof(v_SemicolonParam_List);
    }
    for (k := 0; k < lengthof(p_SemicolonParam_List2); k := k + 1) {      /* @sic R5s140580 change 3 sic@ */
      v_SemicolonParam_List[i+k] := p_SemicolonParam_List2[k];
    }
    return v_SemicolonParam_List;
  }

  /*
   * @desc      Add element to semi colon seperated parameter list
   * @param     p_SemicolonParam_List
   * @param     p_GenericParam
   * @return    template (value) SemicolonParam_List
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_SemicolonParam_List_Add_TX(template (omit) SemicolonParam_List p_SemicolonParam_List,
                                        template (value) GenericParam p_GenericParam) return template (value) SemicolonParam_List
  {
    return f_SemicolonParam_List_Concat_TX(p_SemicolonParam_List, {p_GenericParam});
  }

  /*
   * @desc      Add element to semi colon seperated parameter list
   * @param     p_SemicolonParam_List
   * @param     p_GenericParam
   * @return    template (present) SemicolonParam_List
   * @status    APPROVED (IMS, LTE_A_IRAT, LTE_IRAT)
   */
  function f_SemicolonParam_List_Add_RX(template (present) SemicolonParam_List p_SemicolonParam_List,
                                        template (present) GenericParam p_GenericParam) return template (present) SemicolonParam_List
  { /* NOTE: may need to be enhanced like f_SemicolonParam_List_Concat_TX/f_SemicolonParam_List_Add_TX */
    var template (present) SemicolonParam_List v_SemicolonParam_List := p_SemicolonParam_List;
    var integer i := lengthof(p_SemicolonParam_List);
    v_SemicolonParam_List[i] := p_GenericParam;
    return v_SemicolonParam_List;
  }

  /******************************************************************************/
  /* AuxiliaryTemplates */
  /******************************************************************************/
  /*
   * @desc      return parameter value ("" if none)
   * @param     p_GenericParam
   * @return    charstring
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_GenericParam_GetValue(template (omit) GenericParam p_GenericParam) return charstring
  {
    var GenValue v_GenValue;

    if (ispresent(p_GenericParam.paramValue)) {
      v_GenValue := valueof(p_GenericParam.paramValue);
      if (ischosen(v_GenValue.quotedString)) {
        return v_GenValue.quotedString;
      }
      else {
        return v_GenValue.tokenOrHost;
      }
    }
    return "";
  }

  // Generic parameter as used in SemicolonParam_List, AmpersandParam_List, CommaParam_List:

  template (value) GenValue cs_GenValueToken(template (value) charstring p_ParamValue) := {tokenOrHost := p_ParamValue};                                        /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
  template (value) GenValue cs_GenValueQuoted(template (value) charstring p_ParamValue) := {quotedString := p_ParamValue};                                      /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
  template (present) GenValue cr_GenValueToken(template (present) charstring p_ParamValue) := {tokenOrHost := p_ParamValue};                                    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
  template (present) GenValue cr_GenValueQuoted(template (present) charstring p_ParamValue) := {quotedString := p_ParamValue};                                  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
  template (present) GenValue cr_GenValue(template (present) charstring p_ParamValue) := (cr_GenValueToken(p_ParamValue), cr_GenValueQuoted(p_ParamValue));     /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */

  template GenValue cr_GenValue_None := cr_GenValue("") ifpresent;     /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
  

  /*
   * @desc      build GenValue considering that value my be omit
   * @param     p_ParamValue
   * @return    template (omit) GenValue
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function fl_GenValue_TX(template (omit) charstring p_ParamValue) return template (omit) GenValue
  {
    var template (omit) GenValue v_GenValue := omit;

    if (isvalue(p_ParamValue)) {
      v_GenValue := cs_GenValueToken(valueof(p_ParamValue));
    }
    return v_GenValue;
  }

  template (value) GenericParam cs_GenericParam_Common(charstring p_Id,
                                                       template (omit) GenValue p_GenValue := omit) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    id := p_Id,
    paramValue := p_GenValue
  };
  
  template (value) GenericParam cs_GenericParamQuoted(charstring p_Id,
                                                      template (value) charstring p_ParamValue) := cs_GenericParam_Common(p_Id, cs_GenValueQuoted(p_ParamValue));  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */

  template (value) GenericParam cs_GenericParamToken(charstring p_Id,
                                                     charstring p_ParamValue) := cs_GenericParam_Common(p_Id, cs_GenValueToken(p_ParamValue));    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
  
  template (value) GenericParam cs_GenericParam(charstring p_Id,
                                                template (omit) charstring p_ParamValue := omit) :=
  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    cs_GenericParam_Common(p_Id, fl_GenValue_TX(p_ParamValue));     // @sic R5w140111 sic@
  


  template (present) GenericParam cr_GenericParam_Common(template (present) charstring p_Id := ?,
                                                         template GenValue p_GenValue := *) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    id := p_Id,
    paramValue := p_GenValue
  };


  template (present) GenericParam cr_GenericParam(template (present) charstring p_Id,
                                                  template (present) charstring p_ParamValue) :=
  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    cr_GenericParam_Common(p_Id, cr_GenValue(p_ParamValue));     // @sic R5w140111 sic@
  
  template (present) GenericParam cr_GenericParam_Any := cr_GenericParam_Common(?, *); /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT) */

  /*
   * @desc      build GenericParam to be used as receive template with same id and value as given send template
   * @param     p_GenericParamTX
   * @return    template (present) GenericParam
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  function f_GenericParam_AsTX(template (value) GenericParam p_GenericParamTX) return template (present) GenericParam
  {
    var template GenValue v_GenValue;

    if (ispresent(p_GenericParamTX.paramValue)) {
      v_GenValue := cr_GenValue(f_GenericParam_GetValue(p_GenericParamTX));
    } else {
      v_GenValue := omit;
    }
    return cr_GenericParam_Common(p_GenericParamTX.id, v_GenValue);
  }
    

  //----------------------------------------------------------------------------
  // templates to check SemicolonParam_List for a specific parameter

  template (present) SemicolonParam_List cr_SemicolonParam_List_OneSpecificParam(charstring p_Id,
                                                                                 template GenValue p_GenValue := *) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    /* @sic R5w140111: GenValue sic@ */
    cr_GenericParam_Common(p_Id, p_GenValue),
    *
  };
  
  template (present) SemicolonParam_List cr_SemicolonParam_List_OneSpecificParamWithValue(charstring p_Id,
                                                                                          template (present) charstring p_ParamValue := ?) :=    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    cr_SemicolonParam_List_OneSpecificParam(p_Id, cr_GenValue(p_ParamValue));

  template (present) SemicolonParam_List cr_SemicolonParam_List_OneSpecificParamNoValue(charstring p_Id) :=                                      /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    cr_SemicolonParam_List_OneSpecificParam(p_Id, omit);
    
  /******************************************************************************/
  /* AuxiliaryFunctions */
  /******************************************************************************/
  
  type set of GenericParam GenericParamList_Type;   /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
  
  //****************************************************************************
  // auxilliary functions to access comma, semicolon or ampersand separated lists
  //----------------------------------------------------------------------------
  /*
   * @desc      common function to get index of specific parameter from GenericParamList or -1 when the parameter has not been found
   * @param     p_ParamList
   * @param     p_ParamId
   * @param     p_StartIndex        (default value: 0)
   * @return    integer
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function fl_SIP_GenericParamList_GetParamIndex(GenericParamList_Type p_ParamList,
                                                 charstring p_ParamId,
                                                 integer p_StartIndex := 0) return integer
  { /* @sic R5w140111: GenValue sic@ */
    var integer v_NoOfParams := lengthof(p_ParamList);
    var charstring v_RequestedParamId := f_StringToLower(p_ParamId);  /* @sic R5s150910 change 2: f_StringToLower sic@ */
    var charstring v_ParamId;
    var integer i;
    
    for (i:= p_StartIndex; i < v_NoOfParams; i := i + 1) {
      v_ParamId := f_StringToLower(p_ParamList[i].id);
      if (v_ParamId == v_RequestedParamId) {
        return i;
      }
    }
    return -1;
  }

  /*
   * @desc      common function to get specific parameter from GenericParamList
   * @param     p_ParamList
   * @param     p_ParamId
   * @return    template (omit) GenericParam
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function fl_SIP_GenericParamList_GetParam(GenericParamList_Type p_ParamList,
                                            charstring p_ParamId) return template (omit) GenericParam
  { /* @sic R5w140111: GenValue sic@ */
    var integer i := fl_SIP_GenericParamList_GetParamIndex(p_ParamList, p_ParamId);
    
    if (i >= 0) {
      return p_ParamList[i];
    }
    return omit;
  }
  
  /*
   * @desc      convert CommaParamList to GenericParamList
   * @param     p_ParamList
   * @return    GenericParamList_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function fl_SIP_CommaParamList2GenericParamList(template (omit) CommaParam_List p_ParamList) return GenericParamList_Type
  {
    var CommaParam_List v_ParamList;
    var GenericParamList_Type v_GenericParamList := {};
    var integer i;

    if (isvalue(p_ParamList)) {
      v_ParamList := valueof(p_ParamList);
      for (i:=0; i < lengthof(v_ParamList); i := i+1) {v_GenericParamList[i] := v_ParamList[i];}
    }
    return v_GenericParamList;
  }

  /*
   * @desc      convert SemicolonParamList to GenericParamList
   * @param     p_ParamList
   * @return    GenericParamList_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function fl_SIP_SemicolonParamList2GenericParamList(template (omit) SemicolonParam_List p_ParamList) return GenericParamList_Type
  {
    var SemicolonParam_List v_ParamList;
    var GenericParamList_Type v_GenericParamList := {};
    var integer i;

    if (isvalue(p_ParamList)) {
      v_ParamList := valueof(p_ParamList);
      for (i:=0; i < lengthof(v_ParamList); i := i+1) {v_GenericParamList[i] := v_ParamList[i];}
    }
    return v_GenericParamList;
  }

  /*
   * @desc      convert AmpersandParamList to GenericParamList
   * @param     p_ParamList
   * @return    GenericParamList_Type
   * @status    APPROVED (IMS)
   */
  function fl_SIP_AmpersandParamList2GenericParamList(template (omit) AmpersandParam_List p_ParamList) return GenericParamList_Type
  {
    var AmpersandParam_List v_ParamList;
    var GenericParamList_Type v_GenericParamList := {};
    var integer i;

    if (isvalue(p_ParamList)) {
      v_ParamList := valueof(p_ParamList);
      for (i:=0; i < lengthof(v_ParamList); i := i+1) {v_GenericParamList[i] := v_ParamList[i];}
    }
    return v_GenericParamList;
  }

  /*
   * @desc      common function to get value of specific parameter from GenericParamList
   * @param     p_ParamId
   * @param     p_GenericParam
   * @return    charstring
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function fl_SIP_GenericParamCheckAndGetValue(charstring p_ParamId,
                                               template (omit) GenericParam p_GenericParam) return charstring
  { /* @sic R5w140111: GenValue sic@ */
    if (not isvalue(p_GenericParam)) {
      f_ErrorLog(__FILE__, __LINE__, "ParamList does not contain '" & p_ParamId & "'");
    }
    return f_GenericParam_GetValue(p_GenericParam);
  }

  /*
   * @desc      return given parameter in parameter list (or omit if not present)
   * @param     p_ParamList
   * @param     p_ParamId
   * @return    template (omit) GenericParam
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_SIP_CommaParamList_GetParam(template (omit) CommaParam_List p_ParamList,
                                         charstring p_ParamId) return template (omit) GenericParam
  { /* @sic R5w140111: GenValue sic@ */
    var GenericParamList_Type v_GenericParamList := fl_SIP_CommaParamList2GenericParamList(p_ParamList);
    return fl_SIP_GenericParamList_GetParam(v_GenericParamList, p_ParamId);
  }

  /*
   * @desc      return value of given parameter in parameter list
   * @param     p_ParamList
   * @param     p_ParamId
   * @return    charstring
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_SIP_CommaParamList_GetParamValue(template (omit) CommaParam_List p_ParamList,
                                              charstring p_ParamId) return charstring
  { /* @sic R5w140111: GenValue sic@ */
    var template (omit) GenericParam v_GenericParam := f_SIP_CommaParamList_GetParam(p_ParamList, p_ParamId);
    return fl_SIP_GenericParamCheckAndGetValue(p_ParamId, v_GenericParam);
  }


  /*
   * @desc      Add element to Comma seperated parameter list
   * @param     p_CommaParam_List
   * @param     p_GenericParam
   * @return    template (value) CommaParam_List
   */
  function f_CommaParam_List_Add_TX(template (value) CommaParam_List p_CommaParam_List,
                                    template (value) GenericParam p_GenericParam) return template (value) CommaParam_List
  {
    var template (value) CommaParam_List v_CommaParam_List := p_CommaParam_List;
    var integer i := lengthof(p_CommaParam_List);
    v_CommaParam_List[i] := p_GenericParam;
    return v_CommaParam_List;
  }

  /*
   * @desc      Add element to comma seperated parameter list
   * @param     p_CommaParam_List
   * @param     p_GenericParam
   * @return    template (present) CommaParam_List
   * @status    APPROVED (IMS)
   */
  function f_CommaParam_List_Add_RX(template (present) CommaParam_List p_CommaParam_List,
                                    template (present) GenericParam p_GenericParam) return template (present) CommaParam_List
  {
    var template (present) CommaParam_List v_CommaParam_List := p_CommaParam_List;
    var integer i := lengthof(p_CommaParam_List);
    v_CommaParam_List[i] := p_GenericParam;
    return v_CommaParam_List;
  }

  /*
   * @desc      return given parameter in parameter list (or omit if not present)
   * @param     p_ParamList
   * @param     p_ParamId
   * @return    template (omit) GenericParam
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_SIP_SemicolonParamList_GetParam(template (omit) SemicolonParam_List p_ParamList,
                                             charstring p_ParamId) return template (omit) GenericParam
  { /* @sic R5w140111: GenValue sic@ */
    var GenericParamList_Type v_GenericParamList := fl_SIP_SemicolonParamList2GenericParamList(p_ParamList);
    return fl_SIP_GenericParamList_GetParam(v_GenericParamList, p_ParamId);
  }
  
  /*
   * @desc      Remove parameter from SemicolonParamList
   * @param     p_ParamList
   * @param     p_ParamId
   * @return    template (omit) SemicolonParam_List
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_SIP_SemicolonParamList_RemoveParam(template (omit) SemicolonParam_List p_ParamList,
                                                charstring p_ParamId) return template (omit) SemicolonParam_List
  {
    var SemicolonParam_List v_SemicolonParamList;
    var GenericParamList_Type v_GenericParamList := fl_SIP_SemicolonParamList2GenericParamList(p_ParamList);
    var integer v_Index := fl_SIP_GenericParamList_GetParamIndex(v_GenericParamList, p_ParamId);
    var integer i;
    var integer k := 0;
    
    if (v_Index >= 0) {
      for (i := 0; i < lengthof(v_GenericParamList); i := i + 1) {
        if (i != v_Index) {
          v_SemicolonParamList[k] := v_GenericParamList[i];
          k := k + 1;
        }
      }
      return v_SemicolonParamList;
    }
    return p_ParamList;
  }

  /*
   * @desc      copy name-value-pairs from source-list to destination-list
   * @param     p_ParamListSource
   * @param     p_ParamListDest
   * @param     p_FmtpParametersToBoCopied
   * @return    template (omit) SemicolonParam_List
   * @status    APPROVED (IMS, LTE, LTE_A_IRAT, LTE_A_R12)
   */
  function f_SIP_SemicolonParamList_CopyParams(template (omit) SemicolonParam_List p_ParamListSource,
                                               template (omit) SemicolonParam_List p_ParamListDest,
                                               CharStringList_Type p_FmtpParametersToBoCopied) return template (omit) SemicolonParam_List
  {
    var template (omit) SemicolonParam_List v_ParamListDest := p_ParamListDest;
    var template (omit) GenericParam v_GenericParam;
    var integer i;

    for (i:=0; i < lengthof(p_FmtpParametersToBoCopied); i:=i+1) {
      v_GenericParam := f_SIP_SemicolonParamList_GetParam(p_ParamListSource, p_FmtpParametersToBoCopied[i]);
      if (ispresent(v_GenericParam)) {
        v_ParamListDest := f_SemicolonParam_List_Add_TX(v_ParamListDest, valueof(v_GenericParam));
      }
    }
    return v_ParamListDest;
  }

  /*
   * @desc      return value of given parameter in parameter list
   * @param     p_ParamList
   * @param     p_ParamId
   * @return    charstring
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_SIP_SemicolonParamList_GetParamValue(template (omit) SemicolonParam_List p_ParamList,
                                                  charstring p_ParamId) return charstring
  { /* @sic R5w140111: GenValue sic@ */
    var template (omit) GenericParam v_GenericParam := f_SIP_SemicolonParamList_GetParam(p_ParamList, p_ParamId);
    return fl_SIP_GenericParamCheckAndGetValue(p_ParamId, v_GenericParam);
  }

  /*
   * @desc      return given parameter in parameter list (or omit if not present)
   * @param     p_ParamList
   * @param     p_ParamId
   * @return    template (omit) GenericParam
   * @status    APPROVED (IMS)
   */
  function f_SIP_AmpersandParamList_GetParam(template (omit) AmpersandParam_List p_ParamList,
                                             charstring p_ParamId) return template (omit) GenericParam
  { /* @sic R5w140111: GenValue sic@ */
    var GenericParamList_Type v_GenericParamList := fl_SIP_AmpersandParamList2GenericParamList(p_ParamList);
    return fl_SIP_GenericParamList_GetParam(v_GenericParamList, p_ParamId);
  }

  function f_SIP_AmpersandParamList_GetParamValue(template (omit) AmpersandParam_List p_ParamList,
                                                  charstring p_ParamId) return charstring
  { /* @sic R5w140111: GenValue sic@ */
    var template (omit) GenericParam v_GenericParam := f_SIP_AmpersandParamList_GetParam(p_ParamList, p_ParamId);
    return fl_SIP_GenericParamCheckAndGetValue(p_ParamId, v_GenericParam);
  }

}
