/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-05 20:03:08 +0100 (Sat, 05 Mar 2016) $
// $Rev: 15515 $
/******************************************************************************/

module TCP_Functions {
  import from CommonDefs all;
  import from IP_ASP_TypeDefs all;
  import from IP_PTC_Templates all;
  import from IP_PTC_Component all;
  import from IP_PTC_Routing all;


  /*
   * @desc      close TCP connection of TCP server;
   *            p_RemoteSocket shall be omit if (and only if) the server shall be stopped;
   *            otherwise p_LocalSocket and p_RemoteSocket shall refer to the TCP connection to be released
   * @param     p_IP_Connection
   * @param     p_WaitForCNF        (default value: true)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  function fl_TCP_Close(template (value) IP_Connection_Type p_IP_Connection,
                        boolean p_WaitForCNF := true) runs on IP_PTC
  {
    IP_SOCK_CTRL.send(cs_TCP_CLOSE_REQ(p_IP_Connection));
    if (p_WaitForCNF) {
      alt {
        [] IP_SOCK_CTRL.receive(cr_TCP_CLOSE_CNF(p_IP_Connection)) { }
        [] IP_SOCK_CTRL.receive(cr_TCP_CLOSE_IND(p_IP_Connection)) { repeat; }   // @sic R5s150270 change 1 sic@
      }
    }
  }

  //----------------------------------------------------------------------------
  // TCP Server

  /*
   * @desc      start TCP server
   * @param     p_LocalIpAddr
   * @param     p_LocalPort
   * @param     p_DrbInfo
   * @param     p_Application       (default value: ims)
   * @param     p_TLSInfo           (default value: omit)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  function f_TcpServer_Start(template (value) IP_AddrInfo_Type p_LocalIpAddr,
                             template (value) PortNumber_Type p_LocalPort,
                             IP_DrbInfo_Type p_DrbInfo,
                             InternetApplication_Type p_Application := ims,
                             template (omit) TLSInfo_Type p_TLSInfo := omit) runs on IP_PTC
  { /* @sic R5w140125: additional parameter p_TLSInfo sic@ */
   
    var template (value) IP_Socket_Type v_LocalSocket := cs_IP_Socket(p_LocalIpAddr, p_LocalPort);

    // Start TCP socket
    IP_SOCK_CTRL.send(cs_TCP_LISTEN_REQ(v_LocalSocket, p_Application, p_TLSInfo));

    // Modify routing table
    f_IP_PTC_RoutingTable_AddEntry(cs_TCP_ConnectionId(v_LocalSocket), p_DrbInfo);
  }

  /*
   * @desc      stop a TCP server
   * @param     p_LocalIpAddr
   * @param     p_LocalPort
   * @param     p_WaitForCNF        (default value: true)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  function f_TcpServer_Stop(template (value) IP_AddrInfo_Type p_LocalIpAddr,
                            template (value) PortNumber_Type p_LocalPort,
                            boolean p_WaitForCNF := true) runs on IP_PTC
  { /* NOTE: stoping the server causes all connections to this server to be closed too */
    /* @sic R5s141154: new parameter p_WaitForCNF sic@ */
    var template (value) IP_Connection_Type v_IP_Connection := cs_TCP_ConnectionId(cs_IP_Socket(p_LocalIpAddr, p_LocalPort), omit);

    // Close TCP socket
    fl_TCP_Close(v_IP_Connection, p_WaitForCNF);
    
    // Modify routing table
    f_IP_PTC_RoutingTable_RemoveEntry(v_IP_Connection);
  }

  //----------------------------------------------------------------------------
  // handling of connections from the UE to the server

  /*
   * @desc      close a UE's connection to the server
   * @param     p_IP_Connection
   * @param     p_WaitForCNF        (default value: true)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  function f_TcpServer_CloseConnection(template (omit) IP_Connection_Type p_IP_Connection,
                                       boolean p_WaitForCNF := true) runs on IP_PTC
  {
    var IP_Connection_Type v_IP_Connection;

    if (isvalue(p_IP_Connection)) {
      v_IP_Connection := valueof(p_IP_Connection);
      
      fl_TCP_Close(v_IP_Connection, p_WaitForCNF);
      
      /* routing table does not need to be modified as the server is still running */
    }
  }

  /*
   * @desc      deal with the UE connecting to the TCP server;
   *            p_IP_Connection returns the new connection
   * @param     p_IP_Connection     (by reference)
   * @param     p_ServerSocket
   * @param     p_RemoteSocket      (default value: cr_IP_Socket_Any)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  altstep a_TcpServer_AcceptConnection(out template (omit) IP_Connection_Type p_IP_Connection,
                                       template (present) IP_Socket_Type p_ServerSocket,
                                       template (present) IP_Socket_Type p_RemoteSocket := cr_IP_Socket_Any) runs on IP_PTC
  { /* @sic R5s140172: when there is already a connection which needs to be released, the confirmation may come even after date on the new connection sic@ */
    /* @sic R5-145732: parameters p_ServerSocket and p_RemoteSocket for support of 2nd security context sic@ */
    var IP_SOCKET_IND v_IP_SOCKET_IND;

    [] IP_SOCK_CTRL.receive(cr_TCP_ACCEPT_IND(p_ServerSocket, p_RemoteSocket)) -> value v_IP_SOCKET_IND {
      /* @sic R5-145732: new connection is checked and handled by the caller sic@ */
      p_IP_Connection := v_IP_SOCKET_IND.CTRL.ConnectionId;
      /* the routing table does not need to be changed as there is an entry for the TCP server already and there is no need to change it just because of the remote address */
    }
    [] IP_SOCK_CTRL.receive(cr_TCP_CLOSE_CNF(cr_TCP_ConnectionId(?, ?))) {  /* @sic R5s140172: Accept any confirmation sic@
                                                                               NOTE: to check that the CNF refers to the correct connection we would need to store it globally  => this seems not to be necessary/useful in this case */
      p_IP_Connection := omit;
    }
  }

  /*
   * @desc      deal with the UE closing a TCP connection
   * @param     p_IP_Connection
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  altstep a_TcpServer_CloseConnection(IP_Connection_Type p_IP_Connection) runs on IP_PTC
  {
    [] IP_SOCK_CTRL.receive(cr_TCP_CLOSE_IND(p_IP_Connection)) {
      /* routing table does not need to be modified as the server is still running */
    }
  }

  //----------------------------------------------------------------------------
  // TCP Client

  /*
   * @desc      connect to a TCP server at the UE's side
   * @param     p_LocalSocket
   * @param     p_RemoteSocket
   * @param     p_Application
   * @return    IP_Connection_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  function f_TcpClient_ConnectReq(template (value) IP_Socket_Type p_LocalSocket,
                                  template (value) IP_Socket_Type p_RemoteSocket,
                                  InternetApplication_Type p_Application) runs on IP_PTC return IP_Connection_Type
  { /* @sic R5s140713 - MCC160 implementation sic@ */
    IP_SOCK_CTRL.send(cs_TCP_CONNECT_REQ(p_LocalSocket, p_RemoteSocket, p_Application));
    return valueof(cs_TCP_ConnectionId(p_LocalSocket, p_RemoteSocket));
  }

  /*
   * @desc      locall function to allow modification of the TCP connection in alt step
   * @param     p_TcpConnection
   * @return    template (present) IP_SOCKET_IND
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  function fl_TCP_CONNECT_CNF(IP_Connection_Type p_TcpConnection) return template (present) IP_SOCKET_IND
  {
    var template (present) IP_Connection_Type v_TcpConnection := p_TcpConnection;
    if (not isvalue(p_TcpConnection.Local.Port)) {   /* => ephemeral port */
      v_TcpConnection.Local.Port := ?;
    }
    return cr_TCP_CONNECT_CNF(v_TcpConnection);
  }
  
  /*
   * @desc      to receive confirmation for pending TCP close
   * @param     p_TcpConnection     (by reference)
   * @param     p_DrbInfo
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  altstep a_TcpClient_ConnectCnf(inout IP_Connection_Type p_TcpConnection,
                                 IP_DrbInfo_Type p_DrbInfo) runs on IP_PTC
  { /* @sic R5s140713 - MCC160 implementation sic@ */
    var IP_SOCKET_IND v_IP_SOCKET_IND;

    [] IP_SOCK_CTRL.receive(fl_TCP_CONNECT_CNF(p_TcpConnection)) -> value v_IP_SOCKET_IND
      {
        p_TcpConnection.Local.Port := v_IP_SOCKET_IND.CTRL.ConnectionId.Local.Port;  // in case of ephemeral port
      
        // Modify routing table
        f_IP_PTC_RoutingTable_AddEntry(p_TcpConnection, p_DrbInfo);
      }
  }
   
/*   altstep a_TcpClient_CloseConnection(IP_Connection_Type p_IP_Connection) runs on IP_PTC */
/*   { /\* deal with the UE closing a TCP connection (exceptional case: normally the SS shall close its client connections to the UE's server) *\/ */

/*     [] IP_SOCK_CTRL.receive(cr_TCP_CLOSE_IND(p_IP_Connection)) { */

/*       // Modify routing table */
/*       f_IP_PTC_RoutingTable_RemoveEntry(p_IP_Connection); */
/*     } */
/*   } */

  /*
   * @desc      close TCP connection to the UE's server
   * @param     p_IP_Connection
   * @param     p_WaitForCNF        (default value: true)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)
   */
  function f_TcpClient_CloseConnection(template (omit) IP_Connection_Type p_IP_Connection,
                                       boolean p_WaitForCNF := true) runs on IP_PTC
  { /* @sic R5s141154: new parameter p_WaitForCNF sic@ */
    var IP_Connection_Type v_IP_Connection;

    if (isvalue(p_IP_Connection)) {
      v_IP_Connection := valueof(p_IP_Connection);

      fl_TCP_Close(v_IP_Connection, p_WaitForCNF);
      
      // Modify routing table
      f_IP_PTC_RoutingTable_RemoveEntry(v_IP_Connection);
    }
  }
}
