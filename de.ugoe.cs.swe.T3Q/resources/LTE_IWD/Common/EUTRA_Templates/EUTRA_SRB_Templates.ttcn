/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-10 19:24:00 +0100 (Thu, 10 Mar 2016) $
// $Rev: 15705 $
/******************************************************************************/

module EUTRA_SRB_Templates {
  import from EUTRA_RRC_ASN1_Definitions language "ASN.1:2002" all;
  import from CommonDefs all;
  import from EUTRA_CommonDefs all;
  import from EUTRA_ASP_TypeDefs all;
  import from EUTRA_ASP_SrbDefs all;
  import from EPS_NAS_MsgContainers all;          // NAS_MSG_Request_Type, NAS_MSG_Indication_Type
  import from EUTRA_AspCommon_Templates all;      // cs_TimingInfo_Now, cr_CnfAspCommonPart_CellCfg, cs_ReqAspCommonPart_CellCfg
  import from EUTRA_RRC_Templates all;            // cs_TX_AM_RLC_SRB1_Def, cs_RX_AM_RLC_SRB1_Def, cs_TX_AM_RLC_SRB2_Def, cs_RX_AM_RLC_SRB2_Def

  const integer tsc_SRB0_LogicalChannelPriority := 1;           /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
  const integer tsc_SRB1_LogicalChannelPriority := 1;           /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
  const integer tsc_SRB2_LogicalChannelPriority := 3;           /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */

  //****************************************************************************
  // Templates for configuration of SRBs (RadioBearer_Type)
  //----------------------------------------------------------------------------
  // All SRBs

  template (value) RadioBearer_Type cs_SRB_Release(SRB_Identity_Type p_Srb) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Id := {
      Srb := p_Srb
    },
    Config := {
      Release := true
    }
  };

  // SRB0

  template (value) RadioBearer_Type cs_SRB0_ConfigDef :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Id := {
      Srb := tsc_SRB0
    },
    Config := {
      AddOrReconfigure := {
        Pdcp := {
          None:=true
        },
        Rlc := {
          Rb := {
            TM := true
          },
          TestMode := {
            None:=true
          }
        },
        LogicalChannelId := 0,
        Mac := {
          LogicalChannel := {
            Priority := tsc_SRB0_LogicalChannelPriority,
            PrioritizedBitRate := infinity_
          },
          TestMode := {
            None:=true
          }
        },
        DiscardULData := omit     // @sic RAN5 #55 sidebar meeting sic@
      }
    }
  };

  //----------------------------------------------------------------------------
  // SRB1

  template (value) RadioBearer_Type cs_SRB1_ConfigDef :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Id := {
      Srb := tsc_SRB1
    },
    Config := {
      AddOrReconfigure := {
        Pdcp := {
          Config := {
            Rb := {
              Srb := true
            },
            TestMode := {
              None:=true
            }
          }
        },
        Rlc := {
          Rb := {
            AM := {
              Tx := cs_UL_AM_RLC_r8(cs_TX_AM_RLC_SRB1_Def),
              Rx := cs_DL_AM_RLC_r8(cs_RX_AM_RLC_SRB1_Def),
              ExtendedLI := omit
            }
          },
          TestMode := {
            None:=true
          }
        },
        LogicalChannelId := 1,
        Mac := {
          LogicalChannel := {
            Priority := tsc_SRB1_LogicalChannelPriority,
            PrioritizedBitRate := infinity_
          },
          TestMode := {
            None:=true
          }
        },
        DiscardULData := omit     // @sic RAN5 #55 sidebar meeting sic@
      }
    }
  };

  //----------------------------------------------------------------------------
  // SRB2

  template (value) RadioBearer_Type cs_SRB2_ConfigDef :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Id := {
      Srb := tsc_SRB2
    },
    Config := {
      AddOrReconfigure := {
        Pdcp := {
          Config := {
            Rb := {
              Srb := true
            },
            TestMode := {
              None:=true
            }
          }
        },
        Rlc := {
          Rb := {
            AM := {
              Tx := cs_UL_AM_RLC_r8(cs_TX_AM_RLC_SRB2_Def),
              Rx := cs_DL_AM_RLC_r8(cs_RX_AM_RLC_SRB2_Def),
              ExtendedLI := omit
            }
          },
          TestMode := {
            None:=true
          }
        },
        LogicalChannelId := 2,
        Mac := {
          LogicalChannel := {
            Priority := tsc_SRB2_LogicalChannelPriority,
            PrioritizedBitRate := infinity_
          },
          TestMode := {
            None:=true
          }
        },
        DiscardULData := omit     // @sic RAN5 #55 sidebar meeting sic@
      }
    }
  };

  //****************************************************************************
  // ASP Templates for configuration of SRBs
  //----------------------------------------------------------------------------

  template (value) SYSTEM_CTRL_REQ cas_CommonRadioBearerConfig_REQ(EUTRA_CellId_Type p_CellId,
                                                                   template (value) TimingInfo_Type p_TimingInfo,
                                                                   template (value) RadioBearerList_Type p_RadioBearerList,
                                                                   template (omit) boolean p_CnfFlag := omit) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    /* @sic R5s100135 sic@ */
    /* @sic R5s110634 additional changes: optional parameter p_CnfFlag sic@ */
    Common := cs_ReqAspCommonPart_CellCfg(p_CellId, p_TimingInfo, p_CnfFlag),
    Request := {
      RadioBearerList := p_RadioBearerList
    }
  };

  template (present) SYSTEM_CTRL_CNF car_CommonRadioBearerConfig_CNF(EUTRA_CellId_Type p_CellId) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Common := cr_CnfAspCommonPart_CellCfg(p_CellId),
    Confirm := {
      RadioBearerList := true
    }
  };

  //****************************************************************************
  // ASP Templates for signalling at SRB0, SRB1, SRB2
  //----------------------------------------------------------------------------
  // common part:

  template (value) ReqAspCommonPart_Type cs_ReqAspCommonPart_SRB(EUTRA_CellId_Type p_CellId,
                                                                 SRB_Identity_Type p_SrbId,
                                                                 template (value) TimingInfo_Type p_TimingInfo := cs_TimingInfo_Now,
                                                                 boolean p_FollowOnFlag := false) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    CellId := p_CellId,
    RoutingInfo := {
      RadioBearerId := {
        Srb := p_SrbId
      }
    },
    TimingInfo := p_TimingInfo,
    ControlInfo := {
      CnfFlag := false,
      FollowOnFlag := p_FollowOnFlag
    },
    CA_Info := omit
  };

  template (present) IndAspCommonPart_Type cr_IndAspCommonPart_SRB(template (present) EUTRA_CellId_Type p_CellId,
                                                                   template (present) SRB_Identity_Type p_SrbId,
                                                                   template (present) TimingInfo_Type p_TimingInfo := cr_TimingInfo_Any) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    CellId := p_CellId,
    RoutingInfo := {
      RadioBearerId := {
        Srb := p_SrbId
      }
    },
    TimingInfo := p_TimingInfo,
    Status := {
      Ok := true
    },
    CA_Info := omit
  };

  //----------------------------------------------------------------------------
  // DL Signalling
  //----------------------------------------------------------------------------
  // SRB0: ASP to send RRC message

  template (value) SRB_COMMON_REQ cas_SRB0_RrcPdu_REQ(EUTRA_CellId_Type p_CellId,
                                                      template (value) TimingInfo_Type p_TimingInfo,
                                                      template (value) DL_CCCH_Message p_RrcPdu) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Common := cs_ReqAspCommonPart_SRB(p_CellId, tsc_SRB0, p_TimingInfo),
    Signalling := {
      Rrc := {
        Ccch := p_RrcPdu
      },
      Nas := omit
    }
  };

  //----------------------------------------------------------------------------
  // SRB1/2: ASP to send RRC message

/*   template (value) SRB_COMMON_REQ cas_SRB_RrcPdu_REQ(EUTRA_CellId_Type p_CellId, */
/*                                                      SRB_Identity_Type p_SrbId, */
/*                                                      template (value) TimingInfo_Type p_TimingInfo, */
/*                                                      template (value) DL_DCCH_Message p_RrcPdu) := */
/*   { */
/*     Common := cs_ReqAspCommonPart_SRB(p_CellId, p_SrbId, p_TimingInfo), */
/*     Signalling := { */
/*       Rrc := { */
/*         Dcch := p_RrcPdu */
/*       }, */
/*       Nas := omit */
/*     } */
/*   }; */

  //----------------------------------------------------------------------------
  // SRB1: ASP to send RRC message

  template (value) SRB_COMMON_REQ cas_SRB1_RrcPdu_REQ(EUTRA_CellId_Type p_CellId,
                                                      template (value) TimingInfo_Type p_TimingInfo,
                                                      template (value) DL_DCCH_Message p_RrcPdu) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Common := cs_ReqAspCommonPart_SRB(p_CellId, tsc_SRB1, p_TimingInfo),
    Signalling := {
      Rrc := {
        Dcch := p_RrcPdu
      },
      Nas := omit
    }
  };

  //----------------------------------------------------------------------------
  // SRB1: ASP to send RRC message (RRCConnectionReconfiguration) + one or more piggybacked NAS messages
  //       (more than one NAS PDU may occur only for RRCConnectionReconfiguration)

  template (value) SRB_COMMON_REQ cas_SRB1_RrcNasPduList_REQ(EUTRA_CellId_Type p_CellId,
                                                             template (value) TimingInfo_Type p_TimingInfo,
                                                             template (value) DL_DCCH_Message p_RrcPdu,
                                                             template (omit)  NAS_MSG_RequestList_Type p_NasMsgList) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Common := cs_ReqAspCommonPart_SRB(p_CellId, tsc_SRB1, p_TimingInfo),
    Signalling := {
      Rrc := {
        Dcch := p_RrcPdu
      },
      Nas := p_NasMsgList
    }
  };

  //----------------------------------------------------------------------------
  // SRB1: ASP to send RRC message (RRCConnectionReconfiguration) + piggybacked NAS message

  template (value) SRB_COMMON_REQ cas_SRB1_RrcNasPdu_REQ(EUTRA_CellId_Type p_CellId,
                                                         template (value) TimingInfo_Type p_TimingInfo,
                                                         template (value) DL_DCCH_Message p_RrcPdu,
                                                         template (value) NAS_MSG_Request_Type p_NasMsg) :=
  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    cas_SRB1_RrcNasPduList_REQ(p_CellId, p_TimingInfo, p_RrcPdu, {p_NasMsg});

  //----------------------------------------------------------------------------
  //

  template (value) SRB_COMMON_REQ cas_SRB_NasPdu_REQ(EUTRA_CellId_Type p_CellId,
                                                     SRB_Identity_Type p_SrbId,
                                                     template (value) TimingInfo_Type p_TimingInfo,
                                                     template (value) NAS_MSG_Request_Type p_NasMsg) :=
  { /* SRB1/2: ASP to send NAS message (within RRC DLInformationTransfer)
       @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Common := cs_ReqAspCommonPart_SRB(p_CellId, p_SrbId, p_TimingInfo),
    Signalling := {
      Rrc := omit,
      Nas := { p_NasMsg }
    }
  };

  //----------------------------------------------------------------------------
  // SRB1: ASP to send NAS message (within RRC DLInformationTransfer)

  template (value) SRB_COMMON_REQ cas_SRB1_NasPdu_REQ(EUTRA_CellId_Type p_CellId,
                                                      template (value) TimingInfo_Type p_TimingInfo,
                                                      template (value) NAS_MSG_Request_Type p_NasMsg) :=
    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    cas_SRB_NasPdu_REQ(p_CellId, tsc_SRB1, p_TimingInfo, p_NasMsg);

  //----------------------------------------------------------------------------
  // SRB2: ASP to send NAS message (within RRC DLInformationTransfer)

  template (value) SRB_COMMON_REQ cas_SRB2_NasPdu_REQ(EUTRA_CellId_Type p_CellId,
                                                      template (value) TimingInfo_Type p_TimingInfo,
                                                      template (value) NAS_MSG_Request_Type p_NasMsg) :=
    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_IRAT) */
    cas_SRB_NasPdu_REQ(p_CellId, tsc_SRB2, p_TimingInfo, p_NasMsg);

  //----------------------------------------------------------------------------
  // SRB2: ASP to send RRC message (RRC DLInformationTransfer containing NAS message)

  template (value) SRB_COMMON_REQ cas_SRB2_RrcPdu_REQ(EUTRA_CellId_Type p_CellId,
                                                      template (value) TimingInfo_Type p_TimingInfo,
                                                      template (value) DL_DCCH_Message p_RrcPdu) :=
  { /* @status    APPROVED (LTE_IRAT) */
    Common := cs_ReqAspCommonPart_SRB(p_CellId, tsc_SRB2, p_TimingInfo),
    Signalling := {
      Rrc := {
        Dcch := p_RrcPdu
      },
      Nas := omit
    }
  };

  //----------------------------------------------------------------------------
  // UL Signalling
  //----------------------------------------------------------------------------
  // SRB0: ASP to receive RRC message

  template (present) SRB_COMMON_IND car_SRB0_RrcPdu_IND(template (present) EUTRA_CellId_Type p_CellId,
                                                        template (present) UL_CCCH_Message p_RrcPdu) :=
  { /* @sic R5s100127 further MCC160 changes sic@
       @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Common := cr_IndAspCommonPart_SRB(p_CellId, tsc_SRB0, ?),
    Signalling := {
      Rrc := {
        Ccch := p_RrcPdu
      },
      Nas := omit
    }
  };

  //----------------------------------------------------------------------------
  // SRB1/2: ASP to receive RRC message

  template (present) SRB_COMMON_IND car_SRB_RrcPdu_IND_Common(EUTRA_CellId_Type p_CellId,
                                                              SRB_Identity_Type p_SrbId,
                                                              template (present) UL_DCCH_Message p_RrcPdu,
                                                              template (present) TimingInfo_Type p_TimingInfo := ?) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Common := cr_IndAspCommonPart_SRB(p_CellId, p_SrbId, p_TimingInfo),
    Signalling := {
      Rrc := {
        Dcch := p_RrcPdu
      },
      Nas := omit
    }
  };

  template (present) SRB_COMMON_IND car_SRB1_RrcPdu_IND(EUTRA_CellId_Type p_CellId,
                                                        template (present) UL_DCCH_Message p_RrcPdu,
                                                        template (present) TimingInfo_Type p_TimingInfo := ?) :=
  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    car_SRB_RrcPdu_IND_Common(p_CellId, tsc_SRB1, p_RrcPdu, p_TimingInfo);
    
  template (present) SRB_COMMON_IND car_SRB2_RrcPdu_IND(EUTRA_CellId_Type p_CellId,
                                                        template (present) UL_DCCH_Message p_RrcPdu,
                                                        template (present) TimingInfo_Type p_TimingInfo := ?) :=
  /* @status    APPROVED (LTE_A_IRAT, LTE_A_R10_R11, LTE_IRAT) */
    car_SRB_RrcPdu_IND_Common(p_CellId, tsc_SRB2, p_RrcPdu, p_TimingInfo);

  //----------------------------------------------------------------------------
  // SRB1: ASP to receive RRC message (RRCConnectionSetupComplete) + piggybacked NAS message

  template (present) SRB_COMMON_IND car_SRB1_RrcNasPdu_IND(EUTRA_CellId_Type p_CellId,
                                                           template (present) UL_DCCH_Message p_RrcPdu,
                                                           template (present) NAS_MSG_Indication_Type p_NasMsg) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Common := cr_IndAspCommonPart_SRB(p_CellId, tsc_SRB1, ?),
    Signalling := {
      Rrc := {
        Dcch := p_RrcPdu
      },
      Nas := { p_NasMsg }
    }
  };

  //----------------------------------------------------------------------------
  // SRB1/2: common ASP to receive NAS message (within RRC ULInformationTransfer)

  template (present) SRB_COMMON_IND car_SRB_NasPdu_IND(EUTRA_CellId_Type p_CellId,
                                                       SRB_Identity_Type p_SrbId,
                                                       template (present) NAS_MSG_Indication_Type p_NasMsg) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    Common := cr_IndAspCommonPart_SRB(p_CellId, p_SrbId, ?),
    Signalling := {
      Rrc := omit,
      Nas := { p_NasMsg }
    }
  };

  //----------------------------------------------------------------------------
  // SRB1: ASP to receive NAS message (within RRC ULInformationTransfer)

  template (present) SRB_COMMON_IND car_SRB1_NasPdu_IND(EUTRA_CellId_Type p_CellId,
                                                        template (present) NAS_MSG_Indication_Type p_NasMsg) :=
    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    car_SRB_NasPdu_IND(p_CellId, tsc_SRB1, p_NasMsg);

  //----------------------------------------------------------------------------
  // SRB2: ASP to receive NAS message (within RRC ULInformationTransfer)

  template (present) SRB_COMMON_IND car_SRB2_NasPdu_IND(EUTRA_CellId_Type p_CellId,
                                                        template (present) NAS_MSG_Indication_Type p_NasMsg) :=
    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    car_SRB_NasPdu_IND(p_CellId, tsc_SRB2, p_NasMsg);


  //----------------------------------------------------------------------------
  // ASP to receive any message on the SRB port

  template (present) SRB_COMMON_IND car_SRB_IND_Any :=
  { /* @status    APPROVED (LTE) */
    Common := cr_IndAspCommonPart_SRB(?, ?),
    Signalling := ?
  };

}
