/******************************************************************************/
// AUTOMATICALLY GENERATED MODULE - DO NOT MODIFY
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-04 18:24:57 +0100 (Fri, 04 Mar 2016) $
// $Rev: 15510 $
/******************************************************************************/

module IMS_CC_Parameters {

  import from CommonDefs all;
  
  //****************************************************************************
  // PIXIT Parameters
  //----------------------------------------------------------------------------
  modulepar charstring                  px_IMS_CalleeContactUri2    := "sip:User-C-Contact@3gpp.org";
                                                                                        // @desc  URI provided by the remote side (i.e. by SS) to be used by the UE as contact address in further SIP signalling of the dialog
                                                                                        // NOTE: in general this URI shall be different than the one in px_CalleeUri
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_IMS_CalleeUri2           := "sip:User-C@3gpp.org";
                                                                                        // @desc  URI of Callee, sent by the UE in INVITE (MO call establishment) to address the remote UE
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_IMS_ConferenceFactoryUri := "sip:mmtel@conf-factory.3gpp.org";
                                                                                        // @desc  conference factory URI as used by the UE for IMS conferencing
  
  modulepar charstring                  px_IMS_FinalConferenceUri   := "sip:final@conf-factory.3gpp.org";
                                                                                        // @desc  final conference URI as sent to the UE during MTSI conference creation
  
  modulepar charstring                  px_IMS_HomeDomainName_Refreshed := "refreshed3gpp.org";
                                                                                        // @desc  Used in 8.15
  
  modulepar charstring                  px_IMS_MessageAccountIdentity := "";            // @desc  message accout identity configured at the UE as used for subscription to message-summary event package
  
  modulepar charstring                  px_IMS_Private_UserId_Refreshed := "privateuser@refreshed3gpp.org";
                                                                                        // @desc  Used in 8.15
  
  modulepar charstring                  px_IMS_PublicUserIdentity1_Refreshed := "sip:PublicId1@refreshed3gpp.org";
                                                                                        // @desc  Used in 8.15
  
  modulepar charstring                  px_IMS_TemporaryConferenceUri := "sip:temporary@conf-factory.3gpp.org";
                                                                                        // @desc  temporary conference URI as sent to the UE during MTSI conference creation
  
  modulepar charstring                  px_SMS_SMSC_InternationalNumber;                // @desc  international number of the SMSC:
                                                                                        // It is set to the same value as used in EFPSISMSC if the EF is present on the ISIM (or the USIM )
                                                                                        // Otherwise it is set to the same value as EFSMSP
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_XCAPServerAddress        := "10.122.11.26";  // @desc  XCAP Server address
                                                                                        // Supported values: IPv4 or IPv6
                                                                                        // Reference to other Spec: 34.229-3
  
  modulepar charstring                  px_XCAP_RootUri             := "/XCAP.3gpp.org";
                                                                                        // @desc  XCAP Root URI
  
  modulepar charstring                  px_XCAP_TargetUri           := "sip:user@domain.com";
                                                                                        // @desc  Target (SIP or TEL URI) where to forward to
  
  modulepar charstring                  px_XCAP_Username;                               // @desc  username to be used for HTTP authentication in case of pc_XCAP_UsernameIsConfiguredInUE==true
  
  
  //****************************************************************************
  // PICS Parameters
  //----------------------------------------------------------------------------
  modulepar boolean                     pc_BidirecVoiceOverIMS      := false;           // @desc  UE capable of initiating a bidirectional voice session over IMS
                                                                                        // Reference: 34.229-2 Table A.12/12
  
  modulepar boolean                     pc_CommunicationHold_DuringEmergencyCall := false;
                                                                                        // @desc  UE supports Communication Hold during emergency call
                                                                                        // Reference: 34.229-2, Table A.12/33
  
  modulepar boolean                     pc_EarlyMedia               := false;           // @desc  UE supports early media
                                                                                        // Reference: 34.229-2 Table A.12/45
  
  modulepar boolean                     pc_HttpDigestAuthentication := false;           // @desc  HTTP Digest XCAP Authentication
                                                                                        // Reference: 34.229-2, Table A.6a/4
  
  modulepar boolean                     pc_HttpGBAAuthentication    := false;           // @desc  GBA XCAP Authentication
                                                                                        // Reference: 34.229-2, Table A.6a/3
  
  modulepar boolean                     pc_IMS_EmergencyCall        := false;           // @desc  UE supports IMS emergency services
                                                                                        // Reference: 34.229-2 Table A.12/26
  
  modulepar boolean                     pc_IMS_TWS                  := false;           // @desc  Three way session
                                                                                        // Reference: 34.229-2, Table A.16/14
  
  modulepar boolean                     pc_IndicateSigcomp          := false;           // @desc  Indicate the willingness to receive the responses and requests compressed from initial REGISTER onwards by using the "comp=sigcomp" parameter
                                                                                        // Reference: 34.229-2 Table A.8/5
  
  modulepar boolean                     pc_InitiateP_CSCFDiscovery_viaDHCPv4 := false;  // @desc  UE capable of being configured to initiate P-CSCF discovery via DHCPv4
                                                                                        // Reference: 34.229-2 Table A.13/1
  
  modulepar boolean                     pc_InitiateP_CSCFDiscovery_viaDHCPv6 := false;  // @desc  UE capable of being configured to initiate P-CSCF discovery via DHCPv6
                                                                                        // Reference: 34.229-2 Table A.12/7
  
  modulepar boolean                     pc_InitiateP_CSCFDiscovery_viaPCO := false;     // @desc  UE capable of being configured to initiate P-CSCF discovery via PCO
                                                                                        // Reference: 34.229-2 Table A.12/5
  
  modulepar boolean                     pc_InitiatesDedicatedPDPContext := false;       // @desc  UE capable of being configured to initiate Dedicated PDP Context
                                                                                        // Reference: 34.229-2 Table A.12/4
  
  modulepar boolean                     pc_MTSI_ACR                 := false;           // @desc  MTSI supplementary services - Anonymous Communication Rejection
                                                                                        // Reference: 34.229-2 Table A.16/12
  
  modulepar boolean                     pc_MTSI_CommDivert          := false;           // @desc  MTSI supplementary services - Communication Diversion
                                                                                        // Reference: 34.229-2 Table A.16/5
  
  modulepar boolean                     pc_MTSI_CommHold            := false;           // @desc  Communication Hold
                                                                                        // Reference: 34.229-2 Table A.16/6
  
  modulepar boolean                     pc_MTSI_CommWaiting         := false;           // @desc  MTSI supplementary services - Communication Waiting
                                                                                        // Reference: 34.229-2 Table A.16/13
  
  modulepar boolean                     pc_MTSI_Conference          := false;           // @desc  MTSI supplementary services - Conference
                                                                                        // Reference: 34.229-2 Table A.16/9
  
  modulepar boolean                     pc_MTSI_Incoming_CB         := false;           // @desc  MTSI supplementary services - Incoming Communication Barring
                                                                                        // Reference: 34.229-2 Table A.16/7
  
  modulepar boolean                     pc_MTSI_MessageWait         := false;           // @desc  MTSI supplementary services - Message Waiting Indication
                                                                                        // Reference: 34.229-2 Table A.16/8
  
  modulepar boolean                     pc_MTSI_NetworkInvitationToConference := false; // @desc  MTSI supplementary services - Network Invitation to Conference
                                                                                        // Reference: 34.229-2 Table A.16/zzz
  
  modulepar boolean                     pc_MTSI_OIP                 := false;           // @desc  MTSI supplementary services - Originating Identification Presentation
                                                                                        // Reference: 34.229-2 Table A.16/1
  
  modulepar boolean                     pc_MTSI_OIR                 := false;           // @desc  MTSI supplementary services - Originating Identification Restriction
                                                                                        // Reference: 34.229-2 Table A.16/2
  
  modulepar boolean                     pc_MTSI_OIR_C               := false;           // @desc  MTSI supplementary services - Originating Identification Restriction - Configuration
                                                                                        // Reference: 34.229-2 Table A.16/2A
  
  modulepar boolean                     pc_MTSI_Outgoing_CB         := false;           // @desc  MTSI supplementary services - Outgoing Communication Barring
                                                                                        // Reference: 34.229-2 Table A.16/15
  
  modulepar boolean                     pc_MTSI_Speech_AMRWB        := false;           // @desc  MTSI media - Speech, AMR wideband
                                                                                        // Reference: 34.229-2 Table A.15/2
  
  modulepar boolean                     pc_MTSI_Speech_EVS          := false;           // @desc  MTSI media - Speech EVS
                                                                                        // Reference: 34.229-2 Table A.15/10
  
  modulepar boolean                     pc_MTSI_TIP                 := false;           // @desc  MTSI supplementary services - Terminating Identification Presentation
                                                                                        // Reference: 34.229-2 Table A.16/3
  
  modulepar boolean                     pc_MTSI_TIR                 := false;           // @desc  MTSI supplementary services - Terminating Identification Restriction
                                                                                        // Reference: 34.229-2 Table A.16/4
  
  modulepar boolean                     pc_MTSI_TIR_C               := false;           // @desc  MTSI supplementary services - Terminating Identification Restriction - Configuration
                                                                                        // Reference: 34.229-2 Table A.16/4A
  
  modulepar boolean                     pc_MTSI_Text_RTP            := false;           // @desc  MTSI media - Text, RTP
                                                                                        // Reference: 34.229-2 Table A.15/7
  
  modulepar boolean                     pc_MultipleIMPU             := false;           // @desc  UE supports multiple IMPU
                                                                                        // Reference: 34.229-2 Table A.12/yyy
  
  modulepar boolean                     pc_P_CSCFDiscovery_viaDHCP  := false;           // @desc  UE supports P-CSCF Discovery via DHCP
                                                                                        // Reference: 34.229-2 Table A.12/9
  
  modulepar boolean                     pc_P_CSCFDiscovery_viaDHCPv4 := false;          // @desc  UE supports P-CSCF discovery via DHCPv4
                                                                                        // Reference: 34.229-2 Table A.13/2
  
  modulepar boolean                     pc_P_CSCFDiscovery_viaDHCPv6 := false;          // @desc  UE supports P-CSCF discovery via DHCPv6
                                                                                        // Reference: 34.229-2 Table A.12/10
  
  modulepar boolean                     pc_P_CSCFDiscovery_viaPCO   := false;           // @desc  UE supports P-CSCF Discovery via PCO
                                                                                        // Reference: 34.229-2 Table A.12/8
  
  modulepar boolean                     pc_Preconditions            := false;           // @desc  Integration of resource management and SIP? (use of preconditions)
                                                                                        // Reference: 34.229-2 Table A.4/16
  
  modulepar boolean                     pc_SMS_MO                   := false;           // @desc  UE supports SM-over-IP sender
                                                                                        // Reference: 34.229-2 Table A.3A/61
  
  modulepar boolean                     pc_VideoCodecH264CBP        := false;           // @desc  MTSI media - Video codec H.264 CBP Level 1.2
                                                                                        // Reference: 34.229-2 Table A.15/9
  
  modulepar boolean                     pc_XCAP_UsernameIsConfiguredInUE := false;      // @desc  UE indicated OMA-TS-XDM_MO-V1_1-20080627-A.doc, section 5.2.8 "Node: /<X>/AAUTHNAME" is configured
                                                                                        // Reference: 34.229-2 Table A.12/37
  
  modulepar boolean                     pc_XCAP_XUIisDefaultPublicUserId := false;      // @desc  UE uses the default public user id received in P-Associated-URI header in 200 OK for REGISTER as XCAP User Identity (XUI)
                                                                                        // Reference: 34.229-2 Table A.12/38
  
  
}
