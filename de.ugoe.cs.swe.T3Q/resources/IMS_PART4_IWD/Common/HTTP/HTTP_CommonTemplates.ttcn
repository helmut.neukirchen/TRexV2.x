/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-07 13:07:26 +0100 (Mon, 07 Mar 2016) $
// $Rev: 15528 $
/******************************************************************************/

module HTTP_CommonTemplates {

  import from CommonDefs all;
  import from IP_ASP_TypeDefs all;
  import from HTTP_ASP_TypeDefs all;
  import from LibSip_SIPTypesAndValues all;     // NOTE: the same header definitions are used for HTTP as for SIP (Authorization, ContentType, WwwAuthenticate, AuthenticationInfo, Expires)
  import from LibSip_Common all;
  import from IMS_SIP_Templates all;            // NOTE: the same header definitions are used for HTTP as for SIP (cs_ContentLength)

  const charstring tsc_HTTP_Version_1_1 := "HTTP/1.1";                                              /* @status    APPROVED (IMS) */
 
  //----------------------------------------------------------------------------
  /*
   * @desc      return ContentLength header with valid length or omit when there is no message body
   * @param     p_MessageBody
   * @return    template (omit) ContentLength
   * @status    APPROVED (IMS)
   */
  function fl_HttpContentLength(template (omit) charstring p_MessageBody) return template (omit) ContentLength
  {
    if (isvalue(p_MessageBody)) {
      return cs_ContentLength(lengthof(p_MessageBody));
    }
    return omit;
  }

  /*
   * @desc      get system time and return HTTP Date/Time formatted according to RFC2616 cl. 3.3.1 and RFC822 cl. 5.1
   * @return    charstring
   * @status    APPROVED (IMS)
   */
  function fl_GetHttpDate() return charstring
  {
    var Struct_tm_Type v_LocalTime;
    var integer v_TimezoneSeconds;
    
    fx_GetSystemTime(v_LocalTime, v_TimezoneSeconds);

    return f_HTTP_FormatHttpDate(v_LocalTime, v_TimezoneSeconds);
  }

  template (value) Date cs_HttpDate :=
  { /* @status    APPROVED (IMS) */
    fieldName := DATE_E,
    sipDate := fl_GetHttpDate()
  };

  //----------------------------------------------------------------------------

  template (value) WwwAuthenticate cs_HTTP_WwwAuthenticate(template (value) CommaParam_List p_ChallangeParams) :=
  { /* @status    APPROVED (IMS) */
    fieldName := WWW_AUTHENTICATE_E,
    challenge := {
      digestCln := p_ChallangeParams
    }
  };

  //----------------------------------------------------------------------------

  template (value) AuthenticationInfo cs_AuthenticationInfo(template (value) CommaParam_List p_AuthInfoParams) :=
  { /* @status    APPROVED (IMS) */
    fieldName := AUTHENTICATION_INFO_E,
    ainfo := p_AuthInfoParams
  };

  //----------------------------------------------------------------------------

  template (value) SIP_ETag cs_HttpETag(charstring p_EntityTag) :=
  { /* @status    APPROVED (IMS) */
    fieldName := SIP_ETAG_E,
    entityTag := p_EntityTag
  };

  //----------------------------------------------------------------------------

  template (value) Server cs_Server(template (value) ServerVal_List p_ProductTokens) :=
  { /* @status    APPROVED (IMS) */
    fieldName := SERVER_E,
    serverBody := p_ProductTokens
  };

  //----------------------------------------------------------------------------
  
  template (present) UserAgent cr_UserAgent(template (present) ServerVal_List p_ProductTokens) :=
  { /* @status    APPROVED (IMS) */
    fieldName := USER_AGENT_E,
    userAgentBody := p_ProductTokens
  };

  /*
   * @desc      build up common receive template for UserAgent header
   * @param     p_ProductTokens
   * @return    template UserAgent
   * @status    APPROVED (IMS)
   */
  function f_UserAgentRX(template (present) ServerVal_List p_ProductTokens) return template UserAgent
  {
    var template UserAgent v_UserAgent := *;
    var template (present) ServerVal_List v_ProductTokens;

    if (lengthof(p_ProductTokens) > 0) {
      v_ProductTokens := superset(all from p_ProductTokens);
      v_UserAgent := cr_UserAgent(v_ProductTokens);
    }
    return v_UserAgent;
  }

  //----------------------------------------------------------------------------

  template (present) Host cr_Host(charstring p_Host,
                                  template integer p_Port := omit) :=
  { /* @status    APPROVED (IMS) */
    fieldName := HOST_E,
    host := p_Host,
    portField := p_Port
  };

  //----------------------------------------------------------------------------

  template (value) HttpStatusLine_Type cs_HttpStatusLine(integer p_Code,
                                                         charstring p_ReasonPhrase) :=
  { /* @status    APPROVED (IMS) */
    version := tsc_HTTP_Version_1_1,
    code := p_Code,
    reasonPhrase := p_ReasonPhrase
  };
  
  template (value) HttpStatusLine_Type cs_HttpStatusLine_200 := cs_HttpStatusLine(200, "OK");               /* @status    APPROVED (IMS) */
  template (value) HttpStatusLine_Type cs_HttpStatusLine_401 := cs_HttpStatusLine(401, "Unauthorized");     /* @status    APPROVED (IMS) */
  template (value) HttpStatusLine_Type cs_HttpStatusLine_404 := cs_HttpStatusLine(404, "File Not Found");   /* @status    APPROVED (IMS) */
  
  template (value) HttpResponse_Type cs_HttpResponse(template (value) HttpStatusLine_Type p_HttpStatusLine,
                                                     template (omit)  SIP_ETag p_HttpETag := omit,
                                                     template (omit)  Server p_Server := omit,
                                                     template (omit)  WwwAuthenticate p_WwwAuthenticate := omit,
                                                     template (omit)  AuthenticationInfo p_AuthenticationInfo := omit,
                                                     template (omit)  Expires p_Expires := omit,
                                                     template (omit)  ContentType p_ContentType := omit,
                                                     template (omit)  charstring p_MessageBody := omit) :=
  { /* @status    APPROVED (IMS) */
    statusLine := p_HttpStatusLine,
    date := cs_HttpDate,                                            // @sic R5-151795 sic@
    eTag := p_HttpETag,                                             // @sic R5-151795 sic@
    server := p_Server,                                             // @sic R5-151795 sic@
    wwwauthenticate := p_WwwAuthenticate,
    authenticationInfo := p_AuthenticationInfo,
    contentType := p_ContentType,
    contentLength := fl_HttpContentLength(p_MessageBody),           // @sic R5-151795 sic@
    expires := p_Expires,
    messageBody := p_MessageBody
  };

  //----------------------------------------------------------------------------
 
  template (value) HttpResponse_Type cs_HttpResponse_200(template (omit) SIP_ETag p_HttpETag := omit,      // @sic R5-151795 sic@
                                                         template (omit) Server p_Server := omit,          // @sic R5-151795 sic@
                                                         template (omit) AuthenticationInfo p_AuthenticationInfo := omit,      // @sic R5-151795 sic@
                                                         template (omit) ContentType p_ContentType := omit,
                                                         template (omit) charstring p_MessageBody := omit) :=
  /* @status    APPROVED (IMS) */
    cs_HttpResponse(cs_HttpStatusLine_200, p_HttpETag, p_Server, -, p_AuthenticationInfo, -, p_ContentType, p_MessageBody);

  template (value) HttpResponse_Type cs_HttpResponse_401(ServerVal_List p_ServerProductList,               // @sic R5-151795 sic@
                                                         template (omit) WwwAuthenticate p_Authorization) :=
  /* @status    APPROVED (IMS) */
    cs_HttpResponse(cs_HttpStatusLine_401, -, cs_Server(p_ServerProductList), p_Authorization);
  
  template (value) HttpResponse_Type cs_HttpResponse_404(ServerVal_List p_ServerProductList) :=            // @sic R5-151795 sic@
  /* @status    APPROVED (IMS) */
    cs_HttpResponse(cs_HttpStatusLine_404, -, cs_Server(p_ServerProductList));

  //----------------------------------------------------------------------------
 
  /*
   * @desc      build HTTP 200 response
   * @param     p_ServerProductList
   * @param     p_EntityTag         (default value: omit)
   * @param     p_MediaType         (default value: omit)
   * @param     p_MessageBody       (default value: omit)
   * @return    template (value) HttpResponse_Type
   * @status    APPROVED (IMS)
   */
  function f_HttpResponse_200(template (omit) ServerVal_List p_ServerProductList := omit,
                              template (omit) charstring p_EntityTag := omit,
                              template (omit) charstring p_MediaType := omit,
                              template (omit) charstring p_MessageBody := omit) return template (value) HttpResponse_Type
  { /* @sic R5-151795: new parameters p_Server, p_ETag sic@ */
    var template (omit) ContentType v_ContentType := omit;
    var template (omit) SIP_ETag v_ETag := omit;
    var template (omit) Server v_Server := omit;

    if (ispresent(p_EntityTag)) {
      v_ETag := cs_HttpETag(valueof(p_EntityTag));       // @sic R5-151795 sic@
    }
    if (ispresent(p_MediaType)) {
      v_ContentType := cs_ContentType(valueof(p_MediaType));
    }
    if (ispresent(p_ServerProductList)) {
      v_Server := cs_Server(valueof(p_ServerProductList));
    }
    return cs_HttpResponse_200(v_ETag, v_Server, -, v_ContentType, p_MessageBody);
  }

  //----------------------------------------------------------------------------
  
  template (present) HttpRequestLine_Type cr_HttpRequestLine(template (present) charstring p_Method,
                                                             template (present) charstring p_Uri := ?) :=
  { /* @status    APPROVED (IMS) */
    method := p_Method,
    uri := p_Uri,
    version := tsc_HTTP_Version_1_1
  };

  template (present) HttpRequest_Type cr_HttpRequest(template (present) HttpRequestLine_Type p_HttpRequestLine,
                                                     template UserAgent p_UserAgent := *,
                                                     template Authorization p_Authorization := *,
                                                     template Host p_Host := *,
                                                     template ContentType p_ContentType := *,
                                                     template charstring p_MessageBody := *)  :=
  { /* @status    APPROVED (IMS) */
    /* @sic R5-155363: p_Host sic@ */
    requestLine := p_HttpRequestLine,
    userAgent := p_UserAgent,                   // @sic R5-151795 sic@
    authorization := p_Authorization,
    host := p_Host,                             // @sic R5-155363 sic@
    contentType := p_ContentType,
    x3GPPIntendedIdentity := *,
    messageBody := p_MessageBody
  };

  //----------------------------------------------------------------------------
  
  template (present) HTTP_DATA_IND cr_HttpDataInd(template (present) IP_Connection_Type p_HttpRoutingInfo,
                                                  template (present) HttpRequest_Type p_HttpRequest) :=
  { /* @status    APPROVED (IMS) */
    routingInfo := p_HttpRoutingInfo,
    httpRequest := p_HttpRequest
  };
  
  template (value) HTTP_DATA_REQ cs_HttpDataReq(template (value) IP_Connection_Type p_HttpRoutingInfo,
                                                template (value) HttpResponse_Type p_HttpResponse) :=
  { /* @status    APPROVED (IMS) */
    routingInfo := p_HttpRoutingInfo,
    httpResponse := p_HttpResponse
  };
}
