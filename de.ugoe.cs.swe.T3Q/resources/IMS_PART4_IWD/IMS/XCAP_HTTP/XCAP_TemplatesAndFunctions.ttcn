/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2014-03-05 19:20:41 +0100 (Wed, 05 Mar 2014) $
// $Rev: 10922 $
/******************************************************************************/

module XCAP_TemplatesAndFunctions {
  import from CommonDefs all;
  import from XCAP_ASP_TypeDefs all;

  template (value) XCAP_REQ cs_XcapRequest(charstring p_Method,
                                           charstring p_XcapUri,
                                           template (omit) charstring p_ContentType := omit,
                                           template (omit) charstring p_XmlBody := omit) :=
  { /* @status    APPROVED (IMS) */
    method := p_Method,
    xcapExpression := p_XcapUri,
    contentType := p_ContentType,
    xmlBody := p_XmlBody
  };
  
  template (value) XCAP_REQ cs_XcapResetReq(charstring p_DocSelector) := cs_XcapRequest("RESET", p_DocSelector); /* @status    APPROVED (IMS) */
  
  template (present) XCAP_RSP cr_XcapRsp :=
  { /* @status    APPROVED (IMS) */
    errorInfo := *,
    contentType := *,
    xmlBody := *
  };
  
  //============================================================================
  /*
   * @desc      receive response from XCAP server
   * @param     p_XcapPort
   * @return    XCAP_RSP
   * @status    APPROVED (IMS)
   */
  function f_XCAP_ReceiveResponse(XCAP_PORT p_XcapPort) return XCAP_RSP
  {
    var XCAP_RSP v_XCAP_RSP;
    p_XcapPort.receive(cr_XcapRsp) -> value v_XCAP_RSP;
    if (ispresent(v_XCAP_RSP.errorInfo)) {
      FatalError(__FILE__, __LINE__, v_XCAP_RSP.errorInfo);
    }
    return v_XCAP_RSP;
  }
  
  /*
   * @desc      get response from XCAP server
   * @param     p_XcapPort
   * @param     p_Method
   * @param     p_XcapUri
   * @param     p_ContentType       (default value: omit)
   * @param     p_XmlBody           (default value: omit)
   * @return    XCAP_RSP
   * @status    APPROVED (IMS)
   */
  function f_XCAP_GetResponse(XCAP_PORT p_XcapPort,
                              charstring p_Method,
                              charstring p_XcapUri,
                              template (omit) charstring p_ContentType := omit,
                              template (omit) charstring p_XmlBody := omit) return XCAP_RSP
  {
    var XCAP_RSP v_XcapResponse;
    
    p_XcapPort.send(cs_XcapRequest(p_Method, p_XcapUri, p_ContentType, p_XmlBody));
    v_XcapResponse := f_XCAP_ReceiveResponse(p_XcapPort);
    return v_XcapResponse;
  }
  
  /*
   * @desc      Send PUT to XCAP server
   * @param     p_XcapPort
   * @param     p_XcapUri
   * @param     p_ContentType       (default value: omit)
   * @param     p_XmlBody           (default value: omit)
   * @status    APPROVED (IMS)
   */
  function f_XCAP_Put(XCAP_PORT p_XcapPort,
                      charstring p_XcapUri,
                      template (omit) charstring p_ContentType := omit,
                      template (omit) charstring p_XmlBody := omit)
  {
    f_XCAP_GetResponse(p_XcapPort, "PUT", p_XcapUri, p_ContentType, p_XmlBody);
  }
  
  /*
   * @desc      Send GET to XCAP server
   * @param     p_XcapPort
   * @param     p_XcapUri
   * @return    XCAP_RSP
   * @status    APPROVED (IMS)
   */
  function f_XCAP_Get(XCAP_PORT p_XcapPort,
                      charstring p_XcapUri) return XCAP_RSP
  {
    return f_XCAP_GetResponse(p_XcapPort, "GET", p_XcapUri);
  }
  
  //----------------------------------------------------------------------------
  /*
   * @desc      Initialises the XCAP server with an initial document
   * @param     p_XcapPort
   * @param     p_DocSelector
   * @status    APPROVED (IMS)
   */
  function f_XCAP_Initialise(XCAP_PORT p_XcapPort,
                             charstring p_DocSelector)
  {
    p_XcapPort.send(cs_XcapResetReq(p_DocSelector));
    f_XCAP_ReceiveResponse(p_XcapPort);
  }
  
}
