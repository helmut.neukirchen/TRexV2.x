/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-02-24 17:02:28 +0100 (Wed, 24 Feb 2016) $
// $Rev: 15347 $
/******************************************************************************/
module IMS_CC_CodecSelecting {

  import from LibSip_SIPTypesAndValues all;
  import from LibSip_SDPTypes all;
  import from LibSip_Common all;
  import from IP_ASP_TypeDefs all;
  import from IMS_Component all;
  import from IMS_CommonTemplates all;
  import from IMS_ASP_TypeDefs all;
  import from IMS_ASP_Templates all;
  import from IMS_SIP_Templates all;
  import from IMS_MessageBody_Templates all;
  import from IMS_SDP_Templates all;
  import from IMS_SDP_Messages all;
  import from IMS_CC_CommonFunctions all;
  import from IMS_Procedures_CallControl all;
  import from IMS_Procedures_Common all;
  import from UpperTesterFunctions all;
  import from IMS_PTC_CoordMsg all;
  
  template (value)   GenericParam cs_Fmtp_ModesetParam(charstring p_ModeSet) := cs_GenericParam("mode-set", p_ModeSet);  /* @status    APPROVED (IMS) */
  template (present) GenericParam cr_Fmtp_ModesetParam(charstring p_ModeSet) := cr_GenericParam("mode-set", p_ModeSet);  /* @status    APPROVED (IMS) */
  
  /*
   * @desc      build SDP message to be used in step 1 of test cases 16.X
   * @param     p_BandwidthAS
   * @param     p_Fmts
   * @param     p_SDP_RtpmapFmtpAttribs
   * @param     p_ConnectionInSessionDesc (default value: true)
   * @return    template (value) SDP_Message
   * @status    APPROVED (IMS)
   */
  function fl_IMS_BuildSDP_16_X_Step1(integer p_BandwidthAS,
                                      SDP_fmt_list p_Fmts,
                                      template (value) SDP_attribute_list p_SDP_RtpmapFmtpAttribs,
                                      boolean p_ConnectionInSessionDesc := true) runs on IMS_PTC
    return template (value) SDP_Message
  { /* @sic R5w140113, R5w140009: enhanced SDP type definitions sic@ */
    /* @sic R5-144700: new parameter p_BandwidthAS sic@ */
    /* @sic R5s141244: use of f_IMS_BuildSDP_TX sic@ */
    var template (omit) SDP_attribute_list v_SDP_MediaAttributes;
    var template (omit) SDP_attribute_list v_SDP_PrecondionAttributes;
    var template (value) SDP_bandwidth_list v_MediaBandwidth_List := cs_SDP_Bandwidth_List_Media(p_BandwidthAS, 0, 2000);
    
    // further media attributes:
    v_SDP_MediaAttributes := p_SDP_RtpmapFmtpAttribs;
    v_SDP_MediaAttributes := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes, cs_SDP_Attribute_ptime);       // "20" per default
    v_SDP_MediaAttributes := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes, cs_SDP_Attribute_maxptime);    // "240" per default
    // precondition attributes:
    v_SDP_PrecondionAttributes := {
      cs_SDP_Attribute_curr_qos(c_local, c_sendrecv),
      cs_SDP_Attribute_curr_qos(c_remote, c_none),
      cs_SDP_Attribute_des_qos(c_mandatory, c_local),
      cs_SDP_Attribute_des_qos(c_optional, c_remote)
    }
    
    return f_IMS_BuildSDP_TX(cs_SDP_Media_MultiAudio(p_Fmts), v_MediaBandwidth_List, v_SDP_MediaAttributes, v_SDP_PrecondionAttributes, -, p_ConnectionInSessionDesc);
  }

  /*
   * @desc      build SDP message to be used in step 1 of test cases 16.2
   * @param     p_ModeSet
   * @return    template (value) SDP_Message
   * @status    APPROVED (IMS)
   */
  function fl_IMS_BuildSDP_16_2_Step1(charstring p_ModeSet) runs on IMS_PTC return template (value) SDP_Message
  {
    var integer v_BandwidthAS := 37;  /* @sic R5-144700 sic@ */
    var charstring v_Fmt := "99";
    var template (value) SemicolonParam_List v_MediaFormatParametersTX := f_SemicolonParam_List_Concat_TX({cs_Fmtp_ModesetParam(p_ModeSet)}, cs_Fmtp_AudioParamsDef);
    var template (value) SDP_attribute_list v_SDP_RtpmapFmtpAttribsTX := {
      cs_SDP_Attribute_rtpmap(v_Fmt, cs_RTPMAP_AMR_8000),
      cs_SDP_Attribute_fmtp(v_Fmt, cs_Fmtp_ParamList(v_MediaFormatParametersTX))
    };

    return fl_IMS_BuildSDP_16_X_Step1(v_BandwidthAS, {v_Fmt}, v_SDP_RtpmapFmtpAttribsTX); /* @sic R5-144700: use default bandwidth for AS sic@ */
  }

  /*
   * @desc      build SDP message to be used in step 1 of test cases 16.3 and 16.4
   * @param     p_BandwidthAS
   * @param     p_SemicolonParam_List_ModeSet1 (default value: omit)
   * @param     p_SemicolonParam_List_ModeSet2 (default value: omit)
   * @param     p_ConnectionInSessionDesc (default value: true)
   * @return    template (value) SDP_Message
   * @status    APPROVED (IMS)
   */
  function fl_IMS_BuildSDP_16_3and4_Step1(integer p_BandwidthAS,
                                          template (omit) SemicolonParam_List p_SemicolonParam_List_ModeSet1 := omit,
                                          template (omit) SemicolonParam_List p_SemicolonParam_List_ModeSet2 := omit,
                                          boolean p_ConnectionInSessionDesc := true) runs on IMS_PTC return template (value) SDP_Message
  { /* @sic R5-144700: new parameter p_BandwidthAS sic@ */
    var charstring v_Fmt1 := "97";
    var charstring v_Fmt2 := "99";
    var template (value) SemicolonParam_List v_MediaFormatParametersTX1 := f_SemicolonParam_List_Concat_TX(p_SemicolonParam_List_ModeSet1, cs_Fmtp_AudioParamsDef);
    var template (value) SemicolonParam_List v_MediaFormatParametersTX2 := f_SemicolonParam_List_Concat_TX(p_SemicolonParam_List_ModeSet2, cs_Fmtp_AudioParamsDef);
    var template (value) SDP_attribute_list v_SDP_RtpmapFmtpAttribsTX := {
      cs_SDP_Attribute_rtpmap(v_Fmt1, cs_RTPMAP_AMR_16000),
      cs_SDP_Attribute_fmtp(v_Fmt1, cs_Fmtp_ParamList(v_MediaFormatParametersTX1)),
      cs_SDP_Attribute_rtpmap(v_Fmt2, cs_RTPMAP_AMR_8000),
      cs_SDP_Attribute_fmtp(v_Fmt2, cs_Fmtp_ParamList(v_MediaFormatParametersTX2))
    };

    return fl_IMS_BuildSDP_16_X_Step1(p_BandwidthAS, {v_Fmt1, v_Fmt2}, v_SDP_RtpmapFmtpAttribsTX, p_ConnectionInSessionDesc);
  }

  /*
   * @desc      build SDP message to be used in step 4/7 of test cases 16.1 and 16.2
   * @param     p_RtpmapCodec
   * @param     p_MediaFormatParameters (default value: {*})
   * @param     p_Direction_curr_qos_local
   * @return    template (present) SDP_Message
   * @status    APPROVED (IMS)
   */
  function f_IMS_BuildSDP_16_X_RX(template (present) SDP_attribute_rtpmap_codec p_RtpmapCodec,
                                  template (present) SemicolonParam_List p_MediaFormatParameters := {*},
                                  template (present) charstring p_Direction_curr_qos_local)
    runs on IMS_PTC
    return template (present) SDP_Message
  { // @sic R5-134233 has been withdrawn => p_MediaFormatParameters := {"*"} sic@
    /* @sic R5w140113, R5w140009: enhanced SDP type definitions sic@ */
    /* @sic R5s150704, R5s150751 - Additional changes: f_IMS_BuildSDP_RX used instead of cr_SDP_Message_Def sic@ */

    var template (present) SDP_attribute_list v_SDP_MediaAttributes := {
      cr_SDP_Attribute_rtpmap(-, p_RtpmapCodec),
      cr_SDP_Attribute_fmtp(-, cr_Fmtp_ParamList(p_MediaFormatParameters))
    };
    var template (present) SDP_attribute_list v_SDP_PrecondionAttributes := cr_SDP_PrecondionAttributes(p_Direction_curr_qos_local, c_sendrecv, c_mandatory, c_mandatory);

    return f_IMS_BuildSDP_RX(-, cr_SDP_Media_Audio, v_SDP_MediaAttributes, v_SDP_PrecondionAttributes);
  }

  /****************************************************************************/
  /*
   * @desc      common test procedure for 16.X test cases
   * @param     p_SDP_Message_Invite
   * @param     p_SDP_Message_183
   * @param     p_SDP_Message_180or200
   * @status    APPROVED (IMS)
   */
  function fl_TC_16_X_CommonProcedure(template (value)   SDP_Message p_SDP_Message_Invite,
                                      template (present) SDP_Message p_SDP_Message_183,
                                      template (present) SDP_Message p_SDP_Message_180or200) runs on IMS_PTC
  { /* @sic R5-142994: Reimplementation - in fact there is the same sequence for 16.2 and 16.3/4 sic@ */
    var IMS_InviteRequestWithSdp_Type v_InviteRequestWithSdp;
    var SipUrl v_ContactUrl := f_IMS_PTC_ImsInfo_GetContactUrl();
    var InternetProtocol_Type v_Protocol;
    var IMS_DATA_RSP v_IMS_DATA_RSP;
    var IMS_ResponseWithSdp_Type v_ResponseWithSdp; /* @sic R5w140112 sic@ */
    var template (value) INVITE_Request v_InviteRequest;
    var template (value) MessageHeader v_MessageHeader_Invite;
    var template (present) MessageHeader v_MessageHeader_183RX;
    var template (present) MessageHeader v_MessageHeader_180RX;
    var template (present) Response v_ExpectedResponse_183RX;
    var template (present) Response v_ExpectedResponse_180RX;
    var Response v_Response;
    var Response v_Response183;
    var Response v_Response180;
    var template (omit) Response v_OptionalResponse180;
    var MessageHeader v_MessageHeader;
    var template SDP_Message v_ExpectedSDP_200 := omit;
    var SDP_Message v_SDP_Message180;

    var boolean v_Trying := false;
    var boolean v_SrvccAlerting := false;

    timer t_Timer := 5.0;   // as for C.11 (-> f_IMS_MTCallSetup_Common_Steps7_11)

    f_IMS_CC_StartCall(IPCAN_MT_SpeechCall);   // @sic R5s140567 sic@

    // Step 1 - Send INVITE
    v_MessageHeader_Invite := f_IMS_InviteRequest_MessageHeaderTX(-, v_SrvccAlerting);

    v_InviteRequest := cs_INVITE_Request(v_ContactUrl, v_MessageHeader_Invite, cs_MessageBody_SDP(p_SDP_Message_Invite));
    v_InviteRequestWithSdp := f_IMS_InviteRequestWithSdpMT(v_InviteRequest, p_SDP_Message_Invite);
    v_Protocol := v_InviteRequestWithSdp.RoutingInfo.Protocol;

    IMS_Client.send(cas_IMS_Invite_Request(v_InviteRequestWithSdp.RoutingInfo, v_InviteRequest));

    // Step 2 - Void
    v_MessageHeader_183RX := f_IMS_InviteResponse_183_MessageHeaderRX(v_InviteRequest, tsc_OptionTagList_precondition);
    v_MessageHeader_180RX := f_IMS_InviteResponse_180_MessageHeaderRX(v_InviteRequest, -, cr_ContentTypeSDP ifpresent);
    v_ExpectedResponse_183RX := cr_Response(c_statusLine183, v_MessageHeader_183RX, cr_MessageBody_SDP);
    v_ExpectedResponse_180RX := cr_Response(c_statusLine180, v_MessageHeader_180RX, cr_MessageBody_SDP ifpresent);

    t_Timer.start;
    alt {
      // @siclog "Step 3" siclog@
      [not v_Trying] a_IMS_ReceiveResponse(v_InviteRequest.msgHeader,
                                           v_Protocol,
                                           cr_Response(c_statusLine100, f_IMS_InviteResponse_100_MessageHeaderRX(v_InviteRequest)),
                                           v_IMS_DATA_RSP)                     /* @sic R5s140350: v_IMS_DATA_RSP is 'out' parameter sic@ */
        {
          v_Trying :=  true;
          repeat;
        }
      // @siclog "Step 3A or 4" siclog@
      [] a_IMS_ReceiveResponse(v_InviteRequest.msgHeader,
                               v_Protocol,
                               (v_ExpectedResponse_183RX, v_ExpectedResponse_180RX),
                               v_IMS_DATA_RSP)
        {
          v_Response := v_IMS_DATA_RSP.Response;
          v_MessageHeader := v_IMS_DATA_RSP.Response.msgHeader;
          f_IMS_Dialog_CompleteCreationForMTCall(v_MessageHeader);
          f_IMS_MessageHeader_Response_CheckRecordRoute(v_MessageHeader);   /* @sic R5s150039 Change 6 sic@ */
          f_IMS_MessageHeader_CheckContactAddr(v_MessageHeader);      // check whether contact contains "SIP URI with IP address or FQDN" (see @sic R5s120727 sic@)
        }
      [] t_Timer.timeout
        {
        }
    }
    f_IMS_IPCAN_SendCoOrdMsg(IPCAN);       /* trigger IPCAN to continue with activation of secondary PDP context or dedicated EPS bearer
                                              @sic R5s140646 sic@ */

    if (isbound(v_Response)) {  // => either 183 or 180 response has been received
      select (v_Response.statusLine.statusCode) {
        case (183) {
          v_Response183 := v_Response;
          f_IMS_SIP_SdpMessageBody_DecodeMatchAndCheckSDP(v_Response183.messageBody, p_SDP_Message_183);
          // @siclog "Step 3B - 3C (optionally 4)" siclog@
          v_OptionalResponse180 := f_IMS_MTCallSetup_SendPRACK_ReceiveOK(v_InviteRequest, v_Response183.msgHeader, v_ExpectedResponse_180RX);  // send PRACK, wait for 200 Ok; in parallel the 180 Ringing may come (optionally)
          if (ispresent(v_OptionalResponse180)) {
            v_Response180 := valueof(v_OptionalResponse180);
          }
        }
        case (180) {
          v_Response180 := v_Response;
        }
      }
      if (not isbound(v_Response180)) {   // .. still not got any 180 Ringing ...
        // @siclog "Step 4" siclog@
        alt {
          [] a_IMS_ReceiveResponse(v_InviteRequest.msgHeader, v_Protocol, v_ExpectedResponse_180RX, v_IMS_DATA_RSP)
            {
              v_Response180 := v_IMS_DATA_RSP.Response;
            }
          [] t_Timer.timeout
            {
            }
        }
      }
      t_Timer.stop;
    }
    f_IMS_IPCAN_WaitForTrigger(IPCAN);     /* wait for completion of RRC/NAS signalling
                                              NOTE: as the coordination ports are not covered by the default behaviour we can just collect the trigger here
                                              @sic R5s140646 sic@ */

    // @siclog "Step 5 - 6" siclog@
    if (isbound(v_Response180)) {
      f_IMS_MessageHeader_Response_CheckRecordRoute(v_MessageHeader);   /* @sic R5s150039 Change 6 sic@ */
      f_IMS_MessageHeader_CheckContactAddr(v_Response180.msgHeader);    // check whether contact contains "SIP URI with IP address or FQDN" (see @sic R5s120727 sic@)
      if (not isbound(v_Response183)) {
        f_IMS_Dialog_CompleteCreationForMTCall(v_Response180.msgHeader);
      }
      if (ispresent(v_Response180.messageBody)) {

        if (isbound(v_Response183)) {
          f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "message body shall only be present in 180 response when there has been no 183 response");     // @sic R5-145728 sic@
        }

        v_SDP_Message180 := f_IMS_SIP_SdpMessageBody_DecodeMatchAndCheckSDP(v_Response180.messageBody, p_SDP_Message_180or200);
      }
      if (f_MessageHeader_CheckRequire(v_Response180.msgHeader, c_tag100rel)) {
        f_IMS_MTCallSetup_SendPRACK_ReceiveOK(v_InviteRequest, v_Response180.msgHeader);
      }
    }
    
    // @siclog "Step 6A" siclog@
    f_UT_AnswerCall(MMI);

    // @siclog "Step 7 - 8" siclog@  Receive OK for INVITE, Send ACK
    if (not isbound(v_SDP_Message180) and not isbound(v_Response183)) {  // @sic R5s140580 change 1 sic@
      v_ExpectedSDP_200 := p_SDP_Message_180or200;
    }
    v_ResponseWithSdp := f_IMS_MTCallSetup_Common_Steps12_13(v_InviteRequestWithSdp, v_ExpectedSDP_200);
    v_ContactUrl := f_MessageHeader_GetContactSipUrl(v_ResponseWithSdp.Response.msgHeader);

    // Step 14-15
    f_IMS_CallReleaseMT(v_InviteRequest, v_ContactUrl);
    // f_IMS_CC_ReleaseConnection(IPCAN_MT_SpeechCall);   @sic R5s140580 additional change by R&S sic@
  }

  /****************************************************************************/
  /*
   * @desc      REFERENCE TS 34.229-1 clause 16.2
   * @status    APPROVED (IMS)
   */
  function f_TC_16_2_IMS1() runs on IMS_PTC
  { /* @sic R5w140113, R5w140009: enhanced SDP type definitions sic@ */
    /* @sic R5-142994: Reimplementation sic@ */
    var charstring v_ModeSet := "0,2,4,7"; /* @sic R5-140916 sic@ */
    var template (present) SemicolonParam_List v_MediaFormatParametersRX := {cr_Fmtp_ModesetParam(v_ModeSet), *};
    var template (value)   SDP_Message v_SDP_Message_Invite;
    var template (present) SDP_Message v_SDP_Message_183;
    var template (present) SDP_Message v_SDP_Message_180or200;

    
    f_IMS_CC_Preamble(IPCAN_SpeechCall, IMS_REGISTERED);
    f_IMS_TestBody_Set(true);
    
    v_SDP_Message_Invite   := fl_IMS_BuildSDP_16_2_Step1(v_ModeSet);
    v_SDP_Message_183      := f_IMS_BuildSDP_16_X_RX(cr_RTPMAP_AMR_8000, -, c_none);
    v_SDP_Message_180or200 := f_IMS_BuildSDP_16_X_RX(cr_RTPMAP_AMR_8000, v_MediaFormatParametersRX, c_sendrecv);

    fl_TC_16_X_CommonProcedure(v_SDP_Message_Invite, v_SDP_Message_183, v_SDP_Message_180or200);
    
    f_IMS_TestBody_Set(false);
    f_IMS_CC_Postamble(IPCAN_MT_SpeechCall);
  }

  /****************************************************************************/
  /*
   * @desc      REFERENCE TS 34.229-1 clause 16.3
   * @status    APPROVED (IMS)
   */
  function f_TC_16_3_IMS1() runs on IMS_PTC
  { /* @sic R5-142994: Reimplementation sic@ */
    var integer v_BandwidthAS := 49;  /* @sic R5-144700 sic@ */
    var template (value)   SDP_Message v_SDP_Message_Invite;
    var template (present) SDP_Message v_SDP_Message_183;
    var template (present) SDP_Message v_SDP_Message_180or200;

    f_IMS_CC_Preamble(IPCAN_SpeechCall, IMS_REGISTERED);
    f_IMS_TestBody_Set(true);

    v_SDP_Message_Invite   := fl_IMS_BuildSDP_16_3and4_Step1(v_BandwidthAS);
    v_SDP_Message_183      := f_IMS_BuildSDP_16_X_RX(cr_RTPMAP_AMR_16000, -, c_none);        /* @sic R5-145728 sic@ */
    v_SDP_Message_180or200 := f_IMS_BuildSDP_16_X_RX(cr_RTPMAP_AMR_16000, -, c_sendrecv);

    fl_TC_16_X_CommonProcedure(v_SDP_Message_Invite, v_SDP_Message_183, v_SDP_Message_180or200);
    
    f_IMS_TestBody_Set(false);
    
    f_IMS_CC_Postamble(IPCAN_MT_SpeechCall);
  }
  
  /****************************************************************************/
  /*
   * @desc      REFERENCE TS 34.229-1 clause 16.4
   * @status    APPROVED (IMS)
   */
  function f_TC_16_4_IMS1() runs on IMS_PTC
  { /* @sic R5-142994: Reimplementation sic@ */
    var integer v_BandwidthAS := 38;  /* @sic R5-144700 sic@ */
    var boolean v_ConnectionInSessionDesc := false;
    var charstring v_ModeSet1 := "0,1,2";     /* @sic R5-140917: mode-set changed from "0,2,5,7,8" to "0,1,2" sic@ */
    var charstring v_ModeSet2 := "0,2,4,7";   /* @sic R5-140917: mode-set changed from "0,2,5,7" to "0,2,4,7" sic@ */
    var template (value) SemicolonParam_List v_SemicolonParam_List_ModeSet1 := {cs_Fmtp_ModesetParam(v_ModeSet1)};
    var template (value) SemicolonParam_List v_SemicolonParam_List_ModeSet2 := {cs_Fmtp_ModesetParam(v_ModeSet2)};
    var template (present) SemicolonParam_List v_MediaFormatParametersRX    := {cr_Fmtp_ModesetParam(v_ModeSet1), *};
    var template (value)   SDP_Message v_SDP_Message_Invite;
    var template (present) SDP_Message v_SDP_Message_183;
    var template (present) SDP_Message v_SDP_Message_180or200;

    f_IMS_CC_Preamble(IPCAN_SpeechCall, IMS_REGISTERED);
    f_IMS_TestBody_Set(true);

    v_SDP_Message_Invite   := fl_IMS_BuildSDP_16_3and4_Step1(v_BandwidthAS, v_SemicolonParam_List_ModeSet1, v_SemicolonParam_List_ModeSet2, v_ConnectionInSessionDesc);
    v_SDP_Message_183      := f_IMS_BuildSDP_16_X_RX(cr_RTPMAP_AMR_16000, v_MediaFormatParametersRX, c_none);        /* @sic R5-145728 sic@ */
    v_SDP_Message_180or200 := f_IMS_BuildSDP_16_X_RX(cr_RTPMAP_AMR_16000, v_MediaFormatParametersRX, c_sendrecv);

    fl_TC_16_X_CommonProcedure(v_SDP_Message_Invite, v_SDP_Message_183, v_SDP_Message_180or200);
    
    f_IMS_TestBody_Set(false);
    
    f_IMS_CC_Postamble(IPCAN_MT_SpeechCall);
  }
  
}
