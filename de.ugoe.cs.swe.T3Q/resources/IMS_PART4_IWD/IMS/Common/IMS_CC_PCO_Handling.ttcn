/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2015-01-18 13:59:16 +0100 (Sun, 18 Jan 2015) $
// $Rev: 12866 $
/******************************************************************************/

module IMS_CC_PCO_Handling {
  import from CommonDefs all;
  import from NAS_CommonTypeDefs all;
  import from NAS_AuxiliaryDefsAndFunctions all;

  const O2_Type tsc_ConfigOptions_P_CSCF_IPv6_Request := '0001'O;   /* @status    APPROVED (IMS) */
  const O2_Type tsc_ConfigOptions_IM_CN_SubsystemFlag := '0002'O;   /* @status    APPROVED (IMS) */
  const O2_Type tsc_ConfigOptions_P_CSCF_IPv4_Request := '000C'O;   /* @status    APPROVED (IMS) */


  //============================================================================
  /*
   * @desc      Check protocol config options for container id
   * @param     p_ConfigOptions
   * @param     p_ContainerId
   * @return    boolean
   * @status    APPROVED (IMS)
   */
  function f_ProtocolConfigOptions_Check(NAS_ProtocolConfigOptions_Type p_ConfigOptions,
                                         O2_Type p_ContainerId) return boolean
  {
    var integer i;
    
    for (i := 0; i < lengthof(p_ConfigOptions); i := i + 1) {
      if (p_ConfigOptions[i].protocolID == p_ContainerId) {
        return true;
      }
    }
    return false;
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      Evaluate PCOs sent by the UE
   * @param     p_ConfigOptionsRX
   * @return    template (value) NAS_ProtocolConfigOptions_Type
   * @status    APPROVED (IMS)
   */
  function f_IMS_CC_ProtocolConfigOption_GetResponse(NAS_ProtocolConfigOptions_Type p_ConfigOptionsRX) return template (value) NAS_ProtocolConfigOptions_Type
  {
    var boolean v_IgnoreIM_CN_SubsystemSignalingFlag := false;
    return f_NAS_GetProtocolConfigOptionList(p_ConfigOptionsRX, -, v_IgnoreIM_CN_SubsystemSignalingFlag);
  }

  //----------------------------------------------------------------------------

/*   function f_ProtocolConfigOption_Remove(NAS_ProtocolConfigOptions_Type p_ConfigOptList, */
/*                                          O2_Type p_ContainerId) return NAS_ProtocolConfigOptions_Type */
/*   { */
/*     var NAS_ProtocolConfigOptions_Type v_ConfigOptList := {}; */
/*     var integer i; */
/*     var integer k := 0; */

/*     for (i := 0; i < lengthof(p_ConfigOptList); i := i + 1) { */
/*       if (p_ConfigOptList[i].protocolID != p_ContainerId) { */
/*         v_ConfigOptList[k] := p_ConfigOptList[i]; */
/*         k := k + 1; */
/*       } */
/*     } */
/*     return v_ConfigOptList; */
/*   } */


/*   function f_ProtocolConfigOption_GetResponse_No_P_CSCF_Addr(NAS_ProtocolConfigOptions_Type p_ConfigOptionsRx) return NAS_ProtocolConfigOptions_Type */
/*   { */
/*     var NAS_ProtocolConfigOptions_Type v_ConfigOptList := f_IMS_CC_ProtocolConfigOption_GetResponse(p_ConfigOptionsRx); */
/*     v_ConfigOptList := f_ProtocolConfigOption_Remove(v_ConfigOptList, tsc_ConfigOptions_P_CSCF_IPv6_Request); */
/*     v_ConfigOptList := f_ProtocolConfigOption_Remove(v_ConfigOptList, tsc_ConfigOptions_P_CSCF_IPv4_Request);    // @sic CR R5s120530 change 1.11 TODO: Prose CR needed sic@ */
/*     return v_ConfigOptList; */
/*   } */

}
