/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2015-12-09 11:32:25 +0100 (Wed, 09 Dec 2015) $
// $Rev: 15053 $
/******************************************************************************/
module IMS_CustomizedAlertingTonesTestcases {

  import from LibSip_Common all;
  import from LibSip_SIPTypesAndValues all;
  import from LibSip_SDPTypes all;
  import from IMS_Component all;
  import from IMS_ASP_TypeDefs all;
  import from IMS_ASP_Templates all;
  import from IMS_CommonParameters all;
  import from IMS_CommonTemplates all;
  import from IMS_CommonDefs all;
  import from IMS_SIP_Templates all;
  import from IMS_MessageBody_Templates all;
  import from IMS_SDP_Templates all;
  import from IMS_SDP_Messages all;
  import from IMS_CC_CommonFunctions all;
  import from IMS_Procedures_CallControl all;
  import from IMS_CommonFunctions all;
  import from IMS_PTC_CoordMsg all;
  
  //============================================================================
  // local aux. functions
  //----------------------------------------------------------------------------
  /*
   * @desc      return template for SDP message at step 9 according to 34.229-1 (derived from f_IMS_BuildSDP_AnnexC21_Step4)
   * @param     p_SDP_MessageInvite
   * @return    template (value) SDP_Message
   * @status    APPROVED (IMS)
   */
  function fl_IMS_BuildSDP_CAT_Step9(SDP_Message p_SDP_MessageInvite) runs on IMS_PTC return template (value) SDP_Message
  { /* @sic R5s150714: optimization see 4.4  sic@ */
    var boolean v_PreconditionsRequired := false;   /* to be added later on */
    var template (value) SDP_Message v_SDP_Message := f_IMS_BuildSDP_AnnexC21_Step4(p_SDP_MessageInvite, v_PreconditionsRequired);
    var template (omit) SDP_attribute_list v_SDP_MediaAttributes;
    var template (omit) SDP_attribute_list v_SDP_PrecondionAttributes;

    v_SDP_PrecondionAttributes := cs_SDP_PrecondionAttributes(c_sendrecv, c_none, c_mandatory, c_mandatory);
    v_SDP_PrecondionAttributes := f_SDP_Attributes_Add_TX(v_SDP_PrecondionAttributes, cs_SDP_Attribute_conf_qos);
    
    v_SDP_MediaAttributes := f_SDP_Attributes_Add_TX(v_SDP_Message.media_list[0].attributes, cs_SDP_Attribute_content("g.3gpp.cat"));
    v_SDP_Message.media_list[0].attributes := f_SDP_Attributes_Concat_TX(v_SDP_MediaAttributes, v_SDP_PrecondionAttributes);
    
    return v_SDP_Message;
  }
  
  /*
   * @desc      34.229-1 CAT 20.1.4, Step 9: send 183 Session In Progress (derived from f_IMS_MOCallSetup_AnnexC21_Step4)
   *            return MessageHeader of the 183 response
   * @param     p_InviteRequestWithSdp
   * @param     p_ContactUri
   * @return    MessageHeader
   * @status    APPROVED (IMS)
   */
  function fl_IMS_MOCallSetup_CAT_Step9(IMS_InviteRequestWithSdp_Type p_InviteRequestWithSdp,
                                        charstring p_ContactUri) runs on IMS_PTC return MessageHeader
  {
    var boolean v_Earlymedia := true;
    var template (value) IMS_RoutingInfo_Type v_RoutingInfo_DL := f_IMS_RoutingInfo_ULtoDL(p_InviteRequestWithSdp.RoutingInfo);
    var INVITE_Request v_InviteRequest := p_InviteRequestWithSdp.Invite;
    var SDP_Message v_SDP_MessageInvite := p_InviteRequestWithSdp.SdpOffer;
    var template (value) MessageHeader v_MessageHeader_Response183;
    var template (value) SDP_Message v_SDP_MessageTx := fl_IMS_BuildSDP_CAT_Step9(v_SDP_MessageInvite);
    var template (value) SemicolonParam_List v_AditionalContactParams := { cs_GenericParam_Common(tsc_IMS_FeatureParamId_3gpp_icsi_ref, cs_GenValueQuoted(cs_IMS_EncodedFeatureParamValue_3gpp_icsi_ref)) };

    // Step 9. Send 183 Session In Progress.
    v_MessageHeader_Response183 := f_IMS_InviteResponse_183_MessageHeaderTX(v_InviteRequest, tsc_OptionTagList_precondition, p_ContactUri, v_AditionalContactParams, -, -, v_Earlymedia); //CAT specific
    v_MessageHeader_Response183.toField.toParams[0].paramValue.tokenOrHost := "xyzTC20_1-INVITE-Totag"; /* @sic R5s150714: dialog 2 setting see 4.2  sic@ */
    f_IMS_PTC_ImsInfo_DialogSetLocalTag("xyzTC20_1-INVITE-Totag");
    IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine183, v_MessageHeader_Response183, cs_MessageBody_SDP(v_SDP_MessageTx))));
    
    return valueof(v_MessageHeader_Response183);
  }
  
  //----------------------------------------------------------------------------
  /*
   * @desc      REFERENCE TS 34.229-1 clause 20.1
   * @status    APPROVED (IMS)
   */
  function f_TC_20_1_IMS1() runs on IMS_PTC
  {
    var IMS_InviteRequestWithSdp_Type v_InviteRequestWithSdp;  /* @sic R5w140112: IMS_InviteRequestWithSdp_Type instead of IMS_DATA_REQ */
    var MessageHeader v_MessageHeader_Response183;
    var boolean v_ResourcesEstablished;
    
    f_IMS_CC_Preamble(IPCAN_SpeechCall, IMS_REGISTERED);
    
    f_IMS_TestBody_Set(true);
    
    // @siclog "Step 1" siclog@
    f_IMS_CC_StartCall(IPCAN_MO_SpeechCall);
    
    // @siclog "Step 2" siclog@
    v_InviteRequestWithSdp := f_IMS_MOCallSetup_AnnexC21_Step2_A4();
    
    // @siclog "Step 3 - 4" siclog@
    v_MessageHeader_Response183 := f_IMS_MOCallSetup_AnnexC21_Step3_4(v_InviteRequestWithSdp);  /* @sic R5w140112 sic@ */
    
    // See TS. 36.508 EUTRA/EPS signalling for IMS MO speech call Table 4.5A.6.3-1 Step 9-11
    f_IMS_CC_TriggerDedicatedBearerActivation(IPCAN_MO_SpeechCall);  // trigger IPCAN to continue with activation of secondary PDP context or dedicated EPS bearer
    
    // Step 5 - 8 (TC 12.12 / C.21)
    f_IMS_MOCallSetup_AnnexC21_Steps5_8(v_InviteRequestWithSdp, v_MessageHeader_Response183);
    
    // Step 9 - 13 (Setup dialog 2): modified Step 4 - 8 (C.21)
    /* @sic R5s150714: dialog setting see 4.1 sic@ */
    f_IMS_PTC_ImsInfo_ActivateDialog(secondDialog);
    f_IMS_PTC_ImsInfo_DialogInit(dialogRemote, v_InviteRequestWithSdp.Invite.msgHeader);
    f_IMS_PTC_ImsInfo_CseqSet(dialogRemote, v_InviteRequestWithSdp.Invite.msgHeader.cSeq.seqNumber);
    
    v_MessageHeader_Response183 := fl_IMS_MOCallSetup_CAT_Step9(v_InviteRequestWithSdp, "sip:cat-as.home1.net");
    v_ResourcesEstablished := true;
    f_IMS_MOCallSetup_AnnexC21_Steps5_8(v_InviteRequestWithSdp, v_MessageHeader_Response183, -, -, -, v_ResourcesEstablished);
    
    // Steps 14-15 (Step 12 - 13 of C.21)
    /* @sic R5s150714: dialog setting see 4.1 sic@ */
    f_IMS_PTC_ImsInfo_ActivateDialog(firstDialog); // Signalling continues on first dialog
    f_IMS_MOCallSetup_AnnexC21C25_Step12_13(v_InviteRequestWithSdp, px_IMS_CalleeContactUri);
    
    // @siclog "Step 16 - 18 (TC 20.1) from Step 14 - 15 (TC 12.12)"
    f_IMS_CallReleaseMO(v_InviteRequestWithSdp.Invite);
    
    f_IMS_TestBody_Set(false);
    
    f_IMS_CC_Postamble(IPCAN_MO_SpeechCall);
  }
  
}
