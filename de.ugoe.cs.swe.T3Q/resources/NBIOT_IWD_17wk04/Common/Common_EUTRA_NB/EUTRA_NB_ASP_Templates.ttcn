/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2017, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_17wk04
// $Date: 2016-12-24 13:03:54 +0100 (Sat, 24 Dec 2016) $
// $Rev: 17690 $
/******************************************************************************/

module EUTRA_NB_ASP_Templates {
  import from EUTRA_NB_CommonDefs all;
  import from EUTRA_NB_ASP_TypeDefs all;

  const TimingAdvanceIndex_Type tsc_TA_Def := 31;               /* by default all timing advance in a typical simulation environment will be zero
                                                                 * @sic R5s090180: 0->31;  sic@
                                                                 * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
  const TimingAdvance_Period_Type tsc_TA_Period_Def := 600;     /* corresponding to Default of TimingAlignment timer =SF750
                                                                 * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */

  /*
   * @desc      auxiliary function to set CNF flag for configuration ASPs
   * @param     p_TimingInfo
   * @param     p_CnfFlag           (default value: omit)
   * @return    boolean
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS)
   */
  function f_CnfFlag_Set(template (value) TimingInfo_Type p_TimingInfo,
                         template (omit) boolean p_CnfFlag := omit) return boolean
  {
    var boolean v_CnfFlag;
    
    if (isvalue(p_CnfFlag)) {
      v_CnfFlag := valueof(p_CnfFlag);
    } else {
      v_CnfFlag := f_TimingInfo_IsNow(p_TimingInfo);
    }
    return v_CnfFlag;
  }
  
  template (value) RapIdCtrl_Type cs_RapIdCtrl_Auto :=
  { /* @status    APPROVED (LTE, NBIOT) */
    Automatic := true   /* SS shall automatically use same RAPID as received from the UE */
  };

  template (value) ContentionResolution_ContainedRlcPdu_Type cs_ContentionResolution_RlcPdu_None :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
    None := true
  };

  template (value) ContentionResolution_ContainedRlcPdu_Type cs_ContentionResolution_RlcPdu(octetstring p_RLC_PDU) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
    RlcPdu := p_RLC_PDU
  };
  
  template (value) TransmissionRepetition_Type cs_TransmissionRepetition_Continuous :=
  { /* Default UL Auto PUCCH Synch to be continous */
    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
    Continuous := true
  };

  template (value) TransmissionRepetition_Type cs_TransmissionRepetitionCyclic(integer p_Cycles) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    NumOfCycles := p_Cycles
  };
  
  template (value) RandomAccessBackoffIndicator_Type cs_BI_None :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
    None  := true
  };

  template (value) TempC_RNTI_Type cs_TempC_RNTI_SameAsC_RNTI :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
    SameAsC_RNTI := true        /* in the RA response SS shall use the same C-RNTI as configured in ActiveCellConfig_Type;
                                   this is useful for initial random access */
  };

  
  //============================================================================
  // UplinkTimeAlignment
  //----------------------------------------------------------------------------

  template (value) UplinkTimeAlignment_AutoSynch_Type cs_UplinkTimeAlignment_AutoSynch_Def(TimingAdvance_Period_Type p_TimingAdvance_Period := tsc_TA_Period_Def) :=
  { /* PUCCH Synch Auto mode */
    /* @sic R5-101050 change 12: TA period as optional parameter sic@ */
    /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    TimingAdvance  := tsc_TA_Def,
    TA_Period      := p_TimingAdvance_Period,              /* time period after which TA MAC control elements need to be automatically transmitted */
    TA_Repetition  := cs_TransmissionRepetition_Continuous /* number of TA MAC control element repetitions to be automatically transmitted or 'Continuous' */
  };

  template (value) UplinkTimeAlignment_Synch_Type cs_UplinkTimeAlignment_Synch_Auto_Def(TimingAdvance_Period_Type p_TimingAdvance_Period := tsc_TA_Period_Def) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    /* @sic R5-101050 change 12: TA period as optional parameter sic@ */
    Auto  := cs_UplinkTimeAlignment_AutoSynch_Def(p_TimingAdvance_Period)
  };

  template (value) UplinkTimeAlignment_Synch_Type cs_UplinkTimeAlignment_Synch_None :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, NBIOT, POS) */
    None  := true
  };
  
  template (omit) UplinkTimeAlignment_Synch_Type cs_UplinkTimeAlignment_Synch_KeepAsItIs := omit; /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */

}
