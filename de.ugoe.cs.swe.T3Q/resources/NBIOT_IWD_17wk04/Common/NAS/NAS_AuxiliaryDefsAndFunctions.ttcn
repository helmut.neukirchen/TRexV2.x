/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2017, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_17wk04
// $Date: 2017-01-25 20:44:24 +0100 (Wed, 25 Jan 2017) $
// $Rev: 17830 $
/******************************************************************************/

module NAS_AuxiliaryDefsAndFunctions {

  import from CommonDefs all;
  import from Parameters all;
  import from NAS_CommonTypeDefs all;
  import from NAS_CommonTemplates all;
  import from CommonIP all;

  //============================================================================
  // Protocol Configuration Options (PCO)
  //----------------------------------------------------------------------------
  /*
   * @desc      Function used to check whether the UE requests in its PCO an address allocation via NAS signalling
   *            If the UE does not send PCO, then address assignment shall be via DHCP. TS 36.508 table 4.7.3-6.
   * @param     p_Pco
   * @return    boolean
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_CheckPCOforIPallocationViaNas(template (omit) ProtocolConfigOptions p_Pco) return boolean
  {
    var NAS_ProtocolConfigOptions_Type v_ProtocolContainerList;
    var integer i;

    if (isvalue(p_Pco.pco)) {      /* @sic R5s170030 change 8 sic@ */
      v_ProtocolContainerList := valueof (p_Pco.pco);
      for (i := 0; i < lengthof(v_ProtocolContainerList); i := i + 1) {
        if (v_ProtocolContainerList[i].protocolID == '000B'O) { // TS 24.008 clause 10.5.6.3  // @sic R5s090322 sic@
          return false;  // @sic R5s090322 sic@
        }
      }
    }
    return true;  // @sic R5s090322 sic@
  }

  //------------------------------------
  /*
   * @desc      Function used to check whether the UE requests in its PCO a specific protocol
   *            (SIP  signalling)
   * @param     p_Pco
   * @param     p_ProtocolID
   * @return    boolean
   * @status    APPROVED (LTE)
   */
  function f_CheckPCOforProtocolID(template (omit) ProtocolConfigOptions p_Pco,
                                   O2_Type p_ProtocolID) return boolean
  {
    var ProtocolConfigOptions v_Pco;
    var integer i;
    
    if (isvalue(p_Pco)) {
      v_Pco := valueof (p_Pco);
      for (i := 0; i < lengthof(v_Pco.pco); i := i + 1) {
        if(v_Pco.pco[i].protocolID == p_ProtocolID) { // TS 24.008 clause 10.5.6.3
        return true;
        }
      }
    }
    return false;
  }
  
 //------------------------------------
  /*
   * @desc      Function used to check whether the UE requests a P-CSCF address or DNS server address in its PCO
   *            If the UE does not support IMS, or doesn't request a P-CSCF address, the function returns the default PCO, PPP
   * @param     p_ConfigOptionsRX
   * @param     p_PdnIndex          (default value: PDN_1)
   * @param     p_IgnoreIM_CN_SubsystemSignalingFlag   (default value: true)
   * @return    template (value) NAS_ProtocolConfigOptions_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_NAS_GetProtocolConfigOptionList(NAS_ProtocolConfigOptions_Type p_ConfigOptionsRX,
                                             PDN_Index_Type p_PdnIndex := PDN_1,
                                             boolean p_IgnoreIM_CN_SubsystemSignalingFlag := true) return template (value) NAS_ProtocolConfigOptions_Type
  {
    var PDN_AddressInfo_Type v_PDN_AddressInfo := f_PDN_AddressInfo_Get(p_PdnIndex);
    var template (value) NAS_ProtocolConfigOptions_Type v_ProtocolContainerList := {};
    var integer v_PcoCnt := 0;
    var O2_Type v_ProtocolId;
    var octetstring v_Contents;
    var integer i;
    
    for (i := 0; i < lengthof(p_ConfigOptionsRX); i := i + 1) {
      
      v_ProtocolId := p_ConfigOptionsRX[i].protocolID;
      v_Contents := ''O;
      
      select (v_ProtocolId) {  // See 24.008 Table 10.5.154
        case ('0001'O) {       // P-CSCF IPv6 address
          if (pc_IMS) {
            v_Contents := f_Convert_IPv6Addr2OctString(v_PDN_AddressInfo.PCSCF_IPAddressIPv6);
          }
        }
        case ('0002'O) {            // IM CN Subsystem Signalling Flag, reply with an empty container
          if (not p_IgnoreIM_CN_SubsystemSignalingFlag) {
            v_ProtocolContainerList[v_PcoCnt] := cs_ProtocolContainer_Common(v_ProtocolId);
            v_PcoCnt := v_PcoCnt + 1;
            continue;
          }
        }
        case ('0003'O) {       // DNS Server IPv6 address
          v_Contents := f_Convert_IPv6Addr2OctString(v_PDN_AddressInfo.DNS_ServerAddressIPv6);
        }
        case ('000C'O) {       // P-CSCF IPv4 address
          if (pc_IMS) {
            v_Contents := f_Convert_IPv4Addr2OctString(v_PDN_AddressInfo.PCSCF_IPAddressIPv4);
          }
        }
        case ('000D'O) {       // DNS Server IPv4 address
          v_Contents := f_Convert_IPv4Addr2OctString(v_PDN_AddressInfo.DNS_ServerAddressIPv4);
        }
      }
      
      if (lengthof(v_Contents) > 0) {
        v_ProtocolContainerList[v_PcoCnt] := cs_ProtocolContainer(v_ProtocolId, v_Contents);
        v_PcoCnt := v_PcoCnt + 1;
      }
    }
    return v_ProtocolContainerList;
  }

  /*
   * @desc      wrapper for f_GetDefaultProtocolConfigOptionList
   * @param     p_Pco
   * @param     p_PdnIndex          (default value: PDN_1)
   * @return    template (value) ProtocolConfigOptions
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_GetDefaultProtocolConfigOptions(template (omit) ProtocolConfigOptions p_Pco,
                                             PDN_Index_Type p_PdnIndex := PDN_1) return template (value) ProtocolConfigOptions
  { /* @sic R5s130362 - MCC160 Implementation sic@ */
    /* @sic R5s141127 change 4 - MCC160 Comments: split into f_GetDefaultProtocolConfigOptions and f_GetDefaultProtocolConfigOptionList sic@ */
    var template (value) NAS_ProtocolConfigOptions_Type v_ProtocolContainerListTX := {};
    var NAS_ProtocolConfigOptions_Type v_ProtocolContainerListRX;
    
    if (isvalue(p_Pco.pco)) { /* @sic R5s170030 change 8 sic@ */
      v_ProtocolContainerListRX := valueof(p_Pco.pco);
      v_ProtocolContainerListTX := f_NAS_GetProtocolConfigOptionList(v_ProtocolContainerListRX, p_PdnIndex);
    }

    return f_NAS_ProtocolConfigOptionsTX(v_ProtocolContainerListTX);
  }

  /*
   * @desc      Decode APN acc. to TS 23.003 cl. 9.1 and RFC 1035 cl. 4.1.2
   * @param     p_APN
   * @return    Charstring
   * @status
   */
  function f_DomainName_Decode(octetstring p_APN) return charstring
  {
    var CharStringList_Type v_LabelList;
    var integer v_Length;
    var integer i;
    var integer k := 0;

    for (i := 0; i < lengthof(p_APN); i := i + 1) {
      v_Length := oct2int(p_APN[i]);
      v_LabelList[k] := oct2char(substr(p_APN, i+1, v_Length));
      i := i + v_Length;
      k := k + 1;
    }
    
    return f_StringJoin(v_LabelList, ".");
  }

  /*
   * @desc      Encode APN acc. to TS 23.003 cl. 9.1 and RFC 1035 cl. 4.1.2
   * @param     p_LabelList
   * @return    octetstring
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_DomainName_EncodeLabels(CharStringList_Type p_LabelList) return octetstring
  {
    var octetstring v_EncodedAPN := ''O;
    var octetstring v_LabelOctets;
    var integer v_Length;
    var integer i;

    for (i := 0; i < lengthof(p_LabelList); i := i + 1) {
      v_LabelOctets := char2oct(p_LabelList[i]);
      v_Length := lengthof(v_LabelOctets);
      v_EncodedAPN := v_EncodedAPN & int2oct(v_Length, 1) & v_LabelOctets;
    }
    return v_EncodedAPN;
  }

  /*
   * @desc      Encode APN acc. to TS 23.003 cl. 9.1 and RFC 1035 cl. 4.1.2
   * @param     p_DomainName
   * @return    octetstring
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_DomainName_Encode(charstring p_DomainName) return octetstring
  {
    var CharStringList_Type v_LabelList := f_StringSplit(p_DomainName, {"."});
    return f_DomainName_EncodeLabels(v_LabelList);
  }

  //============================================================================

  //----------------------------------------------------------------------------
  /*
   * @desc      Convert from IMSI, IMEI or IMEISV of type hexstring to octetstring (to be used in f_Imsi2MobileIdentity, f_Imei2MobileIdentity and f_Imeisv2MobileIdentity)
   * @param     p_MobileId
   * @return    octetstring
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_IRAT, NBIOT, POS, SSNITZ, UTRAN)
   */
  function fl_ImsiImei2Octetstring(hexstring p_MobileId) return octetstring
  {
    var integer v_Length := lengthof(p_MobileId);
    var integer v_Odd := (v_Length rem 2);
    var octetstring v_Other := '00'O;
    var integer I;
    var integer K;
    
    if (v_Odd == 0) {                   // If length is even
      v_Length := v_Length + 1;
      p_MobileId := p_MobileId & 'F'H;          // add '1111' on to the end of the IMSI
    }
    
    // Reverse each pair of digits
    // First digit is not included as it is treated differently
    K:=0;
    for (I:=1; I < v_Length - 1; I:=I+2) {
      v_Other[K] := hex2oct(p_MobileId[I+1] & p_MobileId[I]);
      K := K + 1;
    }
    
    return v_Other;
  }
  
  //----------------------------------------------------------------------------
  /*
   * @desc      Convert from IMSI of type hexstring to NAS MobileIdentity
   * @param     p_IMSI
   * @return    template (value) MobileIdentity
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_IRAT, NBIOT, POS, SSNITZ, UTRAN)
   */
  function f_Imsi2MobileIdentity(hexstring p_IMSI) return template (value) MobileIdentity
  {
    var integer v_ImsiLength := lengthof(p_IMSI);
    var integer v_Odd := (v_ImsiLength rem 2);
    var octetstring v_Other  := fl_ImsiImei2Octetstring (p_IMSI);  // @sic R5s100092 sic@
    var B4_Type v_FirstDigit := hex2bit(p_IMSI[0]);
    var B1_Type v_OddEvenInd := int2bit(v_Odd, 1);
    
    return cds_MobileIdentityImsi_lv(v_FirstDigit, v_OddEvenInd, v_Other);
  }
  
  //----------------------------------------------------------------------------
  /*
   * @desc      Convert from IMEI of type hexstring to NAS MobileIdentity
   *            When transmitted by the MS the CD/SD bit is set to 0.
   * @param     p_IMEI
   * @param     p_NAS_IdType        (default value: tsc_IdType_IMEI)
   * @return    template (present) MobileIdentity
   * @status    APPROVED (LTE, LTE_A_R10_R11, NBIOT)
   */
  function f_Imei2MobileIdentity(hexstring p_IMEI,
                                 NAS_IdType p_NAS_IdType := tsc_IdType_IMEI) return template (present) MobileIdentity // @sic R5s130758 sic@
  {
    var integer v_ImeiLength := lengthof(p_IMEI);
    var integer v_Odd := (v_ImeiLength rem 2);
    var octetstring v_Other  := fl_ImsiImei2Octetstring (substr(p_IMEI, 0, (v_ImeiLength - 1)) & '0'H);  // @sic R5s160006 sic@
    var B4_Type v_FirstDigit := hex2bit(p_IMEI[0]);
    var B1_Type v_OddEvenInd := int2bit(v_Odd, 1);
    
    return cr_MobileIdentityImei(v_FirstDigit, v_OddEvenInd, v_Other, p_NAS_IdType); // @sic R5s130758 sic@
  }
  
  //----------------------------------------------------------------------------
  /*
   * @desc      Convert from IMEISV of type hexstring to NAS MobileIdentity
   * @param     p_IMEISV
   * @return    template (present) MobileIdentity
   * @status    APPROVED (LTE)
   */
  function f_Imeisv2MobileIdentity(hexstring p_IMEISV) return template (present) MobileIdentity
  { // @sic R5-131832 sic@
    var integer v_ImeiLength := lengthof(p_IMEISV);
    var integer v_Odd := (v_ImeiLength rem 2);
    var octetstring v_Other  := fl_ImsiImei2Octetstring (p_IMEISV);
    var B4_Type v_FirstDigit := hex2bit(p_IMEISV[0]);
    var B1_Type v_OddEvenInd := int2bit(v_Odd, 1);
    
    return cr_MobileIdentityImeisv(v_FirstDigit, v_OddEvenInd, v_Other);
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      Convert from Emergency Number of type charstring to octetstring (to be used in Emergency Number List in e.g. Attach Accept)
   * @param     p_EmgNum
   * @return    octetstring
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function fl_EmgNum2Octetstring(hexstring p_EmgNum) return octetstring
  {
    var integer v_Length := lengthof(p_EmgNum);
    var integer v_Odd := (v_Length rem 2);
    var octetstring v_Result := '00'O;
    var integer I;
    var integer K;
    
    if (v_Odd == 1) {               // If length is odd
      v_Length := v_Length + 1;
      p_EmgNum := p_EmgNum & 'F'H;  // add '1111' on to the end of the Emergency Number
    }
    
    // Reverse each pair of digits, note that v_Length is even
    K:=0;
    for (I:=0; I < v_Length - 1; I:=I+2) {
      v_Result[K] := hex2oct(p_EmgNum[I+1] & p_EmgNum[I]);
      K := K + 1;
    }
    
    return v_Result;
  }
  
  /*
   * @desc      To generate an local emergency number list
   * @param     p_NoOfNums - To specify how many numbers to be included in the list (max = 10)
   * @param     p_NumList
   * @return    template (value) EmergNumList
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_Build_EmergNumList(integer p_NoOfNums,
                                EmergencyNumList p_NumList) return  template (value) EmergNumList
  {
    var integer i;
    var template (value) EmergNumList v_EmergNum;
    var octetstring v_LocalNum;
    var integer v_LocalLength := 0;
    var integer v_TotalLength := 0;
    var B7_Type v_ServCat;
    
    v_EmergNum.iei := '34'O;
    for (i := 0; i < p_NoOfNums; i:=i+1) {
      v_LocalNum := fl_EmgNum2Octetstring (p_NumList[i]);
      v_LocalLength := lengthof(v_LocalNum);
      if (i/2 > 0) {
        v_ServCat := ('00'B & ('00001'B << i));
      } else {
        v_ServCat := ('00'B & ('00001'B << i));
      }
      v_EmergNum.emergNum[i]:= cs_EmergencyLocalNumber(int2oct(v_LocalLength + 1, 1),
                                                       cs_EmergServCat(v_ServCat),
                                                       v_LocalNum);
      v_TotalLength := v_TotalLength + 2 + v_LocalLength; // 1 octet for v_LocalLength + 1 for SCValue + length of number
    }
    v_EmergNum.iel := int2oct (v_TotalLength, 1);
    return v_EmergNum;
  }
  
  /*
   * @desc      Generation of a new list of local emergency numbers different from existing ones
   * @param     p_NoOfNums         .. maximum 20
   * @param     p_ExistingNums
   * @return    EmergencyNumList   .. numbers of 3 decimal digits each
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_Get_EmergencyNumList(integer p_NoOfNums,
                                  template (omit) EmergencyNumList p_ExistingNums) return EmergencyNumList
  {
    var EmergencyNumList v_NewNumbers;
    var integer v_Number := 115;  // start as implied by LTE test case 11.2.6  @sic R5s141315 sic@
    var boolean v_NumberExists;
    var hexstring v_NumberDigits;
    var integer i;

    if ( p_NoOfNums > 20 ) {   FatalError(__FILE__, __LINE__, "unsupported number of emg nums") }; // max 20 emg nums may be generated

    for (i:=0; i<p_NoOfNums; i:=i+1) { // @sic R5s130748 sic@
      do {
        v_Number := v_Number mod 1000;
        if (v_Number < 100) { v_Number := v_Number + 100}; // ensure it is 3 digits
        v_NumberDigits := str2hex(int2str(v_Number));
        v_NumberExists := fl_CheckEmergencyNum(p_ExistingNums, v_NumberDigits);
        if (v_NumberExists) {
          v_Number := v_Number + 1;              // find another number
        }
      } while (v_NumberExists);
      v_NewNumbers[i] := v_NumberDigits;
      v_Number := v_Number + 1;
    }
    return v_NewNumbers;
  } // f_Get_EmergencyNumList

  /*
   * @desc      Check if an emergency number is contained in an emergency number list or not
   * @param     p_ExistingNums
   * @param     p_NewNum
   * @return    boolean
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function fl_CheckEmergencyNum(template (omit) EmergencyNumList p_ExistingNums, hexstring p_NewNum) return boolean
  {
    var integer i; // @sic R5s130748 sic@

    if (isvalue (p_ExistingNums)) { /* @sic R5s141315 sic@ */
      for (i := 0; i<lengthof(p_ExistingNums); i := i+1) {
        if (valueof(p_ExistingNums[i]) == p_NewNum){
          return true;
        }
      }
    }
    return false;
  } // fl_CheckEmergencyNum
 

  /*
   * @desc      check response on AT command from the UE
   * @param     p_AT_Response
   * @param     p_ExpectedString
   * @param     p_Text              (default value: "")
   * @param     p_Expression        (default value: "*[""]([^""]+)[""]*")
   * @param     p_Group             (default value: 0)
   * @status    APPROVED (LTE, SSNITZ)
   */
  function f_AT_ResponseCheck(charstring p_AT_Response,
                              charstring p_ExpectedString,
                              charstring p_Text := "",
                              charstring p_Expression := "*[""]([^""]+)[""]*",
                              integer p_Group := 0)
  { /* p_Expression := "*[""]([^""]+)[""]*";    TTCN-3 regular expression: anything + quote + no quote (at least one character) + quote + anything;
     *                                          => the text between the most inner quotes is returned */
    var charstring v_Expression := p_Expression;
    var charstring v_Result;
    var template charstring v_ExpectedString := pattern p_ExpectedString;
    var boolean v_MatchResult;
    var charstring v_CRLF := oct2char('0D'O) & oct2char('0A'O);
    
    v_Result := regexp(p_AT_Response, v_Expression, p_Group);
    
    v_MatchResult := match(v_Result, v_ExpectedString);
    
    if (v_MatchResult == true) {
      f_SetVerdict(pass,__FILE__, __LINE__, p_Text);
    } else  {
      f_ErrorLog(__FILE__, __LINE__, v_CRLF & "Value expected: " & p_ExpectedString & v_CRLF & "Value received: " & v_Result);  // if anything went wrong, show it
      f_SetVerdict(fail,__FILE__, __LINE__, p_Text);
    }
  }
  
}
