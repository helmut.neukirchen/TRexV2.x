/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2017, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_17wk04
// $Date: 2017-01-25 20:44:24 +0100 (Wed, 25 Jan 2017) $
// $Rev: 17830 $
/******************************************************************************/

module NBIOT_SRB_Templates {
  import from NBIOT_RRC_ASN1_Definitions language "ASN.1:2002" all;
  import from CommonDefs all;
  import from NBIOT_CommonDefs all;
  import from EUTRA_NB_CommonDefs all;
  import from NBIOT_ASP_TypeDefs all;
  import from NBIOT_ASP_SrbDefs all;
  import from EPS_NAS_MsgContainers all;          // NAS_MSG_Request_Type, NAS_MSG_Indication_Type
  import from NBIOT_AspCommon_Templates all;      // cr_CnfAspCommonPart_CellCfg, cs_ReqAspCommonPart_CellCfg
  import from NBIOT_RRC_Templates all;

  const integer tsc_SRB1bis_LogicalChannelPriority := 1;  /* @status    APPROVED (NBIOT) */

  //****************************************************************************
  // Templates for configuration of SRBs (RadioBearer_Type)
  //----------------------------------------------------------------------------
  // All SRBs

  template (value) NB_RadioBearer_Type cs_NB_SRB_Release(NB_SRB_Identity_Type p_Srb) :=
  { /* @status    APPROVED (NBIOT) */
    Id := {
      Srb := p_Srb
    },
    L2TestMode := omit,
    Config := {
      Release := true
    }
  };
  
  //----------------------------------------------------------------------------

  template (value) NB_PDCP_Configuration_Type cs_NB_PDCP_Configuration_None := { None  := true };  /* @status    APPROVED (NBIOT) */
  template (value) NB_RLC_RbConfig_Type cs_NB_RLC_RbConfig_TM := { TM := true };                   /* @status    APPROVED (NBIOT) */
  
  template (value) NB_RLC_RbConfig_Type cs_NB_RLC_RbConfig_AM :=
  { /* @status    APPROVED (NBIOT) */
    AM := {
      Tx := cs_NB_UL_AM_RLC_r13(cs_NB_TX_AM_RLC_SRB1_Def),
      Rx := cs_NB_DL_AM_RLC_r13(cs_NB_RX_AM_RLC_SRB1_Def)
    }
  };

  template (value) NB_RLC_Configuration_Type cs_NB_RLC_Configuration(template (value) NB_RLC_RbConfig_Type p_NB_RLC_RbConfig) :=
  { /* @status    APPROVED (NBIOT) */
    Rb := p_NB_RLC_RbConfig,
    TestMode := {
      None := true
    }
  };

  template (value) NB_MAC_TestModeConfig_Type cs_NB_MAC_TestMode_None := { None := true };                /* @status    APPROVED (NBIOT) */
  template (value) NB_MAC_TestModeConfig_Type cs_NB_MAC_TestMode_Transparent := { Transparent := true };

  template (value) NB_MAC_Configuration_Type cs_NB_MAC_Configuration(integer p_LogicalChannelPriority,
                                                                     template (value) NB_MAC_TestModeConfig_Type p_TestModeConfig := cs_NB_MAC_TestMode_None) :=
  { /* @status    APPROVED (NBIOT) */
    LogicalChannel := {
      Priority := p_LogicalChannelPriority
    },
    TestMode := p_TestModeConfig
  };

  //----------------------------------------------------------------------------
  // SRB0

  template (value) NB_RadioBearer_Type cs_NB_SRB0_ConfigDef :=
  { // SRB0 SS Configuration
    /* @status    APPROVED (NBIOT) */
    Id := {
      Srb := tsc_SRB0
    },
    L2TestMode := omit,
    Config := {
      AddOrReconfigure := {
        Pdcp := cs_NB_PDCP_Configuration_None,
        Rlc := cs_NB_RLC_Configuration(cs_NB_RLC_RbConfig_TM),
        LogicalChannelId := 0,
        Mac := cs_NB_MAC_Configuration(tsc_SRB0_LogicalChannelPriority)
      }
    }
  };

  //----------------------------------------------------------------------------
  // SRB1bis
  
  template (value) NB_RadioBearer_Type cs_NB_SRB1bis_ConfigCommon(template (omit) IndicationAndControlMode_Type p_L2TestMode := omit,
                                                                  template (value) NB_RLC_RbConfig_Type p_RLC_RbConfig := cs_NB_RLC_RbConfig_AM,
                                                                  template (value) NB_MAC_TestModeConfig_Type p_MAC_TestModeConfig := cs_NB_MAC_TestMode_None) :=
  { // SRB1bis SS Configuration
    /* @status    APPROVED (NBIOT) */
    Id := {
      Srb := tsc_SRB1bis
    },
    L2TestMode := p_L2TestMode,
    Config := {
      AddOrReconfigure := {
        Pdcp := cs_NB_PDCP_Configuration_None,
        Rlc := cs_NB_RLC_Configuration(p_RLC_RbConfig),
        LogicalChannelId := 3,
        Mac := cs_NB_MAC_Configuration(tsc_SRB1bis_LogicalChannelPriority, p_MAC_TestModeConfig)
      }
    }
  };
 
  template (value) NB_RadioBearer_Type cs_NB_SRB1bis_ConfigDef := cs_NB_SRB1bis_ConfigCommon;       /* @status    APPROVED (NBIOT) */

  //****************************************************************************
  // ASP Templates for configuration of SRBs
  //----------------------------------------------------------------------------

  template (value) NB_SYSTEM_CTRL_REQ cas_NB_CommonRadioBearerConfig_REQ(NBIOT_CellId_Type p_CellId,
                                                                         template (value) TimingInfo_Type p_TimingInfo,
                                                                         template (value) NB_RadioBearerList_Type p_RadioBearerList,
                                                                         template (omit) boolean p_CnfFlag := omit) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cs_NB_ReqAspCommonPart_CellCfg(p_CellId, p_TimingInfo, p_CnfFlag),
    Request := {
      RadioBearerList := p_RadioBearerList
    }
  };
  
  template (present) NB_SYSTEM_CTRL_CNF car_NB_CommonRadioBearerConfig_CNF(NBIOT_CellId_Type p_CellId) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cr_NB_CnfAspCommonPart_CellCfg(p_CellId),
    Confirm := {
      RadioBearerList := true
    }
  };
  
  //****************************************************************************
  // ASP Templates for signalling at SRB0, SRB1bis
  //----------------------------------------------------------------------------
  // common part:
  
  template (value) NB_ReqAspCommonPart_Type cs_NB_ReqAspCommonPart_SRB(NBIOT_CellId_Type p_CellId,
                                                                       NB_SRB_Identity_Type p_SrbId,
                                                                       template (value) TimingInfo_Type p_TimingInfo := cs_TimingInfo_Now,
                                                                       boolean p_FollowOnFlag := false) :=
  { /* @status    APPROVED (NBIOT) */
    CellId := p_CellId,
    RoutingInfo := {
      RadioBearerId := {
        Srb := p_SrbId
      }
    },
    TimingInfo := p_TimingInfo,
    ControlInfo := {
      CnfFlag := false,
      FollowOnFlag := p_FollowOnFlag
    }
  };
  
  template (present) NB_IndAspCommonPart_Type cr_NB_IndAspCommonPart_SRB(template (present) NBIOT_CellId_Type p_CellId,
                                                                         template (present) NB_SRB_Identity_Type p_SrbId,
                                                                         template (present) TimingInfo_Type p_TimingInfo := cr_TimingInfo_Any) :=
  { /* @status    APPROVED (NBIOT) */
    CellId := p_CellId,
    RoutingInfo := {
      RadioBearerId := {
        Srb := p_SrbId
      }
    },
    TimingInfo := p_TimingInfo,
    Status := {
      Ok := true
    }
  };

  //----------------------------------------------------------------------------
  // DL Signalling
  //----------------------------------------------------------------------------
  // SRB0: ASP to send RRC message
  
  template (value) NB_SRB_COMMON_REQ cas_NB_SRB0_RrcPdu_REQ(NBIOT_CellId_Type p_CellId,
                                                            template (value) TimingInfo_Type p_TimingInfo,
                                                            template (value) DL_CCCH_Message_NB p_RrcPdu) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cs_NB_ReqAspCommonPart_SRB(p_CellId, tsc_SRB0, p_TimingInfo),
    Signalling := {
      Rrc := {
        Ccch := p_RrcPdu
      },
      Nas := omit
    }
  };
  
  //----------------------------------------------------------------------------
  // SRB1: ASP to send RRC message
  
  template (value) NB_SRB_COMMON_REQ cas_NB_SRB_RrcPdu_REQ(NBIOT_CellId_Type p_CellId,
                                                           NB_SRB_Identity_Type p_SrbId,
                                                           template (value) TimingInfo_Type p_TimingInfo,
                                                           template (value) DL_DCCH_Message_NB p_RrcPdu) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cs_NB_ReqAspCommonPart_SRB(p_CellId, p_SrbId, p_TimingInfo),
    Signalling := {
      Rrc := {
        Dcch := p_RrcPdu
      },
      Nas := omit
    }
  };
  
  //----------------------------------------------------------------------------
  // SRB1: ASP to send RRC message (RRCConnectionReconfiguration) + one or more piggybacked NAS messages
  //       (more than one NAS PDU may occur only for RRCConnectionReconfiguration)
  
  template (value) NB_SRB_COMMON_REQ cas_NB_SRB1_RrcNasPduList_REQ(NBIOT_CellId_Type p_CellId,
                                                                   template (value) TimingInfo_Type p_TimingInfo,
                                                                   template (value) DL_DCCH_Message_NB p_RrcPdu,
                                                                   template (omit)  NAS_MSG_RequestList_Type p_NasMsgList) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cs_NB_ReqAspCommonPart_SRB(p_CellId, tsc_SRB1, p_TimingInfo),
    Signalling := {
      Rrc := {
        Dcch := p_RrcPdu
      },
      Nas := p_NasMsgList
    }
  };

  //----------------------------------------------------------------------------
  //

  template (value) NB_SRB_COMMON_REQ cas_NB_SRB_NasPdu_REQ(NBIOT_CellId_Type p_CellId,
                                                           NB_SRB_Identity_Type p_SrbId,
                                                           template (value) TimingInfo_Type p_TimingInfo,
                                                           template (value) NAS_MSG_Request_Type p_NasMsg) :=
  { /* SRB1/1bis: ASP to send NAS message (within RRC DLInformationTransfer) */
    /* @status    APPROVED (NBIOT) */
    Common := cs_NB_ReqAspCommonPart_SRB(p_CellId, p_SrbId, p_TimingInfo),
    Signalling := {
      Rrc := omit,
      Nas := { p_NasMsg }
    }
  };
  
  //----------------------------------------------------------------------------
  // SRB1bis: ASP to send NAS message (within RRC DLInformationTransfer)
  
  template (value) NB_SRB_COMMON_REQ cas_NB_SRB1bis_NasPdu_REQ(NBIOT_CellId_Type p_CellId,
                                                               template (value) TimingInfo_Type p_TimingInfo,
                                                               template (value) NAS_MSG_Request_Type p_NasMsg) :=
  /* @status    APPROVED (NBIOT) */
    cas_NB_SRB_NasPdu_REQ(p_CellId, tsc_SRB1bis, p_TimingInfo, p_NasMsg);
  
  //----------------------------------------------------------------------------
  // UL Signalling
  //----------------------------------------------------------------------------
  // SRB0: ASP to receive RRC message
  
  template (present) NB_SRB_COMMON_IND car_NB_SRB0_RrcPdu_IND(template (present) NBIOT_CellId_Type p_CellId,
                                                              template (present) UL_CCCH_Message_NB p_RrcPdu) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cr_NB_IndAspCommonPart_SRB(p_CellId, tsc_SRB0, ?),
    Signalling := {
      Rrc := {
        Ccch := p_RrcPdu
      },
      Nas := omit
    }
  };
  
  //----------------------------------------------------------------------------
  // SRB1/2: ASP to receive RRC message
  
  template (present) NB_SRB_COMMON_IND car_NB_SRB_RrcPdu_IND(NBIOT_CellId_Type p_CellId,
                                                             NB_SRB_Identity_Type p_SrbId,
                                                             template (present) UL_DCCH_Message_NB p_RrcPdu,
                                                             template (present) TimingInfo_Type p_TimingInfo := ?) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cr_NB_IndAspCommonPart_SRB(p_CellId, p_SrbId, p_TimingInfo),
    Signalling := {
      Rrc := {
        Dcch := p_RrcPdu
      },
      Nas := omit
    }
  };
    
  //----------------------------------------------------------------------------
  // SRB1: ASP to receive RRC message (RRCConnectionSetupComplete) + piggybacked NAS message
  
  template (present) NB_SRB_COMMON_IND car_NB_SRB_RrcNasPdu_IND(NBIOT_CellId_Type p_CellId,
                                                                NB_SRB_Identity_Type p_SrbId,
                                                                template (present) UL_DCCH_Message_NB p_RrcPdu,
                                                                template (present) NAS_MSG_Indication_Type p_NasMsg) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cr_NB_IndAspCommonPart_SRB(p_CellId, p_SrbId, ?),
    Signalling := {
      Rrc := {
        Dcch := p_RrcPdu
      },
      Nas := { p_NasMsg }
    }
  };
  
  //----------------------------------------------------------------------------
  // SRB1/2: common ASP to receive NAS message (within RRC ULInformationTransfer)
  
  template (present) NB_SRB_COMMON_IND car_NB_SRB_NasPdu_IND(NBIOT_CellId_Type p_CellId,
                                                             template (present) NB_SRB_Identity_Type p_SrbId,
                                                             template (present) NAS_MSG_Indication_Type p_NasMsg) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cr_NB_IndAspCommonPart_SRB(p_CellId, p_SrbId, ?),
    Signalling := {
      Rrc := omit,
      Nas := { p_NasMsg }
    }
  };
  
  //----------------------------------------------------------------------------
  // SRB1: ASP to receive NAS message (within RRC ULInformationTransfer)
  
  template (present) NB_SRB_COMMON_IND car_NB_SRB1bis_NasPdu_IND(NBIOT_CellId_Type p_CellId,
                                                              template (present) NAS_MSG_Indication_Type p_NasMsg) :=
  /* @status    APPROVED (NBIOT) */
    car_NB_SRB_NasPdu_IND(p_CellId, tsc_SRB1bis, p_NasMsg);
  
  //----------------------------------------------------------------------------
  // ASP to receive any message on the SRB port
  
  template (present) NB_SRB_COMMON_IND car_NB_SRB_IND_Any :=
  {
    Common := cr_NB_IndAspCommonPart_SRB(?, ?),
    Signalling := ?
  };

}
