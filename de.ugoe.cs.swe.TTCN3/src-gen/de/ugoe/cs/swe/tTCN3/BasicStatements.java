/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Statements</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getLog <em>Log</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getBlock <em>Block</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getLoop <em>Loop</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getConditional <em>Conditional</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getSelect <em>Select</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getAssign <em>Assign</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBasicStatements()
 * @model
 * @generated
 */
public interface BasicStatements extends EObject
{
  /**
   * Returns the value of the '<em><b>Log</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Log</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Log</em>' containment reference.
   * @see #setLog(LogStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBasicStatements_Log()
   * @model containment="true"
   * @generated
   */
  LogStatement getLog();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getLog <em>Log</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Log</em>' containment reference.
   * @see #getLog()
   * @generated
   */
  void setLog(LogStatement value);

  /**
   * Returns the value of the '<em><b>Block</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Block</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Block</em>' containment reference.
   * @see #setBlock(StatementBlock)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBasicStatements_Block()
   * @model containment="true"
   * @generated
   */
  StatementBlock getBlock();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getBlock <em>Block</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Block</em>' containment reference.
   * @see #getBlock()
   * @generated
   */
  void setBlock(StatementBlock value);

  /**
   * Returns the value of the '<em><b>Loop</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Loop</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Loop</em>' containment reference.
   * @see #setLoop(LoopConstruct)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBasicStatements_Loop()
   * @model containment="true"
   * @generated
   */
  LoopConstruct getLoop();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getLoop <em>Loop</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Loop</em>' containment reference.
   * @see #getLoop()
   * @generated
   */
  void setLoop(LoopConstruct value);

  /**
   * Returns the value of the '<em><b>Conditional</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Conditional</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Conditional</em>' containment reference.
   * @see #setConditional(ConditionalConstruct)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBasicStatements_Conditional()
   * @model containment="true"
   * @generated
   */
  ConditionalConstruct getConditional();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getConditional <em>Conditional</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Conditional</em>' containment reference.
   * @see #getConditional()
   * @generated
   */
  void setConditional(ConditionalConstruct value);

  /**
   * Returns the value of the '<em><b>Select</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Select</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Select</em>' containment reference.
   * @see #setSelect(SelectCaseConstruct)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBasicStatements_Select()
   * @model containment="true"
   * @generated
   */
  SelectCaseConstruct getSelect();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getSelect <em>Select</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Select</em>' containment reference.
   * @see #getSelect()
   * @generated
   */
  void setSelect(SelectCaseConstruct value);

  /**
   * Returns the value of the '<em><b>Assign</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Assign</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Assign</em>' containment reference.
   * @see #setAssign(Assignment)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBasicStatements_Assign()
   * @model containment="true"
   * @generated
   */
  Assignment getAssign();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BasicStatements#getAssign <em>Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Assign</em>' containment reference.
   * @see #getAssign()
   * @generated
   */
  void setAssign(Assignment value);

} // BasicStatements
