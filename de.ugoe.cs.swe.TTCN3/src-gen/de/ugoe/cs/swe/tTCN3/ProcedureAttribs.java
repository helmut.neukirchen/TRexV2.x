/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure Attribs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ProcedureAttribs#getAddresses <em>Addresses</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ProcedureAttribs#getProcs <em>Procs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ProcedureAttribs#getConfigs <em>Configs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getProcedureAttribs()
 * @model
 * @generated
 */
public interface ProcedureAttribs extends EObject
{
  /**
   * Returns the value of the '<em><b>Addresses</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.AddressDecl}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Addresses</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Addresses</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getProcedureAttribs_Addresses()
   * @model containment="true"
   * @generated
   */
  EList<AddressDecl> getAddresses();

  /**
   * Returns the value of the '<em><b>Procs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ProcedureList}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Procs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Procs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getProcedureAttribs_Procs()
   * @model containment="true"
   * @generated
   */
  EList<ProcedureList> getProcs();

  /**
   * Returns the value of the '<em><b>Configs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ConfigParamDef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Configs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Configs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getProcedureAttribs_Configs()
   * @model containment="true"
   * @generated
   */
  EList<ConfigParamDef> getConfigs();

} // ProcedureAttribs
