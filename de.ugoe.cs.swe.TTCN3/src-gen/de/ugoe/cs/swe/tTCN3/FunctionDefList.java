/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Def List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getLocDef <em>Loc Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getLocInst <em>Loc Inst</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getWs <em>Ws</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDefList()
 * @model
 * @generated
 */
public interface FunctionDefList extends EObject
{
  /**
   * Returns the value of the '<em><b>Loc Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Loc Def</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Loc Def</em>' containment reference.
   * @see #setLocDef(FunctionLocalDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDefList_LocDef()
   * @model containment="true"
   * @generated
   */
  FunctionLocalDef getLocDef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getLocDef <em>Loc Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Loc Def</em>' containment reference.
   * @see #getLocDef()
   * @generated
   */
  void setLocDef(FunctionLocalDef value);

  /**
   * Returns the value of the '<em><b>Loc Inst</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Loc Inst</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Loc Inst</em>' containment reference.
   * @see #setLocInst(FunctionLocalInst)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDefList_LocInst()
   * @model containment="true"
   * @generated
   */
  FunctionLocalInst getLocInst();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getLocInst <em>Loc Inst</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Loc Inst</em>' containment reference.
   * @see #getLocInst()
   * @generated
   */
  void setLocInst(FunctionLocalInst value);

  /**
   * Returns the value of the '<em><b>Ws</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ws</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ws</em>' containment reference.
   * @see #setWs(WithStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDefList_Ws()
   * @model containment="true"
   * @generated
   */
  WithStatement getWs();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getWs <em>Ws</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ws</em>' containment reference.
   * @see #getWs()
   * @generated
   */
  void setWs(WithStatement value);

  /**
   * Returns the value of the '<em><b>Sc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sc</em>' attribute.
   * @see #setSc(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDefList_Sc()
   * @model
   * @generated
   */
  String getSc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDefList#getSc <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sc</em>' attribute.
   * @see #getSc()
   * @generated
   */
  void setSc(String value);

} // FunctionDefList
