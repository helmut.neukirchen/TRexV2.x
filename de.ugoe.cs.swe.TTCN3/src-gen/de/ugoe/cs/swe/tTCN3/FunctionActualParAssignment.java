/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Actual Par Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getComponent <em>Component</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getTimer <em>Timer</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualParAssignment()
 * @model
 * @generated
 */
public interface FunctionActualParAssignment extends EObject
{
  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(TemplateInstanceAssignment)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualParAssignment_Template()
   * @model containment="true"
   * @generated
   */
  TemplateInstanceAssignment getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(TemplateInstanceAssignment value);

  /**
   * Returns the value of the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Component</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Component</em>' containment reference.
   * @see #setComponent(ComponentRefAssignment)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualParAssignment_Component()
   * @model containment="true"
   * @generated
   */
  ComponentRefAssignment getComponent();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getComponent <em>Component</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component</em>' containment reference.
   * @see #getComponent()
   * @generated
   */
  void setComponent(ComponentRefAssignment value);

  /**
   * Returns the value of the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' containment reference.
   * @see #setPort(PortRefAssignment)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualParAssignment_Port()
   * @model containment="true"
   * @generated
   */
  PortRefAssignment getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getPort <em>Port</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' containment reference.
   * @see #getPort()
   * @generated
   */
  void setPort(PortRefAssignment value);

  /**
   * Returns the value of the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' containment reference.
   * @see #setTimer(TimerRefAssignment)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualParAssignment_Timer()
   * @model containment="true"
   * @generated
   */
  TimerRefAssignment getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment#getTimer <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' containment reference.
   * @see #getTimer()
   * @generated
   */
  void setTimer(TimerRefAssignment value);

} // FunctionActualParAssignment
