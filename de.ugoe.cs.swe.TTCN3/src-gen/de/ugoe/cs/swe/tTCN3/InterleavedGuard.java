/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interleaved Guard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.InterleavedGuard#getGuardOp <em>Guard Op</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInterleavedGuard()
 * @model
 * @generated
 */
public interface InterleavedGuard extends EObject
{
  /**
   * Returns the value of the '<em><b>Guard Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Guard Op</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Guard Op</em>' containment reference.
   * @see #setGuardOp(GuardOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInterleavedGuard_GuardOp()
   * @model containment="true"
   * @generated
   */
  GuardOp getGuardOp();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.InterleavedGuard#getGuardOp <em>Guard Op</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Guard Op</em>' containment reference.
   * @see #getGuardOp()
   * @generated
   */
  void setGuardOp(GuardOp value);

} // InterleavedGuard
