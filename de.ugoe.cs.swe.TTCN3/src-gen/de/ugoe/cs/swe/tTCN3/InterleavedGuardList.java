/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interleaved Guard List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.InterleavedGuardList#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInterleavedGuardList()
 * @model
 * @generated
 */
public interface InterleavedGuardList extends InterleavedConstruct
{
  /**
   * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.InterleavedGuardElement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elements</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInterleavedGuardList_Elements()
   * @model containment="true"
   * @generated
   */
  EList<InterleavedGuardElement> getElements();

} // InterleavedGuardList
