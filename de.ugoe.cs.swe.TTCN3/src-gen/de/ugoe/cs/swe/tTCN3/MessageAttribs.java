/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Attribs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MessageAttribs#getAdresses <em>Adresses</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MessageAttribs#getMessages <em>Messages</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MessageAttribs#getConfigs <em>Configs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMessageAttribs()
 * @model
 * @generated
 */
public interface MessageAttribs extends EObject
{
  /**
   * Returns the value of the '<em><b>Adresses</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.AddressDecl}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Adresses</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Adresses</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMessageAttribs_Adresses()
   * @model containment="true"
   * @generated
   */
  EList<AddressDecl> getAdresses();

  /**
   * Returns the value of the '<em><b>Messages</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.MessageList}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Messages</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Messages</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMessageAttribs_Messages()
   * @model containment="true"
   * @generated
   */
  EList<MessageList> getMessages();

  /**
   * Returns the value of the '<em><b>Configs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ConfigParamDef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Configs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Configs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMessageAttribs_Configs()
   * @model containment="true"
   * @generated
   */
  EList<ConfigParamDef> getConfigs();

} // MessageAttribs
