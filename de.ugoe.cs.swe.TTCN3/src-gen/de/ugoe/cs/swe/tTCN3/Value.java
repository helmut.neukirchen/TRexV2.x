/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.Value#getPredef <em>Predef</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.Value#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getValue()
 * @model
 * @generated
 */
public interface Value extends SingleExpression
{
  /**
   * Returns the value of the '<em><b>Predef</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Predef</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Predef</em>' containment reference.
   * @see #setPredef(PredefinedValue)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getValue_Predef()
   * @model containment="true"
   * @generated
   */
  PredefinedValue getPredef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.Value#getPredef <em>Predef</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Predef</em>' containment reference.
   * @see #getPredef()
   * @generated
   */
  void setPredef(PredefinedValue value);

  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(ReferencedValue)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getValue_Ref()
   * @model containment="true"
   * @generated
   */
  ReferencedValue getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.Value#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(ReferencedValue value);

} // Value
