/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Statement Or Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getLocalDef <em>Local Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getLocalInst <em>Local Inst</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getWithstm <em>Withstm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getControl <em>Control</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDef()
 * @model
 * @generated
 */
public interface ControlStatementOrDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Local Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Local Def</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Local Def</em>' containment reference.
   * @see #setLocalDef(FunctionLocalDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDef_LocalDef()
   * @model containment="true"
   * @generated
   */
  FunctionLocalDef getLocalDef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getLocalDef <em>Local Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Local Def</em>' containment reference.
   * @see #getLocalDef()
   * @generated
   */
  void setLocalDef(FunctionLocalDef value);

  /**
   * Returns the value of the '<em><b>Local Inst</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Local Inst</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Local Inst</em>' containment reference.
   * @see #setLocalInst(FunctionLocalInst)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDef_LocalInst()
   * @model containment="true"
   * @generated
   */
  FunctionLocalInst getLocalInst();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getLocalInst <em>Local Inst</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Local Inst</em>' containment reference.
   * @see #getLocalInst()
   * @generated
   */
  void setLocalInst(FunctionLocalInst value);

  /**
   * Returns the value of the '<em><b>Withstm</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Withstm</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Withstm</em>' containment reference.
   * @see #setWithstm(WithStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDef_Withstm()
   * @model containment="true"
   * @generated
   */
  WithStatement getWithstm();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getWithstm <em>Withstm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Withstm</em>' containment reference.
   * @see #getWithstm()
   * @generated
   */
  void setWithstm(WithStatement value);

  /**
   * Returns the value of the '<em><b>Control</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Control</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Control</em>' containment reference.
   * @see #setControl(ControlStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getControlStatementOrDef_Control()
   * @model containment="true"
   * @generated
   */
  ControlStatement getControl();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ControlStatementOrDef#getControl <em>Control</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Control</em>' containment reference.
   * @see #getControl()
   * @generated
   */
  void setControl(ControlStatement value);

} // ControlStatementOrDef
