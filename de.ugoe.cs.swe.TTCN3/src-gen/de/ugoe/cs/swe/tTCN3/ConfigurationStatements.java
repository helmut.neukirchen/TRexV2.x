/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration Statements</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getConnect <em>Connect</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getMap <em>Map</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getDisconnect <em>Disconnect</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getUnmap <em>Unmap</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getDone <em>Done</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getKilled <em>Killed</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getStartTc <em>Start Tc</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getStopTc <em>Stop Tc</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getKillTc <em>Kill Tc</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationStatements()
 * @model
 * @generated
 */
public interface ConfigurationStatements extends EObject
{
  /**
   * Returns the value of the '<em><b>Connect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Connect</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Connect</em>' containment reference.
   * @see #setConnect(ConnectStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationStatements_Connect()
   * @model containment="true"
   * @generated
   */
  ConnectStatement getConnect();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getConnect <em>Connect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Connect</em>' containment reference.
   * @see #getConnect()
   * @generated
   */
  void setConnect(ConnectStatement value);

  /**
   * Returns the value of the '<em><b>Map</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Map</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Map</em>' containment reference.
   * @see #setMap(MapStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationStatements_Map()
   * @model containment="true"
   * @generated
   */
  MapStatement getMap();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getMap <em>Map</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Map</em>' containment reference.
   * @see #getMap()
   * @generated
   */
  void setMap(MapStatement value);

  /**
   * Returns the value of the '<em><b>Disconnect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Disconnect</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Disconnect</em>' containment reference.
   * @see #setDisconnect(DisconnectStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationStatements_Disconnect()
   * @model containment="true"
   * @generated
   */
  DisconnectStatement getDisconnect();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getDisconnect <em>Disconnect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Disconnect</em>' containment reference.
   * @see #getDisconnect()
   * @generated
   */
  void setDisconnect(DisconnectStatement value);

  /**
   * Returns the value of the '<em><b>Unmap</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Unmap</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Unmap</em>' containment reference.
   * @see #setUnmap(UnmapStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationStatements_Unmap()
   * @model containment="true"
   * @generated
   */
  UnmapStatement getUnmap();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getUnmap <em>Unmap</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Unmap</em>' containment reference.
   * @see #getUnmap()
   * @generated
   */
  void setUnmap(UnmapStatement value);

  /**
   * Returns the value of the '<em><b>Done</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Done</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Done</em>' containment reference.
   * @see #setDone(DoneStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationStatements_Done()
   * @model containment="true"
   * @generated
   */
  DoneStatement getDone();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getDone <em>Done</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Done</em>' containment reference.
   * @see #getDone()
   * @generated
   */
  void setDone(DoneStatement value);

  /**
   * Returns the value of the '<em><b>Killed</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Killed</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Killed</em>' containment reference.
   * @see #setKilled(KilledStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationStatements_Killed()
   * @model containment="true"
   * @generated
   */
  KilledStatement getKilled();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getKilled <em>Killed</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Killed</em>' containment reference.
   * @see #getKilled()
   * @generated
   */
  void setKilled(KilledStatement value);

  /**
   * Returns the value of the '<em><b>Start Tc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Start Tc</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Start Tc</em>' containment reference.
   * @see #setStartTc(StartTCStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationStatements_StartTc()
   * @model containment="true"
   * @generated
   */
  StartTCStatement getStartTc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getStartTc <em>Start Tc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Start Tc</em>' containment reference.
   * @see #getStartTc()
   * @generated
   */
  void setStartTc(StartTCStatement value);

  /**
   * Returns the value of the '<em><b>Stop Tc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Stop Tc</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Stop Tc</em>' containment reference.
   * @see #setStopTc(StopTCStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationStatements_StopTc()
   * @model containment="true"
   * @generated
   */
  StopTCStatement getStopTc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getStopTc <em>Stop Tc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Stop Tc</em>' containment reference.
   * @see #getStopTc()
   * @generated
   */
  void setStopTc(StopTCStatement value);

  /**
   * Returns the value of the '<em><b>Kill Tc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Kill Tc</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Kill Tc</em>' containment reference.
   * @see #setKillTc(KillTCStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationStatements_KillTc()
   * @model containment="true"
   * @generated
   */
  KillTCStatement getKillTc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationStatements#getKillTc <em>Kill Tc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Kill Tc</em>' containment reference.
   * @see #getKillTc()
   * @generated
   */
  void setKillTc(KillTCStatement value);

} // ConfigurationStatements
