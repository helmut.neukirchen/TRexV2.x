/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Actual Par List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateActualParList#getActual <em>Actual</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateActualParList#getAssign <em>Assign</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateActualParList()
 * @model
 * @generated
 */
public interface TemplateActualParList extends EObject
{
  /**
   * Returns the value of the '<em><b>Actual</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.TemplateInstanceActualPar}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Actual</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Actual</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateActualParList_Actual()
   * @model containment="true"
   * @generated
   */
  EList<TemplateInstanceActualPar> getActual();

  /**
   * Returns the value of the '<em><b>Assign</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Assign</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Assign</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateActualParList_Assign()
   * @model containment="true"
   * @generated
   */
  EList<TemplateInstanceAssignment> getAssign();

} // TemplateActualParList
