/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Port Par</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FormalPortPar#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalPortPar()
 * @model
 * @generated
 */
public interface FormalPortPar extends FormalPortAndValuePar, RefValue
{
  /**
   * Returns the value of the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' reference.
   * @see #setPort(PortDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalPortPar_Port()
   * @model
   * @generated
   */
  PortDef getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FormalPortPar#getPort <em>Port</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' reference.
   * @see #getPort()
   * @generated
   */
  void setPort(PortDef value);

} // FormalPortPar
