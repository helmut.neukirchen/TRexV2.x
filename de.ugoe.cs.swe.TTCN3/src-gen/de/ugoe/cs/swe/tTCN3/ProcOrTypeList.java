/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Proc Or Type List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ProcOrTypeList#getAll <em>All</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ProcOrTypeList#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getProcOrTypeList()
 * @model
 * @generated
 */
public interface ProcOrTypeList extends EObject
{
  /**
   * Returns the value of the '<em><b>All</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All</em>' attribute.
   * @see #setAll(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getProcOrTypeList_All()
   * @model
   * @generated
   */
  String getAll();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ProcOrTypeList#getAll <em>All</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>All</em>' attribute.
   * @see #getAll()
   * @generated
   */
  void setAll(String value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ProcOrType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getProcOrTypeList_Type()
   * @model containment="true"
   * @generated
   */
  EList<ProcOrType> getType();

} // ProcOrTypeList
