/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionInstance#getRef <em>Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionInstance#getParams <em>Params</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionInstance()
 * @model
 * @generated
 */
public interface FunctionInstance extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' reference.
   * @see #setRef(FunctionRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionInstance_Ref()
   * @model
   * @generated
   */
  FunctionRef getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionInstance#getRef <em>Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' reference.
   * @see #getRef()
   * @generated
   */
  void setRef(FunctionRef value);

  /**
   * Returns the value of the '<em><b>Params</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Params</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Params</em>' containment reference.
   * @see #setParams(FunctionActualParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionInstance_Params()
   * @model containment="true"
   * @generated
   */
  FunctionActualParList getParams();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionInstance#getParams <em>Params</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Params</em>' containment reference.
   * @see #getParams()
   * @generated
   */
  void setParams(FunctionActualParList value);

} // FunctionInstance
