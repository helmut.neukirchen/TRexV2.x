/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AssignmentList#getAssign <em>Assign</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAssignmentList()
 * @model
 * @generated
 */
public interface AssignmentList extends ParamAssignmentList
{
  /**
   * Returns the value of the '<em><b>Assign</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.VariableAssignment}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Assign</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Assign</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAssignmentList_Assign()
   * @model containment="true"
   * @generated
   */
  EList<VariableAssignment> getAssign();

} // AssignmentList
