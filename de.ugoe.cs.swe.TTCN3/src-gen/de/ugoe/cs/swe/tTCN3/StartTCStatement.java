/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Start TC Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StartTCStatement#getRef <em>Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StartTCStatement#getFunction <em>Function</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStartTCStatement()
 * @model
 * @generated
 */
public interface StartTCStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(ComponentOrDefaultReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStartTCStatement_Ref()
   * @model containment="true"
   * @generated
   */
  ComponentOrDefaultReference getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StartTCStatement#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(ComponentOrDefaultReference value);

  /**
   * Returns the value of the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function</em>' containment reference.
   * @see #setFunction(FunctionInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStartTCStatement_Function()
   * @model containment="true"
   * @generated
   */
  FunctionInstance getFunction();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StartTCStatement#getFunction <em>Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Function</em>' containment reference.
   * @see #getFunction()
   * @generated
   */
  void setFunction(FunctionInstance value);

} // StartTCStatement
