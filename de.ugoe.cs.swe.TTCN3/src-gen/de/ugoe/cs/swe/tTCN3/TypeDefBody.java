/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Def Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TypeDefBody#getStructured <em>Structured</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TypeDefBody#getSub <em>Sub</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTypeDefBody()
 * @model
 * @generated
 */
public interface TypeDefBody extends EObject
{
  /**
   * Returns the value of the '<em><b>Structured</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Structured</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Structured</em>' containment reference.
   * @see #setStructured(StructuredTypeDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTypeDefBody_Structured()
   * @model containment="true"
   * @generated
   */
  StructuredTypeDef getStructured();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TypeDefBody#getStructured <em>Structured</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Structured</em>' containment reference.
   * @see #getStructured()
   * @generated
   */
  void setStructured(StructuredTypeDef value);

  /**
   * Returns the value of the '<em><b>Sub</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sub</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sub</em>' containment reference.
   * @see #setSub(SubTypeDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTypeDefBody_Sub()
   * @model containment="true"
   * @generated
   */
  SubTypeDef getSub();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TypeDefBody#getSub <em>Sub</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sub</em>' containment reference.
   * @see #getSub()
   * @generated
   */
  void setSub(SubTypeDef value);

} // TypeDefBody
