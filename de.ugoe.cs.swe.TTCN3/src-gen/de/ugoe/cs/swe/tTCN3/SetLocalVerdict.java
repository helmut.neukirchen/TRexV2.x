/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Local Verdict</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SetLocalVerdict#getExpression <em>Expression</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SetLocalVerdict#getLog <em>Log</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSetLocalVerdict()
 * @model
 * @generated
 */
public interface SetLocalVerdict extends EObject
{
  /**
   * Returns the value of the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression</em>' containment reference.
   * @see #setExpression(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSetLocalVerdict_Expression()
   * @model containment="true"
   * @generated
   */
  SingleExpression getExpression();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SetLocalVerdict#getExpression <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression</em>' containment reference.
   * @see #getExpression()
   * @generated
   */
  void setExpression(SingleExpression value);

  /**
   * Returns the value of the '<em><b>Log</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.LogItem}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Log</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Log</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSetLocalVerdict_Log()
   * @model containment="true"
   * @generated
   */
  EList<LogItem> getLog();

} // SetLocalVerdict
