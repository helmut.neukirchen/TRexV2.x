/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Ref Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortRefAssignment#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortRefAssignment#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRefAssignment()
 * @model
 * @generated
 */
public interface PortRefAssignment extends EObject
{
  /**
   * Returns the value of the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' reference.
   * @see #setPort(FormalPortAndValuePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRefAssignment_Port()
   * @model
   * @generated
   */
  FormalPortAndValuePar getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortRefAssignment#getPort <em>Port</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' reference.
   * @see #getPort()
   * @generated
   */
  void setPort(FormalPortAndValuePar value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(PortRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRefAssignment_Value()
   * @model containment="true"
   * @generated
   */
  PortRef getValue();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortRefAssignment#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(PortRef value);

} // PortRefAssignment
