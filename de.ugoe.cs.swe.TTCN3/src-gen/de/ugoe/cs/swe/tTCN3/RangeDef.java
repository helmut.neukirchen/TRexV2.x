/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RangeDef#getB1 <em>B1</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RangeDef#getB2 <em>B2</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRangeDef()
 * @model
 * @generated
 */
public interface RangeDef extends EObject
{
  /**
   * Returns the value of the '<em><b>B1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>B1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>B1</em>' containment reference.
   * @see #setB1(Bound)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRangeDef_B1()
   * @model containment="true"
   * @generated
   */
  Bound getB1();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RangeDef#getB1 <em>B1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>B1</em>' containment reference.
   * @see #getB1()
   * @generated
   */
  void setB1(Bound value);

  /**
   * Returns the value of the '<em><b>B2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>B2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>B2</em>' containment reference.
   * @see #setB2(Bound)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRangeDef_B2()
   * @model containment="true"
   * @generated
   */
  Bound getB2();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RangeDef#getB2 <em>B2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>B2</em>' containment reference.
   * @see #getB2()
   * @generated
   */
  void setB2(Bound value);

} // RangeDef
