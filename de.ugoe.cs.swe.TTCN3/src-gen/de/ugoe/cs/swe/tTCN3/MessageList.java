/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MessageList#getAllOrTypeList <em>All Or Type List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMessageList()
 * @model
 * @generated
 */
public interface MessageList extends EObject
{
  /**
   * Returns the value of the '<em><b>All Or Type List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All Or Type List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All Or Type List</em>' containment reference.
   * @see #setAllOrTypeList(AllOrTypeList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMessageList_AllOrTypeList()
   * @model containment="true"
   * @generated
   */
  AllOrTypeList getAllOrTypeList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.MessageList#getAllOrTypeList <em>All Or Type List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>All Or Type List</em>' containment reference.
   * @see #getAllOrTypeList()
   * @generated
   */
  void setAllOrTypeList(AllOrTypeList value);

} // MessageList
