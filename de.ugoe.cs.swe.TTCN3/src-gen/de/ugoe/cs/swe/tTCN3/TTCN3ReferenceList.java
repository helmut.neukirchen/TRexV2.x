/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TTCN3ReferenceList#getRefs <em>Refs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTTCN3ReferenceList()
 * @model
 * @generated
 */
public interface TTCN3ReferenceList extends EObject
{
  /**
   * Returns the value of the '<em><b>Refs</b></em>' reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.TTCN3Reference}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Refs</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Refs</em>' reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTTCN3ReferenceList_Refs()
   * @model
   * @generated
   */
  EList<TTCN3Reference> getRefs();

} // TTCN3ReferenceList
