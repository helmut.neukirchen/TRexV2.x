/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Altstep Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getParams <em>Params</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getSpec <em>Spec</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getMtc <em>Mtc</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getSystem <em>System</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getLocal <em>Local</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getGuard <em>Guard</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepDef()
 * @model
 * @generated
 */
public interface AltstepDef extends FunctionRef
{
  /**
   * Returns the value of the '<em><b>Params</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Params</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Params</em>' containment reference.
   * @see #setParams(FunctionFormalParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepDef_Params()
   * @model containment="true"
   * @generated
   */
  FunctionFormalParList getParams();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getParams <em>Params</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Params</em>' containment reference.
   * @see #getParams()
   * @generated
   */
  void setParams(FunctionFormalParList value);

  /**
   * Returns the value of the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spec</em>' containment reference.
   * @see #setSpec(RunsOnSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepDef_Spec()
   * @model containment="true"
   * @generated
   */
  RunsOnSpec getSpec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getSpec <em>Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Spec</em>' containment reference.
   * @see #getSpec()
   * @generated
   */
  void setSpec(RunsOnSpec value);

  /**
   * Returns the value of the '<em><b>Mtc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mtc</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mtc</em>' containment reference.
   * @see #setMtc(MtcSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepDef_Mtc()
   * @model containment="true"
   * @generated
   */
  MtcSpec getMtc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getMtc <em>Mtc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mtc</em>' containment reference.
   * @see #getMtc()
   * @generated
   */
  void setMtc(MtcSpec value);

  /**
   * Returns the value of the '<em><b>System</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>System</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>System</em>' containment reference.
   * @see #setSystem(SystemSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepDef_System()
   * @model containment="true"
   * @generated
   */
  SystemSpec getSystem();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getSystem <em>System</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>System</em>' containment reference.
   * @see #getSystem()
   * @generated
   */
  void setSystem(SystemSpec value);

  /**
   * Returns the value of the '<em><b>Local</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Local</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Local</em>' containment reference.
   * @see #setLocal(AltstepLocalDefList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepDef_Local()
   * @model containment="true"
   * @generated
   */
  AltstepLocalDefList getLocal();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getLocal <em>Local</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Local</em>' containment reference.
   * @see #getLocal()
   * @generated
   */
  void setLocal(AltstepLocalDefList value);

  /**
   * Returns the value of the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Guard</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Guard</em>' containment reference.
   * @see #setGuard(AltGuardList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAltstepDef_Guard()
   * @model containment="true"
   * @generated
   */
  AltGuardList getGuard();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AltstepDef#getGuard <em>Guard</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Guard</em>' containment reference.
   * @see #getGuard()
   * @generated
   */
  void setGuard(AltGuardList value);

} // AltstepDef
