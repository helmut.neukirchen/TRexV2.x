/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Running Timer Op</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RunningTimerOp#getTimerRef <em>Timer Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RunningTimerOp#getIndex <em>Index</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRunningTimerOp()
 * @model
 * @generated
 */
public interface RunningTimerOp extends EObject
{
  /**
   * Returns the value of the '<em><b>Timer Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer Ref</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer Ref</em>' containment reference.
   * @see #setTimerRef(TimerRefOrAny)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRunningTimerOp_TimerRef()
   * @model containment="true"
   * @generated
   */
  TimerRefOrAny getTimerRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RunningTimerOp#getTimerRef <em>Timer Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer Ref</em>' containment reference.
   * @see #getTimerRef()
   * @generated
   */
  void setTimerRef(TimerRefOrAny value);

  /**
   * Returns the value of the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Index</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Index</em>' containment reference.
   * @see #setIndex(IndexAssignment)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRunningTimerOp_Index()
   * @model containment="true"
   * @generated
   */
  IndexAssignment getIndex();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RunningTimerOp#getIndex <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Index</em>' containment reference.
   * @see #getIndex()
   * @generated
   */
  void setIndex(IndexAssignment value);

} // RunningTimerOp
