/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Import Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportElement#getGroup <em>Group</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportElement#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportElement#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportElement#getConst <em>Const</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportElement#getTestcase <em>Testcase</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportElement#getAltstep <em>Altstep</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportElement#getFunction <em>Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportElement#getSignature <em>Signature</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportElement#getModulePar <em>Module Par</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ImportElement#getImportSpec <em>Import Spec</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement()
 * @model
 * @generated
 */
public interface ImportElement extends EObject
{
  /**
   * Returns the value of the '<em><b>Group</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' containment reference.
   * @see #setGroup(ImportGroupSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement_Group()
   * @model containment="true"
   * @generated
   */
  ImportGroupSpec getGroup();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getGroup <em>Group</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Group</em>' containment reference.
   * @see #getGroup()
   * @generated
   */
  void setGroup(ImportGroupSpec value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(ImportTypeDefSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement_Type()
   * @model containment="true"
   * @generated
   */
  ImportTypeDefSpec getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(ImportTypeDefSpec value);

  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(ImportTemplateSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement_Template()
   * @model containment="true"
   * @generated
   */
  ImportTemplateSpec getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(ImportTemplateSpec value);

  /**
   * Returns the value of the '<em><b>Const</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Const</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Const</em>' containment reference.
   * @see #setConst(ImportConstSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement_Const()
   * @model containment="true"
   * @generated
   */
  ImportConstSpec getConst();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getConst <em>Const</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Const</em>' containment reference.
   * @see #getConst()
   * @generated
   */
  void setConst(ImportConstSpec value);

  /**
   * Returns the value of the '<em><b>Testcase</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Testcase</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Testcase</em>' containment reference.
   * @see #setTestcase(ImportTestcaseSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement_Testcase()
   * @model containment="true"
   * @generated
   */
  ImportTestcaseSpec getTestcase();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getTestcase <em>Testcase</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Testcase</em>' containment reference.
   * @see #getTestcase()
   * @generated
   */
  void setTestcase(ImportTestcaseSpec value);

  /**
   * Returns the value of the '<em><b>Altstep</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Altstep</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Altstep</em>' containment reference.
   * @see #setAltstep(ImportAltstepSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement_Altstep()
   * @model containment="true"
   * @generated
   */
  ImportAltstepSpec getAltstep();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getAltstep <em>Altstep</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Altstep</em>' containment reference.
   * @see #getAltstep()
   * @generated
   */
  void setAltstep(ImportAltstepSpec value);

  /**
   * Returns the value of the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function</em>' containment reference.
   * @see #setFunction(ImportFunctionSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement_Function()
   * @model containment="true"
   * @generated
   */
  ImportFunctionSpec getFunction();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getFunction <em>Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Function</em>' containment reference.
   * @see #getFunction()
   * @generated
   */
  void setFunction(ImportFunctionSpec value);

  /**
   * Returns the value of the '<em><b>Signature</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Signature</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Signature</em>' containment reference.
   * @see #setSignature(ImportSignatureSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement_Signature()
   * @model containment="true"
   * @generated
   */
  ImportSignatureSpec getSignature();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getSignature <em>Signature</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Signature</em>' containment reference.
   * @see #getSignature()
   * @generated
   */
  void setSignature(ImportSignatureSpec value);

  /**
   * Returns the value of the '<em><b>Module Par</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Module Par</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Module Par</em>' containment reference.
   * @see #setModulePar(ImportModuleParSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement_ModulePar()
   * @model containment="true"
   * @generated
   */
  ImportModuleParSpec getModulePar();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getModulePar <em>Module Par</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Module Par</em>' containment reference.
   * @see #getModulePar()
   * @generated
   */
  void setModulePar(ImportModuleParSpec value);

  /**
   * Returns the value of the '<em><b>Import Spec</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Import Spec</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Import Spec</em>' attribute.
   * @see #setImportSpec(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getImportElement_ImportSpec()
   * @model
   * @generated
   */
  String getImportSpec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ImportElement#getImportSpec <em>Import Spec</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Import Spec</em>' attribute.
   * @see #getImportSpec()
   * @generated
   */
  void setImportSpec(String value);

} // ImportElement
