/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Call Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortCallBody#getCallBody <em>Call Body</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortCallBody()
 * @model
 * @generated
 */
public interface PortCallBody extends EObject
{
  /**
   * Returns the value of the '<em><b>Call Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Call Body</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Call Body</em>' containment reference.
   * @see #setCallBody(CallBodyStatementList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortCallBody_CallBody()
   * @model containment="true"
   * @generated
   */
  CallBodyStatementList getCallBody();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortCallBody#getCallBody <em>Call Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Call Body</em>' containment reference.
   * @see #getCallBody()
   * @generated
   */
  void setCallBody(CallBodyStatementList value);

} // PortCallBody
