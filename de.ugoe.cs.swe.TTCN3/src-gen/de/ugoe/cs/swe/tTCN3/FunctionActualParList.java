/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Actual Par List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionActualParList#getParams <em>Params</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionActualParList#getAsssign <em>Asssign</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualParList()
 * @model
 * @generated
 */
public interface FunctionActualParList extends EObject
{
  /**
   * Returns the value of the '<em><b>Params</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.FunctionActualPar}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Params</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Params</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualParList_Params()
   * @model containment="true"
   * @generated
   */
  EList<FunctionActualPar> getParams();

  /**
   * Returns the value of the '<em><b>Asssign</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Asssign</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Asssign</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualParList_Asssign()
   * @model containment="true"
   * @generated
   */
  EList<FunctionActualParAssignment> getAsssign();

} // FunctionActualParList
