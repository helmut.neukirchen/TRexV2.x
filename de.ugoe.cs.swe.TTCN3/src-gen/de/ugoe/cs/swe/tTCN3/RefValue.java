/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ref Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRefValue()
 * @model
 * @generated
 */
public interface RefValue extends RefValueHead, RefValueElement, TTCN3Reference
{
} // RefValue
