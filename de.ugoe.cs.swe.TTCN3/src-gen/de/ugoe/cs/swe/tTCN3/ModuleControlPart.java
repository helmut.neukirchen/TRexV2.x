/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module Control Part</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart#getBody <em>Body</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart#getWs <em>Ws</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleControlPart()
 * @model
 * @generated
 */
public interface ModuleControlPart extends EObject
{
  /**
   * Returns the value of the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Body</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Body</em>' containment reference.
   * @see #setBody(ModuleControlBody)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleControlPart_Body()
   * @model containment="true"
   * @generated
   */
  ModuleControlBody getBody();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart#getBody <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Body</em>' containment reference.
   * @see #getBody()
   * @generated
   */
  void setBody(ModuleControlBody value);

  /**
   * Returns the value of the '<em><b>Ws</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ws</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ws</em>' containment reference.
   * @see #setWs(WithStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleControlPart_Ws()
   * @model containment="true"
   * @generated
   */
  WithStatement getWs();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart#getWs <em>Ws</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ws</em>' containment reference.
   * @see #getWs()
   * @generated
   */
  void setWs(WithStatement value);

  /**
   * Returns the value of the '<em><b>Sc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sc</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sc</em>' attribute.
   * @see #setSc(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleControlPart_Sc()
   * @model
   * @generated
   */
  String getSc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ModuleControlPart#getSc <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sc</em>' attribute.
   * @see #getSc()
   * @generated
   */
  void setSc(String value);

} // ModuleControlPart
