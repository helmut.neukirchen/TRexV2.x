/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Line Template</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.InLineTemplate#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.InLineTemplate#getDerived <em>Derived</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.InLineTemplate#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInLineTemplate()
 * @model
 * @generated
 */
public interface InLineTemplate extends TemplateInstanceActualPar
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInLineTemplate_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.InLineTemplate#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Derived</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Derived</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Derived</em>' containment reference.
   * @see #setDerived(DerivedRefWithParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInLineTemplate_Derived()
   * @model containment="true"
   * @generated
   */
  DerivedRefWithParList getDerived();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.InLineTemplate#getDerived <em>Derived</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Derived</em>' containment reference.
   * @see #getDerived()
   * @generated
   */
  void setDerived(DerivedRefWithParList value);

  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(TemplateBody)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInLineTemplate_Template()
   * @model containment="true"
   * @generated
   */
  TemplateBody getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.InLineTemplate#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(TemplateBody value);

} // InLineTemplate
