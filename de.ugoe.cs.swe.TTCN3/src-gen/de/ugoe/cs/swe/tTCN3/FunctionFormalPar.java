/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Formal Par</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getValue <em>Value</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionFormalPar()
 * @model
 * @generated
 */
public interface FunctionFormalPar extends EObject
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(FormalValuePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionFormalPar_Value()
   * @model containment="true"
   * @generated
   */
  FormalValuePar getValue();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(FormalValuePar value);

  /**
   * Returns the value of the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' containment reference.
   * @see #setTimer(FormalTimerPar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionFormalPar_Timer()
   * @model containment="true"
   * @generated
   */
  FormalTimerPar getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getTimer <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' containment reference.
   * @see #getTimer()
   * @generated
   */
  void setTimer(FormalTimerPar value);

  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(FormalTemplatePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionFormalPar_Template()
   * @model containment="true"
   * @generated
   */
  FormalTemplatePar getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(FormalTemplatePar value);

  /**
   * Returns the value of the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' containment reference.
   * @see #setPort(FormalPortPar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionFormalPar_Port()
   * @model containment="true"
   * @generated
   */
  FormalPortPar getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionFormalPar#getPort <em>Port</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' containment reference.
   * @see #getPort()
   * @generated
   */
  void setPort(FormalPortPar value);

} // FunctionFormalPar
