/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attrib Qualifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AttribQualifier#getList <em>List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAttribQualifier()
 * @model
 * @generated
 */
public interface AttribQualifier extends EObject
{
  /**
   * Returns the value of the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List</em>' containment reference.
   * @see #setList(DefOrFieldRefList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAttribQualifier_List()
   * @model containment="true"
   * @generated
   */
  DefOrFieldRefList getList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AttribQualifier#getList <em>List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>List</em>' containment reference.
   * @see #getList()
   * @generated
   */
  void setList(DefOrFieldRefList value);

} // AttribQualifier
