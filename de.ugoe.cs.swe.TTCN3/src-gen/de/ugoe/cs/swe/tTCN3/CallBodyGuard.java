/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Body Guard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CallBodyGuard#getGuard <em>Guard</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CallBodyGuard#getOps <em>Ops</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCallBodyGuard()
 * @model
 * @generated
 */
public interface CallBodyGuard extends EObject
{
  /**
   * Returns the value of the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Guard</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Guard</em>' containment reference.
   * @see #setGuard(AltGuardChar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCallBodyGuard_Guard()
   * @model containment="true"
   * @generated
   */
  AltGuardChar getGuard();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CallBodyGuard#getGuard <em>Guard</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Guard</em>' containment reference.
   * @see #getGuard()
   * @generated
   */
  void setGuard(AltGuardChar value);

  /**
   * Returns the value of the '<em><b>Ops</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ops</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ops</em>' containment reference.
   * @see #setOps(CallBodyOps)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCallBodyGuard_Ops()
   * @model containment="true"
   * @generated
   */
  CallBodyOps getOps();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CallBodyGuard#getOps <em>Ops</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ops</em>' containment reference.
   * @see #getOps()
   * @generated
   */
  void setOps(CallBodyOps value);

} // CallBodyGuard
