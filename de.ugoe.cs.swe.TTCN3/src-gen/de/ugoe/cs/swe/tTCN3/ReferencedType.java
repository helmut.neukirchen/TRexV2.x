/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referenced Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getReferencedType()
 * @model
 * @generated
 */
public interface ReferencedType extends TypeReferenceTailType, TTCN3Reference
{
} // ReferencedType
