/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create Op</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CreateOp#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CreateOp#getExpr1 <em>Expr1</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CreateOp#getExpr2 <em>Expr2</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCreateOp()
 * @model
 * @generated
 */
public interface CreateOp extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' reference.
   * @see #setType(ComponentDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCreateOp_Type()
   * @model
   * @generated
   */
  ComponentDef getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CreateOp#getType <em>Type</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' reference.
   * @see #getType()
   * @generated
   */
  void setType(ComponentDef value);

  /**
   * Returns the value of the '<em><b>Expr1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr1</em>' containment reference.
   * @see #setExpr1(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCreateOp_Expr1()
   * @model containment="true"
   * @generated
   */
  SingleExpression getExpr1();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CreateOp#getExpr1 <em>Expr1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expr1</em>' containment reference.
   * @see #getExpr1()
   * @generated
   */
  void setExpr1(SingleExpression value);

  /**
   * Returns the value of the '<em><b>Expr2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr2</em>' containment reference.
   * @see #setExpr2(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCreateOp_Expr2()
   * @model containment="true"
   * @generated
   */
  SingleExpression getExpr2();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CreateOp#getExpr2 <em>Expr2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expr2</em>' containment reference.
   * @see #getExpr2()
   * @generated
   */
  void setExpr2(SingleExpression value);

} // CreateOp
