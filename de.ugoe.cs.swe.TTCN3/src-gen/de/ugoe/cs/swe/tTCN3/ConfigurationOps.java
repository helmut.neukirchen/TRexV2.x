/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration Ops</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps#getCreate <em>Create</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps#getAlive <em>Alive</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps#getRunning <em>Running</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationOps()
 * @model
 * @generated
 */
public interface ConfigurationOps extends EObject
{
  /**
   * Returns the value of the '<em><b>Create</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Create</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Create</em>' containment reference.
   * @see #setCreate(CreateOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationOps_Create()
   * @model containment="true"
   * @generated
   */
  CreateOp getCreate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps#getCreate <em>Create</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Create</em>' containment reference.
   * @see #getCreate()
   * @generated
   */
  void setCreate(CreateOp value);

  /**
   * Returns the value of the '<em><b>Alive</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Alive</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Alive</em>' containment reference.
   * @see #setAlive(AliveOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationOps_Alive()
   * @model containment="true"
   * @generated
   */
  AliveOp getAlive();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps#getAlive <em>Alive</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Alive</em>' containment reference.
   * @see #getAlive()
   * @generated
   */
  void setAlive(AliveOp value);

  /**
   * Returns the value of the '<em><b>Running</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Running</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Running</em>' containment reference.
   * @see #setRunning(RunningOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigurationOps_Running()
   * @model containment="true"
   * @generated
   */
  RunningOp getRunning();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigurationOps#getRunning <em>Running</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Running</em>' containment reference.
   * @see #getRunning()
   * @generated
   */
  void setRunning(RunningOp value);

} // ConfigurationOps
