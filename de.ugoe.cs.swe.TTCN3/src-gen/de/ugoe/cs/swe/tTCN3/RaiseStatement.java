/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Raise Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RaiseStatement#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RaiseStatement#getArrayRefs <em>Array Refs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RaiseStatement#getOp <em>Op</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRaiseStatement()
 * @model
 * @generated
 */
public interface RaiseStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Port</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' reference.
   * @see #setPort(FormalPortAndValuePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRaiseStatement_Port()
   * @model
   * @generated
   */
  FormalPortAndValuePar getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RaiseStatement#getPort <em>Port</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' reference.
   * @see #getPort()
   * @generated
   */
  void setPort(FormalPortAndValuePar value);

  /**
   * Returns the value of the '<em><b>Array Refs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ArrayOrBitRef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array Refs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array Refs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRaiseStatement_ArrayRefs()
   * @model containment="true"
   * @generated
   */
  EList<ArrayOrBitRef> getArrayRefs();

  /**
   * Returns the value of the '<em><b>Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' containment reference.
   * @see #setOp(PortRaiseOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRaiseStatement_Op()
   * @model containment="true"
   * @generated
   */
  PortRaiseOp getOp();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RaiseStatement#getOp <em>Op</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Op</em>' containment reference.
   * @see #getOp()
   * @generated
   */
  void setOp(PortRaiseOp value);

} // RaiseStatement
