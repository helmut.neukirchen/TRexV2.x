/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Start Timer Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StartTimerStatement#getRef <em>Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StartTimerStatement#getArryRefs <em>Arry Refs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StartTimerStatement#getExpr <em>Expr</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStartTimerStatement()
 * @model
 * @generated
 */
public interface StartTimerStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' reference.
   * @see #setRef(TimerVarInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStartTimerStatement_Ref()
   * @model
   * @generated
   */
  TimerVarInstance getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StartTimerStatement#getRef <em>Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' reference.
   * @see #getRef()
   * @generated
   */
  void setRef(TimerVarInstance value);

  /**
   * Returns the value of the '<em><b>Arry Refs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ArrayOrBitRef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arry Refs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arry Refs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStartTimerStatement_ArryRefs()
   * @model containment="true"
   * @generated
   */
  EList<ArrayOrBitRef> getArryRefs();

  /**
   * Returns the value of the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr</em>' containment reference.
   * @see #setExpr(Expression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStartTimerStatement_Expr()
   * @model containment="true"
   * @generated
   */
  Expression getExpr();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StartTimerStatement#getExpr <em>Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expr</em>' containment reference.
   * @see #getExpr()
   * @generated
   */
  void setExpr(Expression value);

} // StartTimerStatement
