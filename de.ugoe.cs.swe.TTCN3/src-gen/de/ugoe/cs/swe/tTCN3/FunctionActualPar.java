/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Actual Par</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getComponent <em>Component</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualPar()
 * @model
 * @generated
 */
public interface FunctionActualPar extends EObject
{
  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(InLineTemplate)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualPar_Template()
   * @model containment="true"
   * @generated
   */
  InLineTemplate getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(InLineTemplate value);

  /**
   * Returns the value of the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Component</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Component</em>' containment reference.
   * @see #setComponent(ComponentRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualPar_Component()
   * @model containment="true"
   * @generated
   */
  ComponentRef getComponent();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getComponent <em>Component</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component</em>' containment reference.
   * @see #getComponent()
   * @generated
   */
  void setComponent(ComponentRef value);

  /**
   * Returns the value of the '<em><b>Timer</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' reference.
   * @see #setTimer(TimerVarInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualPar_Timer()
   * @model
   * @generated
   */
  TimerVarInstance getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getTimer <em>Timer</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' reference.
   * @see #getTimer()
   * @generated
   */
  void setTimer(TimerVarInstance value);

  /**
   * Returns the value of the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' containment reference.
   * @see #setPort(PortRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionActualPar_Port()
   * @model containment="true"
   * @generated
   */
  PortRef getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionActualPar#getPort <em>Port</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' containment reference.
   * @see #getPort()
   * @generated
   */
  void setPort(PortRef value);

} // FunctionActualPar
