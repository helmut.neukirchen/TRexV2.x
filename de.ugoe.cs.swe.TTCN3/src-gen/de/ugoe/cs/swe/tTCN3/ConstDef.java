/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Const Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConstDef#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConstDef#getDefs <em>Defs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConstDef()
 * @model
 * @generated
 */
public interface ConstDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConstDef_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConstDef#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Defs</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Defs</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Defs</em>' containment reference.
   * @see #setDefs(ConstList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConstDef_Defs()
   * @model containment="true"
   * @generated
   */
  ConstList getDefs();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConstDef#getDefs <em>Defs</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Defs</em>' containment reference.
   * @see #getDefs()
   * @generated
   */
  void setDefs(ConstList value);

} // ConstDef
