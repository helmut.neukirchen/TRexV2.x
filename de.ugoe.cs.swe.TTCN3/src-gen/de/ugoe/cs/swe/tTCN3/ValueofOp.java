/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Valueof Op</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getValueofOp()
 * @model
 * @generated
 */
public interface ValueofOp extends TemplateOps
{
} // ValueofOp
