/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Template Par</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getInOut <em>In Out</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getRestriction <em>Restriction</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getMod <em>Mod</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getTempl <em>Templ</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalTemplatePar()
 * @model
 * @generated
 */
public interface FormalTemplatePar extends RefValue
{
  /**
   * Returns the value of the '<em><b>In Out</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>In Out</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>In Out</em>' attribute.
   * @see #setInOut(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalTemplatePar_InOut()
   * @model
   * @generated
   */
  String getInOut();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getInOut <em>In Out</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>In Out</em>' attribute.
   * @see #getInOut()
   * @generated
   */
  void setInOut(String value);

  /**
   * Returns the value of the '<em><b>Restriction</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Restriction</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Restriction</em>' containment reference.
   * @see #setRestriction(RestrictedTemplate)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalTemplatePar_Restriction()
   * @model containment="true"
   * @generated
   */
  RestrictedTemplate getRestriction();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getRestriction <em>Restriction</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Restriction</em>' containment reference.
   * @see #getRestriction()
   * @generated
   */
  void setRestriction(RestrictedTemplate value);

  /**
   * Returns the value of the '<em><b>Mod</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mod</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mod</em>' attribute.
   * @see #setMod(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalTemplatePar_Mod()
   * @model
   * @generated
   */
  String getMod();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getMod <em>Mod</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mod</em>' attribute.
   * @see #getMod()
   * @generated
   */
  void setMod(String value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalTemplatePar_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Templ</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Templ</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Templ</em>' containment reference.
   * @see #setTempl(InLineTemplate)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalTemplatePar_Templ()
   * @model containment="true"
   * @generated
   */
  InLineTemplate getTempl();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FormalTemplatePar#getTempl <em>Templ</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Templ</em>' containment reference.
   * @see #getTempl()
   * @generated
   */
  void setTempl(InLineTemplate value);

} // FormalTemplatePar
