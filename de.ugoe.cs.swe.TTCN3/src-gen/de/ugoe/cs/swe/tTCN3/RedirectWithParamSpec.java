/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Redirect With Param Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getParam <em>Param</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getSender <em>Sender</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getIndex <em>Index</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectWithParamSpec()
 * @model
 * @generated
 */
public interface RedirectWithParamSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Param</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Param</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Param</em>' containment reference.
   * @see #setParam(ParamSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectWithParamSpec_Param()
   * @model containment="true"
   * @generated
   */
  ParamSpec getParam();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getParam <em>Param</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Param</em>' containment reference.
   * @see #getParam()
   * @generated
   */
  void setParam(ParamSpec value);

  /**
   * Returns the value of the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sender</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sender</em>' containment reference.
   * @see #setSender(SenderSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectWithParamSpec_Sender()
   * @model containment="true"
   * @generated
   */
  SenderSpec getSender();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getSender <em>Sender</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sender</em>' containment reference.
   * @see #getSender()
   * @generated
   */
  void setSender(SenderSpec value);

  /**
   * Returns the value of the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Index</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Index</em>' containment reference.
   * @see #setIndex(IndexSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRedirectWithParamSpec_Index()
   * @model containment="true"
   * @generated
   */
  IndexSpec getIndex();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec#getIndex <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Index</em>' containment reference.
   * @see #getIndex()
   * @generated
   */
  void setIndex(IndexSpec value);

} // RedirectWithParamSpec
