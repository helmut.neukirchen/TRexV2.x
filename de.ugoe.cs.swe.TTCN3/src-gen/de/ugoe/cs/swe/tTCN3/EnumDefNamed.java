/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Def Named</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getEnumDefNamed()
 * @model
 * @generated
 */
public interface EnumDefNamed extends ReferencedType, EnumDef
{
} // EnumDefNamed
