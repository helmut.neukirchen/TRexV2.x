/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Get Reply Op</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getValue <em>Value</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getRedirect <em>Redirect</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortGetReplyOp()
 * @model
 * @generated
 */
public interface PortGetReplyOp extends CheckPortOpsPresent
{
  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(InLineTemplate)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortGetReplyOp_Template()
   * @model containment="true"
   * @generated
   */
  InLineTemplate getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(InLineTemplate value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(ValueMatchSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortGetReplyOp_Value()
   * @model containment="true"
   * @generated
   */
  ValueMatchSpec getValue();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(ValueMatchSpec value);

  /**
   * Returns the value of the '<em><b>Redirect</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Redirect</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Redirect</em>' containment reference.
   * @see #setRedirect(PortRedirectWithValueAndParam)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortGetReplyOp_Redirect()
   * @model containment="true"
   * @generated
   */
  PortRedirectWithValueAndParam getRedirect();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortGetReplyOp#getRedirect <em>Redirect</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Redirect</em>' containment reference.
   * @see #getRedirect()
   * @generated
   */
  void setRedirect(PortRedirectWithValueAndParam value);

} // PortGetReplyOp
