/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Reference Tail Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTypeReferenceTailType()
 * @model
 * @generated
 */
public interface TypeReferenceTailType extends EObject
{
} // TypeReferenceTailType
