/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Instance Actual Par</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateInstanceActualPar()
 * @model
 * @generated
 */
public interface TemplateInstanceActualPar extends EObject
{
} // TemplateInstanceActualPar
