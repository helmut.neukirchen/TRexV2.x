/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Label Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getLabelStatement()
 * @model
 * @generated
 */
public interface LabelStatement extends TTCN3Reference
{
} // LabelStatement
