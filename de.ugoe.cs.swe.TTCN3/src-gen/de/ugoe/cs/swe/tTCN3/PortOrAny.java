/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Or Any</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getReply <em>Reply</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getCheck <em>Check</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getCatch <em>Catch</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getCall <em>Call</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getRef <em>Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getArray <em>Array</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getVariable <em>Variable</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAny()
 * @model
 * @generated
 */
public interface PortOrAny extends GetReplyStatement, CheckStatement, CatchStatement, GetCallStatement, TriggerStatement
{
  /**
   * Returns the value of the '<em><b>Reply</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reply</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reply</em>' containment reference.
   * @see #setReply(PortGetReplyOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAny_Reply()
   * @model containment="true"
   * @generated
   */
  PortGetReplyOp getReply();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getReply <em>Reply</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reply</em>' containment reference.
   * @see #getReply()
   * @generated
   */
  void setReply(PortGetReplyOp value);

  /**
   * Returns the value of the '<em><b>Check</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Check</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Check</em>' containment reference.
   * @see #setCheck(PortCheckOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAny_Check()
   * @model containment="true"
   * @generated
   */
  PortCheckOp getCheck();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getCheck <em>Check</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Check</em>' containment reference.
   * @see #getCheck()
   * @generated
   */
  void setCheck(PortCheckOp value);

  /**
   * Returns the value of the '<em><b>Catch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Catch</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Catch</em>' containment reference.
   * @see #setCatch(PortCatchOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAny_Catch()
   * @model containment="true"
   * @generated
   */
  PortCatchOp getCatch();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getCatch <em>Catch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Catch</em>' containment reference.
   * @see #getCatch()
   * @generated
   */
  void setCatch(PortCatchOp value);

  /**
   * Returns the value of the '<em><b>Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Call</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Call</em>' containment reference.
   * @see #setCall(PortGetCallOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAny_Call()
   * @model containment="true"
   * @generated
   */
  PortGetCallOp getCall();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getCall <em>Call</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Call</em>' containment reference.
   * @see #getCall()
   * @generated
   */
  void setCall(PortGetCallOp value);

  /**
   * Returns the value of the '<em><b>Trigger</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Trigger</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Trigger</em>' containment reference.
   * @see #setTrigger(PortTriggerOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAny_Trigger()
   * @model containment="true"
   * @generated
   */
  PortTriggerOp getTrigger();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getTrigger <em>Trigger</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Trigger</em>' containment reference.
   * @see #getTrigger()
   * @generated
   */
  void setTrigger(PortTriggerOp value);

  /**
   * Returns the value of the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' reference.
   * @see #setRef(FormalPortAndValuePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAny_Ref()
   * @model
   * @generated
   */
  FormalPortAndValuePar getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getRef <em>Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' reference.
   * @see #getRef()
   * @generated
   */
  void setRef(FormalPortAndValuePar value);

  /**
   * Returns the value of the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array</em>' containment reference.
   * @see #setArray(ArrayOrBitRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAny_Array()
   * @model containment="true"
   * @generated
   */
  ArrayOrBitRef getArray();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getArray <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Array</em>' containment reference.
   * @see #getArray()
   * @generated
   */
  void setArray(ArrayOrBitRef value);

  /**
   * Returns the value of the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable</em>' containment reference.
   * @see #setVariable(VariableRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortOrAny_Variable()
   * @model containment="true"
   * @generated
   */
  VariableRef getVariable();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortOrAny#getVariable <em>Variable</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable</em>' containment reference.
   * @see #getVariable()
   * @generated
   */
  void setVariable(VariableRef value);

} // PortOrAny
