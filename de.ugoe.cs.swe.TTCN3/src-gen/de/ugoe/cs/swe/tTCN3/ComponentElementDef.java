/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Element Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getVariable <em>Variable</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getConst <em>Const</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentElementDef()
 * @model
 * @generated
 */
public interface ComponentElementDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' containment reference.
   * @see #setPort(PortInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentElementDef_Port()
   * @model containment="true"
   * @generated
   */
  PortInstance getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getPort <em>Port</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' containment reference.
   * @see #getPort()
   * @generated
   */
  void setPort(PortInstance value);

  /**
   * Returns the value of the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable</em>' containment reference.
   * @see #setVariable(VarInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentElementDef_Variable()
   * @model containment="true"
   * @generated
   */
  VarInstance getVariable();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getVariable <em>Variable</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable</em>' containment reference.
   * @see #getVariable()
   * @generated
   */
  void setVariable(VarInstance value);

  /**
   * Returns the value of the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' containment reference.
   * @see #setTimer(TimerInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentElementDef_Timer()
   * @model containment="true"
   * @generated
   */
  TimerInstance getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getTimer <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' containment reference.
   * @see #getTimer()
   * @generated
   */
  void setTimer(TimerInstance value);

  /**
   * Returns the value of the '<em><b>Const</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Const</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Const</em>' containment reference.
   * @see #setConst(ConstDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentElementDef_Const()
   * @model containment="true"
   * @generated
   */
  ConstDef getConst();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getConst <em>Const</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Const</em>' containment reference.
   * @see #getConst()
   * @generated
   */
  void setConst(ConstDef value);

  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(TemplateDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentElementDef_Template()
   * @model containment="true"
   * @generated
   */
  TemplateDef getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentElementDef#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(TemplateDef value);

} // ComponentElementDef
