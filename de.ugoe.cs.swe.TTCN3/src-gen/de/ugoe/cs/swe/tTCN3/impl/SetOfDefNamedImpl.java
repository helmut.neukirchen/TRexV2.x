/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.NestedTypeDef;
import de.ugoe.cs.swe.tTCN3.SetOfDef;
import de.ugoe.cs.swe.tTCN3.SetOfDefNamed;
import de.ugoe.cs.swe.tTCN3.StringLength;
import de.ugoe.cs.swe.tTCN3.SubTypeSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.Type;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Set Of Def Named</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SetOfDefNamedImpl#getSetLength <em>Set Length</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SetOfDefNamedImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SetOfDefNamedImpl#getNested <em>Nested</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SetOfDefNamedImpl#getSetSpec <em>Set Spec</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SetOfDefNamedImpl extends ReferencedTypeImpl implements SetOfDefNamed
{
  /**
   * The cached value of the '{@link #getSetLength() <em>Set Length</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSetLength()
   * @generated
   * @ordered
   */
  protected StringLength setLength;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected Type type;

  /**
   * The cached value of the '{@link #getNested() <em>Nested</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNested()
   * @generated
   * @ordered
   */
  protected NestedTypeDef nested;

  /**
   * The cached value of the '{@link #getSetSpec() <em>Set Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSetSpec()
   * @generated
   * @ordered
   */
  protected SubTypeSpec setSpec;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SetOfDefNamedImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getSetOfDefNamed();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StringLength getSetLength()
  {
    return setLength;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSetLength(StringLength newSetLength, NotificationChain msgs)
  {
    StringLength oldSetLength = setLength;
    setLength = newSetLength;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH, oldSetLength, newSetLength);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSetLength(StringLength newSetLength)
  {
    if (newSetLength != setLength)
    {
      NotificationChain msgs = null;
      if (setLength != null)
        msgs = ((InternalEObject)setLength).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH, null, msgs);
      if (newSetLength != null)
        msgs = ((InternalEObject)newSetLength).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH, null, msgs);
      msgs = basicSetSetLength(newSetLength, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH, newSetLength, newSetLength));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(Type newType, NotificationChain msgs)
  {
    Type oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SET_OF_DEF_NAMED__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(Type newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SET_OF_DEF_NAMED__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SET_OF_DEF_NAMED__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SET_OF_DEF_NAMED__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedTypeDef getNested()
  {
    return nested;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNested(NestedTypeDef newNested, NotificationChain msgs)
  {
    NestedTypeDef oldNested = nested;
    nested = newNested;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SET_OF_DEF_NAMED__NESTED, oldNested, newNested);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNested(NestedTypeDef newNested)
  {
    if (newNested != nested)
    {
      NotificationChain msgs = null;
      if (nested != null)
        msgs = ((InternalEObject)nested).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SET_OF_DEF_NAMED__NESTED, null, msgs);
      if (newNested != null)
        msgs = ((InternalEObject)newNested).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SET_OF_DEF_NAMED__NESTED, null, msgs);
      msgs = basicSetNested(newNested, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SET_OF_DEF_NAMED__NESTED, newNested, newNested));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubTypeSpec getSetSpec()
  {
    return setSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSetSpec(SubTypeSpec newSetSpec, NotificationChain msgs)
  {
    SubTypeSpec oldSetSpec = setSpec;
    setSpec = newSetSpec;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC, oldSetSpec, newSetSpec);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSetSpec(SubTypeSpec newSetSpec)
  {
    if (newSetSpec != setSpec)
    {
      NotificationChain msgs = null;
      if (setSpec != null)
        msgs = ((InternalEObject)setSpec).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC, null, msgs);
      if (newSetSpec != null)
        msgs = ((InternalEObject)newSetSpec).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC, null, msgs);
      msgs = basicSetSetSpec(newSetSpec, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC, newSetSpec, newSetSpec));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH:
        return basicSetSetLength(null, msgs);
      case TTCN3Package.SET_OF_DEF_NAMED__TYPE:
        return basicSetType(null, msgs);
      case TTCN3Package.SET_OF_DEF_NAMED__NESTED:
        return basicSetNested(null, msgs);
      case TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC:
        return basicSetSetSpec(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH:
        return getSetLength();
      case TTCN3Package.SET_OF_DEF_NAMED__TYPE:
        return getType();
      case TTCN3Package.SET_OF_DEF_NAMED__NESTED:
        return getNested();
      case TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC:
        return getSetSpec();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH:
        setSetLength((StringLength)newValue);
        return;
      case TTCN3Package.SET_OF_DEF_NAMED__TYPE:
        setType((Type)newValue);
        return;
      case TTCN3Package.SET_OF_DEF_NAMED__NESTED:
        setNested((NestedTypeDef)newValue);
        return;
      case TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC:
        setSetSpec((SubTypeSpec)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH:
        setSetLength((StringLength)null);
        return;
      case TTCN3Package.SET_OF_DEF_NAMED__TYPE:
        setType((Type)null);
        return;
      case TTCN3Package.SET_OF_DEF_NAMED__NESTED:
        setNested((NestedTypeDef)null);
        return;
      case TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC:
        setSetSpec((SubTypeSpec)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH:
        return setLength != null;
      case TTCN3Package.SET_OF_DEF_NAMED__TYPE:
        return type != null;
      case TTCN3Package.SET_OF_DEF_NAMED__NESTED:
        return nested != null;
      case TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC:
        return setSpec != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass)
  {
    if (baseClass == SetOfDef.class)
    {
      switch (derivedFeatureID)
      {
        case TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH: return TTCN3Package.SET_OF_DEF__SET_LENGTH;
        case TTCN3Package.SET_OF_DEF_NAMED__TYPE: return TTCN3Package.SET_OF_DEF__TYPE;
        case TTCN3Package.SET_OF_DEF_NAMED__NESTED: return TTCN3Package.SET_OF_DEF__NESTED;
        case TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC: return TTCN3Package.SET_OF_DEF__SET_SPEC;
        default: return -1;
      }
    }
    return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass)
  {
    if (baseClass == SetOfDef.class)
    {
      switch (baseFeatureID)
      {
        case TTCN3Package.SET_OF_DEF__SET_LENGTH: return TTCN3Package.SET_OF_DEF_NAMED__SET_LENGTH;
        case TTCN3Package.SET_OF_DEF__TYPE: return TTCN3Package.SET_OF_DEF_NAMED__TYPE;
        case TTCN3Package.SET_OF_DEF__NESTED: return TTCN3Package.SET_OF_DEF_NAMED__NESTED;
        case TTCN3Package.SET_OF_DEF__SET_SPEC: return TTCN3Package.SET_OF_DEF_NAMED__SET_SPEC;
        default: return -1;
      }
    }
    return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
  }

} //SetOfDefNamedImpl
