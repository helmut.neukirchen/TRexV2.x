/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ComponentDef;
import de.ugoe.cs.swe.tTCN3.ComponentDefList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentDefImpl#getExtends <em>Extends</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ComponentDefImpl#getDefs <em>Defs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentDefImpl extends ReferencedTypeImpl implements ComponentDef
{
  /**
   * The cached value of the '{@link #getExtends() <em>Extends</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtends()
   * @generated
   * @ordered
   */
  protected EList<ComponentDef> extends_;

  /**
   * The cached value of the '{@link #getDefs() <em>Defs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefs()
   * @generated
   * @ordered
   */
  protected EList<ComponentDefList> defs;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComponentDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getComponentDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ComponentDef> getExtends()
  {
    if (extends_ == null)
    {
      extends_ = new EObjectResolvingEList<ComponentDef>(ComponentDef.class, this, TTCN3Package.COMPONENT_DEF__EXTENDS);
    }
    return extends_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ComponentDefList> getDefs()
  {
    if (defs == null)
    {
      defs = new EObjectContainmentEList<ComponentDefList>(ComponentDefList.class, this, TTCN3Package.COMPONENT_DEF__DEFS);
    }
    return defs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_DEF__DEFS:
        return ((InternalEList<?>)getDefs()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_DEF__EXTENDS:
        return getExtends();
      case TTCN3Package.COMPONENT_DEF__DEFS:
        return getDefs();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_DEF__EXTENDS:
        getExtends().clear();
        getExtends().addAll((Collection<? extends ComponentDef>)newValue);
        return;
      case TTCN3Package.COMPONENT_DEF__DEFS:
        getDefs().clear();
        getDefs().addAll((Collection<? extends ComponentDefList>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_DEF__EXTENDS:
        getExtends().clear();
        return;
      case TTCN3Package.COMPONENT_DEF__DEFS:
        getDefs().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.COMPONENT_DEF__EXTENDS:
        return extends_ != null && !extends_.isEmpty();
      case TTCN3Package.COMPONENT_DEF__DEFS:
        return defs != null && !defs.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ComponentDefImpl
