/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.FunctionStatement;
import de.ugoe.cs.swe.tTCN3.FunctionStatementList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Statement List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementListImpl#getStatements <em>Statements</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementListImpl#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionStatementListImpl extends MinimalEObjectImpl.Container implements FunctionStatementList
{
  /**
   * The cached value of the '{@link #getStatements() <em>Statements</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatements()
   * @generated
   * @ordered
   */
  protected EList<FunctionStatement> statements;

  /**
   * The cached value of the '{@link #getSc() <em>Sc</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected EList<String> sc;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionStatementListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFunctionStatementList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FunctionStatement> getStatements()
  {
    if (statements == null)
    {
      statements = new EObjectContainmentEList<FunctionStatement>(FunctionStatement.class, this, TTCN3Package.FUNCTION_STATEMENT_LIST__STATEMENTS);
    }
    return statements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getSc()
  {
    if (sc == null)
    {
      sc = new EDataTypeEList<String>(String.class, this, TTCN3Package.FUNCTION_STATEMENT_LIST__SC);
    }
    return sc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_STATEMENT_LIST__STATEMENTS:
        return ((InternalEList<?>)getStatements()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_STATEMENT_LIST__STATEMENTS:
        return getStatements();
      case TTCN3Package.FUNCTION_STATEMENT_LIST__SC:
        return getSc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_STATEMENT_LIST__STATEMENTS:
        getStatements().clear();
        getStatements().addAll((Collection<? extends FunctionStatement>)newValue);
        return;
      case TTCN3Package.FUNCTION_STATEMENT_LIST__SC:
        getSc().clear();
        getSc().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_STATEMENT_LIST__STATEMENTS:
        getStatements().clear();
        return;
      case TTCN3Package.FUNCTION_STATEMENT_LIST__SC:
        getSc().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_STATEMENT_LIST__STATEMENTS:
        return statements != null && !statements.isEmpty();
      case TTCN3Package.FUNCTION_STATEMENT_LIST__SC:
        return sc != null && !sc.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (sc: ");
    result.append(sc);
    result.append(')');
    return result.toString();
  }

} //FunctionStatementListImpl
