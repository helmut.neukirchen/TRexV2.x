/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.DerivedRefWithParList;
import de.ugoe.cs.swe.tTCN3.InLineTemplate;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateBody;
import de.ugoe.cs.swe.tTCN3.Type;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Line Template</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.InLineTemplateImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.InLineTemplateImpl#getDerived <em>Derived</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.InLineTemplateImpl#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InLineTemplateImpl extends TemplateInstanceActualParImpl implements InLineTemplate
{
  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected Type type;

  /**
   * The cached value of the '{@link #getDerived() <em>Derived</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDerived()
   * @generated
   * @ordered
   */
  protected DerivedRefWithParList derived;

  /**
   * The cached value of the '{@link #getTemplate() <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplate()
   * @generated
   * @ordered
   */
  protected TemplateBody template;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InLineTemplateImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getInLineTemplate();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(Type newType, NotificationChain msgs)
  {
    Type oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IN_LINE_TEMPLATE__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(Type newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IN_LINE_TEMPLATE__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IN_LINE_TEMPLATE__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IN_LINE_TEMPLATE__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DerivedRefWithParList getDerived()
  {
    return derived;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDerived(DerivedRefWithParList newDerived, NotificationChain msgs)
  {
    DerivedRefWithParList oldDerived = derived;
    derived = newDerived;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IN_LINE_TEMPLATE__DERIVED, oldDerived, newDerived);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDerived(DerivedRefWithParList newDerived)
  {
    if (newDerived != derived)
    {
      NotificationChain msgs = null;
      if (derived != null)
        msgs = ((InternalEObject)derived).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IN_LINE_TEMPLATE__DERIVED, null, msgs);
      if (newDerived != null)
        msgs = ((InternalEObject)newDerived).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IN_LINE_TEMPLATE__DERIVED, null, msgs);
      msgs = basicSetDerived(newDerived, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IN_LINE_TEMPLATE__DERIVED, newDerived, newDerived));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateBody getTemplate()
  {
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTemplate(TemplateBody newTemplate, NotificationChain msgs)
  {
    TemplateBody oldTemplate = template;
    template = newTemplate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IN_LINE_TEMPLATE__TEMPLATE, oldTemplate, newTemplate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTemplate(TemplateBody newTemplate)
  {
    if (newTemplate != template)
    {
      NotificationChain msgs = null;
      if (template != null)
        msgs = ((InternalEObject)template).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IN_LINE_TEMPLATE__TEMPLATE, null, msgs);
      if (newTemplate != null)
        msgs = ((InternalEObject)newTemplate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IN_LINE_TEMPLATE__TEMPLATE, null, msgs);
      msgs = basicSetTemplate(newTemplate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IN_LINE_TEMPLATE__TEMPLATE, newTemplate, newTemplate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.IN_LINE_TEMPLATE__TYPE:
        return basicSetType(null, msgs);
      case TTCN3Package.IN_LINE_TEMPLATE__DERIVED:
        return basicSetDerived(null, msgs);
      case TTCN3Package.IN_LINE_TEMPLATE__TEMPLATE:
        return basicSetTemplate(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.IN_LINE_TEMPLATE__TYPE:
        return getType();
      case TTCN3Package.IN_LINE_TEMPLATE__DERIVED:
        return getDerived();
      case TTCN3Package.IN_LINE_TEMPLATE__TEMPLATE:
        return getTemplate();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.IN_LINE_TEMPLATE__TYPE:
        setType((Type)newValue);
        return;
      case TTCN3Package.IN_LINE_TEMPLATE__DERIVED:
        setDerived((DerivedRefWithParList)newValue);
        return;
      case TTCN3Package.IN_LINE_TEMPLATE__TEMPLATE:
        setTemplate((TemplateBody)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.IN_LINE_TEMPLATE__TYPE:
        setType((Type)null);
        return;
      case TTCN3Package.IN_LINE_TEMPLATE__DERIVED:
        setDerived((DerivedRefWithParList)null);
        return;
      case TTCN3Package.IN_LINE_TEMPLATE__TEMPLATE:
        setTemplate((TemplateBody)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.IN_LINE_TEMPLATE__TYPE:
        return type != null;
      case TTCN3Package.IN_LINE_TEMPLATE__DERIVED:
        return derived != null;
      case TTCN3Package.IN_LINE_TEMPLATE__TEMPLATE:
        return template != null;
    }
    return super.eIsSet(featureID);
  }

} //InLineTemplateImpl
