/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.LanguageSpec;
import de.ugoe.cs.swe.tTCN3.ModuleControlPart;
import de.ugoe.cs.swe.tTCN3.ModuleDefinitionsList;
import de.ugoe.cs.swe.tTCN3.TTCN3Module;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.WithStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Module</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TTCN3ModuleImpl#getSpec <em>Spec</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TTCN3ModuleImpl#getDefs <em>Defs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TTCN3ModuleImpl#getControls <em>Controls</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TTCN3ModuleImpl#getWithstm <em>Withstm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TTCN3ModuleImpl#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TTCN3ModuleImpl extends ModuleOrGroupImpl implements TTCN3Module
{
  /**
   * The cached value of the '{@link #getSpec() <em>Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSpec()
   * @generated
   * @ordered
   */
  protected LanguageSpec spec;

  /**
   * The cached value of the '{@link #getDefs() <em>Defs</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefs()
   * @generated
   * @ordered
   */
  protected ModuleDefinitionsList defs;

  /**
   * The cached value of the '{@link #getControls() <em>Controls</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getControls()
   * @generated
   * @ordered
   */
  protected ModuleControlPart controls;

  /**
   * The cached value of the '{@link #getWithstm() <em>Withstm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWithstm()
   * @generated
   * @ordered
   */
  protected WithStatement withstm;

  /**
   * The default value of the '{@link #getSc() <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected static final String SC_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSc() <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected String sc = SC_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TTCN3ModuleImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTTCN3Module();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LanguageSpec getSpec()
  {
    return spec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSpec(LanguageSpec newSpec, NotificationChain msgs)
  {
    LanguageSpec oldSpec = spec;
    spec = newSpec;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TTCN3_MODULE__SPEC, oldSpec, newSpec);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSpec(LanguageSpec newSpec)
  {
    if (newSpec != spec)
    {
      NotificationChain msgs = null;
      if (spec != null)
        msgs = ((InternalEObject)spec).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TTCN3_MODULE__SPEC, null, msgs);
      if (newSpec != null)
        msgs = ((InternalEObject)newSpec).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TTCN3_MODULE__SPEC, null, msgs);
      msgs = basicSetSpec(newSpec, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TTCN3_MODULE__SPEC, newSpec, newSpec));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleDefinitionsList getDefs()
  {
    return defs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDefs(ModuleDefinitionsList newDefs, NotificationChain msgs)
  {
    ModuleDefinitionsList oldDefs = defs;
    defs = newDefs;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TTCN3_MODULE__DEFS, oldDefs, newDefs);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefs(ModuleDefinitionsList newDefs)
  {
    if (newDefs != defs)
    {
      NotificationChain msgs = null;
      if (defs != null)
        msgs = ((InternalEObject)defs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TTCN3_MODULE__DEFS, null, msgs);
      if (newDefs != null)
        msgs = ((InternalEObject)newDefs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TTCN3_MODULE__DEFS, null, msgs);
      msgs = basicSetDefs(newDefs, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TTCN3_MODULE__DEFS, newDefs, newDefs));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleControlPart getControls()
  {
    return controls;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetControls(ModuleControlPart newControls, NotificationChain msgs)
  {
    ModuleControlPart oldControls = controls;
    controls = newControls;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TTCN3_MODULE__CONTROLS, oldControls, newControls);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setControls(ModuleControlPart newControls)
  {
    if (newControls != controls)
    {
      NotificationChain msgs = null;
      if (controls != null)
        msgs = ((InternalEObject)controls).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TTCN3_MODULE__CONTROLS, null, msgs);
      if (newControls != null)
        msgs = ((InternalEObject)newControls).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TTCN3_MODULE__CONTROLS, null, msgs);
      msgs = basicSetControls(newControls, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TTCN3_MODULE__CONTROLS, newControls, newControls));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WithStatement getWithstm()
  {
    return withstm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetWithstm(WithStatement newWithstm, NotificationChain msgs)
  {
    WithStatement oldWithstm = withstm;
    withstm = newWithstm;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TTCN3_MODULE__WITHSTM, oldWithstm, newWithstm);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWithstm(WithStatement newWithstm)
  {
    if (newWithstm != withstm)
    {
      NotificationChain msgs = null;
      if (withstm != null)
        msgs = ((InternalEObject)withstm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TTCN3_MODULE__WITHSTM, null, msgs);
      if (newWithstm != null)
        msgs = ((InternalEObject)newWithstm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TTCN3_MODULE__WITHSTM, null, msgs);
      msgs = basicSetWithstm(newWithstm, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TTCN3_MODULE__WITHSTM, newWithstm, newWithstm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSc()
  {
    return sc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSc(String newSc)
  {
    String oldSc = sc;
    sc = newSc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TTCN3_MODULE__SC, oldSc, sc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.TTCN3_MODULE__SPEC:
        return basicSetSpec(null, msgs);
      case TTCN3Package.TTCN3_MODULE__DEFS:
        return basicSetDefs(null, msgs);
      case TTCN3Package.TTCN3_MODULE__CONTROLS:
        return basicSetControls(null, msgs);
      case TTCN3Package.TTCN3_MODULE__WITHSTM:
        return basicSetWithstm(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TTCN3_MODULE__SPEC:
        return getSpec();
      case TTCN3Package.TTCN3_MODULE__DEFS:
        return getDefs();
      case TTCN3Package.TTCN3_MODULE__CONTROLS:
        return getControls();
      case TTCN3Package.TTCN3_MODULE__WITHSTM:
        return getWithstm();
      case TTCN3Package.TTCN3_MODULE__SC:
        return getSc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TTCN3_MODULE__SPEC:
        setSpec((LanguageSpec)newValue);
        return;
      case TTCN3Package.TTCN3_MODULE__DEFS:
        setDefs((ModuleDefinitionsList)newValue);
        return;
      case TTCN3Package.TTCN3_MODULE__CONTROLS:
        setControls((ModuleControlPart)newValue);
        return;
      case TTCN3Package.TTCN3_MODULE__WITHSTM:
        setWithstm((WithStatement)newValue);
        return;
      case TTCN3Package.TTCN3_MODULE__SC:
        setSc((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TTCN3_MODULE__SPEC:
        setSpec((LanguageSpec)null);
        return;
      case TTCN3Package.TTCN3_MODULE__DEFS:
        setDefs((ModuleDefinitionsList)null);
        return;
      case TTCN3Package.TTCN3_MODULE__CONTROLS:
        setControls((ModuleControlPart)null);
        return;
      case TTCN3Package.TTCN3_MODULE__WITHSTM:
        setWithstm((WithStatement)null);
        return;
      case TTCN3Package.TTCN3_MODULE__SC:
        setSc(SC_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TTCN3_MODULE__SPEC:
        return spec != null;
      case TTCN3Package.TTCN3_MODULE__DEFS:
        return defs != null;
      case TTCN3Package.TTCN3_MODULE__CONTROLS:
        return controls != null;
      case TTCN3Package.TTCN3_MODULE__WITHSTM:
        return withstm != null;
      case TTCN3Package.TTCN3_MODULE__SC:
        return SC_EDEFAULT == null ? sc != null : !SC_EDEFAULT.equals(sc);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (sc: ");
    result.append(sc);
    result.append(')');
    return result.toString();
  }

} //TTCN3ModuleImpl
