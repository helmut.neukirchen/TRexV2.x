/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ArrayOrBitRef;
import de.ugoe.cs.swe.tTCN3.FormalPortAndValuePar;
import de.ugoe.cs.swe.tTCN3.PortSendOp;
import de.ugoe.cs.swe.tTCN3.SendStatement;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Send Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SendStatementImpl#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SendStatementImpl#getArryRefs <em>Arry Refs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SendStatementImpl#getSend <em>Send</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SendStatementImpl extends MinimalEObjectImpl.Container implements SendStatement
{
  /**
   * The cached value of the '{@link #getPort() <em>Port</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPort()
   * @generated
   * @ordered
   */
  protected FormalPortAndValuePar port;

  /**
   * The cached value of the '{@link #getArryRefs() <em>Arry Refs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArryRefs()
   * @generated
   * @ordered
   */
  protected EList<ArrayOrBitRef> arryRefs;

  /**
   * The cached value of the '{@link #getSend() <em>Send</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSend()
   * @generated
   * @ordered
   */
  protected PortSendOp send;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SendStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getSendStatement();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalPortAndValuePar getPort()
  {
    if (port != null && port.eIsProxy())
    {
      InternalEObject oldPort = (InternalEObject)port;
      port = (FormalPortAndValuePar)eResolveProxy(oldPort);
      if (port != oldPort)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, TTCN3Package.SEND_STATEMENT__PORT, oldPort, port));
      }
    }
    return port;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalPortAndValuePar basicGetPort()
  {
    return port;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPort(FormalPortAndValuePar newPort)
  {
    FormalPortAndValuePar oldPort = port;
    port = newPort;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SEND_STATEMENT__PORT, oldPort, port));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ArrayOrBitRef> getArryRefs()
  {
    if (arryRefs == null)
    {
      arryRefs = new EObjectContainmentEList<ArrayOrBitRef>(ArrayOrBitRef.class, this, TTCN3Package.SEND_STATEMENT__ARRY_REFS);
    }
    return arryRefs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortSendOp getSend()
  {
    return send;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSend(PortSendOp newSend, NotificationChain msgs)
  {
    PortSendOp oldSend = send;
    send = newSend;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SEND_STATEMENT__SEND, oldSend, newSend);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSend(PortSendOp newSend)
  {
    if (newSend != send)
    {
      NotificationChain msgs = null;
      if (send != null)
        msgs = ((InternalEObject)send).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SEND_STATEMENT__SEND, null, msgs);
      if (newSend != null)
        msgs = ((InternalEObject)newSend).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SEND_STATEMENT__SEND, null, msgs);
      msgs = basicSetSend(newSend, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SEND_STATEMENT__SEND, newSend, newSend));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.SEND_STATEMENT__ARRY_REFS:
        return ((InternalEList<?>)getArryRefs()).basicRemove(otherEnd, msgs);
      case TTCN3Package.SEND_STATEMENT__SEND:
        return basicSetSend(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.SEND_STATEMENT__PORT:
        if (resolve) return getPort();
        return basicGetPort();
      case TTCN3Package.SEND_STATEMENT__ARRY_REFS:
        return getArryRefs();
      case TTCN3Package.SEND_STATEMENT__SEND:
        return getSend();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.SEND_STATEMENT__PORT:
        setPort((FormalPortAndValuePar)newValue);
        return;
      case TTCN3Package.SEND_STATEMENT__ARRY_REFS:
        getArryRefs().clear();
        getArryRefs().addAll((Collection<? extends ArrayOrBitRef>)newValue);
        return;
      case TTCN3Package.SEND_STATEMENT__SEND:
        setSend((PortSendOp)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SEND_STATEMENT__PORT:
        setPort((FormalPortAndValuePar)null);
        return;
      case TTCN3Package.SEND_STATEMENT__ARRY_REFS:
        getArryRefs().clear();
        return;
      case TTCN3Package.SEND_STATEMENT__SEND:
        setSend((PortSendOp)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SEND_STATEMENT__PORT:
        return port != null;
      case TTCN3Package.SEND_STATEMENT__ARRY_REFS:
        return arryRefs != null && !arryRefs.isEmpty();
      case TTCN3Package.SEND_STATEMENT__SEND:
        return send != null;
    }
    return super.eIsSet(featureID);
  }

} //SendStatementImpl
