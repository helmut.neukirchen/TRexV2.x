/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.InLineTemplate;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TestcaseOperation;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Testcase Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseOperationImpl#getTxt <em>Txt</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseOperationImpl#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestcaseOperationImpl extends MinimalEObjectImpl.Container implements TestcaseOperation
{
  /**
   * The cached value of the '{@link #getTxt() <em>Txt</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTxt()
   * @generated
   * @ordered
   */
  protected EList<String> txt;

  /**
   * The cached value of the '{@link #getTemplate() <em>Template</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplate()
   * @generated
   * @ordered
   */
  protected EList<InLineTemplate> template;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TestcaseOperationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTestcaseOperation();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getTxt()
  {
    if (txt == null)
    {
      txt = new EDataTypeEList<String>(String.class, this, TTCN3Package.TESTCASE_OPERATION__TXT);
    }
    return txt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<InLineTemplate> getTemplate()
  {
    if (template == null)
    {
      template = new EObjectContainmentEList<InLineTemplate>(InLineTemplate.class, this, TTCN3Package.TESTCASE_OPERATION__TEMPLATE);
    }
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_OPERATION__TEMPLATE:
        return ((InternalEList<?>)getTemplate()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_OPERATION__TXT:
        return getTxt();
      case TTCN3Package.TESTCASE_OPERATION__TEMPLATE:
        return getTemplate();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_OPERATION__TXT:
        getTxt().clear();
        getTxt().addAll((Collection<? extends String>)newValue);
        return;
      case TTCN3Package.TESTCASE_OPERATION__TEMPLATE:
        getTemplate().clear();
        getTemplate().addAll((Collection<? extends InLineTemplate>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_OPERATION__TXT:
        getTxt().clear();
        return;
      case TTCN3Package.TESTCASE_OPERATION__TEMPLATE:
        getTemplate().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_OPERATION__TXT:
        return txt != null && !txt.isEmpty();
      case TTCN3Package.TESTCASE_OPERATION__TEMPLATE:
        return template != null && !template.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (txt: ");
    result.append(txt);
    result.append(')');
    return result.toString();
  }

} //TestcaseOperationImpl
