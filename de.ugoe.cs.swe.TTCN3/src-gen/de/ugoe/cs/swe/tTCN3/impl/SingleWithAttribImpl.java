/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AttribQualifier;
import de.ugoe.cs.swe.tTCN3.SingleWithAttrib;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Single With Attrib</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SingleWithAttribImpl#getAttrib <em>Attrib</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SingleWithAttribImpl extends MinimalEObjectImpl.Container implements SingleWithAttrib
{
  /**
   * The cached value of the '{@link #getAttrib() <em>Attrib</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAttrib()
   * @generated
   * @ordered
   */
  protected AttribQualifier attrib;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SingleWithAttribImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getSingleWithAttrib();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AttribQualifier getAttrib()
  {
    return attrib;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAttrib(AttribQualifier newAttrib, NotificationChain msgs)
  {
    AttribQualifier oldAttrib = attrib;
    attrib = newAttrib;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_WITH_ATTRIB__ATTRIB, oldAttrib, newAttrib);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAttrib(AttribQualifier newAttrib)
  {
    if (newAttrib != attrib)
    {
      NotificationChain msgs = null;
      if (attrib != null)
        msgs = ((InternalEObject)attrib).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_WITH_ATTRIB__ATTRIB, null, msgs);
      if (newAttrib != null)
        msgs = ((InternalEObject)newAttrib).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_WITH_ATTRIB__ATTRIB, null, msgs);
      msgs = basicSetAttrib(newAttrib, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_WITH_ATTRIB__ATTRIB, newAttrib, newAttrib));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_WITH_ATTRIB__ATTRIB:
        return basicSetAttrib(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_WITH_ATTRIB__ATTRIB:
        return getAttrib();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_WITH_ATTRIB__ATTRIB:
        setAttrib((AttribQualifier)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_WITH_ATTRIB__ATTRIB:
        setAttrib((AttribQualifier)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_WITH_ATTRIB__ATTRIB:
        return attrib != null;
    }
    return super.eIsSet(featureID);
  }

} //SingleWithAttribImpl
