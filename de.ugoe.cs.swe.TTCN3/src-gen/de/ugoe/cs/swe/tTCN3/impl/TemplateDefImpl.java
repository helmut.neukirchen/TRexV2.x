/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.BaseTemplate;
import de.ugoe.cs.swe.tTCN3.DerivedDef;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateBody;
import de.ugoe.cs.swe.tTCN3.TemplateDef;
import de.ugoe.cs.swe.tTCN3.TemplateRestriction;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateDefImpl#getRestriction <em>Restriction</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateDefImpl#getFuzzy <em>Fuzzy</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateDefImpl#getBase <em>Base</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateDefImpl#getDerived <em>Derived</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateDefImpl#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TemplateDefImpl extends MinimalEObjectImpl.Container implements TemplateDef
{
  /**
   * The cached value of the '{@link #getRestriction() <em>Restriction</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRestriction()
   * @generated
   * @ordered
   */
  protected TemplateRestriction restriction;

  /**
   * The default value of the '{@link #getFuzzy() <em>Fuzzy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFuzzy()
   * @generated
   * @ordered
   */
  protected static final String FUZZY_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFuzzy() <em>Fuzzy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFuzzy()
   * @generated
   * @ordered
   */
  protected String fuzzy = FUZZY_EDEFAULT;

  /**
   * The cached value of the '{@link #getBase() <em>Base</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBase()
   * @generated
   * @ordered
   */
  protected BaseTemplate base;

  /**
   * The cached value of the '{@link #getDerived() <em>Derived</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDerived()
   * @generated
   * @ordered
   */
  protected DerivedDef derived;

  /**
   * The cached value of the '{@link #getBody() <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBody()
   * @generated
   * @ordered
   */
  protected TemplateBody body;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TemplateDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTemplateDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateRestriction getRestriction()
  {
    return restriction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRestriction(TemplateRestriction newRestriction, NotificationChain msgs)
  {
    TemplateRestriction oldRestriction = restriction;
    restriction = newRestriction;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_DEF__RESTRICTION, oldRestriction, newRestriction);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRestriction(TemplateRestriction newRestriction)
  {
    if (newRestriction != restriction)
    {
      NotificationChain msgs = null;
      if (restriction != null)
        msgs = ((InternalEObject)restriction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_DEF__RESTRICTION, null, msgs);
      if (newRestriction != null)
        msgs = ((InternalEObject)newRestriction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_DEF__RESTRICTION, null, msgs);
      msgs = basicSetRestriction(newRestriction, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_DEF__RESTRICTION, newRestriction, newRestriction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFuzzy()
  {
    return fuzzy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFuzzy(String newFuzzy)
  {
    String oldFuzzy = fuzzy;
    fuzzy = newFuzzy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_DEF__FUZZY, oldFuzzy, fuzzy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTemplate getBase()
  {
    return base;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBase(BaseTemplate newBase, NotificationChain msgs)
  {
    BaseTemplate oldBase = base;
    base = newBase;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_DEF__BASE, oldBase, newBase);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBase(BaseTemplate newBase)
  {
    if (newBase != base)
    {
      NotificationChain msgs = null;
      if (base != null)
        msgs = ((InternalEObject)base).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_DEF__BASE, null, msgs);
      if (newBase != null)
        msgs = ((InternalEObject)newBase).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_DEF__BASE, null, msgs);
      msgs = basicSetBase(newBase, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_DEF__BASE, newBase, newBase));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DerivedDef getDerived()
  {
    return derived;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDerived(DerivedDef newDerived, NotificationChain msgs)
  {
    DerivedDef oldDerived = derived;
    derived = newDerived;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_DEF__DERIVED, oldDerived, newDerived);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDerived(DerivedDef newDerived)
  {
    if (newDerived != derived)
    {
      NotificationChain msgs = null;
      if (derived != null)
        msgs = ((InternalEObject)derived).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_DEF__DERIVED, null, msgs);
      if (newDerived != null)
        msgs = ((InternalEObject)newDerived).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_DEF__DERIVED, null, msgs);
      msgs = basicSetDerived(newDerived, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_DEF__DERIVED, newDerived, newDerived));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateBody getBody()
  {
    return body;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBody(TemplateBody newBody, NotificationChain msgs)
  {
    TemplateBody oldBody = body;
    body = newBody;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_DEF__BODY, oldBody, newBody);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBody(TemplateBody newBody)
  {
    if (newBody != body)
    {
      NotificationChain msgs = null;
      if (body != null)
        msgs = ((InternalEObject)body).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_DEF__BODY, null, msgs);
      if (newBody != null)
        msgs = ((InternalEObject)newBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_DEF__BODY, null, msgs);
      msgs = basicSetBody(newBody, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_DEF__BODY, newBody, newBody));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_DEF__RESTRICTION:
        return basicSetRestriction(null, msgs);
      case TTCN3Package.TEMPLATE_DEF__BASE:
        return basicSetBase(null, msgs);
      case TTCN3Package.TEMPLATE_DEF__DERIVED:
        return basicSetDerived(null, msgs);
      case TTCN3Package.TEMPLATE_DEF__BODY:
        return basicSetBody(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_DEF__RESTRICTION:
        return getRestriction();
      case TTCN3Package.TEMPLATE_DEF__FUZZY:
        return getFuzzy();
      case TTCN3Package.TEMPLATE_DEF__BASE:
        return getBase();
      case TTCN3Package.TEMPLATE_DEF__DERIVED:
        return getDerived();
      case TTCN3Package.TEMPLATE_DEF__BODY:
        return getBody();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_DEF__RESTRICTION:
        setRestriction((TemplateRestriction)newValue);
        return;
      case TTCN3Package.TEMPLATE_DEF__FUZZY:
        setFuzzy((String)newValue);
        return;
      case TTCN3Package.TEMPLATE_DEF__BASE:
        setBase((BaseTemplate)newValue);
        return;
      case TTCN3Package.TEMPLATE_DEF__DERIVED:
        setDerived((DerivedDef)newValue);
        return;
      case TTCN3Package.TEMPLATE_DEF__BODY:
        setBody((TemplateBody)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_DEF__RESTRICTION:
        setRestriction((TemplateRestriction)null);
        return;
      case TTCN3Package.TEMPLATE_DEF__FUZZY:
        setFuzzy(FUZZY_EDEFAULT);
        return;
      case TTCN3Package.TEMPLATE_DEF__BASE:
        setBase((BaseTemplate)null);
        return;
      case TTCN3Package.TEMPLATE_DEF__DERIVED:
        setDerived((DerivedDef)null);
        return;
      case TTCN3Package.TEMPLATE_DEF__BODY:
        setBody((TemplateBody)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_DEF__RESTRICTION:
        return restriction != null;
      case TTCN3Package.TEMPLATE_DEF__FUZZY:
        return FUZZY_EDEFAULT == null ? fuzzy != null : !FUZZY_EDEFAULT.equals(fuzzy);
      case TTCN3Package.TEMPLATE_DEF__BASE:
        return base != null;
      case TTCN3Package.TEMPLATE_DEF__DERIVED:
        return derived != null;
      case TTCN3Package.TEMPLATE_DEF__BODY:
        return body != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (fuzzy: ");
    result.append(fuzzy);
    result.append(')');
    return result.toString();
  }

} //TemplateDefImpl
