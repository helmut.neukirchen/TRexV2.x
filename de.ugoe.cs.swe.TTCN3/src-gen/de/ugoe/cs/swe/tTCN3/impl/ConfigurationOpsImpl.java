/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AliveOp;
import de.ugoe.cs.swe.tTCN3.ConfigurationOps;
import de.ugoe.cs.swe.tTCN3.CreateOp;
import de.ugoe.cs.swe.tTCN3.RunningOp;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configuration Ops</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationOpsImpl#getCreate <em>Create</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationOpsImpl#getAlive <em>Alive</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigurationOpsImpl#getRunning <em>Running</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConfigurationOpsImpl extends MinimalEObjectImpl.Container implements ConfigurationOps
{
  /**
   * The cached value of the '{@link #getCreate() <em>Create</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreate()
   * @generated
   * @ordered
   */
  protected CreateOp create;

  /**
   * The cached value of the '{@link #getAlive() <em>Alive</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAlive()
   * @generated
   * @ordered
   */
  protected AliveOp alive;

  /**
   * The cached value of the '{@link #getRunning() <em>Running</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRunning()
   * @generated
   * @ordered
   */
  protected RunningOp running;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ConfigurationOpsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getConfigurationOps();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CreateOp getCreate()
  {
    return create;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCreate(CreateOp newCreate, NotificationChain msgs)
  {
    CreateOp oldCreate = create;
    create = newCreate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_OPS__CREATE, oldCreate, newCreate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreate(CreateOp newCreate)
  {
    if (newCreate != create)
    {
      NotificationChain msgs = null;
      if (create != null)
        msgs = ((InternalEObject)create).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_OPS__CREATE, null, msgs);
      if (newCreate != null)
        msgs = ((InternalEObject)newCreate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_OPS__CREATE, null, msgs);
      msgs = basicSetCreate(newCreate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_OPS__CREATE, newCreate, newCreate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AliveOp getAlive()
  {
    return alive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAlive(AliveOp newAlive, NotificationChain msgs)
  {
    AliveOp oldAlive = alive;
    alive = newAlive;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_OPS__ALIVE, oldAlive, newAlive);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAlive(AliveOp newAlive)
  {
    if (newAlive != alive)
    {
      NotificationChain msgs = null;
      if (alive != null)
        msgs = ((InternalEObject)alive).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_OPS__ALIVE, null, msgs);
      if (newAlive != null)
        msgs = ((InternalEObject)newAlive).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_OPS__ALIVE, null, msgs);
      msgs = basicSetAlive(newAlive, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_OPS__ALIVE, newAlive, newAlive));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RunningOp getRunning()
  {
    return running;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRunning(RunningOp newRunning, NotificationChain msgs)
  {
    RunningOp oldRunning = running;
    running = newRunning;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_OPS__RUNNING, oldRunning, newRunning);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRunning(RunningOp newRunning)
  {
    if (newRunning != running)
    {
      NotificationChain msgs = null;
      if (running != null)
        msgs = ((InternalEObject)running).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_OPS__RUNNING, null, msgs);
      if (newRunning != null)
        msgs = ((InternalEObject)newRunning).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIGURATION_OPS__RUNNING, null, msgs);
      msgs = basicSetRunning(newRunning, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIGURATION_OPS__RUNNING, newRunning, newRunning));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIGURATION_OPS__CREATE:
        return basicSetCreate(null, msgs);
      case TTCN3Package.CONFIGURATION_OPS__ALIVE:
        return basicSetAlive(null, msgs);
      case TTCN3Package.CONFIGURATION_OPS__RUNNING:
        return basicSetRunning(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIGURATION_OPS__CREATE:
        return getCreate();
      case TTCN3Package.CONFIGURATION_OPS__ALIVE:
        return getAlive();
      case TTCN3Package.CONFIGURATION_OPS__RUNNING:
        return getRunning();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIGURATION_OPS__CREATE:
        setCreate((CreateOp)newValue);
        return;
      case TTCN3Package.CONFIGURATION_OPS__ALIVE:
        setAlive((AliveOp)newValue);
        return;
      case TTCN3Package.CONFIGURATION_OPS__RUNNING:
        setRunning((RunningOp)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIGURATION_OPS__CREATE:
        setCreate((CreateOp)null);
        return;
      case TTCN3Package.CONFIGURATION_OPS__ALIVE:
        setAlive((AliveOp)null);
        return;
      case TTCN3Package.CONFIGURATION_OPS__RUNNING:
        setRunning((RunningOp)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIGURATION_OPS__CREATE:
        return create != null;
      case TTCN3Package.CONFIGURATION_OPS__ALIVE:
        return alive != null;
      case TTCN3Package.CONFIGURATION_OPS__RUNNING:
        return running != null;
    }
    return super.eIsSet(featureID);
  }

} //ConfigurationOpsImpl
