/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.RestrictedTemplate;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TempVarList;
import de.ugoe.cs.swe.tTCN3.Type;
import de.ugoe.cs.swe.tTCN3.VarInstance;
import de.ugoe.cs.swe.tTCN3.VarList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Var Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.VarInstanceImpl#getListMod <em>List Mod</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.VarInstanceImpl#getListType <em>List Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.VarInstanceImpl#getList <em>List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.VarInstanceImpl#getResTemplate <em>Res Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.VarInstanceImpl#getTempMod <em>Temp Mod</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.VarInstanceImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.VarInstanceImpl#getTempList <em>Temp List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VarInstanceImpl extends MinimalEObjectImpl.Container implements VarInstance
{
  /**
   * The default value of the '{@link #getListMod() <em>List Mod</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getListMod()
   * @generated
   * @ordered
   */
  protected static final String LIST_MOD_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getListMod() <em>List Mod</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getListMod()
   * @generated
   * @ordered
   */
  protected String listMod = LIST_MOD_EDEFAULT;

  /**
   * The cached value of the '{@link #getListType() <em>List Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getListType()
   * @generated
   * @ordered
   */
  protected Type listType;

  /**
   * The cached value of the '{@link #getList() <em>List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getList()
   * @generated
   * @ordered
   */
  protected VarList list;

  /**
   * The cached value of the '{@link #getResTemplate() <em>Res Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResTemplate()
   * @generated
   * @ordered
   */
  protected RestrictedTemplate resTemplate;

  /**
   * The default value of the '{@link #getTempMod() <em>Temp Mod</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTempMod()
   * @generated
   * @ordered
   */
  protected static final String TEMP_MOD_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTempMod() <em>Temp Mod</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTempMod()
   * @generated
   * @ordered
   */
  protected String tempMod = TEMP_MOD_EDEFAULT;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected Type type;

  /**
   * The cached value of the '{@link #getTempList() <em>Temp List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTempList()
   * @generated
   * @ordered
   */
  protected TempVarList tempList;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VarInstanceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getVarInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getListMod()
  {
    return listMod;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setListMod(String newListMod)
  {
    String oldListMod = listMod;
    listMod = newListMod;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__LIST_MOD, oldListMod, listMod));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getListType()
  {
    return listType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetListType(Type newListType, NotificationChain msgs)
  {
    Type oldListType = listType;
    listType = newListType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__LIST_TYPE, oldListType, newListType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setListType(Type newListType)
  {
    if (newListType != listType)
    {
      NotificationChain msgs = null;
      if (listType != null)
        msgs = ((InternalEObject)listType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.VAR_INSTANCE__LIST_TYPE, null, msgs);
      if (newListType != null)
        msgs = ((InternalEObject)newListType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.VAR_INSTANCE__LIST_TYPE, null, msgs);
      msgs = basicSetListType(newListType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__LIST_TYPE, newListType, newListType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarList getList()
  {
    return list;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetList(VarList newList, NotificationChain msgs)
  {
    VarList oldList = list;
    list = newList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__LIST, oldList, newList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setList(VarList newList)
  {
    if (newList != list)
    {
      NotificationChain msgs = null;
      if (list != null)
        msgs = ((InternalEObject)list).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.VAR_INSTANCE__LIST, null, msgs);
      if (newList != null)
        msgs = ((InternalEObject)newList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.VAR_INSTANCE__LIST, null, msgs);
      msgs = basicSetList(newList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__LIST, newList, newList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RestrictedTemplate getResTemplate()
  {
    return resTemplate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetResTemplate(RestrictedTemplate newResTemplate, NotificationChain msgs)
  {
    RestrictedTemplate oldResTemplate = resTemplate;
    resTemplate = newResTemplate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__RES_TEMPLATE, oldResTemplate, newResTemplate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setResTemplate(RestrictedTemplate newResTemplate)
  {
    if (newResTemplate != resTemplate)
    {
      NotificationChain msgs = null;
      if (resTemplate != null)
        msgs = ((InternalEObject)resTemplate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.VAR_INSTANCE__RES_TEMPLATE, null, msgs);
      if (newResTemplate != null)
        msgs = ((InternalEObject)newResTemplate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.VAR_INSTANCE__RES_TEMPLATE, null, msgs);
      msgs = basicSetResTemplate(newResTemplate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__RES_TEMPLATE, newResTemplate, newResTemplate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTempMod()
  {
    return tempMod;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTempMod(String newTempMod)
  {
    String oldTempMod = tempMod;
    tempMod = newTempMod;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__TEMP_MOD, oldTempMod, tempMod));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(Type newType, NotificationChain msgs)
  {
    Type oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(Type newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.VAR_INSTANCE__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.VAR_INSTANCE__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TempVarList getTempList()
  {
    return tempList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTempList(TempVarList newTempList, NotificationChain msgs)
  {
    TempVarList oldTempList = tempList;
    tempList = newTempList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__TEMP_LIST, oldTempList, newTempList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTempList(TempVarList newTempList)
  {
    if (newTempList != tempList)
    {
      NotificationChain msgs = null;
      if (tempList != null)
        msgs = ((InternalEObject)tempList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.VAR_INSTANCE__TEMP_LIST, null, msgs);
      if (newTempList != null)
        msgs = ((InternalEObject)newTempList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.VAR_INSTANCE__TEMP_LIST, null, msgs);
      msgs = basicSetTempList(newTempList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.VAR_INSTANCE__TEMP_LIST, newTempList, newTempList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.VAR_INSTANCE__LIST_TYPE:
        return basicSetListType(null, msgs);
      case TTCN3Package.VAR_INSTANCE__LIST:
        return basicSetList(null, msgs);
      case TTCN3Package.VAR_INSTANCE__RES_TEMPLATE:
        return basicSetResTemplate(null, msgs);
      case TTCN3Package.VAR_INSTANCE__TYPE:
        return basicSetType(null, msgs);
      case TTCN3Package.VAR_INSTANCE__TEMP_LIST:
        return basicSetTempList(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.VAR_INSTANCE__LIST_MOD:
        return getListMod();
      case TTCN3Package.VAR_INSTANCE__LIST_TYPE:
        return getListType();
      case TTCN3Package.VAR_INSTANCE__LIST:
        return getList();
      case TTCN3Package.VAR_INSTANCE__RES_TEMPLATE:
        return getResTemplate();
      case TTCN3Package.VAR_INSTANCE__TEMP_MOD:
        return getTempMod();
      case TTCN3Package.VAR_INSTANCE__TYPE:
        return getType();
      case TTCN3Package.VAR_INSTANCE__TEMP_LIST:
        return getTempList();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.VAR_INSTANCE__LIST_MOD:
        setListMod((String)newValue);
        return;
      case TTCN3Package.VAR_INSTANCE__LIST_TYPE:
        setListType((Type)newValue);
        return;
      case TTCN3Package.VAR_INSTANCE__LIST:
        setList((VarList)newValue);
        return;
      case TTCN3Package.VAR_INSTANCE__RES_TEMPLATE:
        setResTemplate((RestrictedTemplate)newValue);
        return;
      case TTCN3Package.VAR_INSTANCE__TEMP_MOD:
        setTempMod((String)newValue);
        return;
      case TTCN3Package.VAR_INSTANCE__TYPE:
        setType((Type)newValue);
        return;
      case TTCN3Package.VAR_INSTANCE__TEMP_LIST:
        setTempList((TempVarList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.VAR_INSTANCE__LIST_MOD:
        setListMod(LIST_MOD_EDEFAULT);
        return;
      case TTCN3Package.VAR_INSTANCE__LIST_TYPE:
        setListType((Type)null);
        return;
      case TTCN3Package.VAR_INSTANCE__LIST:
        setList((VarList)null);
        return;
      case TTCN3Package.VAR_INSTANCE__RES_TEMPLATE:
        setResTemplate((RestrictedTemplate)null);
        return;
      case TTCN3Package.VAR_INSTANCE__TEMP_MOD:
        setTempMod(TEMP_MOD_EDEFAULT);
        return;
      case TTCN3Package.VAR_INSTANCE__TYPE:
        setType((Type)null);
        return;
      case TTCN3Package.VAR_INSTANCE__TEMP_LIST:
        setTempList((TempVarList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.VAR_INSTANCE__LIST_MOD:
        return LIST_MOD_EDEFAULT == null ? listMod != null : !LIST_MOD_EDEFAULT.equals(listMod);
      case TTCN3Package.VAR_INSTANCE__LIST_TYPE:
        return listType != null;
      case TTCN3Package.VAR_INSTANCE__LIST:
        return list != null;
      case TTCN3Package.VAR_INSTANCE__RES_TEMPLATE:
        return resTemplate != null;
      case TTCN3Package.VAR_INSTANCE__TEMP_MOD:
        return TEMP_MOD_EDEFAULT == null ? tempMod != null : !TEMP_MOD_EDEFAULT.equals(tempMod);
      case TTCN3Package.VAR_INSTANCE__TYPE:
        return type != null;
      case TTCN3Package.VAR_INSTANCE__TEMP_LIST:
        return tempList != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (listMod: ");
    result.append(listMod);
    result.append(", tempMod: ");
    result.append(tempMod);
    result.append(')');
    return result.toString();
  }

} //VarInstanceImpl
