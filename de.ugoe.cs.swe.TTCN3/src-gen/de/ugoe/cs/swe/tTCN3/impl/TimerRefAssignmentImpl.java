/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.FormalTimerPar;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TimerRefAssignment;
import de.ugoe.cs.swe.tTCN3.TimerVarInstance;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timer Ref Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TimerRefAssignmentImpl#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TimerRefAssignmentImpl#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimerRefAssignmentImpl extends MinimalEObjectImpl.Container implements TimerRefAssignment
{
  /**
   * The cached value of the '{@link #getTimer() <em>Timer</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimer()
   * @generated
   * @ordered
   */
  protected FormalTimerPar timer;

  /**
   * The cached value of the '{@link #getValue() <em>Value</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValue()
   * @generated
   * @ordered
   */
  protected TimerVarInstance value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TimerRefAssignmentImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTimerRefAssignment();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalTimerPar getTimer()
  {
    if (timer != null && timer.eIsProxy())
    {
      InternalEObject oldTimer = (InternalEObject)timer;
      timer = (FormalTimerPar)eResolveProxy(oldTimer);
      if (timer != oldTimer)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, TTCN3Package.TIMER_REF_ASSIGNMENT__TIMER, oldTimer, timer));
      }
    }
    return timer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalTimerPar basicGetTimer()
  {
    return timer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimer(FormalTimerPar newTimer)
  {
    FormalTimerPar oldTimer = timer;
    timer = newTimer;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TIMER_REF_ASSIGNMENT__TIMER, oldTimer, timer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerVarInstance getValue()
  {
    if (value != null && value.eIsProxy())
    {
      InternalEObject oldValue = (InternalEObject)value;
      value = (TimerVarInstance)eResolveProxy(oldValue);
      if (value != oldValue)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, TTCN3Package.TIMER_REF_ASSIGNMENT__VALUE, oldValue, value));
      }
    }
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerVarInstance basicGetValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValue(TimerVarInstance newValue)
  {
    TimerVarInstance oldValue = value;
    value = newValue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TIMER_REF_ASSIGNMENT__VALUE, oldValue, value));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TIMER_REF_ASSIGNMENT__TIMER:
        if (resolve) return getTimer();
        return basicGetTimer();
      case TTCN3Package.TIMER_REF_ASSIGNMENT__VALUE:
        if (resolve) return getValue();
        return basicGetValue();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TIMER_REF_ASSIGNMENT__TIMER:
        setTimer((FormalTimerPar)newValue);
        return;
      case TTCN3Package.TIMER_REF_ASSIGNMENT__VALUE:
        setValue((TimerVarInstance)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TIMER_REF_ASSIGNMENT__TIMER:
        setTimer((FormalTimerPar)null);
        return;
      case TTCN3Package.TIMER_REF_ASSIGNMENT__VALUE:
        setValue((TimerVarInstance)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TIMER_REF_ASSIGNMENT__TIMER:
        return timer != null;
      case TTCN3Package.TIMER_REF_ASSIGNMENT__VALUE:
        return value != null;
    }
    return super.eIsSet(featureID);
  }

} //TimerRefAssignmentImpl
