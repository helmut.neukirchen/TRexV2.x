/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.PortDefAttribs;
import de.ugoe.cs.swe.tTCN3.PortDefBody;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Def Body</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortDefBodyImpl#getAttribs <em>Attribs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortDefBodyImpl extends MinimalEObjectImpl.Container implements PortDefBody
{
  /**
   * The cached value of the '{@link #getAttribs() <em>Attribs</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAttribs()
   * @generated
   * @ordered
   */
  protected PortDefAttribs attribs;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PortDefBodyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getPortDefBody();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortDefAttribs getAttribs()
  {
    return attribs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAttribs(PortDefAttribs newAttribs, NotificationChain msgs)
  {
    PortDefAttribs oldAttribs = attribs;
    attribs = newAttribs;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_DEF_BODY__ATTRIBS, oldAttribs, newAttribs);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAttribs(PortDefAttribs newAttribs)
  {
    if (newAttribs != attribs)
    {
      NotificationChain msgs = null;
      if (attribs != null)
        msgs = ((InternalEObject)attribs).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_DEF_BODY__ATTRIBS, null, msgs);
      if (newAttribs != null)
        msgs = ((InternalEObject)newAttribs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_DEF_BODY__ATTRIBS, null, msgs);
      msgs = basicSetAttribs(newAttribs, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_DEF_BODY__ATTRIBS, newAttribs, newAttribs));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_DEF_BODY__ATTRIBS:
        return basicSetAttribs(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_DEF_BODY__ATTRIBS:
        return getAttribs();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_DEF_BODY__ATTRIBS:
        setAttribs((PortDefAttribs)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_DEF_BODY__ATTRIBS:
        setAttribs((PortDefAttribs)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_DEF_BODY__ATTRIBS:
        return attribs != null;
    }
    return super.eIsSet(featureID);
  }

} //PortDefBodyImpl
