/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.FunctionLocalInst;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TimerInstance;
import de.ugoe.cs.swe.tTCN3.VarInstance;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Local Inst</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionLocalInstImpl#getVariable <em>Variable</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionLocalInstImpl#getTimer <em>Timer</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionLocalInstImpl extends MinimalEObjectImpl.Container implements FunctionLocalInst
{
  /**
   * The cached value of the '{@link #getVariable() <em>Variable</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariable()
   * @generated
   * @ordered
   */
  protected VarInstance variable;

  /**
   * The cached value of the '{@link #getTimer() <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimer()
   * @generated
   * @ordered
   */
  protected TimerInstance timer;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionLocalInstImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFunctionLocalInst();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarInstance getVariable()
  {
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVariable(VarInstance newVariable, NotificationChain msgs)
  {
    VarInstance oldVariable = variable;
    variable = newVariable;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_LOCAL_INST__VARIABLE, oldVariable, newVariable);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVariable(VarInstance newVariable)
  {
    if (newVariable != variable)
    {
      NotificationChain msgs = null;
      if (variable != null)
        msgs = ((InternalEObject)variable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_LOCAL_INST__VARIABLE, null, msgs);
      if (newVariable != null)
        msgs = ((InternalEObject)newVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_LOCAL_INST__VARIABLE, null, msgs);
      msgs = basicSetVariable(newVariable, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_LOCAL_INST__VARIABLE, newVariable, newVariable));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerInstance getTimer()
  {
    return timer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTimer(TimerInstance newTimer, NotificationChain msgs)
  {
    TimerInstance oldTimer = timer;
    timer = newTimer;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_LOCAL_INST__TIMER, oldTimer, newTimer);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimer(TimerInstance newTimer)
  {
    if (newTimer != timer)
    {
      NotificationChain msgs = null;
      if (timer != null)
        msgs = ((InternalEObject)timer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_LOCAL_INST__TIMER, null, msgs);
      if (newTimer != null)
        msgs = ((InternalEObject)newTimer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_LOCAL_INST__TIMER, null, msgs);
      msgs = basicSetTimer(newTimer, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_LOCAL_INST__TIMER, newTimer, newTimer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_LOCAL_INST__VARIABLE:
        return basicSetVariable(null, msgs);
      case TTCN3Package.FUNCTION_LOCAL_INST__TIMER:
        return basicSetTimer(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_LOCAL_INST__VARIABLE:
        return getVariable();
      case TTCN3Package.FUNCTION_LOCAL_INST__TIMER:
        return getTimer();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_LOCAL_INST__VARIABLE:
        setVariable((VarInstance)newValue);
        return;
      case TTCN3Package.FUNCTION_LOCAL_INST__TIMER:
        setTimer((TimerInstance)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_LOCAL_INST__VARIABLE:
        setVariable((VarInstance)null);
        return;
      case TTCN3Package.FUNCTION_LOCAL_INST__TIMER:
        setTimer((TimerInstance)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_LOCAL_INST__VARIABLE:
        return variable != null;
      case TTCN3Package.FUNCTION_LOCAL_INST__TIMER:
        return timer != null;
    }
    return super.eIsSet(featureID);
  }

} //FunctionLocalInstImpl
