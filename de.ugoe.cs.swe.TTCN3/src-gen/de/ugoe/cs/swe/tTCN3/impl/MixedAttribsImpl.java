/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AddressDecl;
import de.ugoe.cs.swe.tTCN3.ConfigParamDef;
import de.ugoe.cs.swe.tTCN3.MixedAttribs;
import de.ugoe.cs.swe.tTCN3.MixedList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mixed Attribs</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MixedAttribsImpl#getAddressDecls <em>Address Decls</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MixedAttribsImpl#getMixedLists <em>Mixed Lists</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MixedAttribsImpl#getConfigDefs <em>Config Defs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MixedAttribsImpl extends MinimalEObjectImpl.Container implements MixedAttribs
{
  /**
   * The cached value of the '{@link #getAddressDecls() <em>Address Decls</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAddressDecls()
   * @generated
   * @ordered
   */
  protected EList<AddressDecl> addressDecls;

  /**
   * The cached value of the '{@link #getMixedLists() <em>Mixed Lists</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMixedLists()
   * @generated
   * @ordered
   */
  protected EList<MixedList> mixedLists;

  /**
   * The cached value of the '{@link #getConfigDefs() <em>Config Defs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfigDefs()
   * @generated
   * @ordered
   */
  protected EList<ConfigParamDef> configDefs;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MixedAttribsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getMixedAttribs();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<AddressDecl> getAddressDecls()
  {
    if (addressDecls == null)
    {
      addressDecls = new EObjectContainmentEList<AddressDecl>(AddressDecl.class, this, TTCN3Package.MIXED_ATTRIBS__ADDRESS_DECLS);
    }
    return addressDecls;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<MixedList> getMixedLists()
  {
    if (mixedLists == null)
    {
      mixedLists = new EObjectContainmentEList<MixedList>(MixedList.class, this, TTCN3Package.MIXED_ATTRIBS__MIXED_LISTS);
    }
    return mixedLists;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ConfigParamDef> getConfigDefs()
  {
    if (configDefs == null)
    {
      configDefs = new EObjectContainmentEList<ConfigParamDef>(ConfigParamDef.class, this, TTCN3Package.MIXED_ATTRIBS__CONFIG_DEFS);
    }
    return configDefs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.MIXED_ATTRIBS__ADDRESS_DECLS:
        return ((InternalEList<?>)getAddressDecls()).basicRemove(otherEnd, msgs);
      case TTCN3Package.MIXED_ATTRIBS__MIXED_LISTS:
        return ((InternalEList<?>)getMixedLists()).basicRemove(otherEnd, msgs);
      case TTCN3Package.MIXED_ATTRIBS__CONFIG_DEFS:
        return ((InternalEList<?>)getConfigDefs()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.MIXED_ATTRIBS__ADDRESS_DECLS:
        return getAddressDecls();
      case TTCN3Package.MIXED_ATTRIBS__MIXED_LISTS:
        return getMixedLists();
      case TTCN3Package.MIXED_ATTRIBS__CONFIG_DEFS:
        return getConfigDefs();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.MIXED_ATTRIBS__ADDRESS_DECLS:
        getAddressDecls().clear();
        getAddressDecls().addAll((Collection<? extends AddressDecl>)newValue);
        return;
      case TTCN3Package.MIXED_ATTRIBS__MIXED_LISTS:
        getMixedLists().clear();
        getMixedLists().addAll((Collection<? extends MixedList>)newValue);
        return;
      case TTCN3Package.MIXED_ATTRIBS__CONFIG_DEFS:
        getConfigDefs().clear();
        getConfigDefs().addAll((Collection<? extends ConfigParamDef>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MIXED_ATTRIBS__ADDRESS_DECLS:
        getAddressDecls().clear();
        return;
      case TTCN3Package.MIXED_ATTRIBS__MIXED_LISTS:
        getMixedLists().clear();
        return;
      case TTCN3Package.MIXED_ATTRIBS__CONFIG_DEFS:
        getConfigDefs().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MIXED_ATTRIBS__ADDRESS_DECLS:
        return addressDecls != null && !addressDecls.isEmpty();
      case TTCN3Package.MIXED_ATTRIBS__MIXED_LISTS:
        return mixedLists != null && !mixedLists.isEmpty();
      case TTCN3Package.MIXED_ATTRIBS__CONFIG_DEFS:
        return configDefs != null && !configDefs.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //MixedAttribsImpl
