/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.FormalPortAndValuePar;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formal Port And Value Par</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FormalPortAndValueParImpl extends MinimalEObjectImpl.Container implements FormalPortAndValuePar
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FormalPortAndValueParImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFormalPortAndValuePar();
  }

} //FormalPortAndValueParImpl
