/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ConfigSpec;
import de.ugoe.cs.swe.tTCN3.RunsOnSpec;
import de.ugoe.cs.swe.tTCN3.SystemSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Config Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigSpecImpl#getRunsOn <em>Runs On</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigSpecImpl#getSystemSpec <em>System Spec</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConfigSpecImpl extends MinimalEObjectImpl.Container implements ConfigSpec
{
  /**
   * The cached value of the '{@link #getRunsOn() <em>Runs On</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRunsOn()
   * @generated
   * @ordered
   */
  protected RunsOnSpec runsOn;

  /**
   * The cached value of the '{@link #getSystemSpec() <em>System Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSystemSpec()
   * @generated
   * @ordered
   */
  protected SystemSpec systemSpec;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ConfigSpecImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getConfigSpec();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RunsOnSpec getRunsOn()
  {
    return runsOn;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRunsOn(RunsOnSpec newRunsOn, NotificationChain msgs)
  {
    RunsOnSpec oldRunsOn = runsOn;
    runsOn = newRunsOn;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIG_SPEC__RUNS_ON, oldRunsOn, newRunsOn);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRunsOn(RunsOnSpec newRunsOn)
  {
    if (newRunsOn != runsOn)
    {
      NotificationChain msgs = null;
      if (runsOn != null)
        msgs = ((InternalEObject)runsOn).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIG_SPEC__RUNS_ON, null, msgs);
      if (newRunsOn != null)
        msgs = ((InternalEObject)newRunsOn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIG_SPEC__RUNS_ON, null, msgs);
      msgs = basicSetRunsOn(newRunsOn, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIG_SPEC__RUNS_ON, newRunsOn, newRunsOn));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SystemSpec getSystemSpec()
  {
    return systemSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSystemSpec(SystemSpec newSystemSpec, NotificationChain msgs)
  {
    SystemSpec oldSystemSpec = systemSpec;
    systemSpec = newSystemSpec;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIG_SPEC__SYSTEM_SPEC, oldSystemSpec, newSystemSpec);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSystemSpec(SystemSpec newSystemSpec)
  {
    if (newSystemSpec != systemSpec)
    {
      NotificationChain msgs = null;
      if (systemSpec != null)
        msgs = ((InternalEObject)systemSpec).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIG_SPEC__SYSTEM_SPEC, null, msgs);
      if (newSystemSpec != null)
        msgs = ((InternalEObject)newSystemSpec).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIG_SPEC__SYSTEM_SPEC, null, msgs);
      msgs = basicSetSystemSpec(newSystemSpec, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIG_SPEC__SYSTEM_SPEC, newSystemSpec, newSystemSpec));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIG_SPEC__RUNS_ON:
        return basicSetRunsOn(null, msgs);
      case TTCN3Package.CONFIG_SPEC__SYSTEM_SPEC:
        return basicSetSystemSpec(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIG_SPEC__RUNS_ON:
        return getRunsOn();
      case TTCN3Package.CONFIG_SPEC__SYSTEM_SPEC:
        return getSystemSpec();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIG_SPEC__RUNS_ON:
        setRunsOn((RunsOnSpec)newValue);
        return;
      case TTCN3Package.CONFIG_SPEC__SYSTEM_SPEC:
        setSystemSpec((SystemSpec)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIG_SPEC__RUNS_ON:
        setRunsOn((RunsOnSpec)null);
        return;
      case TTCN3Package.CONFIG_SPEC__SYSTEM_SPEC:
        setSystemSpec((SystemSpec)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIG_SPEC__RUNS_ON:
        return runsOn != null;
      case TTCN3Package.CONFIG_SPEC__SYSTEM_SPEC:
        return systemSpec != null;
    }
    return super.eIsSet(featureID);
  }

} //ConfigSpecImpl
