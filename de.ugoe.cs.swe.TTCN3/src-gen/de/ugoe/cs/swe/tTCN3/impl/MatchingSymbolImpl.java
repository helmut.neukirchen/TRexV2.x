/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.CharStringMatch;
import de.ugoe.cs.swe.tTCN3.Complement;
import de.ugoe.cs.swe.tTCN3.ListOfTemplates;
import de.ugoe.cs.swe.tTCN3.MatchingSymbol;
import de.ugoe.cs.swe.tTCN3.Range;
import de.ugoe.cs.swe.tTCN3.SubsetMatch;
import de.ugoe.cs.swe.tTCN3.SupersetMatch;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.WildcardLengthMatch;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Matching Symbol</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MatchingSymbolImpl#getComplement <em>Complement</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MatchingSymbolImpl#getQwlm <em>Qwlm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MatchingSymbolImpl#getSwlm <em>Swlm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MatchingSymbolImpl#getRange <em>Range</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MatchingSymbolImpl#getString <em>String</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MatchingSymbolImpl#getSubset <em>Subset</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MatchingSymbolImpl#getSuperset <em>Superset</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MatchingSymbolImpl#getTemplates <em>Templates</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MatchingSymbolImpl extends MinimalEObjectImpl.Container implements MatchingSymbol
{
  /**
   * The cached value of the '{@link #getComplement() <em>Complement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComplement()
   * @generated
   * @ordered
   */
  protected Complement complement;

  /**
   * The cached value of the '{@link #getQwlm() <em>Qwlm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getQwlm()
   * @generated
   * @ordered
   */
  protected WildcardLengthMatch qwlm;

  /**
   * The cached value of the '{@link #getSwlm() <em>Swlm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSwlm()
   * @generated
   * @ordered
   */
  protected WildcardLengthMatch swlm;

  /**
   * The cached value of the '{@link #getRange() <em>Range</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRange()
   * @generated
   * @ordered
   */
  protected Range range;

  /**
   * The cached value of the '{@link #getString() <em>String</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getString()
   * @generated
   * @ordered
   */
  protected CharStringMatch string;

  /**
   * The cached value of the '{@link #getSubset() <em>Subset</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubset()
   * @generated
   * @ordered
   */
  protected SubsetMatch subset;

  /**
   * The cached value of the '{@link #getSuperset() <em>Superset</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSuperset()
   * @generated
   * @ordered
   */
  protected SupersetMatch superset;

  /**
   * The cached value of the '{@link #getTemplates() <em>Templates</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplates()
   * @generated
   * @ordered
   */
  protected ListOfTemplates templates;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MatchingSymbolImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getMatchingSymbol();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Complement getComplement()
  {
    return complement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetComplement(Complement newComplement, NotificationChain msgs)
  {
    Complement oldComplement = complement;
    complement = newComplement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__COMPLEMENT, oldComplement, newComplement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComplement(Complement newComplement)
  {
    if (newComplement != complement)
    {
      NotificationChain msgs = null;
      if (complement != null)
        msgs = ((InternalEObject)complement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__COMPLEMENT, null, msgs);
      if (newComplement != null)
        msgs = ((InternalEObject)newComplement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__COMPLEMENT, null, msgs);
      msgs = basicSetComplement(newComplement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__COMPLEMENT, newComplement, newComplement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WildcardLengthMatch getQwlm()
  {
    return qwlm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetQwlm(WildcardLengthMatch newQwlm, NotificationChain msgs)
  {
    WildcardLengthMatch oldQwlm = qwlm;
    qwlm = newQwlm;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__QWLM, oldQwlm, newQwlm);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setQwlm(WildcardLengthMatch newQwlm)
  {
    if (newQwlm != qwlm)
    {
      NotificationChain msgs = null;
      if (qwlm != null)
        msgs = ((InternalEObject)qwlm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__QWLM, null, msgs);
      if (newQwlm != null)
        msgs = ((InternalEObject)newQwlm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__QWLM, null, msgs);
      msgs = basicSetQwlm(newQwlm, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__QWLM, newQwlm, newQwlm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WildcardLengthMatch getSwlm()
  {
    return swlm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSwlm(WildcardLengthMatch newSwlm, NotificationChain msgs)
  {
    WildcardLengthMatch oldSwlm = swlm;
    swlm = newSwlm;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__SWLM, oldSwlm, newSwlm);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSwlm(WildcardLengthMatch newSwlm)
  {
    if (newSwlm != swlm)
    {
      NotificationChain msgs = null;
      if (swlm != null)
        msgs = ((InternalEObject)swlm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__SWLM, null, msgs);
      if (newSwlm != null)
        msgs = ((InternalEObject)newSwlm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__SWLM, null, msgs);
      msgs = basicSetSwlm(newSwlm, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__SWLM, newSwlm, newSwlm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Range getRange()
  {
    return range;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRange(Range newRange, NotificationChain msgs)
  {
    Range oldRange = range;
    range = newRange;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__RANGE, oldRange, newRange);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRange(Range newRange)
  {
    if (newRange != range)
    {
      NotificationChain msgs = null;
      if (range != null)
        msgs = ((InternalEObject)range).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__RANGE, null, msgs);
      if (newRange != null)
        msgs = ((InternalEObject)newRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__RANGE, null, msgs);
      msgs = basicSetRange(newRange, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__RANGE, newRange, newRange));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CharStringMatch getString()
  {
    return string;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetString(CharStringMatch newString, NotificationChain msgs)
  {
    CharStringMatch oldString = string;
    string = newString;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__STRING, oldString, newString);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setString(CharStringMatch newString)
  {
    if (newString != string)
    {
      NotificationChain msgs = null;
      if (string != null)
        msgs = ((InternalEObject)string).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__STRING, null, msgs);
      if (newString != null)
        msgs = ((InternalEObject)newString).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__STRING, null, msgs);
      msgs = basicSetString(newString, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__STRING, newString, newString));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubsetMatch getSubset()
  {
    return subset;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSubset(SubsetMatch newSubset, NotificationChain msgs)
  {
    SubsetMatch oldSubset = subset;
    subset = newSubset;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__SUBSET, oldSubset, newSubset);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubset(SubsetMatch newSubset)
  {
    if (newSubset != subset)
    {
      NotificationChain msgs = null;
      if (subset != null)
        msgs = ((InternalEObject)subset).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__SUBSET, null, msgs);
      if (newSubset != null)
        msgs = ((InternalEObject)newSubset).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__SUBSET, null, msgs);
      msgs = basicSetSubset(newSubset, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__SUBSET, newSubset, newSubset));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SupersetMatch getSuperset()
  {
    return superset;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSuperset(SupersetMatch newSuperset, NotificationChain msgs)
  {
    SupersetMatch oldSuperset = superset;
    superset = newSuperset;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__SUPERSET, oldSuperset, newSuperset);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSuperset(SupersetMatch newSuperset)
  {
    if (newSuperset != superset)
    {
      NotificationChain msgs = null;
      if (superset != null)
        msgs = ((InternalEObject)superset).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__SUPERSET, null, msgs);
      if (newSuperset != null)
        msgs = ((InternalEObject)newSuperset).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__SUPERSET, null, msgs);
      msgs = basicSetSuperset(newSuperset, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__SUPERSET, newSuperset, newSuperset));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ListOfTemplates getTemplates()
  {
    return templates;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTemplates(ListOfTemplates newTemplates, NotificationChain msgs)
  {
    ListOfTemplates oldTemplates = templates;
    templates = newTemplates;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__TEMPLATES, oldTemplates, newTemplates);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTemplates(ListOfTemplates newTemplates)
  {
    if (newTemplates != templates)
    {
      NotificationChain msgs = null;
      if (templates != null)
        msgs = ((InternalEObject)templates).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__TEMPLATES, null, msgs);
      if (newTemplates != null)
        msgs = ((InternalEObject)newTemplates).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MATCHING_SYMBOL__TEMPLATES, null, msgs);
      msgs = basicSetTemplates(newTemplates, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MATCHING_SYMBOL__TEMPLATES, newTemplates, newTemplates));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.MATCHING_SYMBOL__COMPLEMENT:
        return basicSetComplement(null, msgs);
      case TTCN3Package.MATCHING_SYMBOL__QWLM:
        return basicSetQwlm(null, msgs);
      case TTCN3Package.MATCHING_SYMBOL__SWLM:
        return basicSetSwlm(null, msgs);
      case TTCN3Package.MATCHING_SYMBOL__RANGE:
        return basicSetRange(null, msgs);
      case TTCN3Package.MATCHING_SYMBOL__STRING:
        return basicSetString(null, msgs);
      case TTCN3Package.MATCHING_SYMBOL__SUBSET:
        return basicSetSubset(null, msgs);
      case TTCN3Package.MATCHING_SYMBOL__SUPERSET:
        return basicSetSuperset(null, msgs);
      case TTCN3Package.MATCHING_SYMBOL__TEMPLATES:
        return basicSetTemplates(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.MATCHING_SYMBOL__COMPLEMENT:
        return getComplement();
      case TTCN3Package.MATCHING_SYMBOL__QWLM:
        return getQwlm();
      case TTCN3Package.MATCHING_SYMBOL__SWLM:
        return getSwlm();
      case TTCN3Package.MATCHING_SYMBOL__RANGE:
        return getRange();
      case TTCN3Package.MATCHING_SYMBOL__STRING:
        return getString();
      case TTCN3Package.MATCHING_SYMBOL__SUBSET:
        return getSubset();
      case TTCN3Package.MATCHING_SYMBOL__SUPERSET:
        return getSuperset();
      case TTCN3Package.MATCHING_SYMBOL__TEMPLATES:
        return getTemplates();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.MATCHING_SYMBOL__COMPLEMENT:
        setComplement((Complement)newValue);
        return;
      case TTCN3Package.MATCHING_SYMBOL__QWLM:
        setQwlm((WildcardLengthMatch)newValue);
        return;
      case TTCN3Package.MATCHING_SYMBOL__SWLM:
        setSwlm((WildcardLengthMatch)newValue);
        return;
      case TTCN3Package.MATCHING_SYMBOL__RANGE:
        setRange((Range)newValue);
        return;
      case TTCN3Package.MATCHING_SYMBOL__STRING:
        setString((CharStringMatch)newValue);
        return;
      case TTCN3Package.MATCHING_SYMBOL__SUBSET:
        setSubset((SubsetMatch)newValue);
        return;
      case TTCN3Package.MATCHING_SYMBOL__SUPERSET:
        setSuperset((SupersetMatch)newValue);
        return;
      case TTCN3Package.MATCHING_SYMBOL__TEMPLATES:
        setTemplates((ListOfTemplates)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MATCHING_SYMBOL__COMPLEMENT:
        setComplement((Complement)null);
        return;
      case TTCN3Package.MATCHING_SYMBOL__QWLM:
        setQwlm((WildcardLengthMatch)null);
        return;
      case TTCN3Package.MATCHING_SYMBOL__SWLM:
        setSwlm((WildcardLengthMatch)null);
        return;
      case TTCN3Package.MATCHING_SYMBOL__RANGE:
        setRange((Range)null);
        return;
      case TTCN3Package.MATCHING_SYMBOL__STRING:
        setString((CharStringMatch)null);
        return;
      case TTCN3Package.MATCHING_SYMBOL__SUBSET:
        setSubset((SubsetMatch)null);
        return;
      case TTCN3Package.MATCHING_SYMBOL__SUPERSET:
        setSuperset((SupersetMatch)null);
        return;
      case TTCN3Package.MATCHING_SYMBOL__TEMPLATES:
        setTemplates((ListOfTemplates)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MATCHING_SYMBOL__COMPLEMENT:
        return complement != null;
      case TTCN3Package.MATCHING_SYMBOL__QWLM:
        return qwlm != null;
      case TTCN3Package.MATCHING_SYMBOL__SWLM:
        return swlm != null;
      case TTCN3Package.MATCHING_SYMBOL__RANGE:
        return range != null;
      case TTCN3Package.MATCHING_SYMBOL__STRING:
        return string != null;
      case TTCN3Package.MATCHING_SYMBOL__SUBSET:
        return subset != null;
      case TTCN3Package.MATCHING_SYMBOL__SUPERSET:
        return superset != null;
      case TTCN3Package.MATCHING_SYMBOL__TEMPLATES:
        return templates != null;
    }
    return super.eIsSet(featureID);
  }

} //MatchingSymbolImpl
