/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.PredefinedValue;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.VerdictTypeValue;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Predefined Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getBstring <em>Bstring</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getBoolean <em>Boolean</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getInteger <em>Integer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getHstring <em>Hstring</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getOstring <em>Ostring</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getVerdictType <em>Verdict Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getFloat <em>Float</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getAddress <em>Address</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getOmit <em>Omit</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getCharString <em>Char String</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PredefinedValueImpl#getMacro <em>Macro</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PredefinedValueImpl extends MinimalEObjectImpl.Container implements PredefinedValue
{
  /**
   * The default value of the '{@link #getBstring() <em>Bstring</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBstring()
   * @generated
   * @ordered
   */
  protected static final String BSTRING_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBstring() <em>Bstring</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBstring()
   * @generated
   * @ordered
   */
  protected String bstring = BSTRING_EDEFAULT;

  /**
   * The default value of the '{@link #getBoolean() <em>Boolean</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBoolean()
   * @generated
   * @ordered
   */
  protected static final String BOOLEAN_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBoolean() <em>Boolean</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBoolean()
   * @generated
   * @ordered
   */
  protected String boolean_ = BOOLEAN_EDEFAULT;

  /**
   * The default value of the '{@link #getInteger() <em>Integer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInteger()
   * @generated
   * @ordered
   */
  protected static final String INTEGER_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getInteger() <em>Integer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInteger()
   * @generated
   * @ordered
   */
  protected String integer = INTEGER_EDEFAULT;

  /**
   * The default value of the '{@link #getHstring() <em>Hstring</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHstring()
   * @generated
   * @ordered
   */
  protected static final String HSTRING_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getHstring() <em>Hstring</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHstring()
   * @generated
   * @ordered
   */
  protected String hstring = HSTRING_EDEFAULT;

  /**
   * The default value of the '{@link #getOstring() <em>Ostring</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOstring()
   * @generated
   * @ordered
   */
  protected static final String OSTRING_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOstring() <em>Ostring</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOstring()
   * @generated
   * @ordered
   */
  protected String ostring = OSTRING_EDEFAULT;

  /**
   * The default value of the '{@link #getVerdictType() <em>Verdict Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVerdictType()
   * @generated
   * @ordered
   */
  protected static final VerdictTypeValue VERDICT_TYPE_EDEFAULT = VerdictTypeValue.PASS;

  /**
   * The cached value of the '{@link #getVerdictType() <em>Verdict Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVerdictType()
   * @generated
   * @ordered
   */
  protected VerdictTypeValue verdictType = VERDICT_TYPE_EDEFAULT;

  /**
   * The default value of the '{@link #getFloat() <em>Float</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFloat()
   * @generated
   * @ordered
   */
  protected static final String FLOAT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFloat() <em>Float</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFloat()
   * @generated
   * @ordered
   */
  protected String float_ = FLOAT_EDEFAULT;

  /**
   * The default value of the '{@link #getAddress() <em>Address</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAddress()
   * @generated
   * @ordered
   */
  protected static final String ADDRESS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAddress() <em>Address</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAddress()
   * @generated
   * @ordered
   */
  protected String address = ADDRESS_EDEFAULT;

  /**
   * The default value of the '{@link #getOmit() <em>Omit</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOmit()
   * @generated
   * @ordered
   */
  protected static final String OMIT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOmit() <em>Omit</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOmit()
   * @generated
   * @ordered
   */
  protected String omit = OMIT_EDEFAULT;

  /**
   * The default value of the '{@link #getCharString() <em>Char String</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCharString()
   * @generated
   * @ordered
   */
  protected static final String CHAR_STRING_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCharString() <em>Char String</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCharString()
   * @generated
   * @ordered
   */
  protected String charString = CHAR_STRING_EDEFAULT;

  /**
   * The default value of the '{@link #getMacro() <em>Macro</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMacro()
   * @generated
   * @ordered
   */
  protected static final String MACRO_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMacro() <em>Macro</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMacro()
   * @generated
   * @ordered
   */
  protected String macro = MACRO_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PredefinedValueImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getPredefinedValue();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBstring()
  {
    return bstring;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBstring(String newBstring)
  {
    String oldBstring = bstring;
    bstring = newBstring;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__BSTRING, oldBstring, bstring));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBoolean()
  {
    return boolean_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBoolean(String newBoolean)
  {
    String oldBoolean = boolean_;
    boolean_ = newBoolean;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__BOOLEAN, oldBoolean, boolean_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getInteger()
  {
    return integer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInteger(String newInteger)
  {
    String oldInteger = integer;
    integer = newInteger;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__INTEGER, oldInteger, integer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getHstring()
  {
    return hstring;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHstring(String newHstring)
  {
    String oldHstring = hstring;
    hstring = newHstring;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__HSTRING, oldHstring, hstring));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOstring()
  {
    return ostring;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOstring(String newOstring)
  {
    String oldOstring = ostring;
    ostring = newOstring;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__OSTRING, oldOstring, ostring));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VerdictTypeValue getVerdictType()
  {
    return verdictType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVerdictType(VerdictTypeValue newVerdictType)
  {
    VerdictTypeValue oldVerdictType = verdictType;
    verdictType = newVerdictType == null ? VERDICT_TYPE_EDEFAULT : newVerdictType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__VERDICT_TYPE, oldVerdictType, verdictType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFloat()
  {
    return float_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFloat(String newFloat)
  {
    String oldFloat = float_;
    float_ = newFloat;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__FLOAT, oldFloat, float_));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAddress()
  {
    return address;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAddress(String newAddress)
  {
    String oldAddress = address;
    address = newAddress;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__ADDRESS, oldAddress, address));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOmit()
  {
    return omit;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOmit(String newOmit)
  {
    String oldOmit = omit;
    omit = newOmit;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__OMIT, oldOmit, omit));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCharString()
  {
    return charString;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCharString(String newCharString)
  {
    String oldCharString = charString;
    charString = newCharString;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__CHAR_STRING, oldCharString, charString));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMacro()
  {
    return macro;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMacro(String newMacro)
  {
    String oldMacro = macro;
    macro = newMacro;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PREDEFINED_VALUE__MACRO, oldMacro, macro));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.PREDEFINED_VALUE__BSTRING:
        return getBstring();
      case TTCN3Package.PREDEFINED_VALUE__BOOLEAN:
        return getBoolean();
      case TTCN3Package.PREDEFINED_VALUE__INTEGER:
        return getInteger();
      case TTCN3Package.PREDEFINED_VALUE__HSTRING:
        return getHstring();
      case TTCN3Package.PREDEFINED_VALUE__OSTRING:
        return getOstring();
      case TTCN3Package.PREDEFINED_VALUE__VERDICT_TYPE:
        return getVerdictType();
      case TTCN3Package.PREDEFINED_VALUE__FLOAT:
        return getFloat();
      case TTCN3Package.PREDEFINED_VALUE__ADDRESS:
        return getAddress();
      case TTCN3Package.PREDEFINED_VALUE__OMIT:
        return getOmit();
      case TTCN3Package.PREDEFINED_VALUE__CHAR_STRING:
        return getCharString();
      case TTCN3Package.PREDEFINED_VALUE__MACRO:
        return getMacro();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.PREDEFINED_VALUE__BSTRING:
        setBstring((String)newValue);
        return;
      case TTCN3Package.PREDEFINED_VALUE__BOOLEAN:
        setBoolean((String)newValue);
        return;
      case TTCN3Package.PREDEFINED_VALUE__INTEGER:
        setInteger((String)newValue);
        return;
      case TTCN3Package.PREDEFINED_VALUE__HSTRING:
        setHstring((String)newValue);
        return;
      case TTCN3Package.PREDEFINED_VALUE__OSTRING:
        setOstring((String)newValue);
        return;
      case TTCN3Package.PREDEFINED_VALUE__VERDICT_TYPE:
        setVerdictType((VerdictTypeValue)newValue);
        return;
      case TTCN3Package.PREDEFINED_VALUE__FLOAT:
        setFloat((String)newValue);
        return;
      case TTCN3Package.PREDEFINED_VALUE__ADDRESS:
        setAddress((String)newValue);
        return;
      case TTCN3Package.PREDEFINED_VALUE__OMIT:
        setOmit((String)newValue);
        return;
      case TTCN3Package.PREDEFINED_VALUE__CHAR_STRING:
        setCharString((String)newValue);
        return;
      case TTCN3Package.PREDEFINED_VALUE__MACRO:
        setMacro((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PREDEFINED_VALUE__BSTRING:
        setBstring(BSTRING_EDEFAULT);
        return;
      case TTCN3Package.PREDEFINED_VALUE__BOOLEAN:
        setBoolean(BOOLEAN_EDEFAULT);
        return;
      case TTCN3Package.PREDEFINED_VALUE__INTEGER:
        setInteger(INTEGER_EDEFAULT);
        return;
      case TTCN3Package.PREDEFINED_VALUE__HSTRING:
        setHstring(HSTRING_EDEFAULT);
        return;
      case TTCN3Package.PREDEFINED_VALUE__OSTRING:
        setOstring(OSTRING_EDEFAULT);
        return;
      case TTCN3Package.PREDEFINED_VALUE__VERDICT_TYPE:
        setVerdictType(VERDICT_TYPE_EDEFAULT);
        return;
      case TTCN3Package.PREDEFINED_VALUE__FLOAT:
        setFloat(FLOAT_EDEFAULT);
        return;
      case TTCN3Package.PREDEFINED_VALUE__ADDRESS:
        setAddress(ADDRESS_EDEFAULT);
        return;
      case TTCN3Package.PREDEFINED_VALUE__OMIT:
        setOmit(OMIT_EDEFAULT);
        return;
      case TTCN3Package.PREDEFINED_VALUE__CHAR_STRING:
        setCharString(CHAR_STRING_EDEFAULT);
        return;
      case TTCN3Package.PREDEFINED_VALUE__MACRO:
        setMacro(MACRO_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PREDEFINED_VALUE__BSTRING:
        return BSTRING_EDEFAULT == null ? bstring != null : !BSTRING_EDEFAULT.equals(bstring);
      case TTCN3Package.PREDEFINED_VALUE__BOOLEAN:
        return BOOLEAN_EDEFAULT == null ? boolean_ != null : !BOOLEAN_EDEFAULT.equals(boolean_);
      case TTCN3Package.PREDEFINED_VALUE__INTEGER:
        return INTEGER_EDEFAULT == null ? integer != null : !INTEGER_EDEFAULT.equals(integer);
      case TTCN3Package.PREDEFINED_VALUE__HSTRING:
        return HSTRING_EDEFAULT == null ? hstring != null : !HSTRING_EDEFAULT.equals(hstring);
      case TTCN3Package.PREDEFINED_VALUE__OSTRING:
        return OSTRING_EDEFAULT == null ? ostring != null : !OSTRING_EDEFAULT.equals(ostring);
      case TTCN3Package.PREDEFINED_VALUE__VERDICT_TYPE:
        return verdictType != VERDICT_TYPE_EDEFAULT;
      case TTCN3Package.PREDEFINED_VALUE__FLOAT:
        return FLOAT_EDEFAULT == null ? float_ != null : !FLOAT_EDEFAULT.equals(float_);
      case TTCN3Package.PREDEFINED_VALUE__ADDRESS:
        return ADDRESS_EDEFAULT == null ? address != null : !ADDRESS_EDEFAULT.equals(address);
      case TTCN3Package.PREDEFINED_VALUE__OMIT:
        return OMIT_EDEFAULT == null ? omit != null : !OMIT_EDEFAULT.equals(omit);
      case TTCN3Package.PREDEFINED_VALUE__CHAR_STRING:
        return CHAR_STRING_EDEFAULT == null ? charString != null : !CHAR_STRING_EDEFAULT.equals(charString);
      case TTCN3Package.PREDEFINED_VALUE__MACRO:
        return MACRO_EDEFAULT == null ? macro != null : !MACRO_EDEFAULT.equals(macro);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (bstring: ");
    result.append(bstring);
    result.append(", boolean: ");
    result.append(boolean_);
    result.append(", integer: ");
    result.append(integer);
    result.append(", hstring: ");
    result.append(hstring);
    result.append(", ostring: ");
    result.append(ostring);
    result.append(", verdictType: ");
    result.append(verdictType);
    result.append(", float: ");
    result.append(float_);
    result.append(", address: ");
    result.append(address);
    result.append(", omit: ");
    result.append(omit);
    result.append(", charString: ");
    result.append(charString);
    result.append(", macro: ");
    result.append(macro);
    result.append(')');
    return result.toString();
  }

} //PredefinedValueImpl
