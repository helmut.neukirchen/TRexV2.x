/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ExceptionSpec;
import de.ugoe.cs.swe.tTCN3.ReturnType;
import de.ugoe.cs.swe.tTCN3.SignatureDef;
import de.ugoe.cs.swe.tTCN3.SignatureFormalParList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signature Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SignatureDefImpl#getParamList <em>Param List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SignatureDefImpl#getReturn <em>Return</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SignatureDefImpl#getSprec <em>Sprec</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignatureDefImpl extends ReferencedTypeImpl implements SignatureDef
{
  /**
   * The cached value of the '{@link #getParamList() <em>Param List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParamList()
   * @generated
   * @ordered
   */
  protected SignatureFormalParList paramList;

  /**
   * The cached value of the '{@link #getReturn() <em>Return</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReturn()
   * @generated
   * @ordered
   */
  protected ReturnType return_;

  /**
   * The cached value of the '{@link #getSprec() <em>Sprec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSprec()
   * @generated
   * @ordered
   */
  protected ExceptionSpec sprec;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SignatureDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getSignatureDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SignatureFormalParList getParamList()
  {
    return paramList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParamList(SignatureFormalParList newParamList, NotificationChain msgs)
  {
    SignatureFormalParList oldParamList = paramList;
    paramList = newParamList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SIGNATURE_DEF__PARAM_LIST, oldParamList, newParamList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParamList(SignatureFormalParList newParamList)
  {
    if (newParamList != paramList)
    {
      NotificationChain msgs = null;
      if (paramList != null)
        msgs = ((InternalEObject)paramList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SIGNATURE_DEF__PARAM_LIST, null, msgs);
      if (newParamList != null)
        msgs = ((InternalEObject)newParamList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SIGNATURE_DEF__PARAM_LIST, null, msgs);
      msgs = basicSetParamList(newParamList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SIGNATURE_DEF__PARAM_LIST, newParamList, newParamList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReturnType getReturn()
  {
    return return_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReturn(ReturnType newReturn, NotificationChain msgs)
  {
    ReturnType oldReturn = return_;
    return_ = newReturn;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SIGNATURE_DEF__RETURN, oldReturn, newReturn);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReturn(ReturnType newReturn)
  {
    if (newReturn != return_)
    {
      NotificationChain msgs = null;
      if (return_ != null)
        msgs = ((InternalEObject)return_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SIGNATURE_DEF__RETURN, null, msgs);
      if (newReturn != null)
        msgs = ((InternalEObject)newReturn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SIGNATURE_DEF__RETURN, null, msgs);
      msgs = basicSetReturn(newReturn, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SIGNATURE_DEF__RETURN, newReturn, newReturn));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptionSpec getSprec()
  {
    return sprec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSprec(ExceptionSpec newSprec, NotificationChain msgs)
  {
    ExceptionSpec oldSprec = sprec;
    sprec = newSprec;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SIGNATURE_DEF__SPREC, oldSprec, newSprec);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSprec(ExceptionSpec newSprec)
  {
    if (newSprec != sprec)
    {
      NotificationChain msgs = null;
      if (sprec != null)
        msgs = ((InternalEObject)sprec).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SIGNATURE_DEF__SPREC, null, msgs);
      if (newSprec != null)
        msgs = ((InternalEObject)newSprec).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SIGNATURE_DEF__SPREC, null, msgs);
      msgs = basicSetSprec(newSprec, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SIGNATURE_DEF__SPREC, newSprec, newSprec));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.SIGNATURE_DEF__PARAM_LIST:
        return basicSetParamList(null, msgs);
      case TTCN3Package.SIGNATURE_DEF__RETURN:
        return basicSetReturn(null, msgs);
      case TTCN3Package.SIGNATURE_DEF__SPREC:
        return basicSetSprec(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.SIGNATURE_DEF__PARAM_LIST:
        return getParamList();
      case TTCN3Package.SIGNATURE_DEF__RETURN:
        return getReturn();
      case TTCN3Package.SIGNATURE_DEF__SPREC:
        return getSprec();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.SIGNATURE_DEF__PARAM_LIST:
        setParamList((SignatureFormalParList)newValue);
        return;
      case TTCN3Package.SIGNATURE_DEF__RETURN:
        setReturn((ReturnType)newValue);
        return;
      case TTCN3Package.SIGNATURE_DEF__SPREC:
        setSprec((ExceptionSpec)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SIGNATURE_DEF__PARAM_LIST:
        setParamList((SignatureFormalParList)null);
        return;
      case TTCN3Package.SIGNATURE_DEF__RETURN:
        setReturn((ReturnType)null);
        return;
      case TTCN3Package.SIGNATURE_DEF__SPREC:
        setSprec((ExceptionSpec)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SIGNATURE_DEF__PARAM_LIST:
        return paramList != null;
      case TTCN3Package.SIGNATURE_DEF__RETURN:
        return return_ != null;
      case TTCN3Package.SIGNATURE_DEF__SPREC:
        return sprec != null;
    }
    return super.eIsSet(featureID);
  }

} //SignatureDefImpl
