/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.IndexSpec;
import de.ugoe.cs.swe.tTCN3.ParamSpec;
import de.ugoe.cs.swe.tTCN3.RedirectWithParamSpec;
import de.ugoe.cs.swe.tTCN3.SenderSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Redirect With Param Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RedirectWithParamSpecImpl#getParam <em>Param</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RedirectWithParamSpecImpl#getSender <em>Sender</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RedirectWithParamSpecImpl#getIndex <em>Index</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RedirectWithParamSpecImpl extends MinimalEObjectImpl.Container implements RedirectWithParamSpec
{
  /**
   * The cached value of the '{@link #getParam() <em>Param</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParam()
   * @generated
   * @ordered
   */
  protected ParamSpec param;

  /**
   * The cached value of the '{@link #getSender() <em>Sender</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSender()
   * @generated
   * @ordered
   */
  protected SenderSpec sender;

  /**
   * The cached value of the '{@link #getIndex() <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIndex()
   * @generated
   * @ordered
   */
  protected IndexSpec index;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RedirectWithParamSpecImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getRedirectWithParamSpec();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParamSpec getParam()
  {
    return param;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParam(ParamSpec newParam, NotificationChain msgs)
  {
    ParamSpec oldParam = param;
    param = newParam;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_PARAM_SPEC__PARAM, oldParam, newParam);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParam(ParamSpec newParam)
  {
    if (newParam != param)
    {
      NotificationChain msgs = null;
      if (param != null)
        msgs = ((InternalEObject)param).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_PARAM_SPEC__PARAM, null, msgs);
      if (newParam != null)
        msgs = ((InternalEObject)newParam).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_PARAM_SPEC__PARAM, null, msgs);
      msgs = basicSetParam(newParam, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_PARAM_SPEC__PARAM, newParam, newParam));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SenderSpec getSender()
  {
    return sender;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSender(SenderSpec newSender, NotificationChain msgs)
  {
    SenderSpec oldSender = sender;
    sender = newSender;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_PARAM_SPEC__SENDER, oldSender, newSender);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSender(SenderSpec newSender)
  {
    if (newSender != sender)
    {
      NotificationChain msgs = null;
      if (sender != null)
        msgs = ((InternalEObject)sender).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_PARAM_SPEC__SENDER, null, msgs);
      if (newSender != null)
        msgs = ((InternalEObject)newSender).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_PARAM_SPEC__SENDER, null, msgs);
      msgs = basicSetSender(newSender, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_PARAM_SPEC__SENDER, newSender, newSender));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IndexSpec getIndex()
  {
    return index;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIndex(IndexSpec newIndex, NotificationChain msgs)
  {
    IndexSpec oldIndex = index;
    index = newIndex;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_PARAM_SPEC__INDEX, oldIndex, newIndex);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIndex(IndexSpec newIndex)
  {
    if (newIndex != index)
    {
      NotificationChain msgs = null;
      if (index != null)
        msgs = ((InternalEObject)index).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_PARAM_SPEC__INDEX, null, msgs);
      if (newIndex != null)
        msgs = ((InternalEObject)newIndex).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.REDIRECT_WITH_PARAM_SPEC__INDEX, null, msgs);
      msgs = basicSetIndex(newIndex, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.REDIRECT_WITH_PARAM_SPEC__INDEX, newIndex, newIndex));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__PARAM:
        return basicSetParam(null, msgs);
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__SENDER:
        return basicSetSender(null, msgs);
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__INDEX:
        return basicSetIndex(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__PARAM:
        return getParam();
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__SENDER:
        return getSender();
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__INDEX:
        return getIndex();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__PARAM:
        setParam((ParamSpec)newValue);
        return;
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__SENDER:
        setSender((SenderSpec)newValue);
        return;
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__INDEX:
        setIndex((IndexSpec)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__PARAM:
        setParam((ParamSpec)null);
        return;
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__SENDER:
        setSender((SenderSpec)null);
        return;
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__INDEX:
        setIndex((IndexSpec)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__PARAM:
        return param != null;
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__SENDER:
        return sender != null;
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC__INDEX:
        return index != null;
    }
    return super.eIsSet(featureID);
  }

} //RedirectWithParamSpecImpl
