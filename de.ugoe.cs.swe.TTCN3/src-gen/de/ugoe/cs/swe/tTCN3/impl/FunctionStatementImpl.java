/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.BasicStatements;
import de.ugoe.cs.swe.tTCN3.BehaviourStatements;
import de.ugoe.cs.swe.tTCN3.CommunicationStatements;
import de.ugoe.cs.swe.tTCN3.ConfigurationStatements;
import de.ugoe.cs.swe.tTCN3.FunctionStatement;
import de.ugoe.cs.swe.tTCN3.SUTStatements;
import de.ugoe.cs.swe.tTCN3.SetLocalVerdict;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TestcaseOperation;
import de.ugoe.cs.swe.tTCN3.TimerStatements;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementImpl#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementImpl#getBasic <em>Basic</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementImpl#getBehavior <em>Behavior</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementImpl#getVerdict <em>Verdict</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementImpl#getCommunication <em>Communication</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementImpl#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementImpl#getSut <em>Sut</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionStatementImpl#getTest <em>Test</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionStatementImpl extends MinimalEObjectImpl.Container implements FunctionStatement
{
  /**
   * The cached value of the '{@link #getTimer() <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimer()
   * @generated
   * @ordered
   */
  protected TimerStatements timer;

  /**
   * The cached value of the '{@link #getBasic() <em>Basic</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBasic()
   * @generated
   * @ordered
   */
  protected BasicStatements basic;

  /**
   * The cached value of the '{@link #getBehavior() <em>Behavior</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBehavior()
   * @generated
   * @ordered
   */
  protected BehaviourStatements behavior;

  /**
   * The cached value of the '{@link #getVerdict() <em>Verdict</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVerdict()
   * @generated
   * @ordered
   */
  protected SetLocalVerdict verdict;

  /**
   * The cached value of the '{@link #getCommunication() <em>Communication</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommunication()
   * @generated
   * @ordered
   */
  protected CommunicationStatements communication;

  /**
   * The cached value of the '{@link #getConfiguration() <em>Configuration</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfiguration()
   * @generated
   * @ordered
   */
  protected ConfigurationStatements configuration;

  /**
   * The cached value of the '{@link #getSut() <em>Sut</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSut()
   * @generated
   * @ordered
   */
  protected SUTStatements sut;

  /**
   * The cached value of the '{@link #getTest() <em>Test</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTest()
   * @generated
   * @ordered
   */
  protected TestcaseOperation test;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFunctionStatement();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerStatements getTimer()
  {
    return timer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTimer(TimerStatements newTimer, NotificationChain msgs)
  {
    TimerStatements oldTimer = timer;
    timer = newTimer;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__TIMER, oldTimer, newTimer);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimer(TimerStatements newTimer)
  {
    if (newTimer != timer)
    {
      NotificationChain msgs = null;
      if (timer != null)
        msgs = ((InternalEObject)timer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__TIMER, null, msgs);
      if (newTimer != null)
        msgs = ((InternalEObject)newTimer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__TIMER, null, msgs);
      msgs = basicSetTimer(newTimer, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__TIMER, newTimer, newTimer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BasicStatements getBasic()
  {
    return basic;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBasic(BasicStatements newBasic, NotificationChain msgs)
  {
    BasicStatements oldBasic = basic;
    basic = newBasic;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__BASIC, oldBasic, newBasic);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBasic(BasicStatements newBasic)
  {
    if (newBasic != basic)
    {
      NotificationChain msgs = null;
      if (basic != null)
        msgs = ((InternalEObject)basic).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__BASIC, null, msgs);
      if (newBasic != null)
        msgs = ((InternalEObject)newBasic).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__BASIC, null, msgs);
      msgs = basicSetBasic(newBasic, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__BASIC, newBasic, newBasic));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BehaviourStatements getBehavior()
  {
    return behavior;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBehavior(BehaviourStatements newBehavior, NotificationChain msgs)
  {
    BehaviourStatements oldBehavior = behavior;
    behavior = newBehavior;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__BEHAVIOR, oldBehavior, newBehavior);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBehavior(BehaviourStatements newBehavior)
  {
    if (newBehavior != behavior)
    {
      NotificationChain msgs = null;
      if (behavior != null)
        msgs = ((InternalEObject)behavior).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__BEHAVIOR, null, msgs);
      if (newBehavior != null)
        msgs = ((InternalEObject)newBehavior).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__BEHAVIOR, null, msgs);
      msgs = basicSetBehavior(newBehavior, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__BEHAVIOR, newBehavior, newBehavior));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SetLocalVerdict getVerdict()
  {
    return verdict;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVerdict(SetLocalVerdict newVerdict, NotificationChain msgs)
  {
    SetLocalVerdict oldVerdict = verdict;
    verdict = newVerdict;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__VERDICT, oldVerdict, newVerdict);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVerdict(SetLocalVerdict newVerdict)
  {
    if (newVerdict != verdict)
    {
      NotificationChain msgs = null;
      if (verdict != null)
        msgs = ((InternalEObject)verdict).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__VERDICT, null, msgs);
      if (newVerdict != null)
        msgs = ((InternalEObject)newVerdict).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__VERDICT, null, msgs);
      msgs = basicSetVerdict(newVerdict, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__VERDICT, newVerdict, newVerdict));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommunicationStatements getCommunication()
  {
    return communication;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCommunication(CommunicationStatements newCommunication, NotificationChain msgs)
  {
    CommunicationStatements oldCommunication = communication;
    communication = newCommunication;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__COMMUNICATION, oldCommunication, newCommunication);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCommunication(CommunicationStatements newCommunication)
  {
    if (newCommunication != communication)
    {
      NotificationChain msgs = null;
      if (communication != null)
        msgs = ((InternalEObject)communication).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__COMMUNICATION, null, msgs);
      if (newCommunication != null)
        msgs = ((InternalEObject)newCommunication).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__COMMUNICATION, null, msgs);
      msgs = basicSetCommunication(newCommunication, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__COMMUNICATION, newCommunication, newCommunication));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConfigurationStatements getConfiguration()
  {
    return configuration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConfiguration(ConfigurationStatements newConfiguration, NotificationChain msgs)
  {
    ConfigurationStatements oldConfiguration = configuration;
    configuration = newConfiguration;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__CONFIGURATION, oldConfiguration, newConfiguration);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConfiguration(ConfigurationStatements newConfiguration)
  {
    if (newConfiguration != configuration)
    {
      NotificationChain msgs = null;
      if (configuration != null)
        msgs = ((InternalEObject)configuration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__CONFIGURATION, null, msgs);
      if (newConfiguration != null)
        msgs = ((InternalEObject)newConfiguration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__CONFIGURATION, null, msgs);
      msgs = basicSetConfiguration(newConfiguration, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__CONFIGURATION, newConfiguration, newConfiguration));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SUTStatements getSut()
  {
    return sut;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSut(SUTStatements newSut, NotificationChain msgs)
  {
    SUTStatements oldSut = sut;
    sut = newSut;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__SUT, oldSut, newSut);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSut(SUTStatements newSut)
  {
    if (newSut != sut)
    {
      NotificationChain msgs = null;
      if (sut != null)
        msgs = ((InternalEObject)sut).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__SUT, null, msgs);
      if (newSut != null)
        msgs = ((InternalEObject)newSut).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__SUT, null, msgs);
      msgs = basicSetSut(newSut, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__SUT, newSut, newSut));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TestcaseOperation getTest()
  {
    return test;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTest(TestcaseOperation newTest, NotificationChain msgs)
  {
    TestcaseOperation oldTest = test;
    test = newTest;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__TEST, oldTest, newTest);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTest(TestcaseOperation newTest)
  {
    if (newTest != test)
    {
      NotificationChain msgs = null;
      if (test != null)
        msgs = ((InternalEObject)test).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__TEST, null, msgs);
      if (newTest != null)
        msgs = ((InternalEObject)newTest).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_STATEMENT__TEST, null, msgs);
      msgs = basicSetTest(newTest, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_STATEMENT__TEST, newTest, newTest));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_STATEMENT__TIMER:
        return basicSetTimer(null, msgs);
      case TTCN3Package.FUNCTION_STATEMENT__BASIC:
        return basicSetBasic(null, msgs);
      case TTCN3Package.FUNCTION_STATEMENT__BEHAVIOR:
        return basicSetBehavior(null, msgs);
      case TTCN3Package.FUNCTION_STATEMENT__VERDICT:
        return basicSetVerdict(null, msgs);
      case TTCN3Package.FUNCTION_STATEMENT__COMMUNICATION:
        return basicSetCommunication(null, msgs);
      case TTCN3Package.FUNCTION_STATEMENT__CONFIGURATION:
        return basicSetConfiguration(null, msgs);
      case TTCN3Package.FUNCTION_STATEMENT__SUT:
        return basicSetSut(null, msgs);
      case TTCN3Package.FUNCTION_STATEMENT__TEST:
        return basicSetTest(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_STATEMENT__TIMER:
        return getTimer();
      case TTCN3Package.FUNCTION_STATEMENT__BASIC:
        return getBasic();
      case TTCN3Package.FUNCTION_STATEMENT__BEHAVIOR:
        return getBehavior();
      case TTCN3Package.FUNCTION_STATEMENT__VERDICT:
        return getVerdict();
      case TTCN3Package.FUNCTION_STATEMENT__COMMUNICATION:
        return getCommunication();
      case TTCN3Package.FUNCTION_STATEMENT__CONFIGURATION:
        return getConfiguration();
      case TTCN3Package.FUNCTION_STATEMENT__SUT:
        return getSut();
      case TTCN3Package.FUNCTION_STATEMENT__TEST:
        return getTest();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_STATEMENT__TIMER:
        setTimer((TimerStatements)newValue);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__BASIC:
        setBasic((BasicStatements)newValue);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__BEHAVIOR:
        setBehavior((BehaviourStatements)newValue);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__VERDICT:
        setVerdict((SetLocalVerdict)newValue);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__COMMUNICATION:
        setCommunication((CommunicationStatements)newValue);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__CONFIGURATION:
        setConfiguration((ConfigurationStatements)newValue);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__SUT:
        setSut((SUTStatements)newValue);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__TEST:
        setTest((TestcaseOperation)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_STATEMENT__TIMER:
        setTimer((TimerStatements)null);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__BASIC:
        setBasic((BasicStatements)null);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__BEHAVIOR:
        setBehavior((BehaviourStatements)null);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__VERDICT:
        setVerdict((SetLocalVerdict)null);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__COMMUNICATION:
        setCommunication((CommunicationStatements)null);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__CONFIGURATION:
        setConfiguration((ConfigurationStatements)null);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__SUT:
        setSut((SUTStatements)null);
        return;
      case TTCN3Package.FUNCTION_STATEMENT__TEST:
        setTest((TestcaseOperation)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_STATEMENT__TIMER:
        return timer != null;
      case TTCN3Package.FUNCTION_STATEMENT__BASIC:
        return basic != null;
      case TTCN3Package.FUNCTION_STATEMENT__BEHAVIOR:
        return behavior != null;
      case TTCN3Package.FUNCTION_STATEMENT__VERDICT:
        return verdict != null;
      case TTCN3Package.FUNCTION_STATEMENT__COMMUNICATION:
        return communication != null;
      case TTCN3Package.FUNCTION_STATEMENT__CONFIGURATION:
        return configuration != null;
      case TTCN3Package.FUNCTION_STATEMENT__SUT:
        return sut != null;
      case TTCN3Package.FUNCTION_STATEMENT__TEST:
        return test != null;
    }
    return super.eIsSet(featureID);
  }

} //FunctionStatementImpl
