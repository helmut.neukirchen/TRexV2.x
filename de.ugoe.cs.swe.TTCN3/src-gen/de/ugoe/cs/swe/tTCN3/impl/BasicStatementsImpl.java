/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.Assignment;
import de.ugoe.cs.swe.tTCN3.BasicStatements;
import de.ugoe.cs.swe.tTCN3.ConditionalConstruct;
import de.ugoe.cs.swe.tTCN3.LogStatement;
import de.ugoe.cs.swe.tTCN3.LoopConstruct;
import de.ugoe.cs.swe.tTCN3.SelectCaseConstruct;
import de.ugoe.cs.swe.tTCN3.StatementBlock;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Basic Statements</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BasicStatementsImpl#getLog <em>Log</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BasicStatementsImpl#getBlock <em>Block</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BasicStatementsImpl#getLoop <em>Loop</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BasicStatementsImpl#getConditional <em>Conditional</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BasicStatementsImpl#getSelect <em>Select</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BasicStatementsImpl#getAssign <em>Assign</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BasicStatementsImpl extends MinimalEObjectImpl.Container implements BasicStatements
{
  /**
   * The cached value of the '{@link #getLog() <em>Log</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLog()
   * @generated
   * @ordered
   */
  protected LogStatement log;

  /**
   * The cached value of the '{@link #getBlock() <em>Block</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBlock()
   * @generated
   * @ordered
   */
  protected StatementBlock block;

  /**
   * The cached value of the '{@link #getLoop() <em>Loop</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoop()
   * @generated
   * @ordered
   */
  protected LoopConstruct loop;

  /**
   * The cached value of the '{@link #getConditional() <em>Conditional</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConditional()
   * @generated
   * @ordered
   */
  protected ConditionalConstruct conditional;

  /**
   * The cached value of the '{@link #getSelect() <em>Select</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSelect()
   * @generated
   * @ordered
   */
  protected SelectCaseConstruct select;

  /**
   * The cached value of the '{@link #getAssign() <em>Assign</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssign()
   * @generated
   * @ordered
   */
  protected Assignment assign;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BasicStatementsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getBasicStatements();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LogStatement getLog()
  {
    return log;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLog(LogStatement newLog, NotificationChain msgs)
  {
    LogStatement oldLog = log;
    log = newLog;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__LOG, oldLog, newLog);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLog(LogStatement newLog)
  {
    if (newLog != log)
    {
      NotificationChain msgs = null;
      if (log != null)
        msgs = ((InternalEObject)log).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__LOG, null, msgs);
      if (newLog != null)
        msgs = ((InternalEObject)newLog).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__LOG, null, msgs);
      msgs = basicSetLog(newLog, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__LOG, newLog, newLog));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementBlock getBlock()
  {
    return block;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBlock(StatementBlock newBlock, NotificationChain msgs)
  {
    StatementBlock oldBlock = block;
    block = newBlock;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__BLOCK, oldBlock, newBlock);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBlock(StatementBlock newBlock)
  {
    if (newBlock != block)
    {
      NotificationChain msgs = null;
      if (block != null)
        msgs = ((InternalEObject)block).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__BLOCK, null, msgs);
      if (newBlock != null)
        msgs = ((InternalEObject)newBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__BLOCK, null, msgs);
      msgs = basicSetBlock(newBlock, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__BLOCK, newBlock, newBlock));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LoopConstruct getLoop()
  {
    return loop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLoop(LoopConstruct newLoop, NotificationChain msgs)
  {
    LoopConstruct oldLoop = loop;
    loop = newLoop;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__LOOP, oldLoop, newLoop);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLoop(LoopConstruct newLoop)
  {
    if (newLoop != loop)
    {
      NotificationChain msgs = null;
      if (loop != null)
        msgs = ((InternalEObject)loop).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__LOOP, null, msgs);
      if (newLoop != null)
        msgs = ((InternalEObject)newLoop).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__LOOP, null, msgs);
      msgs = basicSetLoop(newLoop, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__LOOP, newLoop, newLoop));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConditionalConstruct getConditional()
  {
    return conditional;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConditional(ConditionalConstruct newConditional, NotificationChain msgs)
  {
    ConditionalConstruct oldConditional = conditional;
    conditional = newConditional;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__CONDITIONAL, oldConditional, newConditional);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConditional(ConditionalConstruct newConditional)
  {
    if (newConditional != conditional)
    {
      NotificationChain msgs = null;
      if (conditional != null)
        msgs = ((InternalEObject)conditional).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__CONDITIONAL, null, msgs);
      if (newConditional != null)
        msgs = ((InternalEObject)newConditional).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__CONDITIONAL, null, msgs);
      msgs = basicSetConditional(newConditional, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__CONDITIONAL, newConditional, newConditional));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectCaseConstruct getSelect()
  {
    return select;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSelect(SelectCaseConstruct newSelect, NotificationChain msgs)
  {
    SelectCaseConstruct oldSelect = select;
    select = newSelect;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__SELECT, oldSelect, newSelect);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSelect(SelectCaseConstruct newSelect)
  {
    if (newSelect != select)
    {
      NotificationChain msgs = null;
      if (select != null)
        msgs = ((InternalEObject)select).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__SELECT, null, msgs);
      if (newSelect != null)
        msgs = ((InternalEObject)newSelect).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__SELECT, null, msgs);
      msgs = basicSetSelect(newSelect, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__SELECT, newSelect, newSelect));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Assignment getAssign()
  {
    return assign;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAssign(Assignment newAssign, NotificationChain msgs)
  {
    Assignment oldAssign = assign;
    assign = newAssign;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__ASSIGN, oldAssign, newAssign);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAssign(Assignment newAssign)
  {
    if (newAssign != assign)
    {
      NotificationChain msgs = null;
      if (assign != null)
        msgs = ((InternalEObject)assign).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__ASSIGN, null, msgs);
      if (newAssign != null)
        msgs = ((InternalEObject)newAssign).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BASIC_STATEMENTS__ASSIGN, null, msgs);
      msgs = basicSetAssign(newAssign, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BASIC_STATEMENTS__ASSIGN, newAssign, newAssign));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.BASIC_STATEMENTS__LOG:
        return basicSetLog(null, msgs);
      case TTCN3Package.BASIC_STATEMENTS__BLOCK:
        return basicSetBlock(null, msgs);
      case TTCN3Package.BASIC_STATEMENTS__LOOP:
        return basicSetLoop(null, msgs);
      case TTCN3Package.BASIC_STATEMENTS__CONDITIONAL:
        return basicSetConditional(null, msgs);
      case TTCN3Package.BASIC_STATEMENTS__SELECT:
        return basicSetSelect(null, msgs);
      case TTCN3Package.BASIC_STATEMENTS__ASSIGN:
        return basicSetAssign(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.BASIC_STATEMENTS__LOG:
        return getLog();
      case TTCN3Package.BASIC_STATEMENTS__BLOCK:
        return getBlock();
      case TTCN3Package.BASIC_STATEMENTS__LOOP:
        return getLoop();
      case TTCN3Package.BASIC_STATEMENTS__CONDITIONAL:
        return getConditional();
      case TTCN3Package.BASIC_STATEMENTS__SELECT:
        return getSelect();
      case TTCN3Package.BASIC_STATEMENTS__ASSIGN:
        return getAssign();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.BASIC_STATEMENTS__LOG:
        setLog((LogStatement)newValue);
        return;
      case TTCN3Package.BASIC_STATEMENTS__BLOCK:
        setBlock((StatementBlock)newValue);
        return;
      case TTCN3Package.BASIC_STATEMENTS__LOOP:
        setLoop((LoopConstruct)newValue);
        return;
      case TTCN3Package.BASIC_STATEMENTS__CONDITIONAL:
        setConditional((ConditionalConstruct)newValue);
        return;
      case TTCN3Package.BASIC_STATEMENTS__SELECT:
        setSelect((SelectCaseConstruct)newValue);
        return;
      case TTCN3Package.BASIC_STATEMENTS__ASSIGN:
        setAssign((Assignment)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.BASIC_STATEMENTS__LOG:
        setLog((LogStatement)null);
        return;
      case TTCN3Package.BASIC_STATEMENTS__BLOCK:
        setBlock((StatementBlock)null);
        return;
      case TTCN3Package.BASIC_STATEMENTS__LOOP:
        setLoop((LoopConstruct)null);
        return;
      case TTCN3Package.BASIC_STATEMENTS__CONDITIONAL:
        setConditional((ConditionalConstruct)null);
        return;
      case TTCN3Package.BASIC_STATEMENTS__SELECT:
        setSelect((SelectCaseConstruct)null);
        return;
      case TTCN3Package.BASIC_STATEMENTS__ASSIGN:
        setAssign((Assignment)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.BASIC_STATEMENTS__LOG:
        return log != null;
      case TTCN3Package.BASIC_STATEMENTS__BLOCK:
        return block != null;
      case TTCN3Package.BASIC_STATEMENTS__LOOP:
        return loop != null;
      case TTCN3Package.BASIC_STATEMENTS__CONDITIONAL:
        return conditional != null;
      case TTCN3Package.BASIC_STATEMENTS__SELECT:
        return select != null;
      case TTCN3Package.BASIC_STATEMENTS__ASSIGN:
        return assign != null;
    }
    return super.eIsSet(featureID);
  }

} //BasicStatementsImpl
