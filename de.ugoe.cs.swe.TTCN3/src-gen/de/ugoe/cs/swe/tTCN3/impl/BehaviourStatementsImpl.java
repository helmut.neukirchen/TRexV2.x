/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ActivateOp;
import de.ugoe.cs.swe.tTCN3.AltConstruct;
import de.ugoe.cs.swe.tTCN3.AltstepInstance;
import de.ugoe.cs.swe.tTCN3.BehaviourStatements;
import de.ugoe.cs.swe.tTCN3.DeactivateStatement;
import de.ugoe.cs.swe.tTCN3.FunctionInstance;
import de.ugoe.cs.swe.tTCN3.GotoStatement;
import de.ugoe.cs.swe.tTCN3.InterleavedConstruct;
import de.ugoe.cs.swe.tTCN3.LabelStatement;
import de.ugoe.cs.swe.tTCN3.ReturnStatement;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TestcaseInstance;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Behaviour Statements</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl#getTestcase <em>Testcase</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl#getFunction <em>Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl#getReturn <em>Return</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl#getAlt <em>Alt</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl#getInterleaved <em>Interleaved</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl#getGoto <em>Goto</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl#getDeactivate <em>Deactivate</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl#getAltstep <em>Altstep</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.BehaviourStatementsImpl#getActivate <em>Activate</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BehaviourStatementsImpl extends MinimalEObjectImpl.Container implements BehaviourStatements
{
  /**
   * The cached value of the '{@link #getTestcase() <em>Testcase</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTestcase()
   * @generated
   * @ordered
   */
  protected TestcaseInstance testcase;

  /**
   * The cached value of the '{@link #getFunction() <em>Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunction()
   * @generated
   * @ordered
   */
  protected FunctionInstance function;

  /**
   * The cached value of the '{@link #getReturn() <em>Return</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReturn()
   * @generated
   * @ordered
   */
  protected ReturnStatement return_;

  /**
   * The cached value of the '{@link #getAlt() <em>Alt</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAlt()
   * @generated
   * @ordered
   */
  protected AltConstruct alt;

  /**
   * The cached value of the '{@link #getInterleaved() <em>Interleaved</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInterleaved()
   * @generated
   * @ordered
   */
  protected InterleavedConstruct interleaved;

  /**
   * The cached value of the '{@link #getLabel() <em>Label</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabel()
   * @generated
   * @ordered
   */
  protected LabelStatement label;

  /**
   * The cached value of the '{@link #getGoto() <em>Goto</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGoto()
   * @generated
   * @ordered
   */
  protected GotoStatement goto_;

  /**
   * The cached value of the '{@link #getDeactivate() <em>Deactivate</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDeactivate()
   * @generated
   * @ordered
   */
  protected DeactivateStatement deactivate;

  /**
   * The cached value of the '{@link #getAltstep() <em>Altstep</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAltstep()
   * @generated
   * @ordered
   */
  protected AltstepInstance altstep;

  /**
   * The cached value of the '{@link #getActivate() <em>Activate</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getActivate()
   * @generated
   * @ordered
   */
  protected ActivateOp activate;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BehaviourStatementsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getBehaviourStatements();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TestcaseInstance getTestcase()
  {
    return testcase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTestcase(TestcaseInstance newTestcase, NotificationChain msgs)
  {
    TestcaseInstance oldTestcase = testcase;
    testcase = newTestcase;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__TESTCASE, oldTestcase, newTestcase);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTestcase(TestcaseInstance newTestcase)
  {
    if (newTestcase != testcase)
    {
      NotificationChain msgs = null;
      if (testcase != null)
        msgs = ((InternalEObject)testcase).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__TESTCASE, null, msgs);
      if (newTestcase != null)
        msgs = ((InternalEObject)newTestcase).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__TESTCASE, null, msgs);
      msgs = basicSetTestcase(newTestcase, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__TESTCASE, newTestcase, newTestcase));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionInstance getFunction()
  {
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFunction(FunctionInstance newFunction, NotificationChain msgs)
  {
    FunctionInstance oldFunction = function;
    function = newFunction;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__FUNCTION, oldFunction, newFunction);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFunction(FunctionInstance newFunction)
  {
    if (newFunction != function)
    {
      NotificationChain msgs = null;
      if (function != null)
        msgs = ((InternalEObject)function).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__FUNCTION, null, msgs);
      if (newFunction != null)
        msgs = ((InternalEObject)newFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__FUNCTION, null, msgs);
      msgs = basicSetFunction(newFunction, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__FUNCTION, newFunction, newFunction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReturnStatement getReturn()
  {
    return return_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReturn(ReturnStatement newReturn, NotificationChain msgs)
  {
    ReturnStatement oldReturn = return_;
    return_ = newReturn;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__RETURN, oldReturn, newReturn);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReturn(ReturnStatement newReturn)
  {
    if (newReturn != return_)
    {
      NotificationChain msgs = null;
      if (return_ != null)
        msgs = ((InternalEObject)return_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__RETURN, null, msgs);
      if (newReturn != null)
        msgs = ((InternalEObject)newReturn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__RETURN, null, msgs);
      msgs = basicSetReturn(newReturn, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__RETURN, newReturn, newReturn));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltConstruct getAlt()
  {
    return alt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAlt(AltConstruct newAlt, NotificationChain msgs)
  {
    AltConstruct oldAlt = alt;
    alt = newAlt;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__ALT, oldAlt, newAlt);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAlt(AltConstruct newAlt)
  {
    if (newAlt != alt)
    {
      NotificationChain msgs = null;
      if (alt != null)
        msgs = ((InternalEObject)alt).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__ALT, null, msgs);
      if (newAlt != null)
        msgs = ((InternalEObject)newAlt).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__ALT, null, msgs);
      msgs = basicSetAlt(newAlt, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__ALT, newAlt, newAlt));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InterleavedConstruct getInterleaved()
  {
    return interleaved;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetInterleaved(InterleavedConstruct newInterleaved, NotificationChain msgs)
  {
    InterleavedConstruct oldInterleaved = interleaved;
    interleaved = newInterleaved;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__INTERLEAVED, oldInterleaved, newInterleaved);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInterleaved(InterleavedConstruct newInterleaved)
  {
    if (newInterleaved != interleaved)
    {
      NotificationChain msgs = null;
      if (interleaved != null)
        msgs = ((InternalEObject)interleaved).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__INTERLEAVED, null, msgs);
      if (newInterleaved != null)
        msgs = ((InternalEObject)newInterleaved).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__INTERLEAVED, null, msgs);
      msgs = basicSetInterleaved(newInterleaved, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__INTERLEAVED, newInterleaved, newInterleaved));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LabelStatement getLabel()
  {
    return label;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLabel(LabelStatement newLabel, NotificationChain msgs)
  {
    LabelStatement oldLabel = label;
    label = newLabel;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__LABEL, oldLabel, newLabel);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLabel(LabelStatement newLabel)
  {
    if (newLabel != label)
    {
      NotificationChain msgs = null;
      if (label != null)
        msgs = ((InternalEObject)label).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__LABEL, null, msgs);
      if (newLabel != null)
        msgs = ((InternalEObject)newLabel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__LABEL, null, msgs);
      msgs = basicSetLabel(newLabel, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__LABEL, newLabel, newLabel));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GotoStatement getGoto()
  {
    return goto_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGoto(GotoStatement newGoto, NotificationChain msgs)
  {
    GotoStatement oldGoto = goto_;
    goto_ = newGoto;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__GOTO, oldGoto, newGoto);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGoto(GotoStatement newGoto)
  {
    if (newGoto != goto_)
    {
      NotificationChain msgs = null;
      if (goto_ != null)
        msgs = ((InternalEObject)goto_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__GOTO, null, msgs);
      if (newGoto != null)
        msgs = ((InternalEObject)newGoto).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__GOTO, null, msgs);
      msgs = basicSetGoto(newGoto, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__GOTO, newGoto, newGoto));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DeactivateStatement getDeactivate()
  {
    return deactivate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDeactivate(DeactivateStatement newDeactivate, NotificationChain msgs)
  {
    DeactivateStatement oldDeactivate = deactivate;
    deactivate = newDeactivate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__DEACTIVATE, oldDeactivate, newDeactivate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDeactivate(DeactivateStatement newDeactivate)
  {
    if (newDeactivate != deactivate)
    {
      NotificationChain msgs = null;
      if (deactivate != null)
        msgs = ((InternalEObject)deactivate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__DEACTIVATE, null, msgs);
      if (newDeactivate != null)
        msgs = ((InternalEObject)newDeactivate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__DEACTIVATE, null, msgs);
      msgs = basicSetDeactivate(newDeactivate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__DEACTIVATE, newDeactivate, newDeactivate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltstepInstance getAltstep()
  {
    return altstep;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAltstep(AltstepInstance newAltstep, NotificationChain msgs)
  {
    AltstepInstance oldAltstep = altstep;
    altstep = newAltstep;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__ALTSTEP, oldAltstep, newAltstep);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAltstep(AltstepInstance newAltstep)
  {
    if (newAltstep != altstep)
    {
      NotificationChain msgs = null;
      if (altstep != null)
        msgs = ((InternalEObject)altstep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__ALTSTEP, null, msgs);
      if (newAltstep != null)
        msgs = ((InternalEObject)newAltstep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__ALTSTEP, null, msgs);
      msgs = basicSetAltstep(newAltstep, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__ALTSTEP, newAltstep, newAltstep));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActivateOp getActivate()
  {
    return activate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetActivate(ActivateOp newActivate, NotificationChain msgs)
  {
    ActivateOp oldActivate = activate;
    activate = newActivate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__ACTIVATE, oldActivate, newActivate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setActivate(ActivateOp newActivate)
  {
    if (newActivate != activate)
    {
      NotificationChain msgs = null;
      if (activate != null)
        msgs = ((InternalEObject)activate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__ACTIVATE, null, msgs);
      if (newActivate != null)
        msgs = ((InternalEObject)newActivate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.BEHAVIOUR_STATEMENTS__ACTIVATE, null, msgs);
      msgs = basicSetActivate(newActivate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.BEHAVIOUR_STATEMENTS__ACTIVATE, newActivate, newActivate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.BEHAVIOUR_STATEMENTS__TESTCASE:
        return basicSetTestcase(null, msgs);
      case TTCN3Package.BEHAVIOUR_STATEMENTS__FUNCTION:
        return basicSetFunction(null, msgs);
      case TTCN3Package.BEHAVIOUR_STATEMENTS__RETURN:
        return basicSetReturn(null, msgs);
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ALT:
        return basicSetAlt(null, msgs);
      case TTCN3Package.BEHAVIOUR_STATEMENTS__INTERLEAVED:
        return basicSetInterleaved(null, msgs);
      case TTCN3Package.BEHAVIOUR_STATEMENTS__LABEL:
        return basicSetLabel(null, msgs);
      case TTCN3Package.BEHAVIOUR_STATEMENTS__GOTO:
        return basicSetGoto(null, msgs);
      case TTCN3Package.BEHAVIOUR_STATEMENTS__DEACTIVATE:
        return basicSetDeactivate(null, msgs);
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ALTSTEP:
        return basicSetAltstep(null, msgs);
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ACTIVATE:
        return basicSetActivate(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.BEHAVIOUR_STATEMENTS__TESTCASE:
        return getTestcase();
      case TTCN3Package.BEHAVIOUR_STATEMENTS__FUNCTION:
        return getFunction();
      case TTCN3Package.BEHAVIOUR_STATEMENTS__RETURN:
        return getReturn();
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ALT:
        return getAlt();
      case TTCN3Package.BEHAVIOUR_STATEMENTS__INTERLEAVED:
        return getInterleaved();
      case TTCN3Package.BEHAVIOUR_STATEMENTS__LABEL:
        return getLabel();
      case TTCN3Package.BEHAVIOUR_STATEMENTS__GOTO:
        return getGoto();
      case TTCN3Package.BEHAVIOUR_STATEMENTS__DEACTIVATE:
        return getDeactivate();
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ALTSTEP:
        return getAltstep();
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ACTIVATE:
        return getActivate();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.BEHAVIOUR_STATEMENTS__TESTCASE:
        setTestcase((TestcaseInstance)newValue);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__FUNCTION:
        setFunction((FunctionInstance)newValue);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__RETURN:
        setReturn((ReturnStatement)newValue);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ALT:
        setAlt((AltConstruct)newValue);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__INTERLEAVED:
        setInterleaved((InterleavedConstruct)newValue);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__LABEL:
        setLabel((LabelStatement)newValue);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__GOTO:
        setGoto((GotoStatement)newValue);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__DEACTIVATE:
        setDeactivate((DeactivateStatement)newValue);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ALTSTEP:
        setAltstep((AltstepInstance)newValue);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ACTIVATE:
        setActivate((ActivateOp)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.BEHAVIOUR_STATEMENTS__TESTCASE:
        setTestcase((TestcaseInstance)null);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__FUNCTION:
        setFunction((FunctionInstance)null);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__RETURN:
        setReturn((ReturnStatement)null);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ALT:
        setAlt((AltConstruct)null);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__INTERLEAVED:
        setInterleaved((InterleavedConstruct)null);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__LABEL:
        setLabel((LabelStatement)null);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__GOTO:
        setGoto((GotoStatement)null);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__DEACTIVATE:
        setDeactivate((DeactivateStatement)null);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ALTSTEP:
        setAltstep((AltstepInstance)null);
        return;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ACTIVATE:
        setActivate((ActivateOp)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.BEHAVIOUR_STATEMENTS__TESTCASE:
        return testcase != null;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__FUNCTION:
        return function != null;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__RETURN:
        return return_ != null;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ALT:
        return alt != null;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__INTERLEAVED:
        return interleaved != null;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__LABEL:
        return label != null;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__GOTO:
        return goto_ != null;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__DEACTIVATE:
        return deactivate != null;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ALTSTEP:
        return altstep != null;
      case TTCN3Package.BEHAVIOUR_STATEMENTS__ACTIVATE:
        return activate != null;
    }
    return super.eIsSet(featureID);
  }

} //BehaviourStatementsImpl
