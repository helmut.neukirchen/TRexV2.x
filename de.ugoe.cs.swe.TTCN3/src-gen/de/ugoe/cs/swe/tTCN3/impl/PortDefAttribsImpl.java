/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.MessageAttribs;
import de.ugoe.cs.swe.tTCN3.MixedAttribs;
import de.ugoe.cs.swe.tTCN3.PortDefAttribs;
import de.ugoe.cs.swe.tTCN3.ProcedureAttribs;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Def Attribs</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortDefAttribsImpl#getMessage <em>Message</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortDefAttribsImpl#getProcedure <em>Procedure</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.PortDefAttribsImpl#getMixed <em>Mixed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortDefAttribsImpl extends MinimalEObjectImpl.Container implements PortDefAttribs
{
  /**
   * The cached value of the '{@link #getMessage() <em>Message</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMessage()
   * @generated
   * @ordered
   */
  protected MessageAttribs message;

  /**
   * The cached value of the '{@link #getProcedure() <em>Procedure</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProcedure()
   * @generated
   * @ordered
   */
  protected ProcedureAttribs procedure;

  /**
   * The cached value of the '{@link #getMixed() <em>Mixed</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMixed()
   * @generated
   * @ordered
   */
  protected MixedAttribs mixed;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PortDefAttribsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getPortDefAttribs();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MessageAttribs getMessage()
  {
    return message;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMessage(MessageAttribs newMessage, NotificationChain msgs)
  {
    MessageAttribs oldMessage = message;
    message = newMessage;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_DEF_ATTRIBS__MESSAGE, oldMessage, newMessage);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMessage(MessageAttribs newMessage)
  {
    if (newMessage != message)
    {
      NotificationChain msgs = null;
      if (message != null)
        msgs = ((InternalEObject)message).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_DEF_ATTRIBS__MESSAGE, null, msgs);
      if (newMessage != null)
        msgs = ((InternalEObject)newMessage).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_DEF_ATTRIBS__MESSAGE, null, msgs);
      msgs = basicSetMessage(newMessage, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_DEF_ATTRIBS__MESSAGE, newMessage, newMessage));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcedureAttribs getProcedure()
  {
    return procedure;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetProcedure(ProcedureAttribs newProcedure, NotificationChain msgs)
  {
    ProcedureAttribs oldProcedure = procedure;
    procedure = newProcedure;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_DEF_ATTRIBS__PROCEDURE, oldProcedure, newProcedure);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setProcedure(ProcedureAttribs newProcedure)
  {
    if (newProcedure != procedure)
    {
      NotificationChain msgs = null;
      if (procedure != null)
        msgs = ((InternalEObject)procedure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_DEF_ATTRIBS__PROCEDURE, null, msgs);
      if (newProcedure != null)
        msgs = ((InternalEObject)newProcedure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_DEF_ATTRIBS__PROCEDURE, null, msgs);
      msgs = basicSetProcedure(newProcedure, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_DEF_ATTRIBS__PROCEDURE, newProcedure, newProcedure));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MixedAttribs getMixed()
  {
    return mixed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMixed(MixedAttribs newMixed, NotificationChain msgs)
  {
    MixedAttribs oldMixed = mixed;
    mixed = newMixed;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_DEF_ATTRIBS__MIXED, oldMixed, newMixed);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMixed(MixedAttribs newMixed)
  {
    if (newMixed != mixed)
    {
      NotificationChain msgs = null;
      if (mixed != null)
        msgs = ((InternalEObject)mixed).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_DEF_ATTRIBS__MIXED, null, msgs);
      if (newMixed != null)
        msgs = ((InternalEObject)newMixed).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.PORT_DEF_ATTRIBS__MIXED, null, msgs);
      msgs = basicSetMixed(newMixed, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.PORT_DEF_ATTRIBS__MIXED, newMixed, newMixed));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_DEF_ATTRIBS__MESSAGE:
        return basicSetMessage(null, msgs);
      case TTCN3Package.PORT_DEF_ATTRIBS__PROCEDURE:
        return basicSetProcedure(null, msgs);
      case TTCN3Package.PORT_DEF_ATTRIBS__MIXED:
        return basicSetMixed(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_DEF_ATTRIBS__MESSAGE:
        return getMessage();
      case TTCN3Package.PORT_DEF_ATTRIBS__PROCEDURE:
        return getProcedure();
      case TTCN3Package.PORT_DEF_ATTRIBS__MIXED:
        return getMixed();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_DEF_ATTRIBS__MESSAGE:
        setMessage((MessageAttribs)newValue);
        return;
      case TTCN3Package.PORT_DEF_ATTRIBS__PROCEDURE:
        setProcedure((ProcedureAttribs)newValue);
        return;
      case TTCN3Package.PORT_DEF_ATTRIBS__MIXED:
        setMixed((MixedAttribs)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_DEF_ATTRIBS__MESSAGE:
        setMessage((MessageAttribs)null);
        return;
      case TTCN3Package.PORT_DEF_ATTRIBS__PROCEDURE:
        setProcedure((ProcedureAttribs)null);
        return;
      case TTCN3Package.PORT_DEF_ATTRIBS__MIXED:
        setMixed((MixedAttribs)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.PORT_DEF_ATTRIBS__MESSAGE:
        return message != null;
      case TTCN3Package.PORT_DEF_ATTRIBS__PROCEDURE:
        return procedure != null;
      case TTCN3Package.PORT_DEF_ATTRIBS__MIXED:
        return mixed != null;
    }
    return super.eIsSet(featureID);
  }

} //PortDefAttribsImpl
