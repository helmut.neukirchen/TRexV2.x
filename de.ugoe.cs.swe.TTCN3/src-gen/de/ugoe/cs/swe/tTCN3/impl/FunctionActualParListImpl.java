/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.FunctionActualPar;
import de.ugoe.cs.swe.tTCN3.FunctionActualParAssignment;
import de.ugoe.cs.swe.tTCN3.FunctionActualParList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Actual Par List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionActualParListImpl#getParams <em>Params</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionActualParListImpl#getAsssign <em>Asssign</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionActualParListImpl extends MinimalEObjectImpl.Container implements FunctionActualParList
{
  /**
   * The cached value of the '{@link #getParams() <em>Params</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParams()
   * @generated
   * @ordered
   */
  protected EList<FunctionActualPar> params;

  /**
   * The cached value of the '{@link #getAsssign() <em>Asssign</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAsssign()
   * @generated
   * @ordered
   */
  protected EList<FunctionActualParAssignment> asssign;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionActualParListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFunctionActualParList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FunctionActualPar> getParams()
  {
    if (params == null)
    {
      params = new EObjectContainmentEList<FunctionActualPar>(FunctionActualPar.class, this, TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__PARAMS);
    }
    return params;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FunctionActualParAssignment> getAsssign()
  {
    if (asssign == null)
    {
      asssign = new EObjectContainmentEList<FunctionActualParAssignment>(FunctionActualParAssignment.class, this, TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__ASSSIGN);
    }
    return asssign;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__PARAMS:
        return ((InternalEList<?>)getParams()).basicRemove(otherEnd, msgs);
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__ASSSIGN:
        return ((InternalEList<?>)getAsssign()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__PARAMS:
        return getParams();
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__ASSSIGN:
        return getAsssign();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__PARAMS:
        getParams().clear();
        getParams().addAll((Collection<? extends FunctionActualPar>)newValue);
        return;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__ASSSIGN:
        getAsssign().clear();
        getAsssign().addAll((Collection<? extends FunctionActualParAssignment>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__PARAMS:
        getParams().clear();
        return;
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__ASSSIGN:
        getAsssign().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__PARAMS:
        return params != null && !params.isEmpty();
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST__ASSSIGN:
        return asssign != null && !asssign.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //FunctionActualParListImpl
