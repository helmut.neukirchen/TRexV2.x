/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.TTCN3Factory;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.io.IOException;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TTCN3PackageImpl extends EPackageImpl implements TTCN3Package
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected String packageFilename = "tTCN3.loadinitialization_ecore";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ttcn3FileEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeReferenceTailTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeReferenceTailEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeReferenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass specTypeElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ttcn3ModuleEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass languageSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass moduleParDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass moduleParEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass moduleParListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass moduleParameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass multitypedModuleParListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass moduleDefinitionsListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass moduleDefinitionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass friendModuleDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass groupDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass extFunctionDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass extConstDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass identifierObjectListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass namedObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allWithExceptsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptsDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass identifierListOrAllEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptGroupSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptTypeDefSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptTemplateSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptConstSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptTestcaseSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptAltstepSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptFunctionSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptSignatureSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptModuleParSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importGroupSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass identifierListOrAllWithExceptEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allWithExceptEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importTypeDefSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importTemplateSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importConstSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importAltstepSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importTestcaseSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importFunctionSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importSignatureSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass importModuleParSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass groupRefListWithExceptEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass qualifiedIdentifierWithExceptEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allGroupsWithExceptEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass altstepDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass altstepLocalDefListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass altstepLocalDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass testcaseDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateOrValueFormalParListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateOrValueFormalParEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass configSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass systemSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass signatureDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass exceptionSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass signatureFormalParListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass moduleControlPartEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass moduleControlBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass controlStatementOrDefListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass controlStatementOrDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass controlStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sutStatementsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass actionTextEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass behaviourStatementsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass activateOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass deactivateStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass labelStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass gotoStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass returnStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass interleavedConstructEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass interleavedGuardListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass interleavedGuardElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass interleavedGuardEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass altConstructEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass altGuardListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass elseStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass guardStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass guardOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass doneStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass killedStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentOrAnyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass indexAssignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass indexSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass getReplyStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass checkStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portCheckOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass checkParameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass redirectPresentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fromClausePresentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass checkPortOpsPresentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portGetReplyOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portRedirectWithValueAndParamEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass redirectWithValueAndParamSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass valueMatchSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass catchStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portCatchOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass catchOpParameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass getCallStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portGetCallOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portRedirectWithParamEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass redirectWithParamSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass paramSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass paramAssignmentListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass assignmentListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variableAssignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variableListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variableEntryEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass triggerStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portTriggerOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass receiveStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portOrAnyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portReceiveOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fromClauseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass addressRefListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portRedirectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass senderSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass valueSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass singleValueSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass altGuardCharEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass altstepInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionRefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionActualParListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionActualParAssignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentRefAssignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass formalPortAndValueParEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portRefAssignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timerRefAssignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionActualParEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentRefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentOrDefaultReferenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass testcaseInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass testcaseActualParListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timerStatementsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timeoutStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass startTimerStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stopTimerStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timerRefOrAnyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timerRefOrAllEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass basicStatementsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass statementBlockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionStatementListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass testcaseOperationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass setLocalVerdictEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass configurationStatementsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass killTCStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stopTCStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentReferenceOrLiteralEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass startTCStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unmapStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass disconnectStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allConnectionsSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allPortsSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mapStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass paramClauseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass connectStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass singleConnectionSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portRefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass communicationStatementsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass checkStateStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portOrAllAnyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass haltStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass startStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stopStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass clearStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portOrAllEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass raiseStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portRaiseOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass replyStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portReplyOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass replyValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portCallOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callParametersEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callTimerValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portCallBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callBodyStatementListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callBodyStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callBodyGuardEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callBodyOpsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sendStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portSendOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass toClauseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionDefListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionLocalDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionLocalInstEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timerInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass varInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass varListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass moduleOrGroupEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass referencedTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeDefBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subTypeDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subTypeDefNamedEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass structuredTypeDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordDefNamedEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordOfDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass recordOfDefNamedEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass structDefBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass structFieldDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass setOfDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass setOfDefNamedEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portDefBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portDefAttribsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mixedAttribsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mixedListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass procOrTypeListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass procOrTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass messageAttribsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass configParamDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mapParamDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unmapParamDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass addressDeclEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass procedureAttribsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentDefListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass portElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass componentElementDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass procedureListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allOrSignatureListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass signatureListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass enumDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass enumDefNamedEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nestedTypeDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nestedRecordDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nestedUnionDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nestedSetDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nestedRecordOfDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nestedSetOfDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nestedEnumDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass messageListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allOrTypeListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unionDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unionDefNamedEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unionDefBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass enumerationListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass enumerationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unionFieldDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass setDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass setDefNamedEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subTypeSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allowedValuesSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateOrRangeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rangeDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass boundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stringLengthEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass charStringMatchEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass patternParticleEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass derivedDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass baseTemplateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass tempVarListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timerVarInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass singleVarInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass singleTempVarInstanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass inLineTemplateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass derivedRefWithParListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass extraMatchingAttributesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldSpecListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simpleSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simpleTemplateSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass singleTemplateExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateRefWithParListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateActualParListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass matchingSymbolEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subsetMatchEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass supersetMatchEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rangeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass wildcardLengthMatchEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass complementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass listOfTemplatesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateOpsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass matchOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass valueofOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass configurationOpsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass createOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass runningOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass opCallEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass aliveOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateListItemEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allElementsFromEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass signatureEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateInstanceAssignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateInstanceActualParEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass templateRestrictionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass restrictedTemplateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass logStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass logItemEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass expressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass withStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass withAttribListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass multiWithAttribEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass singleWithAttribEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass attribQualifierEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass identifierListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass qualifiedIdentifierListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass singleConstDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass compoundExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldExpressionListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayElementExpressionListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldExpressionSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass notUsedOrExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constantExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass compoundConstExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldConstExpressionListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldConstExpressionSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayConstExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayElementConstExpressionListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass valueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass referencedValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass refValueHeadEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass refValueElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass headEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass refValueTailEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass specElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass extendedFieldReferenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass refValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass predefinedValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass singleExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass defOrFieldRefListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass defOrFieldRefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass allRefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass groupRefListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ttcn3ReferenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ttcn3ReferenceListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldReferenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayOrBitRefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldOrBitNumberEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayValueOrAttribEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayElementSpecListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass arrayElementSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass timerOpsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass runningTimerOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass readTimerOpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass permutationMatchEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass loopConstructEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass forStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass whileStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass doWhileStatementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass conditionalConstructEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass elseIfClauseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass elseClauseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass initialEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectCaseConstructEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectCaseBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectCaseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mtcSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionFormalParListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass functionFormalParEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass formalValueParEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass formalTimerParEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass formalPortParEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass formalTemplateParEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass runsOnSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass returnTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass assignmentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variableRefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass preDefFunctionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fint2charEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fint2unicharEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fint2bitEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fint2enumEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fint2hexEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fint2octEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fint2strEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fint2floatEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ffloat2intEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fchar2intEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fchar2octEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass funichar2intEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass funichar2octEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fbit2intEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fbit2hexEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fbit2octEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fbit2strEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fhex2intEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fhex2bitEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fhex2octEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fhex2strEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass foct2intEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass foct2bitEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass foct2hexEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass foct2strEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass foct2charEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass foct2unicharEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fstr2intEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fstr2hexEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fstr2octEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fstr2floatEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fenum2intEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass flengthofEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fsizeofEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fispresentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fischosenEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fisvalueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fisboundEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fregexpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fsubstrEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass freplaceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fencvalueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fdecvalueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fencvalueUnicharEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fdecvalueUnicharEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass frndEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ftestcasenameEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass xorExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass andExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass equalExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass relExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass shiftExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass bitOrExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass bitXorExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass bitAndExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass addExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mulExpressionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum visibilityEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum verdictTypeValueEEnum = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#eNS_URI
   * @see #init()
   * @generated
   */
  private TTCN3PackageImpl()
  {
    super(eNS_URI, TTCN3Factory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link TTCN3Package#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @generated
   */
  public static TTCN3Package init()
  {
    if (isInited) return (TTCN3Package)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI);

    // Obtain or create and register package
    TTCN3PackageImpl theTTCN3Package = (TTCN3PackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TTCN3PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TTCN3PackageImpl());

    isInited = true;

    // Load packages
    theTTCN3Package.loadPackage();

    // Fix loaded packages
    theTTCN3Package.fixPackageContents();

    // Mark meta-data to indicate it can't be changed
    theTTCN3Package.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(TTCN3Package.eNS_URI, theTTCN3Package);
    return theTTCN3Package;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTTCN3File()
  {
    if (ttcn3FileEClass == null)
    {
      ttcn3FileEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(0);
    }
    return ttcn3FileEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTTCN3File_Modules()
  {
        return (EReference)getTTCN3File().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstDef()
  {
    if (constDefEClass == null)
    {
      constDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(1);
    }
    return constDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstDef_Type()
  {
        return (EReference)getConstDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstDef_Defs()
  {
        return (EReference)getConstDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getType()
  {
    if (typeEClass == null)
    {
      typeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(2);
    }
    return typeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getType_Pre()
  {
        return (EAttribute)getType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_Ref()
  {
        return (EReference)getType().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getType_Extensions()
  {
        return (EReference)getType().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeReferenceTailType()
  {
    if (typeReferenceTailTypeEClass == null)
    {
      typeReferenceTailTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(3);
    }
    return typeReferenceTailTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeReferenceTail()
  {
    if (typeReferenceTailEClass == null)
    {
      typeReferenceTailEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(4);
    }
    return typeReferenceTailEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTypeReferenceTail_Type()
  {
        return (EReference)getTypeReferenceTail().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTypeReferenceTail_Array()
  {
        return (EReference)getTypeReferenceTail().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeReference()
  {
    if (typeReferenceEClass == null)
    {
      typeReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(5);
    }
    return typeReferenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTypeReference_Head()
  {
        return (EReference)getTypeReference().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSpecTypeElement()
  {
    if (specTypeElementEClass == null)
    {
      specTypeElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(6);
    }
    return specTypeElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSpecTypeElement_Tail()
  {
        return (EReference)getSpecTypeElement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayDef()
  {
    if (arrayDefEClass == null)
    {
      arrayDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(7);
    }
    return arrayDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayDef_Expr()
  {
        return (EReference)getArrayDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayDef_Range()
  {
        return (EReference)getArrayDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTTCN3Module()
  {
    if (ttcn3ModuleEClass == null)
    {
      ttcn3ModuleEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(8);
    }
    return ttcn3ModuleEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTTCN3Module_Spec()
  {
        return (EReference)getTTCN3Module().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTTCN3Module_Defs()
  {
        return (EReference)getTTCN3Module().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTTCN3Module_Controls()
  {
        return (EReference)getTTCN3Module().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTTCN3Module_Withstm()
  {
        return (EReference)getTTCN3Module().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTCN3Module_Sc()
  {
        return (EAttribute)getTTCN3Module().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLanguageSpec()
  {
    if (languageSpecEClass == null)
    {
      languageSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(9);
    }
    return languageSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLanguageSpec_Txt()
  {
        return (EAttribute)getLanguageSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModuleParDef()
  {
    if (moduleParDefEClass == null)
    {
      moduleParDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(10);
    }
    return moduleParDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModuleParDef_Param()
  {
        return (EReference)getModuleParDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModuleParDef_MultitypeParam()
  {
        return (EReference)getModuleParDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModulePar()
  {
    if (moduleParEClass == null)
    {
      moduleParEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(11);
    }
    return moduleParEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModulePar_Type()
  {
        return (EReference)getModulePar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModulePar_List()
  {
        return (EReference)getModulePar().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModuleParList()
  {
    if (moduleParListEClass == null)
    {
      moduleParListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(12);
    }
    return moduleParListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModuleParList_Params()
  {
        return (EReference)getModuleParList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModuleParameter()
  {
    if (moduleParameterEClass == null)
    {
      moduleParameterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(13);
    }
    return moduleParameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModuleParameter_ConstExpr()
  {
        return (EReference)getModuleParameter().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMultitypedModuleParList()
  {
    if (multitypedModuleParListEClass == null)
    {
      multitypedModuleParListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(14);
    }
    return multitypedModuleParListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMultitypedModuleParList_Params()
  {
        return (EReference)getMultitypedModuleParList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModuleDefinitionsList()
  {
    if (moduleDefinitionsListEClass == null)
    {
      moduleDefinitionsListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(15);
    }
    return moduleDefinitionsListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModuleDefinitionsList_Definitions()
  {
        return (EReference)getModuleDefinitionsList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModuleDefinition()
  {
    if (moduleDefinitionEClass == null)
    {
      moduleDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(16);
    }
    return moduleDefinitionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getModuleDefinition_Visibility()
  {
        return (EAttribute)getModuleDefinition().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModuleDefinition_Def()
  {
        return (EReference)getModuleDefinition().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getModuleDefinition_PublicGroup()
  {
        return (EAttribute)getModuleDefinition().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getModuleDefinition_PrivateFriend()
  {
        return (EAttribute)getModuleDefinition().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModuleDefinition_Statement()
  {
        return (EReference)getModuleDefinition().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFriendModuleDef()
  {
    if (friendModuleDefEClass == null)
    {
      friendModuleDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(17);
    }
    return friendModuleDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFriendModuleDef_Id()
  {
        return (EReference)getFriendModuleDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGroupDef()
  {
    if (groupDefEClass == null)
    {
      groupDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(18);
    }
    return groupDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGroupDef_List()
  {
        return (EReference)getGroupDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExtFunctionDef()
  {
    if (extFunctionDefEClass == null)
    {
      extFunctionDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(19);
    }
    return extFunctionDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getExtFunctionDef_Det()
  {
        return (EAttribute)getExtFunctionDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExtFunctionDef_List()
  {
        return (EReference)getExtFunctionDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExtFunctionDef_Return()
  {
        return (EReference)getExtFunctionDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExtConstDef()
  {
    if (extConstDefEClass == null)
    {
      extConstDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(20);
    }
    return extConstDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExtConstDef_Type()
  {
        return (EReference)getExtConstDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExtConstDef_Id()
  {
        return (EReference)getExtConstDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIdentifierObjectList()
  {
    if (identifierObjectListEClass == null)
    {
      identifierObjectListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(21);
    }
    return identifierObjectListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifierObjectList_Ids()
  {
        return (EReference)getIdentifierObjectList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNamedObject()
  {
    if (namedObjectEClass == null)
    {
      namedObjectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(22);
    }
    return namedObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportDef()
  {
    if (importDefEClass == null)
    {
      importDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(23);
    }
    return importDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getImportDef_Name()
  {
        return (EAttribute)getImportDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportDef_Spec()
  {
        return (EReference)getImportDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportDef_All()
  {
        return (EReference)getImportDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportDef_ImportSpec()
  {
        return (EReference)getImportDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllWithExcepts()
  {
    if (allWithExceptsEClass == null)
    {
      allWithExceptsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(24);
    }
    return allWithExceptsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllWithExcepts_Def()
  {
        return (EReference)getAllWithExcepts().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptsDef()
  {
    if (exceptsDefEClass == null)
    {
      exceptsDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(25);
    }
    return exceptsDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptsDef_Spec()
  {
        return (EReference)getExceptsDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptSpec()
  {
    if (exceptSpecEClass == null)
    {
      exceptSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(26);
    }
    return exceptSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptSpec_Element()
  {
        return (EReference)getExceptSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptElement()
  {
    if (exceptElementEClass == null)
    {
      exceptElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(27);
    }
    return exceptElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptElement_Group()
  {
        return (EReference)getExceptElement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptElement_Type()
  {
        return (EReference)getExceptElement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptElement_Template()
  {
        return (EReference)getExceptElement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptElement_Const()
  {
        return (EReference)getExceptElement().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptElement_Testcase()
  {
        return (EReference)getExceptElement().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptElement_Altstep()
  {
        return (EReference)getExceptElement().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptElement_Function()
  {
        return (EReference)getExceptElement().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptElement_Signature()
  {
        return (EReference)getExceptElement().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptElement_ModulePar()
  {
        return (EReference)getExceptElement().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIdentifierListOrAll()
  {
    if (identifierListOrAllEClass == null)
    {
      identifierListOrAllEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(28);
    }
    return identifierListOrAllEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifierListOrAll_IdList()
  {
        return (EReference)getIdentifierListOrAll().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIdentifierListOrAll_All()
  {
        return (EAttribute)getIdentifierListOrAll().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptGroupSpec()
  {
    if (exceptGroupSpecEClass == null)
    {
      exceptGroupSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(29);
    }
    return exceptGroupSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptGroupSpec_IdList()
  {
        return (EReference)getExceptGroupSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getExceptGroupSpec_All()
  {
        return (EAttribute)getExceptGroupSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptTypeDefSpec()
  {
    if (exceptTypeDefSpecEClass == null)
    {
      exceptTypeDefSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(30);
    }
    return exceptTypeDefSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptTypeDefSpec_IdOrAll()
  {
        return (EReference)getExceptTypeDefSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptTemplateSpec()
  {
    if (exceptTemplateSpecEClass == null)
    {
      exceptTemplateSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(31);
    }
    return exceptTemplateSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptTemplateSpec_IdOrAll()
  {
        return (EReference)getExceptTemplateSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptConstSpec()
  {
    if (exceptConstSpecEClass == null)
    {
      exceptConstSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(32);
    }
    return exceptConstSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptConstSpec_IdOrAll()
  {
        return (EReference)getExceptConstSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptTestcaseSpec()
  {
    if (exceptTestcaseSpecEClass == null)
    {
      exceptTestcaseSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(33);
    }
    return exceptTestcaseSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptTestcaseSpec_IdOrAll()
  {
        return (EReference)getExceptTestcaseSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptAltstepSpec()
  {
    if (exceptAltstepSpecEClass == null)
    {
      exceptAltstepSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(34);
    }
    return exceptAltstepSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptAltstepSpec_IdOrAll()
  {
        return (EReference)getExceptAltstepSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptFunctionSpec()
  {
    if (exceptFunctionSpecEClass == null)
    {
      exceptFunctionSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(35);
    }
    return exceptFunctionSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptFunctionSpec_IdOrAll()
  {
        return (EReference)getExceptFunctionSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptSignatureSpec()
  {
    if (exceptSignatureSpecEClass == null)
    {
      exceptSignatureSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(36);
    }
    return exceptSignatureSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptSignatureSpec_IdOrAll()
  {
        return (EReference)getExceptSignatureSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptModuleParSpec()
  {
    if (exceptModuleParSpecEClass == null)
    {
      exceptModuleParSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(37);
    }
    return exceptModuleParSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExceptModuleParSpec_IdOrAll()
  {
        return (EReference)getExceptModuleParSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportSpec()
  {
    if (importSpecEClass == null)
    {
      importSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(38);
    }
    return importSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportSpec_Element()
  {
        return (EReference)getImportSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportElement()
  {
    if (importElementEClass == null)
    {
      importElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(39);
    }
    return importElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportElement_Group()
  {
        return (EReference)getImportElement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportElement_Type()
  {
        return (EReference)getImportElement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportElement_Template()
  {
        return (EReference)getImportElement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportElement_Const()
  {
        return (EReference)getImportElement().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportElement_Testcase()
  {
        return (EReference)getImportElement().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportElement_Altstep()
  {
        return (EReference)getImportElement().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportElement_Function()
  {
        return (EReference)getImportElement().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportElement_Signature()
  {
        return (EReference)getImportElement().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportElement_ModulePar()
  {
        return (EReference)getImportElement().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getImportElement_ImportSpec()
  {
        return (EAttribute)getImportElement().getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportGroupSpec()
  {
    if (importGroupSpecEClass == null)
    {
      importGroupSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(40);
    }
    return importGroupSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportGroupSpec_GroupRef()
  {
        return (EReference)getImportGroupSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportGroupSpec_Group()
  {
        return (EReference)getImportGroupSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIdentifierListOrAllWithExcept()
  {
    if (identifierListOrAllWithExceptEClass == null)
    {
      identifierListOrAllWithExceptEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(41);
    }
    return identifierListOrAllWithExceptEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifierListOrAllWithExcept_IdList()
  {
        return (EReference)getIdentifierListOrAllWithExcept().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifierListOrAllWithExcept_All()
  {
        return (EReference)getIdentifierListOrAllWithExcept().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllWithExcept()
  {
    if (allWithExceptEClass == null)
    {
      allWithExceptEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(42);
    }
    return allWithExceptEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllWithExcept_IdList()
  {
        return (EReference)getAllWithExcept().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportTypeDefSpec()
  {
    if (importTypeDefSpecEClass == null)
    {
      importTypeDefSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(43);
    }
    return importTypeDefSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportTypeDefSpec_IdOrAll()
  {
        return (EReference)getImportTypeDefSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportTemplateSpec()
  {
    if (importTemplateSpecEClass == null)
    {
      importTemplateSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(44);
    }
    return importTemplateSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportTemplateSpec_IdOrAll()
  {
        return (EReference)getImportTemplateSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportConstSpec()
  {
    if (importConstSpecEClass == null)
    {
      importConstSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(45);
    }
    return importConstSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportConstSpec_IdOrAll()
  {
        return (EReference)getImportConstSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportAltstepSpec()
  {
    if (importAltstepSpecEClass == null)
    {
      importAltstepSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(46);
    }
    return importAltstepSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportAltstepSpec_IdOrAll()
  {
        return (EReference)getImportAltstepSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportTestcaseSpec()
  {
    if (importTestcaseSpecEClass == null)
    {
      importTestcaseSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(47);
    }
    return importTestcaseSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportTestcaseSpec_IdOrAll()
  {
        return (EReference)getImportTestcaseSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportFunctionSpec()
  {
    if (importFunctionSpecEClass == null)
    {
      importFunctionSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(48);
    }
    return importFunctionSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportFunctionSpec_IdOrAll()
  {
        return (EReference)getImportFunctionSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportSignatureSpec()
  {
    if (importSignatureSpecEClass == null)
    {
      importSignatureSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(49);
    }
    return importSignatureSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportSignatureSpec_IdOrAll()
  {
        return (EReference)getImportSignatureSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImportModuleParSpec()
  {
    if (importModuleParSpecEClass == null)
    {
      importModuleParSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(50);
    }
    return importModuleParSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getImportModuleParSpec_IdOrAll()
  {
        return (EReference)getImportModuleParSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGroupRefListWithExcept()
  {
    if (groupRefListWithExceptEClass == null)
    {
      groupRefListWithExceptEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(51);
    }
    return groupRefListWithExceptEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGroupRefListWithExcept_Qualifier()
  {
        return (EReference)getGroupRefListWithExcept().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getQualifiedIdentifierWithExcept()
  {
    if (qualifiedIdentifierWithExceptEClass == null)
    {
      qualifiedIdentifierWithExceptEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(52);
    }
    return qualifiedIdentifierWithExceptEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getQualifiedIdentifierWithExcept_Id()
  {
        return (EAttribute)getQualifiedIdentifierWithExcept().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getQualifiedIdentifierWithExcept_Def()
  {
        return (EReference)getQualifiedIdentifierWithExcept().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllGroupsWithExcept()
  {
    if (allGroupsWithExceptEClass == null)
    {
      allGroupsWithExceptEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(53);
    }
    return allGroupsWithExceptEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllGroupsWithExcept_IdList()
  {
        return (EReference)getAllGroupsWithExcept().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAltstepDef()
  {
    if (altstepDefEClass == null)
    {
      altstepDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(54);
    }
    return altstepDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepDef_Params()
  {
        return (EReference)getAltstepDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepDef_Spec()
  {
        return (EReference)getAltstepDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepDef_Mtc()
  {
        return (EReference)getAltstepDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepDef_System()
  {
        return (EReference)getAltstepDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepDef_Local()
  {
        return (EReference)getAltstepDef().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepDef_Guard()
  {
        return (EReference)getAltstepDef().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAltstepLocalDefList()
  {
    if (altstepLocalDefListEClass == null)
    {
      altstepLocalDefListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(55);
    }
    return altstepLocalDefListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepLocalDefList_Defs()
  {
        return (EReference)getAltstepLocalDefList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepLocalDefList_Withstats()
  {
        return (EReference)getAltstepLocalDefList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAltstepLocalDefList_Sc()
  {
        return (EAttribute)getAltstepLocalDefList().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAltstepLocalDef()
  {
    if (altstepLocalDefEClass == null)
    {
      altstepLocalDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(56);
    }
    return altstepLocalDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepLocalDef_Variable()
  {
        return (EReference)getAltstepLocalDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepLocalDef_Timer()
  {
        return (EReference)getAltstepLocalDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepLocalDef_Const()
  {
        return (EReference)getAltstepLocalDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepLocalDef_Template()
  {
        return (EReference)getAltstepLocalDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTestcaseDef()
  {
    if (testcaseDefEClass == null)
    {
      testcaseDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(57);
    }
    return testcaseDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTestcaseDef_ParList()
  {
        return (EReference)getTestcaseDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTestcaseDef_Spec()
  {
        return (EReference)getTestcaseDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTestcaseDef_Statement()
  {
        return (EReference)getTestcaseDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateOrValueFormalParList()
  {
    if (templateOrValueFormalParListEClass == null)
    {
      templateOrValueFormalParListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(58);
    }
    return templateOrValueFormalParListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateOrValueFormalParList_Params()
  {
        return (EReference)getTemplateOrValueFormalParList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateOrValueFormalPar()
  {
    if (templateOrValueFormalParEClass == null)
    {
      templateOrValueFormalParEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(59);
    }
    return templateOrValueFormalParEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateOrValueFormalPar_Value()
  {
        return (EReference)getTemplateOrValueFormalPar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateOrValueFormalPar_Template()
  {
        return (EReference)getTemplateOrValueFormalPar().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConfigSpec()
  {
    if (configSpecEClass == null)
    {
      configSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(60);
    }
    return configSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigSpec_RunsOn()
  {
        return (EReference)getConfigSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigSpec_SystemSpec()
  {
        return (EReference)getConfigSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSystemSpec()
  {
    if (systemSpecEClass == null)
    {
      systemSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(61);
    }
    return systemSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSystemSpec_Component()
  {
        return (EReference)getSystemSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSignatureDef()
  {
    if (signatureDefEClass == null)
    {
      signatureDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(62);
    }
    return signatureDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSignatureDef_ParamList()
  {
        return (EReference)getSignatureDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSignatureDef_Return()
  {
        return (EReference)getSignatureDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSignatureDef_Sprec()
  {
        return (EReference)getSignatureDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExceptionSpec()
  {
    if (exceptionSpecEClass == null)
    {
      exceptionSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(63);
    }
    return exceptionSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSignatureFormalParList()
  {
    if (signatureFormalParListEClass == null)
    {
      signatureFormalParListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(64);
    }
    return signatureFormalParListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSignatureFormalParList_Params()
  {
        return (EReference)getSignatureFormalParList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModuleControlPart()
  {
    if (moduleControlPartEClass == null)
    {
      moduleControlPartEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(65);
    }
    return moduleControlPartEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModuleControlPart_Body()
  {
        return (EReference)getModuleControlPart().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModuleControlPart_Ws()
  {
        return (EReference)getModuleControlPart().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getModuleControlPart_Sc()
  {
        return (EAttribute)getModuleControlPart().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModuleControlBody()
  {
    if (moduleControlBodyEClass == null)
    {
      moduleControlBodyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(66);
    }
    return moduleControlBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModuleControlBody_List()
  {
        return (EReference)getModuleControlBody().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getControlStatementOrDefList()
  {
    if (controlStatementOrDefListEClass == null)
    {
      controlStatementOrDefListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(67);
    }
    return controlStatementOrDefListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatementOrDefList_LocalDef()
  {
        return (EReference)getControlStatementOrDefList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatementOrDefList_LocalInst()
  {
        return (EReference)getControlStatementOrDefList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatementOrDefList_Withstm()
  {
        return (EReference)getControlStatementOrDefList().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatementOrDefList_Control()
  {
        return (EReference)getControlStatementOrDefList().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getControlStatementOrDefList_Sc()
  {
        return (EAttribute)getControlStatementOrDefList().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getControlStatementOrDef()
  {
    if (controlStatementOrDefEClass == null)
    {
      controlStatementOrDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(68);
    }
    return controlStatementOrDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatementOrDef_LocalDef()
  {
        return (EReference)getControlStatementOrDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatementOrDef_LocalInst()
  {
        return (EReference)getControlStatementOrDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatementOrDef_Withstm()
  {
        return (EReference)getControlStatementOrDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatementOrDef_Control()
  {
        return (EReference)getControlStatementOrDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getControlStatement()
  {
    if (controlStatementEClass == null)
    {
      controlStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(69);
    }
    return controlStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatement_Timer()
  {
        return (EReference)getControlStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatement_Basic()
  {
        return (EReference)getControlStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatement_Behavior()
  {
        return (EReference)getControlStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlStatement_Sut()
  {
        return (EReference)getControlStatement().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSUTStatements()
  {
    if (sutStatementsEClass == null)
    {
      sutStatementsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(70);
    }
    return sutStatementsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSUTStatements_Txt()
  {
        return (EReference)getSUTStatements().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getActionText()
  {
    if (actionTextEClass == null)
    {
      actionTextEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(71);
    }
    return actionTextEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getActionText_Expr()
  {
        return (EReference)getActionText().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBehaviourStatements()
  {
    if (behaviourStatementsEClass == null)
    {
      behaviourStatementsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(72);
    }
    return behaviourStatementsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBehaviourStatements_Testcase()
  {
        return (EReference)getBehaviourStatements().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBehaviourStatements_Function()
  {
        return (EReference)getBehaviourStatements().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBehaviourStatements_Return()
  {
        return (EReference)getBehaviourStatements().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBehaviourStatements_Alt()
  {
        return (EReference)getBehaviourStatements().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBehaviourStatements_Interleaved()
  {
        return (EReference)getBehaviourStatements().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBehaviourStatements_Label()
  {
        return (EReference)getBehaviourStatements().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBehaviourStatements_Goto()
  {
        return (EReference)getBehaviourStatements().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBehaviourStatements_Deactivate()
  {
        return (EReference)getBehaviourStatements().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBehaviourStatements_Altstep()
  {
        return (EReference)getBehaviourStatements().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBehaviourStatements_Activate()
  {
        return (EReference)getBehaviourStatements().getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getActivateOp()
  {
    if (activateOpEClass == null)
    {
      activateOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(73);
    }
    return activateOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getActivateOp_Altstep()
  {
        return (EReference)getActivateOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDeactivateStatement()
  {
    if (deactivateStatementEClass == null)
    {
      deactivateStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(74);
    }
    return deactivateStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDeactivateStatement_Component()
  {
        return (EReference)getDeactivateStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLabelStatement()
  {
    if (labelStatementEClass == null)
    {
      labelStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(75);
    }
    return labelStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGotoStatement()
  {
    if (gotoStatementEClass == null)
    {
      gotoStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(76);
    }
    return gotoStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReturnStatement()
  {
    if (returnStatementEClass == null)
    {
      returnStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(77);
    }
    return returnStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReturnStatement_Expression()
  {
        return (EReference)getReturnStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReturnStatement_Template()
  {
        return (EReference)getReturnStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInterleavedConstruct()
  {
    if (interleavedConstructEClass == null)
    {
      interleavedConstructEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(78);
    }
    return interleavedConstructEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInterleavedGuardList()
  {
    if (interleavedGuardListEClass == null)
    {
      interleavedGuardListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(79);
    }
    return interleavedGuardListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInterleavedGuardList_Elements()
  {
        return (EReference)getInterleavedGuardList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInterleavedGuardElement()
  {
    if (interleavedGuardElementEClass == null)
    {
      interleavedGuardElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(80);
    }
    return interleavedGuardElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInterleavedGuardElement_Guard()
  {
        return (EReference)getInterleavedGuardElement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInterleavedGuardElement_Statement()
  {
        return (EReference)getInterleavedGuardElement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInterleavedGuard()
  {
    if (interleavedGuardEClass == null)
    {
      interleavedGuardEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(81);
    }
    return interleavedGuardEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInterleavedGuard_GuardOp()
  {
        return (EReference)getInterleavedGuard().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAltConstruct()
  {
    if (altConstructEClass == null)
    {
      altConstructEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(82);
    }
    return altConstructEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltConstruct_AgList()
  {
        return (EReference)getAltConstruct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAltGuardList()
  {
    if (altGuardListEClass == null)
    {
      altGuardListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(83);
    }
    return altGuardListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltGuardList_GuardList()
  {
        return (EReference)getAltGuardList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltGuardList_ElseList()
  {
        return (EReference)getAltGuardList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getElseStatement()
  {
    if (elseStatementEClass == null)
    {
      elseStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(84);
    }
    return elseStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getElseStatement_Block()
  {
        return (EReference)getElseStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGuardStatement()
  {
    if (guardStatementEClass == null)
    {
      guardStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(85);
    }
    return guardStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardStatement_Guard()
  {
        return (EReference)getGuardStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardStatement_Step()
  {
        return (EReference)getGuardStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardStatement_Block()
  {
        return (EReference)getGuardStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardStatement_Op()
  {
        return (EReference)getGuardStatement().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGuardOp()
  {
    if (guardOpEClass == null)
    {
      guardOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(86);
    }
    return guardOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardOp_Timeout()
  {
        return (EReference)getGuardOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardOp_Receive()
  {
        return (EReference)getGuardOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardOp_Trigger()
  {
        return (EReference)getGuardOp().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardOp_GetCall()
  {
        return (EReference)getGuardOp().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardOp_Catch()
  {
        return (EReference)getGuardOp().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardOp_Check()
  {
        return (EReference)getGuardOp().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardOp_GetReply()
  {
        return (EReference)getGuardOp().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardOp_Done()
  {
        return (EReference)getGuardOp().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGuardOp_Killed()
  {
        return (EReference)getGuardOp().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDoneStatement()
  {
    if (doneStatementEClass == null)
    {
      doneStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(87);
    }
    return doneStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDoneStatement_Component()
  {
        return (EReference)getDoneStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDoneStatement_Index()
  {
        return (EReference)getDoneStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getKilledStatement()
  {
    if (killedStatementEClass == null)
    {
      killedStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(88);
    }
    return killedStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getKilledStatement_Component()
  {
        return (EReference)getKilledStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getKilledStatement_Index()
  {
        return (EReference)getKilledStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentOrAny()
  {
    if (componentOrAnyEClass == null)
    {
      componentOrAnyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(89);
    }
    return componentOrAnyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentOrAny_CompOrDefault()
  {
        return (EReference)getComponentOrAny().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentOrAny_Ref()
  {
        return (EReference)getComponentOrAny().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIndexAssignment()
  {
    if (indexAssignmentEClass == null)
    {
      indexAssignmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(90);
    }
    return indexAssignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIndexAssignment_Index()
  {
        return (EReference)getIndexAssignment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIndexSpec()
  {
    if (indexSpecEClass == null)
    {
      indexSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(91);
    }
    return indexSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIndexSpec_Ref()
  {
        return (EReference)getIndexSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGetReplyStatement()
  {
    if (getReplyStatementEClass == null)
    {
      getReplyStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(92);
    }
    return getReplyStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCheckStatement()
  {
    if (checkStatementEClass == null)
    {
      checkStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(93);
    }
    return checkStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortCheckOp()
  {
    if (portCheckOpEClass == null)
    {
      portCheckOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(94);
    }
    return portCheckOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortCheckOp_Check()
  {
        return (EReference)getPortCheckOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCheckParameter()
  {
    if (checkParameterEClass == null)
    {
      checkParameterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(95);
    }
    return checkParameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRedirectPresent()
  {
    if (redirectPresentEClass == null)
    {
      redirectPresentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(96);
    }
    return redirectPresentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRedirectPresent_Sender()
  {
        return (EReference)getRedirectPresent().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRedirectPresent_Index()
  {
        return (EReference)getRedirectPresent().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFromClausePresent()
  {
    if (fromClausePresentEClass == null)
    {
      fromClausePresentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(97);
    }
    return fromClausePresentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCheckPortOpsPresent()
  {
    if (checkPortOpsPresentEClass == null)
    {
      checkPortOpsPresentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(98);
    }
    return checkPortOpsPresentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCheckPortOpsPresent_From()
  {
        return (EReference)getCheckPortOpsPresent().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortGetReplyOp()
  {
    if (portGetReplyOpEClass == null)
    {
      portGetReplyOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(99);
    }
    return portGetReplyOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortGetReplyOp_Template()
  {
        return (EReference)getPortGetReplyOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortGetReplyOp_Value()
  {
        return (EReference)getPortGetReplyOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortGetReplyOp_Redirect()
  {
        return (EReference)getPortGetReplyOp().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortRedirectWithValueAndParam()
  {
    if (portRedirectWithValueAndParamEClass == null)
    {
      portRedirectWithValueAndParamEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(100);
    }
    return portRedirectWithValueAndParamEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRedirectWithValueAndParamSpec()
  {
    if (redirectWithValueAndParamSpecEClass == null)
    {
      redirectWithValueAndParamSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(101);
    }
    return redirectWithValueAndParamSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRedirectWithValueAndParamSpec_Value()
  {
        return (EReference)getRedirectWithValueAndParamSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRedirectWithValueAndParamSpec_Param()
  {
        return (EReference)getRedirectWithValueAndParamSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRedirectWithValueAndParamSpec_Sender()
  {
        return (EReference)getRedirectWithValueAndParamSpec().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRedirectWithValueAndParamSpec_Index()
  {
        return (EReference)getRedirectWithValueAndParamSpec().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRedirectWithValueAndParamSpec_Redirect()
  {
        return (EReference)getRedirectWithValueAndParamSpec().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getValueMatchSpec()
  {
    if (valueMatchSpecEClass == null)
    {
      valueMatchSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(102);
    }
    return valueMatchSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getValueMatchSpec_Template()
  {
        return (EReference)getValueMatchSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCatchStatement()
  {
    if (catchStatementEClass == null)
    {
      catchStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(103);
    }
    return catchStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortCatchOp()
  {
    if (portCatchOpEClass == null)
    {
      portCatchOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(104);
    }
    return portCatchOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortCatchOp_Param()
  {
        return (EReference)getPortCatchOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortCatchOp_Redirect()
  {
        return (EReference)getPortCatchOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCatchOpParameter()
  {
    if (catchOpParameterEClass == null)
    {
      catchOpParameterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(105);
    }
    return catchOpParameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCatchOpParameter_Signature()
  {
        return (EReference)getCatchOpParameter().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCatchOpParameter_Template()
  {
        return (EReference)getCatchOpParameter().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGetCallStatement()
  {
    if (getCallStatementEClass == null)
    {
      getCallStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(106);
    }
    return getCallStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortGetCallOp()
  {
    if (portGetCallOpEClass == null)
    {
      portGetCallOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(107);
    }
    return portGetCallOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortGetCallOp_Template()
  {
        return (EReference)getPortGetCallOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortGetCallOp_Redirect()
  {
        return (EReference)getPortGetCallOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortRedirectWithParam()
  {
    if (portRedirectWithParamEClass == null)
    {
      portRedirectWithParamEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(108);
    }
    return portRedirectWithParamEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRedirectWithParam_Redirect()
  {
        return (EReference)getPortRedirectWithParam().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRedirectWithParamSpec()
  {
    if (redirectWithParamSpecEClass == null)
    {
      redirectWithParamSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(109);
    }
    return redirectWithParamSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRedirectWithParamSpec_Param()
  {
        return (EReference)getRedirectWithParamSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRedirectWithParamSpec_Sender()
  {
        return (EReference)getRedirectWithParamSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRedirectWithParamSpec_Index()
  {
        return (EReference)getRedirectWithParamSpec().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParamSpec()
  {
    if (paramSpecEClass == null)
    {
      paramSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(110);
    }
    return paramSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParamAssignmentList()
  {
    if (paramAssignmentListEClass == null)
    {
      paramAssignmentListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(111);
    }
    return paramAssignmentListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAssignmentList()
  {
    if (assignmentListEClass == null)
    {
      assignmentListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(112);
    }
    return assignmentListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignmentList_Assign()
  {
        return (EReference)getAssignmentList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariableAssignment()
  {
    if (variableAssignmentEClass == null)
    {
      variableAssignmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(113);
    }
    return variableAssignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariableAssignment_Ref()
  {
        return (EReference)getVariableAssignment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVariableAssignment_Name()
  {
        return (EAttribute)getVariableAssignment().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariableList()
  {
    if (variableListEClass == null)
    {
      variableListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(114);
    }
    return variableListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariableList_Entries()
  {
        return (EReference)getVariableList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariableEntry()
  {
    if (variableEntryEClass == null)
    {
      variableEntryEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(115);
    }
    return variableEntryEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTriggerStatement()
  {
    if (triggerStatementEClass == null)
    {
      triggerStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(116);
    }
    return triggerStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortTriggerOp()
  {
    if (portTriggerOpEClass == null)
    {
      portTriggerOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(117);
    }
    return portTriggerOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortTriggerOp_Template()
  {
        return (EReference)getPortTriggerOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortTriggerOp_From()
  {
        return (EReference)getPortTriggerOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortTriggerOp_Redirect()
  {
        return (EReference)getPortTriggerOp().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReceiveStatement()
  {
    if (receiveStatementEClass == null)
    {
      receiveStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(118);
    }
    return receiveStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReceiveStatement_Any()
  {
        return (EReference)getReceiveStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReceiveStatement_Receive()
  {
        return (EReference)getReceiveStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortOrAny()
  {
    if (portOrAnyEClass == null)
    {
      portOrAnyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(119);
    }
    return portOrAnyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAny_Reply()
  {
        return (EReference)getPortOrAny().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAny_Check()
  {
        return (EReference)getPortOrAny().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAny_Catch()
  {
        return (EReference)getPortOrAny().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAny_Call()
  {
        return (EReference)getPortOrAny().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAny_Trigger()
  {
        return (EReference)getPortOrAny().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAny_Ref()
  {
        return (EReference)getPortOrAny().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAny_Array()
  {
        return (EReference)getPortOrAny().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAny_Variable()
  {
        return (EReference)getPortOrAny().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortReceiveOp()
  {
    if (portReceiveOpEClass == null)
    {
      portReceiveOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(120);
    }
    return portReceiveOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortReceiveOp_Template()
  {
        return (EReference)getPortReceiveOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortReceiveOp_Redirect()
  {
        return (EReference)getPortReceiveOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFromClause()
  {
    if (fromClauseEClass == null)
    {
      fromClauseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(121);
    }
    return fromClauseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFromClause_Sender()
  {
        return (EReference)getFromClause().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFromClause_Index()
  {
        return (EReference)getFromClause().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFromClause_Template()
  {
        return (EReference)getFromClause().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFromClause_Addresses()
  {
        return (EReference)getFromClause().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAddressRefList()
  {
    if (addressRefListEClass == null)
    {
      addressRefListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(122);
    }
    return addressRefListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAddressRefList_Templates()
  {
        return (EReference)getAddressRefList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortRedirect()
  {
    if (portRedirectEClass == null)
    {
      portRedirectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(123);
    }
    return portRedirectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRedirect_Value()
  {
        return (EReference)getPortRedirect().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRedirect_Sender()
  {
        return (EReference)getPortRedirect().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRedirect_Index()
  {
        return (EReference)getPortRedirect().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSenderSpec()
  {
    if (senderSpecEClass == null)
    {
      senderSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(124);
    }
    return senderSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSenderSpec_Variable()
  {
        return (EReference)getSenderSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getValueSpec()
  {
    if (valueSpecEClass == null)
    {
      valueSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(125);
    }
    return valueSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getValueSpec_Variable()
  {
        return (EReference)getValueSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getValueSpec_Specs()
  {
        return (EReference)getValueSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSingleValueSpec()
  {
    if (singleValueSpecEClass == null)
    {
      singleValueSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(126);
    }
    return singleValueSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleValueSpec_Variable()
  {
        return (EReference)getSingleValueSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleValueSpec_Field()
  {
        return (EReference)getSingleValueSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleValueSpec_Ext()
  {
        return (EReference)getSingleValueSpec().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAltGuardChar()
  {
    if (altGuardCharEClass == null)
    {
      altGuardCharEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(127);
    }
    return altGuardCharEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltGuardChar_Expr()
  {
        return (EReference)getAltGuardChar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAltstepInstance()
  {
    if (altstepInstanceEClass == null)
    {
      altstepInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(128);
    }
    return altstepInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepInstance_Ref()
  {
        return (EReference)getAltstepInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAltstepInstance_List()
  {
        return (EReference)getAltstepInstance().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionInstance()
  {
    if (functionInstanceEClass == null)
    {
      functionInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(129);
    }
    return functionInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionInstance_Ref()
  {
        return (EReference)getFunctionInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionInstance_Params()
  {
        return (EReference)getFunctionInstance().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionRef()
  {
    if (functionRefEClass == null)
    {
      functionRefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(130);
    }
    return functionRefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionActualParList()
  {
    if (functionActualParListEClass == null)
    {
      functionActualParListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(131);
    }
    return functionActualParListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionActualParList_Params()
  {
        return (EReference)getFunctionActualParList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionActualParList_Asssign()
  {
        return (EReference)getFunctionActualParList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionActualParAssignment()
  {
    if (functionActualParAssignmentEClass == null)
    {
      functionActualParAssignmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(132);
    }
    return functionActualParAssignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionActualParAssignment_Template()
  {
        return (EReference)getFunctionActualParAssignment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionActualParAssignment_Component()
  {
        return (EReference)getFunctionActualParAssignment().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionActualParAssignment_Port()
  {
        return (EReference)getFunctionActualParAssignment().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionActualParAssignment_Timer()
  {
        return (EReference)getFunctionActualParAssignment().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentRefAssignment()
  {
    if (componentRefAssignmentEClass == null)
    {
      componentRefAssignmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(133);
    }
    return componentRefAssignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentRefAssignment_Ref()
  {
        return (EReference)getComponentRefAssignment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentRefAssignment_Value()
  {
        return (EReference)getComponentRefAssignment().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFormalPortAndValuePar()
  {
    if (formalPortAndValueParEClass == null)
    {
      formalPortAndValueParEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(134);
    }
    return formalPortAndValueParEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortRefAssignment()
  {
    if (portRefAssignmentEClass == null)
    {
      portRefAssignmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(135);
    }
    return portRefAssignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRefAssignment_Port()
  {
        return (EReference)getPortRefAssignment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRefAssignment_Value()
  {
        return (EReference)getPortRefAssignment().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimerRefAssignment()
  {
    if (timerRefAssignmentEClass == null)
    {
      timerRefAssignmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(136);
    }
    return timerRefAssignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimerRefAssignment_Timer()
  {
        return (EReference)getTimerRefAssignment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimerRefAssignment_Value()
  {
        return (EReference)getTimerRefAssignment().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionActualPar()
  {
    if (functionActualParEClass == null)
    {
      functionActualParEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(137);
    }
    return functionActualParEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionActualPar_Template()
  {
        return (EReference)getFunctionActualPar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionActualPar_Component()
  {
        return (EReference)getFunctionActualPar().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionActualPar_Timer()
  {
        return (EReference)getFunctionActualPar().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionActualPar_Port()
  {
        return (EReference)getFunctionActualPar().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentRef()
  {
    if (componentRefEClass == null)
    {
      componentRefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(138);
    }
    return componentRefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentRef_Ref()
  {
        return (EReference)getComponentRef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComponentRef_System()
  {
        return (EAttribute)getComponentRef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComponentRef_Self()
  {
        return (EAttribute)getComponentRef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComponentRef_Mtc()
  {
        return (EAttribute)getComponentRef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentOrDefaultReference()
  {
    if (componentOrDefaultReferenceEClass == null)
    {
      componentOrDefaultReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(139);
    }
    return componentOrDefaultReferenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentOrDefaultReference_Variable()
  {
        return (EReference)getComponentOrDefaultReference().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTestcaseInstance()
  {
    if (testcaseInstanceEClass == null)
    {
      testcaseInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(140);
    }
    return testcaseInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTestcaseInstance_Ref()
  {
        return (EReference)getTestcaseInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTestcaseInstance_TcParams()
  {
        return (EReference)getTestcaseInstance().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTestcaseInstance_Expr()
  {
        return (EReference)getTestcaseInstance().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTestcaseInstance_Sexpr()
  {
        return (EReference)getTestcaseInstance().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTestcaseActualParList()
  {
    if (testcaseActualParListEClass == null)
    {
      testcaseActualParListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(141);
    }
    return testcaseActualParListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTestcaseActualParList_TemplParam()
  {
        return (EReference)getTestcaseActualParList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTestcaseActualParList_TemplAssign()
  {
        return (EReference)getTestcaseActualParList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimerStatements()
  {
    if (timerStatementsEClass == null)
    {
      timerStatementsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(142);
    }
    return timerStatementsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimerStatements_Start()
  {
        return (EReference)getTimerStatements().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimerStatements_Stop()
  {
        return (EReference)getTimerStatements().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimerStatements_Timeout()
  {
        return (EReference)getTimerStatements().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimeoutStatement()
  {
    if (timeoutStatementEClass == null)
    {
      timeoutStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(143);
    }
    return timeoutStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimeoutStatement_Ref()
  {
        return (EReference)getTimeoutStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimeoutStatement_Index()
  {
        return (EReference)getTimeoutStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStartTimerStatement()
  {
    if (startTimerStatementEClass == null)
    {
      startTimerStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(144);
    }
    return startTimerStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStartTimerStatement_Ref()
  {
        return (EReference)getStartTimerStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStartTimerStatement_ArryRefs()
  {
        return (EReference)getStartTimerStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStartTimerStatement_Expr()
  {
        return (EReference)getStartTimerStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStopTimerStatement()
  {
    if (stopTimerStatementEClass == null)
    {
      stopTimerStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(145);
    }
    return stopTimerStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStopTimerStatement_Ref()
  {
        return (EReference)getStopTimerStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimerRefOrAny()
  {
    if (timerRefOrAnyEClass == null)
    {
      timerRefOrAnyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(146);
    }
    return timerRefOrAnyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimerRefOrAny_Ref()
  {
        return (EReference)getTimerRefOrAny().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTimerRefOrAny_Timer()
  {
        return (EAttribute)getTimerRefOrAny().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTimerRefOrAny_From()
  {
        return (EAttribute)getTimerRefOrAny().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimerRefOrAll()
  {
    if (timerRefOrAllEClass == null)
    {
      timerRefOrAllEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(147);
    }
    return timerRefOrAllEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimerRefOrAll_Ref()
  {
        return (EReference)getTimerRefOrAll().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTimerRefOrAll_Timer()
  {
        return (EAttribute)getTimerRefOrAll().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBasicStatements()
  {
    if (basicStatementsEClass == null)
    {
      basicStatementsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(148);
    }
    return basicStatementsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBasicStatements_Log()
  {
        return (EReference)getBasicStatements().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBasicStatements_Block()
  {
        return (EReference)getBasicStatements().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBasicStatements_Loop()
  {
        return (EReference)getBasicStatements().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBasicStatements_Conditional()
  {
        return (EReference)getBasicStatements().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBasicStatements_Select()
  {
        return (EReference)getBasicStatements().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBasicStatements_Assign()
  {
        return (EReference)getBasicStatements().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStatementBlock()
  {
    if (statementBlockEClass == null)
    {
      statementBlockEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(149);
    }
    return statementBlockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementBlock_Def()
  {
        return (EReference)getStatementBlock().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStatementBlock_Stat()
  {
        return (EReference)getStatementBlock().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionStatementList()
  {
    if (functionStatementListEClass == null)
    {
      functionStatementListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(150);
    }
    return functionStatementListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionStatementList_Statements()
  {
        return (EReference)getFunctionStatementList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunctionStatementList_Sc()
  {
        return (EAttribute)getFunctionStatementList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionStatement()
  {
    if (functionStatementEClass == null)
    {
      functionStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(151);
    }
    return functionStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionStatement_Timer()
  {
        return (EReference)getFunctionStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionStatement_Basic()
  {
        return (EReference)getFunctionStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionStatement_Behavior()
  {
        return (EReference)getFunctionStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionStatement_Verdict()
  {
        return (EReference)getFunctionStatement().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionStatement_Communication()
  {
        return (EReference)getFunctionStatement().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionStatement_Configuration()
  {
        return (EReference)getFunctionStatement().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionStatement_Sut()
  {
        return (EReference)getFunctionStatement().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionStatement_Test()
  {
        return (EReference)getFunctionStatement().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTestcaseOperation()
  {
    if (testcaseOperationEClass == null)
    {
      testcaseOperationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(152);
    }
    return testcaseOperationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTestcaseOperation_Txt()
  {
        return (EAttribute)getTestcaseOperation().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTestcaseOperation_Template()
  {
        return (EReference)getTestcaseOperation().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSetLocalVerdict()
  {
    if (setLocalVerdictEClass == null)
    {
      setLocalVerdictEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(153);
    }
    return setLocalVerdictEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSetLocalVerdict_Expression()
  {
        return (EReference)getSetLocalVerdict().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSetLocalVerdict_Log()
  {
        return (EReference)getSetLocalVerdict().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConfigurationStatements()
  {
    if (configurationStatementsEClass == null)
    {
      configurationStatementsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(154);
    }
    return configurationStatementsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationStatements_Connect()
  {
        return (EReference)getConfigurationStatements().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationStatements_Map()
  {
        return (EReference)getConfigurationStatements().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationStatements_Disconnect()
  {
        return (EReference)getConfigurationStatements().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationStatements_Unmap()
  {
        return (EReference)getConfigurationStatements().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationStatements_Done()
  {
        return (EReference)getConfigurationStatements().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationStatements_Killed()
  {
        return (EReference)getConfigurationStatements().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationStatements_StartTc()
  {
        return (EReference)getConfigurationStatements().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationStatements_StopTc()
  {
        return (EReference)getConfigurationStatements().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationStatements_KillTc()
  {
        return (EReference)getConfigurationStatements().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getKillTCStatement()
  {
    if (killTCStatementEClass == null)
    {
      killTCStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(155);
    }
    return killTCStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStopTCStatement()
  {
    if (stopTCStatementEClass == null)
    {
      stopTCStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(156);
    }
    return stopTCStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentReferenceOrLiteral()
  {
    if (componentReferenceOrLiteralEClass == null)
    {
      componentReferenceOrLiteralEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(157);
    }
    return componentReferenceOrLiteralEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentReferenceOrLiteral_Ref()
  {
        return (EReference)getComponentReferenceOrLiteral().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStartTCStatement()
  {
    if (startTCStatementEClass == null)
    {
      startTCStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(158);
    }
    return startTCStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStartTCStatement_Ref()
  {
        return (EReference)getStartTCStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStartTCStatement_Function()
  {
        return (EReference)getStartTCStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnmapStatement()
  {
    if (unmapStatementEClass == null)
    {
      unmapStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(159);
    }
    return unmapStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnmapStatement_Spec()
  {
        return (EReference)getUnmapStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnmapStatement_Clause()
  {
        return (EReference)getUnmapStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDisconnectStatement()
  {
    if (disconnectStatementEClass == null)
    {
      disconnectStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(160);
    }
    return disconnectStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDisconnectStatement_Spec()
  {
        return (EReference)getDisconnectStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllConnectionsSpec()
  {
    if (allConnectionsSpecEClass == null)
    {
      allConnectionsSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(161);
    }
    return allConnectionsSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllPortsSpec()
  {
    if (allPortsSpecEClass == null)
    {
      allPortsSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(162);
    }
    return allPortsSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllPortsSpec_Component()
  {
        return (EReference)getAllPortsSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMapStatement()
  {
    if (mapStatementEClass == null)
    {
      mapStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(163);
    }
    return mapStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMapStatement_Spec()
  {
        return (EReference)getMapStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMapStatement_Clause()
  {
        return (EReference)getMapStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getParamClause()
  {
    if (paramClauseEClass == null)
    {
      paramClauseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(164);
    }
    return paramClauseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getParamClause_List()
  {
        return (EReference)getParamClause().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConnectStatement()
  {
    if (connectStatementEClass == null)
    {
      connectStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(165);
    }
    return connectStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConnectStatement_Spec()
  {
        return (EReference)getConnectStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSingleConnectionSpec()
  {
    if (singleConnectionSpecEClass == null)
    {
      singleConnectionSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(166);
    }
    return singleConnectionSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleConnectionSpec_Port1()
  {
        return (EReference)getSingleConnectionSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleConnectionSpec_Port2()
  {
        return (EReference)getSingleConnectionSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortRef()
  {
    if (portRefEClass == null)
    {
      portRefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(167);
    }
    return portRefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRef_Component()
  {
        return (EReference)getPortRef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRef_Port()
  {
        return (EReference)getPortRef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRef_Array()
  {
        return (EReference)getPortRef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCommunicationStatements()
  {
    if (communicationStatementsEClass == null)
    {
      communicationStatementsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(168);
    }
    return communicationStatementsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Send()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Call()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Reply()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Raise()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Receive()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Trigger()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_GetCall()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_GetReply()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Catch()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Check()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Clear()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(10);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Start()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(11);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Stop()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(12);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_Halt()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(13);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommunicationStatements_CheckState()
  {
        return (EReference)getCommunicationStatements().getEStructuralFeatures().get(14);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCheckStateStatement()
  {
    if (checkStateStatementEClass == null)
    {
      checkStateStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(169);
    }
    return checkStateStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCheckStateStatement_Port()
  {
        return (EReference)getCheckStateStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCheckStateStatement_Expr()
  {
        return (EReference)getCheckStateStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortOrAllAny()
  {
    if (portOrAllAnyEClass == null)
    {
      portOrAllAnyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(170);
    }
    return portOrAllAnyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAllAny_Port()
  {
        return (EReference)getPortOrAllAny().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHaltStatement()
  {
    if (haltStatementEClass == null)
    {
      haltStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(171);
    }
    return haltStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHaltStatement_Port()
  {
        return (EReference)getHaltStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStartStatement()
  {
    if (startStatementEClass == null)
    {
      startStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(172);
    }
    return startStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStartStatement_Port()
  {
        return (EReference)getStartStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStopStatement()
  {
    if (stopStatementEClass == null)
    {
      stopStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(173);
    }
    return stopStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStopStatement_Port()
  {
        return (EReference)getStopStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getClearStatement()
  {
    if (clearStatementEClass == null)
    {
      clearStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(174);
    }
    return clearStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getClearStatement_Port()
  {
        return (EReference)getClearStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortOrAll()
  {
    if (portOrAllEClass == null)
    {
      portOrAllEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(175);
    }
    return portOrAllEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAll_Port()
  {
        return (EReference)getPortOrAll().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortOrAll_ArrayRefs()
  {
        return (EReference)getPortOrAll().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRaiseStatement()
  {
    if (raiseStatementEClass == null)
    {
      raiseStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(176);
    }
    return raiseStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRaiseStatement_Port()
  {
        return (EReference)getRaiseStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRaiseStatement_ArrayRefs()
  {
        return (EReference)getRaiseStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRaiseStatement_Op()
  {
        return (EReference)getRaiseStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortRaiseOp()
  {
    if (portRaiseOpEClass == null)
    {
      portRaiseOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(177);
    }
    return portRaiseOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRaiseOp_Signature()
  {
        return (EReference)getPortRaiseOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRaiseOp_Template()
  {
        return (EReference)getPortRaiseOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortRaiseOp_To()
  {
        return (EReference)getPortRaiseOp().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReplyStatement()
  {
    if (replyStatementEClass == null)
    {
      replyStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(178);
    }
    return replyStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReplyStatement_Port()
  {
        return (EReference)getReplyStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReplyStatement_ArrayRefs()
  {
        return (EReference)getReplyStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReplyStatement_Op()
  {
        return (EReference)getReplyStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortReplyOp()
  {
    if (portReplyOpEClass == null)
    {
      portReplyOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(179);
    }
    return portReplyOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortReplyOp_Template()
  {
        return (EReference)getPortReplyOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortReplyOp_Value()
  {
        return (EReference)getPortReplyOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortReplyOp_To()
  {
        return (EReference)getPortReplyOp().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReplyValue()
  {
    if (replyValueEClass == null)
    {
      replyValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(180);
    }
    return replyValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReplyValue_Expr()
  {
        return (EReference)getReplyValue().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallStatement()
  {
    if (callStatementEClass == null)
    {
      callStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(181);
    }
    return callStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallStatement_Port()
  {
        return (EReference)getCallStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallStatement_ArrayRefs()
  {
        return (EReference)getCallStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallStatement_Op()
  {
        return (EReference)getCallStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallStatement_Body()
  {
        return (EReference)getCallStatement().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortCallOp()
  {
    if (portCallOpEClass == null)
    {
      portCallOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(182);
    }
    return portCallOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortCallOp_Params()
  {
        return (EReference)getPortCallOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortCallOp_To()
  {
        return (EReference)getPortCallOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallParameters()
  {
    if (callParametersEClass == null)
    {
      callParametersEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(183);
    }
    return callParametersEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallParameters_Template()
  {
        return (EReference)getCallParameters().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallParameters_Value()
  {
        return (EReference)getCallParameters().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallTimerValue()
  {
    if (callTimerValueEClass == null)
    {
      callTimerValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(184);
    }
    return callTimerValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallTimerValue_Expr()
  {
        return (EReference)getCallTimerValue().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortCallBody()
  {
    if (portCallBodyEClass == null)
    {
      portCallBodyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(185);
    }
    return portCallBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortCallBody_CallBody()
  {
        return (EReference)getPortCallBody().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallBodyStatementList()
  {
    if (callBodyStatementListEClass == null)
    {
      callBodyStatementListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(186);
    }
    return callBodyStatementListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallBodyStatementList_Body()
  {
        return (EReference)getCallBodyStatementList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallBodyStatement()
  {
    if (callBodyStatementEClass == null)
    {
      callBodyStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(187);
    }
    return callBodyStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallBodyStatement_Call()
  {
        return (EReference)getCallBodyStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallBodyStatement_Block()
  {
        return (EReference)getCallBodyStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallBodyGuard()
  {
    if (callBodyGuardEClass == null)
    {
      callBodyGuardEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(188);
    }
    return callBodyGuardEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallBodyGuard_Guard()
  {
        return (EReference)getCallBodyGuard().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallBodyGuard_Ops()
  {
        return (EReference)getCallBodyGuard().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallBodyOps()
  {
    if (callBodyOpsEClass == null)
    {
      callBodyOpsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(189);
    }
    return callBodyOpsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallBodyOps_Reply()
  {
        return (EReference)getCallBodyOps().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallBodyOps_Catch()
  {
        return (EReference)getCallBodyOps().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSendStatement()
  {
    if (sendStatementEClass == null)
    {
      sendStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(190);
    }
    return sendStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSendStatement_Port()
  {
        return (EReference)getSendStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSendStatement_ArryRefs()
  {
        return (EReference)getSendStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSendStatement_Send()
  {
        return (EReference)getSendStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortSendOp()
  {
    if (portSendOpEClass == null)
    {
      portSendOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(191);
    }
    return portSendOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortSendOp_Template()
  {
        return (EReference)getPortSendOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortSendOp_To()
  {
        return (EReference)getPortSendOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getToClause()
  {
    if (toClauseEClass == null)
    {
      toClauseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(192);
    }
    return toClauseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getToClause_Template()
  {
        return (EReference)getToClause().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getToClause_Ref()
  {
        return (EReference)getToClause().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionDefList()
  {
    if (functionDefListEClass == null)
    {
      functionDefListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(193);
    }
    return functionDefListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionDefList_LocDef()
  {
        return (EReference)getFunctionDefList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionDefList_LocInst()
  {
        return (EReference)getFunctionDefList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionDefList_Ws()
  {
        return (EReference)getFunctionDefList().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunctionDefList_Sc()
  {
        return (EAttribute)getFunctionDefList().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionLocalDef()
  {
    if (functionLocalDefEClass == null)
    {
      functionLocalDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(194);
    }
    return functionLocalDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionLocalDef_ConstDef()
  {
        return (EReference)getFunctionLocalDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionLocalDef_TemplateDef()
  {
        return (EReference)getFunctionLocalDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionLocalInst()
  {
    if (functionLocalInstEClass == null)
    {
      functionLocalInstEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(195);
    }
    return functionLocalInstEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionLocalInst_Variable()
  {
        return (EReference)getFunctionLocalInst().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionLocalInst_Timer()
  {
        return (EReference)getFunctionLocalInst().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimerInstance()
  {
    if (timerInstanceEClass == null)
    {
      timerInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(196);
    }
    return timerInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimerInstance_List()
  {
        return (EReference)getTimerInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVarInstance()
  {
    if (varInstanceEClass == null)
    {
      varInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(197);
    }
    return varInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVarInstance_ListMod()
  {
        return (EAttribute)getVarInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVarInstance_ListType()
  {
        return (EReference)getVarInstance().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVarInstance_List()
  {
        return (EReference)getVarInstance().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVarInstance_ResTemplate()
  {
        return (EReference)getVarInstance().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getVarInstance_TempMod()
  {
        return (EAttribute)getVarInstance().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVarInstance_Type()
  {
        return (EReference)getVarInstance().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVarInstance_TempList()
  {
        return (EReference)getVarInstance().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVarList()
  {
    if (varListEClass == null)
    {
      varListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(198);
    }
    return varListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVarList_Variables()
  {
        return (EReference)getVarList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModuleOrGroup()
  {
    if (moduleOrGroupEClass == null)
    {
      moduleOrGroupEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(199);
    }
    return moduleOrGroupEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReferencedType()
  {
    if (referencedTypeEClass == null)
    {
      referencedTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(200);
    }
    return referencedTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeDef()
  {
    if (typeDefEClass == null)
    {
      typeDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(201);
    }
    return typeDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTypeDef_Body()
  {
        return (EReference)getTypeDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeDefBody()
  {
    if (typeDefBodyEClass == null)
    {
      typeDefBodyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(202);
    }
    return typeDefBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTypeDefBody_Structured()
  {
        return (EReference)getTypeDefBody().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTypeDefBody_Sub()
  {
        return (EReference)getTypeDefBody().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubTypeDef()
  {
    if (subTypeDefEClass == null)
    {
      subTypeDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(203);
    }
    return subTypeDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSubTypeDef_Type()
  {
        return (EReference)getSubTypeDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSubTypeDef_Array()
  {
        return (EReference)getSubTypeDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSubTypeDef_Spec()
  {
        return (EReference)getSubTypeDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubTypeDefNamed()
  {
    if (subTypeDefNamedEClass == null)
    {
      subTypeDefNamedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(204);
    }
    return subTypeDefNamedEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStructuredTypeDef()
  {
    if (structuredTypeDefEClass == null)
    {
      structuredTypeDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(205);
    }
    return structuredTypeDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructuredTypeDef_Record()
  {
        return (EReference)getStructuredTypeDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructuredTypeDef_Union()
  {
        return (EReference)getStructuredTypeDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructuredTypeDef_Set()
  {
        return (EReference)getStructuredTypeDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructuredTypeDef_RecordOf()
  {
        return (EReference)getStructuredTypeDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructuredTypeDef_SetOf()
  {
        return (EReference)getStructuredTypeDef().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructuredTypeDef_EnumDef()
  {
        return (EReference)getStructuredTypeDef().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructuredTypeDef_Port()
  {
        return (EReference)getStructuredTypeDef().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructuredTypeDef_Component()
  {
        return (EReference)getStructuredTypeDef().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordDef()
  {
    if (recordDefEClass == null)
    {
      recordDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(206);
    }
    return recordDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordDefNamed()
  {
    if (recordDefNamedEClass == null)
    {
      recordDefNamedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(207);
    }
    return recordDefNamedEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordDefNamed_Body()
  {
        return (EReference)getRecordDefNamed().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordOfDef()
  {
    if (recordOfDefEClass == null)
    {
      recordOfDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(208);
    }
    return recordOfDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordOfDef_Lenght()
  {
        return (EReference)getRecordOfDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordOfDef_Type()
  {
        return (EReference)getRecordOfDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordOfDef_Nested()
  {
        return (EReference)getRecordOfDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRecordOfDef_Spec()
  {
        return (EReference)getRecordOfDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRecordOfDefNamed()
  {
    if (recordOfDefNamedEClass == null)
    {
      recordOfDefNamedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(209);
    }
    return recordOfDefNamedEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStructDefBody()
  {
    if (structDefBodyEClass == null)
    {
      structDefBodyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(210);
    }
    return structDefBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructDefBody_Defs()
  {
        return (EReference)getStructDefBody().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStructFieldDef()
  {
    if (structFieldDefEClass == null)
    {
      structFieldDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(211);
    }
    return structFieldDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructFieldDef_Type()
  {
        return (EReference)getStructFieldDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructFieldDef_NestedType()
  {
        return (EReference)getStructFieldDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructFieldDef_Array()
  {
        return (EReference)getStructFieldDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStructFieldDef_Spec()
  {
        return (EReference)getStructFieldDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStructFieldDef_Optional()
  {
        return (EAttribute)getStructFieldDef().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSetOfDef()
  {
    if (setOfDefEClass == null)
    {
      setOfDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(212);
    }
    return setOfDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSetOfDef_SetLength()
  {
        return (EReference)getSetOfDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSetOfDef_Type()
  {
        return (EReference)getSetOfDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSetOfDef_Nested()
  {
        return (EReference)getSetOfDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSetOfDef_SetSpec()
  {
        return (EReference)getSetOfDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSetOfDefNamed()
  {
    if (setOfDefNamedEClass == null)
    {
      setOfDefNamedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(213);
    }
    return setOfDefNamedEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortDef()
  {
    if (portDefEClass == null)
    {
      portDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(214);
    }
    return portDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortDef_Body()
  {
        return (EReference)getPortDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortDefBody()
  {
    if (portDefBodyEClass == null)
    {
      portDefBodyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(215);
    }
    return portDefBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortDefBody_Attribs()
  {
        return (EReference)getPortDefBody().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortDefAttribs()
  {
    if (portDefAttribsEClass == null)
    {
      portDefAttribsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(216);
    }
    return portDefAttribsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortDefAttribs_Message()
  {
        return (EReference)getPortDefAttribs().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortDefAttribs_Procedure()
  {
        return (EReference)getPortDefAttribs().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortDefAttribs_Mixed()
  {
        return (EReference)getPortDefAttribs().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMixedAttribs()
  {
    if (mixedAttribsEClass == null)
    {
      mixedAttribsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(217);
    }
    return mixedAttribsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMixedAttribs_AddressDecls()
  {
        return (EReference)getMixedAttribs().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMixedAttribs_MixedLists()
  {
        return (EReference)getMixedAttribs().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMixedAttribs_ConfigDefs()
  {
        return (EReference)getMixedAttribs().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMixedList()
  {
    if (mixedListEClass == null)
    {
      mixedListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(218);
    }
    return mixedListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMixedList_List()
  {
        return (EReference)getMixedList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProcOrTypeList()
  {
    if (procOrTypeListEClass == null)
    {
      procOrTypeListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(219);
    }
    return procOrTypeListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getProcOrTypeList_All()
  {
        return (EAttribute)getProcOrTypeList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcOrTypeList_Type()
  {
        return (EReference)getProcOrTypeList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProcOrType()
  {
    if (procOrTypeEClass == null)
    {
      procOrTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(220);
    }
    return procOrTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcOrType_Type()
  {
        return (EReference)getProcOrType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMessageAttribs()
  {
    if (messageAttribsEClass == null)
    {
      messageAttribsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(221);
    }
    return messageAttribsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMessageAttribs_Adresses()
  {
        return (EReference)getMessageAttribs().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMessageAttribs_Messages()
  {
        return (EReference)getMessageAttribs().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMessageAttribs_Configs()
  {
        return (EReference)getMessageAttribs().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConfigParamDef()
  {
    if (configParamDefEClass == null)
    {
      configParamDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(222);
    }
    return configParamDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigParamDef_Map()
  {
        return (EReference)getConfigParamDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigParamDef_Unmap()
  {
        return (EReference)getConfigParamDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMapParamDef()
  {
    if (mapParamDefEClass == null)
    {
      mapParamDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(223);
    }
    return mapParamDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMapParamDef_Values()
  {
        return (EReference)getMapParamDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnmapParamDef()
  {
    if (unmapParamDefEClass == null)
    {
      unmapParamDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(224);
    }
    return unmapParamDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnmapParamDef_Values()
  {
        return (EReference)getUnmapParamDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAddressDecl()
  {
    if (addressDeclEClass == null)
    {
      addressDeclEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(225);
    }
    return addressDeclEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAddressDecl_Type()
  {
        return (EReference)getAddressDecl().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProcedureAttribs()
  {
    if (procedureAttribsEClass == null)
    {
      procedureAttribsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(226);
    }
    return procedureAttribsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcedureAttribs_Addresses()
  {
        return (EReference)getProcedureAttribs().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcedureAttribs_Procs()
  {
        return (EReference)getProcedureAttribs().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcedureAttribs_Configs()
  {
        return (EReference)getProcedureAttribs().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentDef()
  {
    if (componentDefEClass == null)
    {
      componentDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(227);
    }
    return componentDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDef_Extends()
  {
        return (EReference)getComponentDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDef_Defs()
  {
        return (EReference)getComponentDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentDefList()
  {
    if (componentDefListEClass == null)
    {
      componentDefListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(228);
    }
    return componentDefListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDefList_Element()
  {
        return (EReference)getComponentDefList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentDefList_Wst()
  {
        return (EReference)getComponentDefList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComponentDefList_Sc()
  {
        return (EAttribute)getComponentDefList().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortInstance()
  {
    if (portInstanceEClass == null)
    {
      portInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(229);
    }
    return portInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortInstance_Ref()
  {
        return (EReference)getPortInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortInstance_Instances()
  {
        return (EReference)getPortInstance().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPortElement()
  {
    if (portElementEClass == null)
    {
      portElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(230);
    }
    return portElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPortElement_Array()
  {
        return (EReference)getPortElement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComponentElementDef()
  {
    if (componentElementDefEClass == null)
    {
      componentElementDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(231);
    }
    return componentElementDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentElementDef_Port()
  {
        return (EReference)getComponentElementDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentElementDef_Variable()
  {
        return (EReference)getComponentElementDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentElementDef_Timer()
  {
        return (EReference)getComponentElementDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentElementDef_Const()
  {
        return (EReference)getComponentElementDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComponentElementDef_Template()
  {
        return (EReference)getComponentElementDef().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProcedureList()
  {
    if (procedureListEClass == null)
    {
      procedureListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(232);
    }
    return procedureListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcedureList_AllOrSigList()
  {
        return (EReference)getProcedureList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllOrSignatureList()
  {
    if (allOrSignatureListEClass == null)
    {
      allOrSignatureListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(233);
    }
    return allOrSignatureListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAllOrSignatureList_All()
  {
        return (EAttribute)getAllOrSignatureList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllOrSignatureList_SignatureList()
  {
        return (EReference)getAllOrSignatureList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSignatureList()
  {
    if (signatureListEClass == null)
    {
      signatureListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(234);
    }
    return signatureListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSignatureList_Sigs()
  {
        return (EReference)getSignatureList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEnumDef()
  {
    if (enumDefEClass == null)
    {
      enumDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(235);
    }
    return enumDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEnumDef_List()
  {
        return (EReference)getEnumDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEnumDefNamed()
  {
    if (enumDefNamedEClass == null)
    {
      enumDefNamedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(236);
    }
    return enumDefNamedEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNestedTypeDef()
  {
    if (nestedTypeDefEClass == null)
    {
      nestedTypeDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(237);
    }
    return nestedTypeDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNestedRecordDef()
  {
    if (nestedRecordDefEClass == null)
    {
      nestedRecordDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(238);
    }
    return nestedRecordDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedRecordDef_Defs()
  {
        return (EReference)getNestedRecordDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNestedUnionDef()
  {
    if (nestedUnionDefEClass == null)
    {
      nestedUnionDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(239);
    }
    return nestedUnionDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedUnionDef_Defs()
  {
        return (EReference)getNestedUnionDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNestedSetDef()
  {
    if (nestedSetDefEClass == null)
    {
      nestedSetDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(240);
    }
    return nestedSetDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedSetDef_Defs()
  {
        return (EReference)getNestedSetDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNestedRecordOfDef()
  {
    if (nestedRecordOfDefEClass == null)
    {
      nestedRecordOfDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(241);
    }
    return nestedRecordOfDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedRecordOfDef_Length()
  {
        return (EReference)getNestedRecordOfDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedRecordOfDef_Type()
  {
        return (EReference)getNestedRecordOfDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedRecordOfDef_Nested()
  {
        return (EReference)getNestedRecordOfDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNestedSetOfDef()
  {
    if (nestedSetOfDefEClass == null)
    {
      nestedSetOfDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(242);
    }
    return nestedSetOfDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedSetOfDef_Length()
  {
        return (EReference)getNestedSetOfDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedSetOfDef_Type()
  {
        return (EReference)getNestedSetOfDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedSetOfDef_Nested()
  {
        return (EReference)getNestedSetOfDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNestedEnumDef()
  {
    if (nestedEnumDefEClass == null)
    {
      nestedEnumDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(243);
    }
    return nestedEnumDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedEnumDef_List()
  {
        return (EReference)getNestedEnumDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMessageList()
  {
    if (messageListEClass == null)
    {
      messageListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(244);
    }
    return messageListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMessageList_AllOrTypeList()
  {
        return (EReference)getMessageList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllOrTypeList()
  {
    if (allOrTypeListEClass == null)
    {
      allOrTypeListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(245);
    }
    return allOrTypeListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAllOrTypeList_All()
  {
        return (EAttribute)getAllOrTypeList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllOrTypeList_TypeList()
  {
        return (EReference)getAllOrTypeList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTypeList()
  {
    if (typeListEClass == null)
    {
      typeListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(246);
    }
    return typeListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTypeList_Types()
  {
        return (EReference)getTypeList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnionDef()
  {
    if (unionDefEClass == null)
    {
      unionDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(247);
    }
    return unionDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnionDef_Body()
  {
        return (EReference)getUnionDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnionDefNamed()
  {
    if (unionDefNamedEClass == null)
    {
      unionDefNamedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(248);
    }
    return unionDefNamedEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnionDefBody()
  {
    if (unionDefBodyEClass == null)
    {
      unionDefBodyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(249);
    }
    return unionDefBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnionDefBody_Defs()
  {
        return (EReference)getUnionDefBody().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEnumerationList()
  {
    if (enumerationListEClass == null)
    {
      enumerationListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(250);
    }
    return enumerationListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getEnumerationList_Enums()
  {
        return (EReference)getEnumerationList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEnumeration()
  {
    if (enumerationEClass == null)
    {
      enumerationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(251);
    }
    return enumerationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnionFieldDef()
  {
    if (unionFieldDefEClass == null)
    {
      unionFieldDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(252);
    }
    return unionFieldDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnionFieldDef_Type()
  {
        return (EReference)getUnionFieldDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnionFieldDef_NestedType()
  {
        return (EReference)getUnionFieldDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnionFieldDef_Array()
  {
        return (EReference)getUnionFieldDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnionFieldDef_SubType()
  {
        return (EReference)getUnionFieldDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSetDef()
  {
    if (setDefEClass == null)
    {
      setDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(253);
    }
    return setDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSetDef_Body()
  {
        return (EReference)getSetDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSetDefNamed()
  {
    if (setDefNamedEClass == null)
    {
      setDefNamedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(254);
    }
    return setDefNamedEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubTypeSpec()
  {
    if (subTypeSpecEClass == null)
    {
      subTypeSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(255);
    }
    return subTypeSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSubTypeSpec_Length()
  {
        return (EReference)getSubTypeSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllowedValuesSpec()
  {
    if (allowedValuesSpecEClass == null)
    {
      allowedValuesSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(256);
    }
    return allowedValuesSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllowedValuesSpec_Template()
  {
        return (EReference)getAllowedValuesSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateOrRange()
  {
    if (templateOrRangeEClass == null)
    {
      templateOrRangeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(257);
    }
    return templateOrRangeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateOrRange_Range()
  {
        return (EReference)getTemplateOrRange().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateOrRange_Template()
  {
        return (EReference)getTemplateOrRange().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateOrRange_Type()
  {
        return (EReference)getTemplateOrRange().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRangeDef()
  {
    if (rangeDefEClass == null)
    {
      rangeDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(258);
    }
    return rangeDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRangeDef_B1()
  {
        return (EReference)getRangeDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRangeDef_B2()
  {
        return (EReference)getRangeDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBound()
  {
    if (boundEClass == null)
    {
      boundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(259);
    }
    return boundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBound_Expr()
  {
        return (EReference)getBound().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStringLength()
  {
    if (stringLengthEClass == null)
    {
      stringLengthEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(260);
    }
    return stringLengthEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStringLength_Expr()
  {
        return (EReference)getStringLength().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStringLength_Bound()
  {
        return (EReference)getStringLength().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCharStringMatch()
  {
    if (charStringMatchEClass == null)
    {
      charStringMatchEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(261);
    }
    return charStringMatchEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCharStringMatch_Pattern()
  {
        return (EReference)getCharStringMatch().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPatternParticle()
  {
    if (patternParticleEClass == null)
    {
      patternParticleEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(262);
    }
    return patternParticleEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateDef()
  {
    if (templateDefEClass == null)
    {
      templateDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(263);
    }
    return templateDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateDef_Restriction()
  {
        return (EReference)getTemplateDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTemplateDef_Fuzzy()
  {
        return (EAttribute)getTemplateDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateDef_Base()
  {
        return (EReference)getTemplateDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateDef_Derived()
  {
        return (EReference)getTemplateDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateDef_Body()
  {
        return (EReference)getTemplateDef().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDerivedDef()
  {
    if (derivedDefEClass == null)
    {
      derivedDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(264);
    }
    return derivedDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDerivedDef_Template()
  {
        return (EReference)getDerivedDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBaseTemplate()
  {
    if (baseTemplateEClass == null)
    {
      baseTemplateEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(265);
    }
    return baseTemplateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBaseTemplate_Type()
  {
        return (EReference)getBaseTemplate().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getBaseTemplate_ParList()
  {
        return (EReference)getBaseTemplate().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTempVarList()
  {
    if (tempVarListEClass == null)
    {
      tempVarListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(266);
    }
    return tempVarListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTempVarList_Variables()
  {
        return (EReference)getTempVarList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimerVarInstance()
  {
    if (timerVarInstanceEClass == null)
    {
      timerVarInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(267);
    }
    return timerVarInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSingleVarInstance()
  {
    if (singleVarInstanceEClass == null)
    {
      singleVarInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(268);
    }
    return singleVarInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleVarInstance_Array()
  {
        return (EReference)getSingleVarInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSingleVarInstance_Ac()
  {
        return (EAttribute)getSingleVarInstance().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleVarInstance_Expr()
  {
        return (EReference)getSingleVarInstance().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSingleTempVarInstance()
  {
    if (singleTempVarInstanceEClass == null)
    {
      singleTempVarInstanceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(269);
    }
    return singleTempVarInstanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleTempVarInstance_Array()
  {
        return (EReference)getSingleTempVarInstance().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSingleTempVarInstance_Ac()
  {
        return (EAttribute)getSingleTempVarInstance().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleTempVarInstance_Template()
  {
        return (EReference)getSingleTempVarInstance().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInLineTemplate()
  {
    if (inLineTemplateEClass == null)
    {
      inLineTemplateEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(270);
    }
    return inLineTemplateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInLineTemplate_Type()
  {
        return (EReference)getInLineTemplate().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInLineTemplate_Derived()
  {
        return (EReference)getInLineTemplate().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInLineTemplate_Template()
  {
        return (EReference)getInLineTemplate().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDerivedRefWithParList()
  {
    if (derivedRefWithParListEClass == null)
    {
      derivedRefWithParListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(271);
    }
    return derivedRefWithParListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDerivedRefWithParList_Template()
  {
        return (EReference)getDerivedRefWithParList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateBody()
  {
    if (templateBodyEClass == null)
    {
      templateBodyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(272);
    }
    return templateBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateBody_Simple()
  {
        return (EReference)getTemplateBody().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateBody_Field()
  {
        return (EReference)getTemplateBody().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateBody_Array()
  {
        return (EReference)getTemplateBody().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateBody_Extra()
  {
        return (EReference)getTemplateBody().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExtraMatchingAttributes()
  {
    if (extraMatchingAttributesEClass == null)
    {
      extraMatchingAttributesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(273);
    }
    return extraMatchingAttributesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldSpecList()
  {
    if (fieldSpecListEClass == null)
    {
      fieldSpecListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(274);
    }
    return fieldSpecListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldSpecList_Spec()
  {
        return (EReference)getFieldSpecList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldSpec()
  {
    if (fieldSpecEClass == null)
    {
      fieldSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(275);
    }
    return fieldSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldSpec_Ref()
  {
        return (EReference)getFieldSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldSpec_Body()
  {
        return (EReference)getFieldSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimpleSpec()
  {
    if (simpleSpecEClass == null)
    {
      simpleSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(276);
    }
    return simpleSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimpleSpec_Expr()
  {
        return (EReference)getSimpleSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimpleSpec_Spec()
  {
        return (EReference)getSimpleSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimpleTemplateSpec()
  {
    if (simpleTemplateSpecEClass == null)
    {
      simpleTemplateSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(277);
    }
    return simpleTemplateSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimpleTemplateSpec_Expr()
  {
        return (EReference)getSimpleTemplateSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimpleTemplateSpec_Spec()
  {
        return (EReference)getSimpleTemplateSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSingleTemplateExpression()
  {
    if (singleTemplateExpressionEClass == null)
    {
      singleTemplateExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(278);
    }
    return singleTemplateExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleTemplateExpression_Symbol()
  {
        return (EReference)getSingleTemplateExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleTemplateExpression_List()
  {
        return (EReference)getSingleTemplateExpression().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleTemplateExpression_Extended()
  {
        return (EReference)getSingleTemplateExpression().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateRefWithParList()
  {
    if (templateRefWithParListEClass == null)
    {
      templateRefWithParListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(279);
    }
    return templateRefWithParListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateRefWithParList_Template()
  {
        return (EReference)getTemplateRefWithParList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateRefWithParList_List()
  {
        return (EReference)getTemplateRefWithParList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateActualParList()
  {
    if (templateActualParListEClass == null)
    {
      templateActualParListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(280);
    }
    return templateActualParListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateActualParList_Actual()
  {
        return (EReference)getTemplateActualParList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateActualParList_Assign()
  {
        return (EReference)getTemplateActualParList().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMatchingSymbol()
  {
    if (matchingSymbolEClass == null)
    {
      matchingSymbolEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(281);
    }
    return matchingSymbolEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMatchingSymbol_Complement()
  {
        return (EReference)getMatchingSymbol().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMatchingSymbol_Qwlm()
  {
        return (EReference)getMatchingSymbol().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMatchingSymbol_Swlm()
  {
        return (EReference)getMatchingSymbol().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMatchingSymbol_Range()
  {
        return (EReference)getMatchingSymbol().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMatchingSymbol_String()
  {
        return (EReference)getMatchingSymbol().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMatchingSymbol_Subset()
  {
        return (EReference)getMatchingSymbol().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMatchingSymbol_Superset()
  {
        return (EReference)getMatchingSymbol().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMatchingSymbol_Templates()
  {
        return (EReference)getMatchingSymbol().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubsetMatch()
  {
    if (subsetMatchEClass == null)
    {
      subsetMatchEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(282);
    }
    return subsetMatchEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSupersetMatch()
  {
    if (supersetMatchEClass == null)
    {
      supersetMatchEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(283);
    }
    return supersetMatchEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRange()
  {
    if (rangeEClass == null)
    {
      rangeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(284);
    }
    return rangeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRange_B1()
  {
        return (EReference)getRange().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRange_B2()
  {
        return (EReference)getRange().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWildcardLengthMatch()
  {
    if (wildcardLengthMatchEClass == null)
    {
      wildcardLengthMatchEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(285);
    }
    return wildcardLengthMatchEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComplement()
  {
    if (complementEClass == null)
    {
      complementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(286);
    }
    return complementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getListOfTemplates()
  {
    if (listOfTemplatesEClass == null)
    {
      listOfTemplatesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(287);
    }
    return listOfTemplatesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getListOfTemplates_Items()
  {
        return (EReference)getListOfTemplates().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateOps()
  {
    if (templateOpsEClass == null)
    {
      templateOpsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(288);
    }
    return templateOpsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateOps_Template()
  {
        return (EReference)getTemplateOps().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMatchOp()
  {
    if (matchOpEClass == null)
    {
      matchOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(289);
    }
    return matchOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMatchOp_Expr()
  {
        return (EReference)getMatchOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getValueofOp()
  {
    if (valueofOpEClass == null)
    {
      valueofOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(290);
    }
    return valueofOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConfigurationOps()
  {
    if (configurationOpsEClass == null)
    {
      configurationOpsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(291);
    }
    return configurationOpsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationOps_Create()
  {
        return (EReference)getConfigurationOps().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationOps_Alive()
  {
        return (EReference)getConfigurationOps().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConfigurationOps_Running()
  {
        return (EReference)getConfigurationOps().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCreateOp()
  {
    if (createOpEClass == null)
    {
      createOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(292);
    }
    return createOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCreateOp_Type()
  {
        return (EReference)getCreateOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCreateOp_Expr1()
  {
        return (EReference)getCreateOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCreateOp_Expr2()
  {
        return (EReference)getCreateOp().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRunningOp()
  {
    if (runningOpEClass == null)
    {
      runningOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(293);
    }
    return runningOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRunningOp_Component()
  {
        return (EReference)getRunningOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRunningOp_Index()
  {
        return (EReference)getRunningOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOpCall()
  {
    if (opCallEClass == null)
    {
      opCallEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(294);
    }
    return opCallEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpCall_Configuration()
  {
        return (EReference)getOpCall().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOpCall_Verdict()
  {
        return (EAttribute)getOpCall().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpCall_Timer()
  {
        return (EReference)getOpCall().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpCall_Function()
  {
        return (EReference)getOpCall().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpCall_ExtendedFunction()
  {
        return (EReference)getOpCall().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpCall_PreFunction()
  {
        return (EReference)getOpCall().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpCall_Testcase()
  {
        return (EReference)getOpCall().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpCall_Activate()
  {
        return (EReference)getOpCall().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpCall_TemplateOps()
  {
        return (EReference)getOpCall().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpCall_ExtendedTemplate()
  {
        return (EReference)getOpCall().getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpCall_Field()
  {
        return (EReference)getOpCall().getEStructuralFeatures().get(10);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAliveOp()
  {
    if (aliveOpEClass == null)
    {
      aliveOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(295);
    }
    return aliveOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAliveOp_Component()
  {
        return (EReference)getAliveOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAliveOp_Index()
  {
        return (EReference)getAliveOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateListItem()
  {
    if (templateListItemEClass == null)
    {
      templateListItemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(296);
    }
    return templateListItemEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateListItem_Body()
  {
        return (EReference)getTemplateListItem().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateListItem_All()
  {
        return (EReference)getTemplateListItem().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllElementsFrom()
  {
    if (allElementsFromEClass == null)
    {
      allElementsFromEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(297);
    }
    return allElementsFromEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllElementsFrom_Body()
  {
        return (EReference)getAllElementsFrom().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSignature()
  {
    if (signatureEClass == null)
    {
      signatureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(298);
    }
    return signatureEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSignature_Ref()
  {
        return (EReference)getSignature().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateInstanceAssignment()
  {
    if (templateInstanceAssignmentEClass == null)
    {
      templateInstanceAssignmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(299);
    }
    return templateInstanceAssignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateInstanceAssignment_Name()
  {
        return (EReference)getTemplateInstanceAssignment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTemplateInstanceAssignment_Template()
  {
        return (EReference)getTemplateInstanceAssignment().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateInstanceActualPar()
  {
    if (templateInstanceActualParEClass == null)
    {
      templateInstanceActualParEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(300);
    }
    return templateInstanceActualParEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTemplateRestriction()
  {
    if (templateRestrictionEClass == null)
    {
      templateRestrictionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(301);
    }
    return templateRestrictionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTemplateRestriction_Omit()
  {
        return (EAttribute)getTemplateRestriction().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTemplateRestriction_Value()
  {
        return (EAttribute)getTemplateRestriction().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTemplateRestriction_Present()
  {
        return (EAttribute)getTemplateRestriction().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRestrictedTemplate()
  {
    if (restrictedTemplateEClass == null)
    {
      restrictedTemplateEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(302);
    }
    return restrictedTemplateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRestrictedTemplate_Restriction()
  {
        return (EReference)getRestrictedTemplate().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLogStatement()
  {
    if (logStatementEClass == null)
    {
      logStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(303);
    }
    return logStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLogStatement_Item()
  {
        return (EReference)getLogStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLogItem()
  {
    if (logItemEClass == null)
    {
      logItemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(304);
    }
    return logItemEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLogItem_Template()
  {
        return (EReference)getLogItem().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExpression()
  {
    if (expressionEClass == null)
    {
      expressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(305);
    }
    return expressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWithStatement()
  {
    if (withStatementEClass == null)
    {
      withStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(306);
    }
    return withStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWithStatement_Attrib()
  {
        return (EReference)getWithStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWithAttribList()
  {
    if (withAttribListEClass == null)
    {
      withAttribListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(307);
    }
    return withAttribListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWithAttribList_Multi()
  {
        return (EReference)getWithAttribList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMultiWithAttrib()
  {
    if (multiWithAttribEClass == null)
    {
      multiWithAttribEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(308);
    }
    return multiWithAttribEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMultiWithAttrib_Single()
  {
        return (EReference)getMultiWithAttrib().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSingleWithAttrib()
  {
    if (singleWithAttribEClass == null)
    {
      singleWithAttribEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(309);
    }
    return singleWithAttribEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleWithAttrib_Attrib()
  {
        return (EReference)getSingleWithAttrib().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAttribQualifier()
  {
    if (attribQualifierEClass == null)
    {
      attribQualifierEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(310);
    }
    return attribQualifierEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAttribQualifier_List()
  {
        return (EReference)getAttribQualifier().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIdentifierList()
  {
    if (identifierListEClass == null)
    {
      identifierListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(311);
    }
    return identifierListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIdentifierList_Ids()
  {
        return (EAttribute)getIdentifierList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getQualifiedIdentifierList()
  {
    if (qualifiedIdentifierListEClass == null)
    {
      qualifiedIdentifierListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(312);
    }
    return qualifiedIdentifierListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getQualifiedIdentifierList_Qids()
  {
        return (EAttribute)getQualifiedIdentifierList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSingleConstDef()
  {
    if (singleConstDefEClass == null)
    {
      singleConstDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(313);
    }
    return singleConstDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleConstDef_Array()
  {
        return (EReference)getSingleConstDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSingleConstDef_Assign()
  {
        return (EAttribute)getSingleConstDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleConstDef_Expr()
  {
        return (EReference)getSingleConstDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCompoundExpression()
  {
    if (compoundExpressionEClass == null)
    {
      compoundExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(314);
    }
    return compoundExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayExpression()
  {
    if (arrayExpressionEClass == null)
    {
      arrayExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(315);
    }
    return arrayExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayExpression_List()
  {
        return (EReference)getArrayExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldExpressionList()
  {
    if (fieldExpressionListEClass == null)
    {
      fieldExpressionListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(316);
    }
    return fieldExpressionListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldExpressionList_Specs()
  {
        return (EReference)getFieldExpressionList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayElementExpressionList()
  {
    if (arrayElementExpressionListEClass == null)
    {
      arrayElementExpressionListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(317);
    }
    return arrayElementExpressionListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayElementExpressionList_Expr()
  {
        return (EReference)getArrayElementExpressionList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldExpressionSpec()
  {
    if (fieldExpressionSpecEClass == null)
    {
      fieldExpressionSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(318);
    }
    return fieldExpressionSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldExpressionSpec_FieldRef()
  {
        return (EReference)getFieldExpressionSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFieldExpressionSpec_Type()
  {
        return (EAttribute)getFieldExpressionSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldExpressionSpec_Array()
  {
        return (EReference)getFieldExpressionSpec().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldExpressionSpec_Expr()
  {
        return (EReference)getFieldExpressionSpec().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNotUsedOrExpression()
  {
    if (notUsedOrExpressionEClass == null)
    {
      notUsedOrExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(319);
    }
    return notUsedOrExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstantExpression()
  {
    if (constantExpressionEClass == null)
    {
      constantExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(320);
    }
    return constantExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCompoundConstExpression()
  {
    if (compoundConstExpressionEClass == null)
    {
      compoundConstExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(321);
    }
    return compoundConstExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldConstExpressionList()
  {
    if (fieldConstExpressionListEClass == null)
    {
      fieldConstExpressionListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(322);
    }
    return fieldConstExpressionListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldConstExpressionList_Specs()
  {
        return (EReference)getFieldConstExpressionList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldConstExpressionSpec()
  {
    if (fieldConstExpressionSpecEClass == null)
    {
      fieldConstExpressionSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(323);
    }
    return fieldConstExpressionSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldConstExpressionSpec_FieldRef()
  {
        return (EReference)getFieldConstExpressionSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFieldConstExpressionSpec_Type()
  {
        return (EAttribute)getFieldConstExpressionSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldConstExpressionSpec_Array()
  {
        return (EReference)getFieldConstExpressionSpec().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFieldConstExpressionSpec_ConstExpr()
  {
        return (EReference)getFieldConstExpressionSpec().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayConstExpression()
  {
    if (arrayConstExpressionEClass == null)
    {
      arrayConstExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(324);
    }
    return arrayConstExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayConstExpression_List()
  {
        return (EReference)getArrayConstExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayElementConstExpressionList()
  {
    if (arrayElementConstExpressionListEClass == null)
    {
      arrayElementConstExpressionListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(325);
    }
    return arrayElementConstExpressionListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayElementConstExpressionList_Expr()
  {
        return (EReference)getArrayElementConstExpressionList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstList()
  {
    if (constListEClass == null)
    {
      constListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(326);
    }
    return constListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConstList_List()
  {
        return (EReference)getConstList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getValue()
  {
    if (valueEClass == null)
    {
      valueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(327);
    }
    return valueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getValue_Predef()
  {
        return (EReference)getValue().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getValue_Ref()
  {
        return (EReference)getValue().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReferencedValue()
  {
    if (referencedValueEClass == null)
    {
      referencedValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(328);
    }
    return referencedValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReferencedValue_Head()
  {
        return (EReference)getReferencedValue().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReferencedValue_Fields()
  {
        return (EReference)getReferencedValue().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRefValueHead()
  {
    if (refValueHeadEClass == null)
    {
      refValueHeadEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(329);
    }
    return refValueHeadEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRefValueElement()
  {
    if (refValueElementEClass == null)
    {
      refValueElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(330);
    }
    return refValueElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHead()
  {
    if (headEClass == null)
    {
      headEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(331);
    }
    return headEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHead_Target()
  {
        return (EReference)getHead().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRefValueTail()
  {
    if (refValueTailEClass == null)
    {
      refValueTailEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(332);
    }
    return refValueTailEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRefValueTail_Value()
  {
        return (EReference)getRefValueTail().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRefValueTail_Array()
  {
        return (EReference)getRefValueTail().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSpecElement()
  {
    if (specElementEClass == null)
    {
      specElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(333);
    }
    return specElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSpecElement_Tail()
  {
        return (EReference)getSpecElement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExtendedFieldReference()
  {
    if (extendedFieldReferenceEClass == null)
    {
      extendedFieldReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(334);
    }
    return extendedFieldReferenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExtendedFieldReference_Field()
  {
        return (EReference)getExtendedFieldReference().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getExtendedFieldReference_Type()
  {
        return (EAttribute)getExtendedFieldReference().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExtendedFieldReference_Array()
  {
        return (EReference)getExtendedFieldReference().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRefValue()
  {
    if (refValueEClass == null)
    {
      refValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(335);
    }
    return refValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPredefinedValue()
  {
    if (predefinedValueEClass == null)
    {
      predefinedValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(336);
    }
    return predefinedValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_Bstring()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_Boolean()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_Integer()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_Hstring()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_Ostring()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_VerdictType()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_Float()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_Address()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_Omit()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_CharString()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPredefinedValue_Macro()
  {
        return (EAttribute)getPredefinedValue().getEStructuralFeatures().get(10);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanExpression()
  {
    if (booleanExpressionEClass == null)
    {
      booleanExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(337);
    }
    return booleanExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSingleExpression()
  {
    if (singleExpressionEClass == null)
    {
      singleExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(338);
    }
    return singleExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleExpression_Left()
  {
        return (EReference)getSingleExpression().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSingleExpression_Right()
  {
        return (EReference)getSingleExpression().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDefOrFieldRefList()
  {
    if (defOrFieldRefListEClass == null)
    {
      defOrFieldRefListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(339);
    }
    return defOrFieldRefListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDefOrFieldRefList_Refs()
  {
        return (EReference)getDefOrFieldRefList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDefOrFieldRef()
  {
    if (defOrFieldRefEClass == null)
    {
      defOrFieldRefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(340);
    }
    return defOrFieldRefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDefOrFieldRef_Id()
  {
        return (EReference)getDefOrFieldRef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDefOrFieldRef_Field()
  {
        return (EReference)getDefOrFieldRef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDefOrFieldRef_Extended()
  {
        return (EReference)getDefOrFieldRef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDefOrFieldRef_All()
  {
        return (EReference)getDefOrFieldRef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAllRef()
  {
    if (allRefEClass == null)
    {
      allRefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(341);
    }
    return allRefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllRef_GroupList()
  {
        return (EReference)getAllRef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAllRef_IdList()
  {
        return (EReference)getAllRef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGroupRefList()
  {
    if (groupRefListEClass == null)
    {
      groupRefListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(342);
    }
    return groupRefListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGroupRefList_Groups()
  {
        return (EReference)getGroupRefList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTTCN3Reference()
  {
    if (ttcn3ReferenceEClass == null)
    {
      ttcn3ReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(343);
    }
    return ttcn3ReferenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTTCN3Reference_Name()
  {
        return (EAttribute)getTTCN3Reference().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTTCN3ReferenceList()
  {
    if (ttcn3ReferenceListEClass == null)
    {
      ttcn3ReferenceListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(344);
    }
    return ttcn3ReferenceListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTTCN3ReferenceList_Refs()
  {
        return (EReference)getTTCN3ReferenceList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldReference()
  {
    if (fieldReferenceEClass == null)
    {
      fieldReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(345);
    }
    return fieldReferenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayOrBitRef()
  {
    if (arrayOrBitRefEClass == null)
    {
      arrayOrBitRefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(346);
    }
    return arrayOrBitRefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldOrBitNumber()
  {
    if (fieldOrBitNumberEClass == null)
    {
      fieldOrBitNumberEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(347);
    }
    return fieldOrBitNumberEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayValueOrAttrib()
  {
    if (arrayValueOrAttribEClass == null)
    {
      arrayValueOrAttribEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(348);
    }
    return arrayValueOrAttribEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayValueOrAttrib_List()
  {
        return (EReference)getArrayValueOrAttrib().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayElementSpecList()
  {
    if (arrayElementSpecListEClass == null)
    {
      arrayElementSpecListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(349);
    }
    return arrayElementSpecListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayElementSpecList_Spec()
  {
        return (EReference)getArrayElementSpecList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getArrayElementSpec()
  {
    if (arrayElementSpecEClass == null)
    {
      arrayElementSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(350);
    }
    return arrayElementSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayElementSpec_Match()
  {
        return (EReference)getArrayElementSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getArrayElementSpec_Body()
  {
        return (EReference)getArrayElementSpec().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTimerOps()
  {
    if (timerOpsEClass == null)
    {
      timerOpsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(351);
    }
    return timerOpsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimerOps_Read()
  {
        return (EReference)getTimerOps().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTimerOps_Run()
  {
        return (EReference)getTimerOps().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRunningTimerOp()
  {
    if (runningTimerOpEClass == null)
    {
      runningTimerOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(352);
    }
    return runningTimerOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRunningTimerOp_TimerRef()
  {
        return (EReference)getRunningTimerOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRunningTimerOp_Index()
  {
        return (EReference)getRunningTimerOp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReadTimerOp()
  {
    if (readTimerOpEClass == null)
    {
      readTimerOpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(353);
    }
    return readTimerOpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReadTimerOp_Timer()
  {
        return (EReference)getReadTimerOp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPermutationMatch()
  {
    if (permutationMatchEClass == null)
    {
      permutationMatchEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(354);
    }
    return permutationMatchEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPermutationMatch_List()
  {
        return (EReference)getPermutationMatch().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLoopConstruct()
  {
    if (loopConstructEClass == null)
    {
      loopConstructEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(355);
    }
    return loopConstructEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLoopConstruct_ForStm()
  {
        return (EReference)getLoopConstruct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLoopConstruct_WhileStm()
  {
        return (EReference)getLoopConstruct().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLoopConstruct_DoStm()
  {
        return (EReference)getLoopConstruct().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getForStatement()
  {
    if (forStatementEClass == null)
    {
      forStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(356);
    }
    return forStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getForStatement_Init()
  {
        return (EReference)getForStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getForStatement_Expression()
  {
        return (EReference)getForStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getForStatement_Assign()
  {
        return (EReference)getForStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getForStatement_Statement()
  {
        return (EReference)getForStatement().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWhileStatement()
  {
    if (whileStatementEClass == null)
    {
      whileStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(357);
    }
    return whileStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWhileStatement_Expression()
  {
        return (EReference)getWhileStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWhileStatement_Statement()
  {
        return (EReference)getWhileStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDoWhileStatement()
  {
    if (doWhileStatementEClass == null)
    {
      doWhileStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(358);
    }
    return doWhileStatementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDoWhileStatement_KeyDo()
  {
        return (EAttribute)getDoWhileStatement().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDoWhileStatement_Statement()
  {
        return (EReference)getDoWhileStatement().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDoWhileStatement_KeyWhile()
  {
        return (EAttribute)getDoWhileStatement().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDoWhileStatement_Expression()
  {
        return (EReference)getDoWhileStatement().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConditionalConstruct()
  {
    if (conditionalConstructEClass == null)
    {
      conditionalConstructEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(359);
    }
    return conditionalConstructEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalConstruct_Expression()
  {
        return (EReference)getConditionalConstruct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalConstruct_Statement()
  {
        return (EReference)getConditionalConstruct().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalConstruct_Elseifs()
  {
        return (EReference)getConditionalConstruct().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalConstruct_Else()
  {
        return (EReference)getConditionalConstruct().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getElseIfClause()
  {
    if (elseIfClauseEClass == null)
    {
      elseIfClauseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(360);
    }
    return elseIfClauseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getElseIfClause_KeyElse()
  {
        return (EAttribute)getElseIfClause().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getElseIfClause_KeyIf()
  {
        return (EAttribute)getElseIfClause().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getElseIfClause_Expression()
  {
        return (EReference)getElseIfClause().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getElseIfClause_Statement()
  {
        return (EReference)getElseIfClause().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getElseClause()
  {
    if (elseClauseEClass == null)
    {
      elseClauseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(361);
    }
    return elseClauseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getElseClause_Statement()
  {
        return (EReference)getElseClause().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInitial()
  {
    if (initialEClass == null)
    {
      initialEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(362);
    }
    return initialEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInitial_Variable()
  {
        return (EReference)getInitial().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInitial_Assignment()
  {
        return (EReference)getInitial().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectCaseConstruct()
  {
    if (selectCaseConstructEClass == null)
    {
      selectCaseConstructEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(363);
    }
    return selectCaseConstructEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectCaseConstruct_Expression()
  {
        return (EReference)getSelectCaseConstruct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectCaseConstruct_Body()
  {
        return (EReference)getSelectCaseConstruct().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectCaseBody()
  {
    if (selectCaseBodyEClass == null)
    {
      selectCaseBodyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(364);
    }
    return selectCaseBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectCaseBody_Cases()
  {
        return (EReference)getSelectCaseBody().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectCase()
  {
    if (selectCaseEClass == null)
    {
      selectCaseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(365);
    }
    return selectCaseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectCase_Template()
  {
        return (EReference)getSelectCase().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSelectCase_Else()
  {
        return (EAttribute)getSelectCase().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectCase_Statement()
  {
        return (EReference)getSelectCase().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionDef()
  {
    if (functionDefEClass == null)
    {
      functionDefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(366);
    }
    return functionDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFunctionDef_Det()
  {
        return (EAttribute)getFunctionDef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionDef_ParameterList()
  {
        return (EReference)getFunctionDef().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionDef_RunsOn()
  {
        return (EReference)getFunctionDef().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionDef_Mtc()
  {
        return (EReference)getFunctionDef().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionDef_System()
  {
        return (EReference)getFunctionDef().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionDef_ReturnType()
  {
        return (EReference)getFunctionDef().getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionDef_Statement()
  {
        return (EReference)getFunctionDef().getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMtcSpec()
  {
    if (mtcSpecEClass == null)
    {
      mtcSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(367);
    }
    return mtcSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMtcSpec_Component()
  {
        return (EReference)getMtcSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionFormalParList()
  {
    if (functionFormalParListEClass == null)
    {
      functionFormalParListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(368);
    }
    return functionFormalParListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionFormalParList_Params()
  {
        return (EReference)getFunctionFormalParList().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunctionFormalPar()
  {
    if (functionFormalParEClass == null)
    {
      functionFormalParEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(369);
    }
    return functionFormalParEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionFormalPar_Value()
  {
        return (EReference)getFunctionFormalPar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionFormalPar_Timer()
  {
        return (EReference)getFunctionFormalPar().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionFormalPar_Template()
  {
        return (EReference)getFunctionFormalPar().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunctionFormalPar_Port()
  {
        return (EReference)getFunctionFormalPar().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFormalValuePar()
  {
    if (formalValueParEClass == null)
    {
      formalValueParEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(370);
    }
    return formalValueParEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFormalValuePar_InOut()
  {
        return (EAttribute)getFormalValuePar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFormalValuePar_Mod()
  {
        return (EAttribute)getFormalValuePar().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFormalValuePar_Type()
  {
        return (EReference)getFormalValuePar().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFormalValuePar_Expression()
  {
        return (EReference)getFormalValuePar().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFormalTimerPar()
  {
    if (formalTimerParEClass == null)
    {
      formalTimerParEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(371);
    }
    return formalTimerParEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFormalPortPar()
  {
    if (formalPortParEClass == null)
    {
      formalPortParEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(372);
    }
    return formalPortParEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFormalPortPar_Port()
  {
        return (EReference)getFormalPortPar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFormalTemplatePar()
  {
    if (formalTemplateParEClass == null)
    {
      formalTemplateParEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(373);
    }
    return formalTemplateParEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFormalTemplatePar_InOut()
  {
        return (EAttribute)getFormalTemplatePar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFormalTemplatePar_Restriction()
  {
        return (EReference)getFormalTemplatePar().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFormalTemplatePar_Mod()
  {
        return (EAttribute)getFormalTemplatePar().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFormalTemplatePar_Type()
  {
        return (EReference)getFormalTemplatePar().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFormalTemplatePar_Templ()
  {
        return (EReference)getFormalTemplatePar().getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRunsOnSpec()
  {
    if (runsOnSpecEClass == null)
    {
      runsOnSpecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(374);
    }
    return runsOnSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRunsOnSpec_Component()
  {
        return (EReference)getRunsOnSpec().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReturnType()
  {
    if (returnTypeEClass == null)
    {
      returnTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(375);
    }
    return returnTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReturnType_Type()
  {
        return (EReference)getReturnType().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAssignment()
  {
    if (assignmentEClass == null)
    {
      assignmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(376);
    }
    return assignmentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignment_Ref()
  {
        return (EReference)getAssignment().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignment_Expression()
  {
        return (EReference)getAssignment().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignment_Body()
  {
        return (EReference)getAssignment().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAssignment_Extra()
  {
        return (EReference)getAssignment().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariableRef()
  {
    if (variableRefEClass == null)
    {
      variableRefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(377);
    }
    return variableRefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariableRef_Ref()
  {
        return (EReference)getVariableRef().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPreDefFunction()
  {
    if (preDefFunctionEClass == null)
    {
      preDefFunctionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(380);
    }
    return preDefFunctionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFint2char()
  {
    if (fint2charEClass == null)
    {
      fint2charEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(381);
    }
    return fint2charEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2char_E1()
  {
        return (EReference)getFint2char().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFint2unichar()
  {
    if (fint2unicharEClass == null)
    {
      fint2unicharEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(382);
    }
    return fint2unicharEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2unichar_E1()
  {
        return (EReference)getFint2unichar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFint2bit()
  {
    if (fint2bitEClass == null)
    {
      fint2bitEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(383);
    }
    return fint2bitEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2bit_E1()
  {
        return (EReference)getFint2bit().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2bit_E2()
  {
        return (EReference)getFint2bit().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFint2enum()
  {
    if (fint2enumEClass == null)
    {
      fint2enumEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(384);
    }
    return fint2enumEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2enum_E1()
  {
        return (EReference)getFint2enum().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2enum_E2()
  {
        return (EReference)getFint2enum().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFint2hex()
  {
    if (fint2hexEClass == null)
    {
      fint2hexEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(385);
    }
    return fint2hexEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2hex_E1()
  {
        return (EReference)getFint2hex().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2hex_E2()
  {
        return (EReference)getFint2hex().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFint2oct()
  {
    if (fint2octEClass == null)
    {
      fint2octEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(386);
    }
    return fint2octEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2oct_E1()
  {
        return (EReference)getFint2oct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2oct_E2()
  {
        return (EReference)getFint2oct().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFint2str()
  {
    if (fint2strEClass == null)
    {
      fint2strEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(387);
    }
    return fint2strEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2str_E1()
  {
        return (EReference)getFint2str().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFint2float()
  {
    if (fint2floatEClass == null)
    {
      fint2floatEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(388);
    }
    return fint2floatEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFint2float_E1()
  {
        return (EReference)getFint2float().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFfloat2int()
  {
    if (ffloat2intEClass == null)
    {
      ffloat2intEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(389);
    }
    return ffloat2intEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFfloat2int_E1()
  {
        return (EReference)getFfloat2int().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFchar2int()
  {
    if (fchar2intEClass == null)
    {
      fchar2intEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(390);
    }
    return fchar2intEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFchar2int_E1()
  {
        return (EReference)getFchar2int().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFchar2oct()
  {
    if (fchar2octEClass == null)
    {
      fchar2octEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(391);
    }
    return fchar2octEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFchar2oct_E1()
  {
        return (EReference)getFchar2oct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunichar2int()
  {
    if (funichar2intEClass == null)
    {
      funichar2intEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(392);
    }
    return funichar2intEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunichar2int_E1()
  {
        return (EReference)getFunichar2int().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFunichar2oct()
  {
    if (funichar2octEClass == null)
    {
      funichar2octEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(393);
    }
    return funichar2octEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFunichar2oct_Expr()
  {
        return (EReference)getFunichar2oct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFbit2int()
  {
    if (fbit2intEClass == null)
    {
      fbit2intEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(394);
    }
    return fbit2intEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFbit2int_E1()
  {
        return (EReference)getFbit2int().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFbit2hex()
  {
    if (fbit2hexEClass == null)
    {
      fbit2hexEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(395);
    }
    return fbit2hexEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFbit2hex_E1()
  {
        return (EReference)getFbit2hex().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFbit2oct()
  {
    if (fbit2octEClass == null)
    {
      fbit2octEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(396);
    }
    return fbit2octEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFbit2oct_E1()
  {
        return (EReference)getFbit2oct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFbit2str()
  {
    if (fbit2strEClass == null)
    {
      fbit2strEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(397);
    }
    return fbit2strEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFbit2str_E1()
  {
        return (EReference)getFbit2str().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFhex2int()
  {
    if (fhex2intEClass == null)
    {
      fhex2intEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(398);
    }
    return fhex2intEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFhex2int_E1()
  {
        return (EReference)getFhex2int().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFhex2bit()
  {
    if (fhex2bitEClass == null)
    {
      fhex2bitEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(399);
    }
    return fhex2bitEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFhex2bit_E1()
  {
        return (EReference)getFhex2bit().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFhex2oct()
  {
    if (fhex2octEClass == null)
    {
      fhex2octEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(400);
    }
    return fhex2octEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFhex2oct_E1()
  {
        return (EReference)getFhex2oct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFhex2str()
  {
    if (fhex2strEClass == null)
    {
      fhex2strEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(401);
    }
    return fhex2strEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFhex2str_E1()
  {
        return (EReference)getFhex2str().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFoct2int()
  {
    if (foct2intEClass == null)
    {
      foct2intEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(402);
    }
    return foct2intEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFoct2int_E1()
  {
        return (EReference)getFoct2int().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFoct2bit()
  {
    if (foct2bitEClass == null)
    {
      foct2bitEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(403);
    }
    return foct2bitEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFoct2bit_E1()
  {
        return (EReference)getFoct2bit().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFoct2hex()
  {
    if (foct2hexEClass == null)
    {
      foct2hexEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(404);
    }
    return foct2hexEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFoct2hex_E1()
  {
        return (EReference)getFoct2hex().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFoct2str()
  {
    if (foct2strEClass == null)
    {
      foct2strEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(405);
    }
    return foct2strEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFoct2str_E1()
  {
        return (EReference)getFoct2str().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFoct2char()
  {
    if (foct2charEClass == null)
    {
      foct2charEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(406);
    }
    return foct2charEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFoct2char_E1()
  {
        return (EReference)getFoct2char().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFoct2unichar()
  {
    if (foct2unicharEClass == null)
    {
      foct2unicharEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(407);
    }
    return foct2unicharEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFoct2unichar_Expr()
  {
        return (EReference)getFoct2unichar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFstr2int()
  {
    if (fstr2intEClass == null)
    {
      fstr2intEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(408);
    }
    return fstr2intEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFstr2int_E1()
  {
        return (EReference)getFstr2int().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFstr2hex()
  {
    if (fstr2hexEClass == null)
    {
      fstr2hexEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(409);
    }
    return fstr2hexEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFstr2hex_E1()
  {
        return (EReference)getFstr2hex().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFstr2oct()
  {
    if (fstr2octEClass == null)
    {
      fstr2octEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(410);
    }
    return fstr2octEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFstr2oct_E1()
  {
        return (EReference)getFstr2oct().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFstr2float()
  {
    if (fstr2floatEClass == null)
    {
      fstr2floatEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(411);
    }
    return fstr2floatEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFstr2float_E1()
  {
        return (EReference)getFstr2float().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFenum2int()
  {
    if (fenum2intEClass == null)
    {
      fenum2intEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(412);
    }
    return fenum2intEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFenum2int_E1()
  {
        return (EReference)getFenum2int().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFlengthof()
  {
    if (flengthofEClass == null)
    {
      flengthofEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(413);
    }
    return flengthofEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFlengthof_T1()
  {
        return (EReference)getFlengthof().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFsizeof()
  {
    if (fsizeofEClass == null)
    {
      fsizeofEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(414);
    }
    return fsizeofEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFsizeof_T1()
  {
        return (EReference)getFsizeof().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFispresent()
  {
    if (fispresentEClass == null)
    {
      fispresentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(415);
    }
    return fispresentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFispresent_T1()
  {
        return (EReference)getFispresent().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFischosen()
  {
    if (fischosenEClass == null)
    {
      fischosenEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(416);
    }
    return fischosenEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFischosen_T1()
  {
        return (EReference)getFischosen().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFisvalue()
  {
    if (fisvalueEClass == null)
    {
      fisvalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(417);
    }
    return fisvalueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFisvalue_T1()
  {
        return (EReference)getFisvalue().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFisbound()
  {
    if (fisboundEClass == null)
    {
      fisboundEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(418);
    }
    return fisboundEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFisbound_T1()
  {
        return (EReference)getFisbound().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFregexp()
  {
    if (fregexpEClass == null)
    {
      fregexpEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(419);
    }
    return fregexpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFregexp_T1()
  {
        return (EReference)getFregexp().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFregexp_E1()
  {
        return (EReference)getFregexp().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFregexp_E2()
  {
        return (EReference)getFregexp().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFsubstr()
  {
    if (fsubstrEClass == null)
    {
      fsubstrEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(420);
    }
    return fsubstrEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFsubstr_T1()
  {
        return (EReference)getFsubstr().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFsubstr_E1()
  {
        return (EReference)getFsubstr().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFsubstr_E2()
  {
        return (EReference)getFsubstr().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFreplace()
  {
    if (freplaceEClass == null)
    {
      freplaceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(421);
    }
    return freplaceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFreplace_E1()
  {
        return (EReference)getFreplace().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFreplace_E2()
  {
        return (EReference)getFreplace().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFreplace_E3()
  {
        return (EReference)getFreplace().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFreplace_E4()
  {
        return (EReference)getFreplace().getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFencvalue()
  {
    if (fencvalueEClass == null)
    {
      fencvalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(422);
    }
    return fencvalueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFencvalue_T1()
  {
        return (EReference)getFencvalue().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFdecvalue()
  {
    if (fdecvalueEClass == null)
    {
      fdecvalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(423);
    }
    return fdecvalueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFdecvalue_E1()
  {
        return (EReference)getFdecvalue().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFdecvalue_E2()
  {
        return (EReference)getFdecvalue().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFencvalueUnichar()
  {
    if (fencvalueUnicharEClass == null)
    {
      fencvalueUnicharEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(424);
    }
    return fencvalueUnicharEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFencvalueUnichar_T1()
  {
        return (EReference)getFencvalueUnichar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFencvalueUnichar_E1()
  {
        return (EReference)getFencvalueUnichar().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFdecvalueUnichar()
  {
    if (fdecvalueUnicharEClass == null)
    {
      fdecvalueUnicharEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(425);
    }
    return fdecvalueUnicharEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFdecvalueUnichar_E1()
  {
        return (EReference)getFdecvalueUnichar().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFdecvalueUnichar_E2()
  {
        return (EReference)getFdecvalueUnichar().getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFdecvalueUnichar_E3()
  {
        return (EReference)getFdecvalueUnichar().getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFrnd()
  {
    if (frndEClass == null)
    {
      frndEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(426);
    }
    return frndEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFrnd_E1()
  {
        return (EReference)getFrnd().getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFtestcasename()
  {
    if (ftestcasenameEClass == null)
    {
      ftestcasenameEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(427);
    }
    return ftestcasenameEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getXorExpression()
  {
    if (xorExpressionEClass == null)
    {
      xorExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(428);
    }
    return xorExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAndExpression()
  {
    if (andExpressionEClass == null)
    {
      andExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(429);
    }
    return andExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEqualExpression()
  {
    if (equalExpressionEClass == null)
    {
      equalExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(430);
    }
    return equalExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRelExpression()
  {
    if (relExpressionEClass == null)
    {
      relExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(431);
    }
    return relExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getShiftExpression()
  {
    if (shiftExpressionEClass == null)
    {
      shiftExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(432);
    }
    return shiftExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBitOrExpression()
  {
    if (bitOrExpressionEClass == null)
    {
      bitOrExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(433);
    }
    return bitOrExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBitXorExpression()
  {
    if (bitXorExpressionEClass == null)
    {
      bitXorExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(434);
    }
    return bitXorExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBitAndExpression()
  {
    if (bitAndExpressionEClass == null)
    {
      bitAndExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(435);
    }
    return bitAndExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAddExpression()
  {
    if (addExpressionEClass == null)
    {
      addExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(436);
    }
    return addExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMulExpression()
  {
    if (mulExpressionEClass == null)
    {
      mulExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(437);
    }
    return mulExpressionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getVisibility()
  {
    if (visibilityEEnum == null)
    {
      visibilityEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(378);
    }
    return visibilityEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getVerdictTypeValue()
  {
    if (verdictTypeValueEEnum == null)
    {
      verdictTypeValueEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(TTCN3Package.eNS_URI).getEClassifiers().get(379);
    }
    return verdictTypeValueEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3Factory getTTCN3Factory()
  {
    return (TTCN3Factory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isLoaded = false;

  /**
   * Laods the package and any sub-packages from their serialized form.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void loadPackage()
  {
    if (isLoaded) return;
    isLoaded = true;

    URL url = getClass().getResource(packageFilename);
    if (url == null)
    {
      throw new RuntimeException("Missing serialized package: " + packageFilename);
    }
    URI uri = URI.createURI(url.toString());
    Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
    try
    {
      resource.load(null);
    }
    catch (IOException exception)
    {
      throw new WrappedException(exception);
    }
    initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
    createResource(eNS_URI);
  }


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isFixed = false;

  /**
   * Fixes up the loaded package, to make it appear as if it had been programmatically built.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void fixPackageContents()
  {
    if (isFixed) return;
    isFixed = true;
    fixEClassifiers();
  }

  /**
   * Sets the instance class on the given classifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void fixInstanceClass(EClassifier eClassifier)
  {
    if (eClassifier.getInstanceClassName() == null)
    {
      eClassifier.setInstanceClassName("de.ugoe.cs.swe.tTCN3." + eClassifier.getName());
      setGeneratedClassName(eClassifier);
    }
  }

} //TTCN3PackageImpl
