/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.PortOrAny;
import de.ugoe.cs.swe.tTCN3.PortReceiveOp;
import de.ugoe.cs.swe.tTCN3.ReceiveStatement;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Receive Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ReceiveStatementImpl#getAny <em>Any</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ReceiveStatementImpl#getReceive <em>Receive</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReceiveStatementImpl extends MinimalEObjectImpl.Container implements ReceiveStatement
{
  /**
   * The cached value of the '{@link #getAny() <em>Any</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAny()
   * @generated
   * @ordered
   */
  protected PortOrAny any;

  /**
   * The cached value of the '{@link #getReceive() <em>Receive</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReceive()
   * @generated
   * @ordered
   */
  protected PortReceiveOp receive;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ReceiveStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getReceiveStatement();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortOrAny getAny()
  {
    return any;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAny(PortOrAny newAny, NotificationChain msgs)
  {
    PortOrAny oldAny = any;
    any = newAny;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.RECEIVE_STATEMENT__ANY, oldAny, newAny);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAny(PortOrAny newAny)
  {
    if (newAny != any)
    {
      NotificationChain msgs = null;
      if (any != null)
        msgs = ((InternalEObject)any).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RECEIVE_STATEMENT__ANY, null, msgs);
      if (newAny != null)
        msgs = ((InternalEObject)newAny).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RECEIVE_STATEMENT__ANY, null, msgs);
      msgs = basicSetAny(newAny, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.RECEIVE_STATEMENT__ANY, newAny, newAny));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortReceiveOp getReceive()
  {
    return receive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReceive(PortReceiveOp newReceive, NotificationChain msgs)
  {
    PortReceiveOp oldReceive = receive;
    receive = newReceive;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.RECEIVE_STATEMENT__RECEIVE, oldReceive, newReceive);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReceive(PortReceiveOp newReceive)
  {
    if (newReceive != receive)
    {
      NotificationChain msgs = null;
      if (receive != null)
        msgs = ((InternalEObject)receive).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RECEIVE_STATEMENT__RECEIVE, null, msgs);
      if (newReceive != null)
        msgs = ((InternalEObject)newReceive).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RECEIVE_STATEMENT__RECEIVE, null, msgs);
      msgs = basicSetReceive(newReceive, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.RECEIVE_STATEMENT__RECEIVE, newReceive, newReceive));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.RECEIVE_STATEMENT__ANY:
        return basicSetAny(null, msgs);
      case TTCN3Package.RECEIVE_STATEMENT__RECEIVE:
        return basicSetReceive(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.RECEIVE_STATEMENT__ANY:
        return getAny();
      case TTCN3Package.RECEIVE_STATEMENT__RECEIVE:
        return getReceive();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.RECEIVE_STATEMENT__ANY:
        setAny((PortOrAny)newValue);
        return;
      case TTCN3Package.RECEIVE_STATEMENT__RECEIVE:
        setReceive((PortReceiveOp)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.RECEIVE_STATEMENT__ANY:
        setAny((PortOrAny)null);
        return;
      case TTCN3Package.RECEIVE_STATEMENT__RECEIVE:
        setReceive((PortReceiveOp)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.RECEIVE_STATEMENT__ANY:
        return any != null;
      case TTCN3Package.RECEIVE_STATEMENT__RECEIVE:
        return receive != null;
    }
    return super.eIsSet(featureID);
  }

} //ReceiveStatementImpl
