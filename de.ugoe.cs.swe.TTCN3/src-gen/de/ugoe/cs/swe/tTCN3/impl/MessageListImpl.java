/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AllOrTypeList;
import de.ugoe.cs.swe.tTCN3.MessageList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MessageListImpl#getAllOrTypeList <em>All Or Type List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MessageListImpl extends MinimalEObjectImpl.Container implements MessageList
{
  /**
   * The cached value of the '{@link #getAllOrTypeList() <em>All Or Type List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAllOrTypeList()
   * @generated
   * @ordered
   */
  protected AllOrTypeList allOrTypeList;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MessageListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getMessageList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllOrTypeList getAllOrTypeList()
  {
    return allOrTypeList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAllOrTypeList(AllOrTypeList newAllOrTypeList, NotificationChain msgs)
  {
    AllOrTypeList oldAllOrTypeList = allOrTypeList;
    allOrTypeList = newAllOrTypeList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MESSAGE_LIST__ALL_OR_TYPE_LIST, oldAllOrTypeList, newAllOrTypeList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAllOrTypeList(AllOrTypeList newAllOrTypeList)
  {
    if (newAllOrTypeList != allOrTypeList)
    {
      NotificationChain msgs = null;
      if (allOrTypeList != null)
        msgs = ((InternalEObject)allOrTypeList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MESSAGE_LIST__ALL_OR_TYPE_LIST, null, msgs);
      if (newAllOrTypeList != null)
        msgs = ((InternalEObject)newAllOrTypeList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MESSAGE_LIST__ALL_OR_TYPE_LIST, null, msgs);
      msgs = basicSetAllOrTypeList(newAllOrTypeList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MESSAGE_LIST__ALL_OR_TYPE_LIST, newAllOrTypeList, newAllOrTypeList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.MESSAGE_LIST__ALL_OR_TYPE_LIST:
        return basicSetAllOrTypeList(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.MESSAGE_LIST__ALL_OR_TYPE_LIST:
        return getAllOrTypeList();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.MESSAGE_LIST__ALL_OR_TYPE_LIST:
        setAllOrTypeList((AllOrTypeList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MESSAGE_LIST__ALL_OR_TYPE_LIST:
        setAllOrTypeList((AllOrTypeList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MESSAGE_LIST__ALL_OR_TYPE_LIST:
        return allOrTypeList != null;
    }
    return super.eIsSet(featureID);
  }

} //MessageListImpl
