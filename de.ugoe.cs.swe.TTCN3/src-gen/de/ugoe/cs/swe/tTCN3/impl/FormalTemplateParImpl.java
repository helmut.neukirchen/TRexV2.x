/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.FormalTemplatePar;
import de.ugoe.cs.swe.tTCN3.InLineTemplate;
import de.ugoe.cs.swe.tTCN3.RestrictedTemplate;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.Type;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formal Template Par</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FormalTemplateParImpl#getInOut <em>In Out</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FormalTemplateParImpl#getRestriction <em>Restriction</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FormalTemplateParImpl#getMod <em>Mod</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FormalTemplateParImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FormalTemplateParImpl#getTempl <em>Templ</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FormalTemplateParImpl extends RefValueImpl implements FormalTemplatePar
{
  /**
   * The default value of the '{@link #getInOut() <em>In Out</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInOut()
   * @generated
   * @ordered
   */
  protected static final String IN_OUT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getInOut() <em>In Out</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInOut()
   * @generated
   * @ordered
   */
  protected String inOut = IN_OUT_EDEFAULT;

  /**
   * The cached value of the '{@link #getRestriction() <em>Restriction</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRestriction()
   * @generated
   * @ordered
   */
  protected RestrictedTemplate restriction;

  /**
   * The default value of the '{@link #getMod() <em>Mod</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMod()
   * @generated
   * @ordered
   */
  protected static final String MOD_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMod() <em>Mod</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMod()
   * @generated
   * @ordered
   */
  protected String mod = MOD_EDEFAULT;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected Type type;

  /**
   * The cached value of the '{@link #getTempl() <em>Templ</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTempl()
   * @generated
   * @ordered
   */
  protected InLineTemplate templ;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FormalTemplateParImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFormalTemplatePar();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getInOut()
  {
    return inOut;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInOut(String newInOut)
  {
    String oldInOut = inOut;
    inOut = newInOut;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FORMAL_TEMPLATE_PAR__IN_OUT, oldInOut, inOut));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RestrictedTemplate getRestriction()
  {
    return restriction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRestriction(RestrictedTemplate newRestriction, NotificationChain msgs)
  {
    RestrictedTemplate oldRestriction = restriction;
    restriction = newRestriction;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FORMAL_TEMPLATE_PAR__RESTRICTION, oldRestriction, newRestriction);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRestriction(RestrictedTemplate newRestriction)
  {
    if (newRestriction != restriction)
    {
      NotificationChain msgs = null;
      if (restriction != null)
        msgs = ((InternalEObject)restriction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FORMAL_TEMPLATE_PAR__RESTRICTION, null, msgs);
      if (newRestriction != null)
        msgs = ((InternalEObject)newRestriction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FORMAL_TEMPLATE_PAR__RESTRICTION, null, msgs);
      msgs = basicSetRestriction(newRestriction, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FORMAL_TEMPLATE_PAR__RESTRICTION, newRestriction, newRestriction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMod()
  {
    return mod;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMod(String newMod)
  {
    String oldMod = mod;
    mod = newMod;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FORMAL_TEMPLATE_PAR__MOD, oldMod, mod));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(Type newType, NotificationChain msgs)
  {
    Type oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FORMAL_TEMPLATE_PAR__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(Type newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FORMAL_TEMPLATE_PAR__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FORMAL_TEMPLATE_PAR__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FORMAL_TEMPLATE_PAR__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InLineTemplate getTempl()
  {
    return templ;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTempl(InLineTemplate newTempl, NotificationChain msgs)
  {
    InLineTemplate oldTempl = templ;
    templ = newTempl;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FORMAL_TEMPLATE_PAR__TEMPL, oldTempl, newTempl);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTempl(InLineTemplate newTempl)
  {
    if (newTempl != templ)
    {
      NotificationChain msgs = null;
      if (templ != null)
        msgs = ((InternalEObject)templ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FORMAL_TEMPLATE_PAR__TEMPL, null, msgs);
      if (newTempl != null)
        msgs = ((InternalEObject)newTempl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FORMAL_TEMPLATE_PAR__TEMPL, null, msgs);
      msgs = basicSetTempl(newTempl, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FORMAL_TEMPLATE_PAR__TEMPL, newTempl, newTempl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FORMAL_TEMPLATE_PAR__RESTRICTION:
        return basicSetRestriction(null, msgs);
      case TTCN3Package.FORMAL_TEMPLATE_PAR__TYPE:
        return basicSetType(null, msgs);
      case TTCN3Package.FORMAL_TEMPLATE_PAR__TEMPL:
        return basicSetTempl(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FORMAL_TEMPLATE_PAR__IN_OUT:
        return getInOut();
      case TTCN3Package.FORMAL_TEMPLATE_PAR__RESTRICTION:
        return getRestriction();
      case TTCN3Package.FORMAL_TEMPLATE_PAR__MOD:
        return getMod();
      case TTCN3Package.FORMAL_TEMPLATE_PAR__TYPE:
        return getType();
      case TTCN3Package.FORMAL_TEMPLATE_PAR__TEMPL:
        return getTempl();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FORMAL_TEMPLATE_PAR__IN_OUT:
        setInOut((String)newValue);
        return;
      case TTCN3Package.FORMAL_TEMPLATE_PAR__RESTRICTION:
        setRestriction((RestrictedTemplate)newValue);
        return;
      case TTCN3Package.FORMAL_TEMPLATE_PAR__MOD:
        setMod((String)newValue);
        return;
      case TTCN3Package.FORMAL_TEMPLATE_PAR__TYPE:
        setType((Type)newValue);
        return;
      case TTCN3Package.FORMAL_TEMPLATE_PAR__TEMPL:
        setTempl((InLineTemplate)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FORMAL_TEMPLATE_PAR__IN_OUT:
        setInOut(IN_OUT_EDEFAULT);
        return;
      case TTCN3Package.FORMAL_TEMPLATE_PAR__RESTRICTION:
        setRestriction((RestrictedTemplate)null);
        return;
      case TTCN3Package.FORMAL_TEMPLATE_PAR__MOD:
        setMod(MOD_EDEFAULT);
        return;
      case TTCN3Package.FORMAL_TEMPLATE_PAR__TYPE:
        setType((Type)null);
        return;
      case TTCN3Package.FORMAL_TEMPLATE_PAR__TEMPL:
        setTempl((InLineTemplate)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FORMAL_TEMPLATE_PAR__IN_OUT:
        return IN_OUT_EDEFAULT == null ? inOut != null : !IN_OUT_EDEFAULT.equals(inOut);
      case TTCN3Package.FORMAL_TEMPLATE_PAR__RESTRICTION:
        return restriction != null;
      case TTCN3Package.FORMAL_TEMPLATE_PAR__MOD:
        return MOD_EDEFAULT == null ? mod != null : !MOD_EDEFAULT.equals(mod);
      case TTCN3Package.FORMAL_TEMPLATE_PAR__TYPE:
        return type != null;
      case TTCN3Package.FORMAL_TEMPLATE_PAR__TEMPL:
        return templ != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (inOut: ");
    result.append(inOut);
    result.append(", mod: ");
    result.append(mod);
    result.append(')');
    return result.toString();
  }

} //FormalTemplateParImpl
