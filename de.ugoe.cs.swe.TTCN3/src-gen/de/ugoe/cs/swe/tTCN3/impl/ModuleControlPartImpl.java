/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ModuleControlBody;
import de.ugoe.cs.swe.tTCN3.ModuleControlPart;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.WithStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Module Control Part</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ModuleControlPartImpl#getBody <em>Body</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ModuleControlPartImpl#getWs <em>Ws</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ModuleControlPartImpl#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModuleControlPartImpl extends MinimalEObjectImpl.Container implements ModuleControlPart
{
  /**
   * The cached value of the '{@link #getBody() <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBody()
   * @generated
   * @ordered
   */
  protected ModuleControlBody body;

  /**
   * The cached value of the '{@link #getWs() <em>Ws</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWs()
   * @generated
   * @ordered
   */
  protected WithStatement ws;

  /**
   * The default value of the '{@link #getSc() <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected static final String SC_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSc() <em>Sc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected String sc = SC_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModuleControlPartImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getModuleControlPart();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleControlBody getBody()
  {
    return body;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBody(ModuleControlBody newBody, NotificationChain msgs)
  {
    ModuleControlBody oldBody = body;
    body = newBody;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_CONTROL_PART__BODY, oldBody, newBody);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBody(ModuleControlBody newBody)
  {
    if (newBody != body)
    {
      NotificationChain msgs = null;
      if (body != null)
        msgs = ((InternalEObject)body).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_CONTROL_PART__BODY, null, msgs);
      if (newBody != null)
        msgs = ((InternalEObject)newBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_CONTROL_PART__BODY, null, msgs);
      msgs = basicSetBody(newBody, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_CONTROL_PART__BODY, newBody, newBody));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WithStatement getWs()
  {
    return ws;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetWs(WithStatement newWs, NotificationChain msgs)
  {
    WithStatement oldWs = ws;
    ws = newWs;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_CONTROL_PART__WS, oldWs, newWs);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWs(WithStatement newWs)
  {
    if (newWs != ws)
    {
      NotificationChain msgs = null;
      if (ws != null)
        msgs = ((InternalEObject)ws).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_CONTROL_PART__WS, null, msgs);
      if (newWs != null)
        msgs = ((InternalEObject)newWs).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.MODULE_CONTROL_PART__WS, null, msgs);
      msgs = basicSetWs(newWs, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_CONTROL_PART__WS, newWs, newWs));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSc()
  {
    return sc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSc(String newSc)
  {
    String oldSc = sc;
    sc = newSc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.MODULE_CONTROL_PART__SC, oldSc, sc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_CONTROL_PART__BODY:
        return basicSetBody(null, msgs);
      case TTCN3Package.MODULE_CONTROL_PART__WS:
        return basicSetWs(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_CONTROL_PART__BODY:
        return getBody();
      case TTCN3Package.MODULE_CONTROL_PART__WS:
        return getWs();
      case TTCN3Package.MODULE_CONTROL_PART__SC:
        return getSc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_CONTROL_PART__BODY:
        setBody((ModuleControlBody)newValue);
        return;
      case TTCN3Package.MODULE_CONTROL_PART__WS:
        setWs((WithStatement)newValue);
        return;
      case TTCN3Package.MODULE_CONTROL_PART__SC:
        setSc((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_CONTROL_PART__BODY:
        setBody((ModuleControlBody)null);
        return;
      case TTCN3Package.MODULE_CONTROL_PART__WS:
        setWs((WithStatement)null);
        return;
      case TTCN3Package.MODULE_CONTROL_PART__SC:
        setSc(SC_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MODULE_CONTROL_PART__BODY:
        return body != null;
      case TTCN3Package.MODULE_CONTROL_PART__WS:
        return ws != null;
      case TTCN3Package.MODULE_CONTROL_PART__SC:
        return SC_EDEFAULT == null ? sc != null : !SC_EDEFAULT.equals(sc);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (sc: ");
    result.append(sc);
    result.append(')');
    return result.toString();
  }

} //ModuleControlPartImpl
