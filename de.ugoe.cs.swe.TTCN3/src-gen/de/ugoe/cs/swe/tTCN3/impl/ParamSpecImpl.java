/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ParamSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Param Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ParamSpecImpl extends MinimalEObjectImpl.Container implements ParamSpec
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ParamSpecImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getParamSpec();
  }

} //ParamSpecImpl
