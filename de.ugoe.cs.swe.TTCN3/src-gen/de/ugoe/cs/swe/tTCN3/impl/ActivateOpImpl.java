/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ActivateOp;
import de.ugoe.cs.swe.tTCN3.AltstepInstance;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activate Op</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ActivateOpImpl#getAltstep <em>Altstep</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActivateOpImpl extends MinimalEObjectImpl.Container implements ActivateOp
{
  /**
   * The cached value of the '{@link #getAltstep() <em>Altstep</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAltstep()
   * @generated
   * @ordered
   */
  protected AltstepInstance altstep;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ActivateOpImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getActivateOp();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltstepInstance getAltstep()
  {
    return altstep;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAltstep(AltstepInstance newAltstep, NotificationChain msgs)
  {
    AltstepInstance oldAltstep = altstep;
    altstep = newAltstep;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ACTIVATE_OP__ALTSTEP, oldAltstep, newAltstep);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAltstep(AltstepInstance newAltstep)
  {
    if (newAltstep != altstep)
    {
      NotificationChain msgs = null;
      if (altstep != null)
        msgs = ((InternalEObject)altstep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ACTIVATE_OP__ALTSTEP, null, msgs);
      if (newAltstep != null)
        msgs = ((InternalEObject)newAltstep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ACTIVATE_OP__ALTSTEP, null, msgs);
      msgs = basicSetAltstep(newAltstep, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ACTIVATE_OP__ALTSTEP, newAltstep, newAltstep));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.ACTIVATE_OP__ALTSTEP:
        return basicSetAltstep(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.ACTIVATE_OP__ALTSTEP:
        return getAltstep();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.ACTIVATE_OP__ALTSTEP:
        setAltstep((AltstepInstance)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ACTIVATE_OP__ALTSTEP:
        setAltstep((AltstepInstance)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ACTIVATE_OP__ALTSTEP:
        return altstep != null;
    }
    return super.eIsSet(featureID);
  }

} //ActivateOpImpl
