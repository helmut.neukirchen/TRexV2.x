/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ActivateOp;
import de.ugoe.cs.swe.tTCN3.ConfigurationOps;
import de.ugoe.cs.swe.tTCN3.ExtendedFieldReference;
import de.ugoe.cs.swe.tTCN3.FunctionInstance;
import de.ugoe.cs.swe.tTCN3.OpCall;
import de.ugoe.cs.swe.tTCN3.PreDefFunction;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateOps;
import de.ugoe.cs.swe.tTCN3.TestcaseInstance;
import de.ugoe.cs.swe.tTCN3.TimerOps;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Op Call</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getVerdict <em>Verdict</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getFunction <em>Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getExtendedFunction <em>Extended Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getPreFunction <em>Pre Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getTestcase <em>Testcase</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getActivate <em>Activate</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getTemplateOps <em>Template Ops</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getExtendedTemplate <em>Extended Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.OpCallImpl#getField <em>Field</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OpCallImpl extends SingleExpressionImpl implements OpCall
{
  /**
   * The cached value of the '{@link #getConfiguration() <em>Configuration</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfiguration()
   * @generated
   * @ordered
   */
  protected ConfigurationOps configuration;

  /**
   * The default value of the '{@link #getVerdict() <em>Verdict</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVerdict()
   * @generated
   * @ordered
   */
  protected static final String VERDICT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVerdict() <em>Verdict</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVerdict()
   * @generated
   * @ordered
   */
  protected String verdict = VERDICT_EDEFAULT;

  /**
   * The cached value of the '{@link #getTimer() <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimer()
   * @generated
   * @ordered
   */
  protected TimerOps timer;

  /**
   * The cached value of the '{@link #getFunction() <em>Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunction()
   * @generated
   * @ordered
   */
  protected FunctionInstance function;

  /**
   * The cached value of the '{@link #getExtendedFunction() <em>Extended Function</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtendedFunction()
   * @generated
   * @ordered
   */
  protected EList<ExtendedFieldReference> extendedFunction;

  /**
   * The cached value of the '{@link #getPreFunction() <em>Pre Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPreFunction()
   * @generated
   * @ordered
   */
  protected PreDefFunction preFunction;

  /**
   * The cached value of the '{@link #getTestcase() <em>Testcase</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTestcase()
   * @generated
   * @ordered
   */
  protected TestcaseInstance testcase;

  /**
   * The cached value of the '{@link #getActivate() <em>Activate</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getActivate()
   * @generated
   * @ordered
   */
  protected ActivateOp activate;

  /**
   * The cached value of the '{@link #getTemplateOps() <em>Template Ops</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplateOps()
   * @generated
   * @ordered
   */
  protected TemplateOps templateOps;

  /**
   * The cached value of the '{@link #getExtendedTemplate() <em>Extended Template</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtendedTemplate()
   * @generated
   * @ordered
   */
  protected EList<ExtendedFieldReference> extendedTemplate;

  /**
   * The cached value of the '{@link #getField() <em>Field</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getField()
   * @generated
   * @ordered
   */
  protected EList<ExtendedFieldReference> field;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OpCallImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getOpCall();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConfigurationOps getConfiguration()
  {
    return configuration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConfiguration(ConfigurationOps newConfiguration, NotificationChain msgs)
  {
    ConfigurationOps oldConfiguration = configuration;
    configuration = newConfiguration;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__CONFIGURATION, oldConfiguration, newConfiguration);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConfiguration(ConfigurationOps newConfiguration)
  {
    if (newConfiguration != configuration)
    {
      NotificationChain msgs = null;
      if (configuration != null)
        msgs = ((InternalEObject)configuration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__CONFIGURATION, null, msgs);
      if (newConfiguration != null)
        msgs = ((InternalEObject)newConfiguration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__CONFIGURATION, null, msgs);
      msgs = basicSetConfiguration(newConfiguration, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__CONFIGURATION, newConfiguration, newConfiguration));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVerdict()
  {
    return verdict;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVerdict(String newVerdict)
  {
    String oldVerdict = verdict;
    verdict = newVerdict;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__VERDICT, oldVerdict, verdict));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerOps getTimer()
  {
    return timer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTimer(TimerOps newTimer, NotificationChain msgs)
  {
    TimerOps oldTimer = timer;
    timer = newTimer;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__TIMER, oldTimer, newTimer);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimer(TimerOps newTimer)
  {
    if (newTimer != timer)
    {
      NotificationChain msgs = null;
      if (timer != null)
        msgs = ((InternalEObject)timer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__TIMER, null, msgs);
      if (newTimer != null)
        msgs = ((InternalEObject)newTimer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__TIMER, null, msgs);
      msgs = basicSetTimer(newTimer, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__TIMER, newTimer, newTimer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionInstance getFunction()
  {
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFunction(FunctionInstance newFunction, NotificationChain msgs)
  {
    FunctionInstance oldFunction = function;
    function = newFunction;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__FUNCTION, oldFunction, newFunction);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFunction(FunctionInstance newFunction)
  {
    if (newFunction != function)
    {
      NotificationChain msgs = null;
      if (function != null)
        msgs = ((InternalEObject)function).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__FUNCTION, null, msgs);
      if (newFunction != null)
        msgs = ((InternalEObject)newFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__FUNCTION, null, msgs);
      msgs = basicSetFunction(newFunction, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__FUNCTION, newFunction, newFunction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ExtendedFieldReference> getExtendedFunction()
  {
    if (extendedFunction == null)
    {
      extendedFunction = new EObjectContainmentEList<ExtendedFieldReference>(ExtendedFieldReference.class, this, TTCN3Package.OP_CALL__EXTENDED_FUNCTION);
    }
    return extendedFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PreDefFunction getPreFunction()
  {
    return preFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPreFunction(PreDefFunction newPreFunction, NotificationChain msgs)
  {
    PreDefFunction oldPreFunction = preFunction;
    preFunction = newPreFunction;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__PRE_FUNCTION, oldPreFunction, newPreFunction);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPreFunction(PreDefFunction newPreFunction)
  {
    if (newPreFunction != preFunction)
    {
      NotificationChain msgs = null;
      if (preFunction != null)
        msgs = ((InternalEObject)preFunction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__PRE_FUNCTION, null, msgs);
      if (newPreFunction != null)
        msgs = ((InternalEObject)newPreFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__PRE_FUNCTION, null, msgs);
      msgs = basicSetPreFunction(newPreFunction, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__PRE_FUNCTION, newPreFunction, newPreFunction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TestcaseInstance getTestcase()
  {
    return testcase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTestcase(TestcaseInstance newTestcase, NotificationChain msgs)
  {
    TestcaseInstance oldTestcase = testcase;
    testcase = newTestcase;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__TESTCASE, oldTestcase, newTestcase);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTestcase(TestcaseInstance newTestcase)
  {
    if (newTestcase != testcase)
    {
      NotificationChain msgs = null;
      if (testcase != null)
        msgs = ((InternalEObject)testcase).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__TESTCASE, null, msgs);
      if (newTestcase != null)
        msgs = ((InternalEObject)newTestcase).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__TESTCASE, null, msgs);
      msgs = basicSetTestcase(newTestcase, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__TESTCASE, newTestcase, newTestcase));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActivateOp getActivate()
  {
    return activate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetActivate(ActivateOp newActivate, NotificationChain msgs)
  {
    ActivateOp oldActivate = activate;
    activate = newActivate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__ACTIVATE, oldActivate, newActivate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setActivate(ActivateOp newActivate)
  {
    if (newActivate != activate)
    {
      NotificationChain msgs = null;
      if (activate != null)
        msgs = ((InternalEObject)activate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__ACTIVATE, null, msgs);
      if (newActivate != null)
        msgs = ((InternalEObject)newActivate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__ACTIVATE, null, msgs);
      msgs = basicSetActivate(newActivate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__ACTIVATE, newActivate, newActivate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateOps getTemplateOps()
  {
    return templateOps;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTemplateOps(TemplateOps newTemplateOps, NotificationChain msgs)
  {
    TemplateOps oldTemplateOps = templateOps;
    templateOps = newTemplateOps;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__TEMPLATE_OPS, oldTemplateOps, newTemplateOps);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTemplateOps(TemplateOps newTemplateOps)
  {
    if (newTemplateOps != templateOps)
    {
      NotificationChain msgs = null;
      if (templateOps != null)
        msgs = ((InternalEObject)templateOps).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__TEMPLATE_OPS, null, msgs);
      if (newTemplateOps != null)
        msgs = ((InternalEObject)newTemplateOps).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.OP_CALL__TEMPLATE_OPS, null, msgs);
      msgs = basicSetTemplateOps(newTemplateOps, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.OP_CALL__TEMPLATE_OPS, newTemplateOps, newTemplateOps));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ExtendedFieldReference> getExtendedTemplate()
  {
    if (extendedTemplate == null)
    {
      extendedTemplate = new EObjectContainmentEList<ExtendedFieldReference>(ExtendedFieldReference.class, this, TTCN3Package.OP_CALL__EXTENDED_TEMPLATE);
    }
    return extendedTemplate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ExtendedFieldReference> getField()
  {
    if (field == null)
    {
      field = new EObjectContainmentEList<ExtendedFieldReference>(ExtendedFieldReference.class, this, TTCN3Package.OP_CALL__FIELD);
    }
    return field;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.OP_CALL__CONFIGURATION:
        return basicSetConfiguration(null, msgs);
      case TTCN3Package.OP_CALL__TIMER:
        return basicSetTimer(null, msgs);
      case TTCN3Package.OP_CALL__FUNCTION:
        return basicSetFunction(null, msgs);
      case TTCN3Package.OP_CALL__EXTENDED_FUNCTION:
        return ((InternalEList<?>)getExtendedFunction()).basicRemove(otherEnd, msgs);
      case TTCN3Package.OP_CALL__PRE_FUNCTION:
        return basicSetPreFunction(null, msgs);
      case TTCN3Package.OP_CALL__TESTCASE:
        return basicSetTestcase(null, msgs);
      case TTCN3Package.OP_CALL__ACTIVATE:
        return basicSetActivate(null, msgs);
      case TTCN3Package.OP_CALL__TEMPLATE_OPS:
        return basicSetTemplateOps(null, msgs);
      case TTCN3Package.OP_CALL__EXTENDED_TEMPLATE:
        return ((InternalEList<?>)getExtendedTemplate()).basicRemove(otherEnd, msgs);
      case TTCN3Package.OP_CALL__FIELD:
        return ((InternalEList<?>)getField()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.OP_CALL__CONFIGURATION:
        return getConfiguration();
      case TTCN3Package.OP_CALL__VERDICT:
        return getVerdict();
      case TTCN3Package.OP_CALL__TIMER:
        return getTimer();
      case TTCN3Package.OP_CALL__FUNCTION:
        return getFunction();
      case TTCN3Package.OP_CALL__EXTENDED_FUNCTION:
        return getExtendedFunction();
      case TTCN3Package.OP_CALL__PRE_FUNCTION:
        return getPreFunction();
      case TTCN3Package.OP_CALL__TESTCASE:
        return getTestcase();
      case TTCN3Package.OP_CALL__ACTIVATE:
        return getActivate();
      case TTCN3Package.OP_CALL__TEMPLATE_OPS:
        return getTemplateOps();
      case TTCN3Package.OP_CALL__EXTENDED_TEMPLATE:
        return getExtendedTemplate();
      case TTCN3Package.OP_CALL__FIELD:
        return getField();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.OP_CALL__CONFIGURATION:
        setConfiguration((ConfigurationOps)newValue);
        return;
      case TTCN3Package.OP_CALL__VERDICT:
        setVerdict((String)newValue);
        return;
      case TTCN3Package.OP_CALL__TIMER:
        setTimer((TimerOps)newValue);
        return;
      case TTCN3Package.OP_CALL__FUNCTION:
        setFunction((FunctionInstance)newValue);
        return;
      case TTCN3Package.OP_CALL__EXTENDED_FUNCTION:
        getExtendedFunction().clear();
        getExtendedFunction().addAll((Collection<? extends ExtendedFieldReference>)newValue);
        return;
      case TTCN3Package.OP_CALL__PRE_FUNCTION:
        setPreFunction((PreDefFunction)newValue);
        return;
      case TTCN3Package.OP_CALL__TESTCASE:
        setTestcase((TestcaseInstance)newValue);
        return;
      case TTCN3Package.OP_CALL__ACTIVATE:
        setActivate((ActivateOp)newValue);
        return;
      case TTCN3Package.OP_CALL__TEMPLATE_OPS:
        setTemplateOps((TemplateOps)newValue);
        return;
      case TTCN3Package.OP_CALL__EXTENDED_TEMPLATE:
        getExtendedTemplate().clear();
        getExtendedTemplate().addAll((Collection<? extends ExtendedFieldReference>)newValue);
        return;
      case TTCN3Package.OP_CALL__FIELD:
        getField().clear();
        getField().addAll((Collection<? extends ExtendedFieldReference>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.OP_CALL__CONFIGURATION:
        setConfiguration((ConfigurationOps)null);
        return;
      case TTCN3Package.OP_CALL__VERDICT:
        setVerdict(VERDICT_EDEFAULT);
        return;
      case TTCN3Package.OP_CALL__TIMER:
        setTimer((TimerOps)null);
        return;
      case TTCN3Package.OP_CALL__FUNCTION:
        setFunction((FunctionInstance)null);
        return;
      case TTCN3Package.OP_CALL__EXTENDED_FUNCTION:
        getExtendedFunction().clear();
        return;
      case TTCN3Package.OP_CALL__PRE_FUNCTION:
        setPreFunction((PreDefFunction)null);
        return;
      case TTCN3Package.OP_CALL__TESTCASE:
        setTestcase((TestcaseInstance)null);
        return;
      case TTCN3Package.OP_CALL__ACTIVATE:
        setActivate((ActivateOp)null);
        return;
      case TTCN3Package.OP_CALL__TEMPLATE_OPS:
        setTemplateOps((TemplateOps)null);
        return;
      case TTCN3Package.OP_CALL__EXTENDED_TEMPLATE:
        getExtendedTemplate().clear();
        return;
      case TTCN3Package.OP_CALL__FIELD:
        getField().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.OP_CALL__CONFIGURATION:
        return configuration != null;
      case TTCN3Package.OP_CALL__VERDICT:
        return VERDICT_EDEFAULT == null ? verdict != null : !VERDICT_EDEFAULT.equals(verdict);
      case TTCN3Package.OP_CALL__TIMER:
        return timer != null;
      case TTCN3Package.OP_CALL__FUNCTION:
        return function != null;
      case TTCN3Package.OP_CALL__EXTENDED_FUNCTION:
        return extendedFunction != null && !extendedFunction.isEmpty();
      case TTCN3Package.OP_CALL__PRE_FUNCTION:
        return preFunction != null;
      case TTCN3Package.OP_CALL__TESTCASE:
        return testcase != null;
      case TTCN3Package.OP_CALL__ACTIVATE:
        return activate != null;
      case TTCN3Package.OP_CALL__TEMPLATE_OPS:
        return templateOps != null;
      case TTCN3Package.OP_CALL__EXTENDED_TEMPLATE:
        return extendedTemplate != null && !extendedTemplate.isEmpty();
      case TTCN3Package.OP_CALL__FIELD:
        return field != null && !field.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (verdict: ");
    result.append(verdict);
    result.append(')');
    return result.toString();
  }

} //OpCallImpl
