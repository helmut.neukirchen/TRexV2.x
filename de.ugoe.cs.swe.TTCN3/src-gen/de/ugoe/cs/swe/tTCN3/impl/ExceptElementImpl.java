/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ExceptAltstepSpec;
import de.ugoe.cs.swe.tTCN3.ExceptConstSpec;
import de.ugoe.cs.swe.tTCN3.ExceptElement;
import de.ugoe.cs.swe.tTCN3.ExceptFunctionSpec;
import de.ugoe.cs.swe.tTCN3.ExceptGroupSpec;
import de.ugoe.cs.swe.tTCN3.ExceptModuleParSpec;
import de.ugoe.cs.swe.tTCN3.ExceptSignatureSpec;
import de.ugoe.cs.swe.tTCN3.ExceptTemplateSpec;
import de.ugoe.cs.swe.tTCN3.ExceptTestcaseSpec;
import de.ugoe.cs.swe.tTCN3.ExceptTypeDefSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Except Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl#getConst <em>Const</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl#getTestcase <em>Testcase</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl#getAltstep <em>Altstep</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl#getFunction <em>Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl#getSignature <em>Signature</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ExceptElementImpl#getModulePar <em>Module Par</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExceptElementImpl extends MinimalEObjectImpl.Container implements ExceptElement
{
  /**
   * The cached value of the '{@link #getGroup() <em>Group</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroup()
   * @generated
   * @ordered
   */
  protected ExceptGroupSpec group;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected ExceptTypeDefSpec type;

  /**
   * The cached value of the '{@link #getTemplate() <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplate()
   * @generated
   * @ordered
   */
  protected ExceptTemplateSpec template;

  /**
   * The cached value of the '{@link #getConst() <em>Const</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConst()
   * @generated
   * @ordered
   */
  protected ExceptConstSpec const_;

  /**
   * The cached value of the '{@link #getTestcase() <em>Testcase</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTestcase()
   * @generated
   * @ordered
   */
  protected ExceptTestcaseSpec testcase;

  /**
   * The cached value of the '{@link #getAltstep() <em>Altstep</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAltstep()
   * @generated
   * @ordered
   */
  protected ExceptAltstepSpec altstep;

  /**
   * The cached value of the '{@link #getFunction() <em>Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunction()
   * @generated
   * @ordered
   */
  protected ExceptFunctionSpec function;

  /**
   * The cached value of the '{@link #getSignature() <em>Signature</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSignature()
   * @generated
   * @ordered
   */
  protected ExceptSignatureSpec signature;

  /**
   * The cached value of the '{@link #getModulePar() <em>Module Par</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModulePar()
   * @generated
   * @ordered
   */
  protected ExceptModuleParSpec modulePar;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExceptElementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getExceptElement();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptGroupSpec getGroup()
  {
    return group;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGroup(ExceptGroupSpec newGroup, NotificationChain msgs)
  {
    ExceptGroupSpec oldGroup = group;
    group = newGroup;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__GROUP, oldGroup, newGroup);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGroup(ExceptGroupSpec newGroup)
  {
    if (newGroup != group)
    {
      NotificationChain msgs = null;
      if (group != null)
        msgs = ((InternalEObject)group).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__GROUP, null, msgs);
      if (newGroup != null)
        msgs = ((InternalEObject)newGroup).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__GROUP, null, msgs);
      msgs = basicSetGroup(newGroup, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__GROUP, newGroup, newGroup));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptTypeDefSpec getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(ExceptTypeDefSpec newType, NotificationChain msgs)
  {
    ExceptTypeDefSpec oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(ExceptTypeDefSpec newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptTemplateSpec getTemplate()
  {
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTemplate(ExceptTemplateSpec newTemplate, NotificationChain msgs)
  {
    ExceptTemplateSpec oldTemplate = template;
    template = newTemplate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__TEMPLATE, oldTemplate, newTemplate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTemplate(ExceptTemplateSpec newTemplate)
  {
    if (newTemplate != template)
    {
      NotificationChain msgs = null;
      if (template != null)
        msgs = ((InternalEObject)template).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__TEMPLATE, null, msgs);
      if (newTemplate != null)
        msgs = ((InternalEObject)newTemplate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__TEMPLATE, null, msgs);
      msgs = basicSetTemplate(newTemplate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__TEMPLATE, newTemplate, newTemplate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptConstSpec getConst()
  {
    return const_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConst(ExceptConstSpec newConst, NotificationChain msgs)
  {
    ExceptConstSpec oldConst = const_;
    const_ = newConst;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__CONST, oldConst, newConst);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConst(ExceptConstSpec newConst)
  {
    if (newConst != const_)
    {
      NotificationChain msgs = null;
      if (const_ != null)
        msgs = ((InternalEObject)const_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__CONST, null, msgs);
      if (newConst != null)
        msgs = ((InternalEObject)newConst).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__CONST, null, msgs);
      msgs = basicSetConst(newConst, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__CONST, newConst, newConst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptTestcaseSpec getTestcase()
  {
    return testcase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTestcase(ExceptTestcaseSpec newTestcase, NotificationChain msgs)
  {
    ExceptTestcaseSpec oldTestcase = testcase;
    testcase = newTestcase;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__TESTCASE, oldTestcase, newTestcase);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTestcase(ExceptTestcaseSpec newTestcase)
  {
    if (newTestcase != testcase)
    {
      NotificationChain msgs = null;
      if (testcase != null)
        msgs = ((InternalEObject)testcase).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__TESTCASE, null, msgs);
      if (newTestcase != null)
        msgs = ((InternalEObject)newTestcase).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__TESTCASE, null, msgs);
      msgs = basicSetTestcase(newTestcase, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__TESTCASE, newTestcase, newTestcase));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptAltstepSpec getAltstep()
  {
    return altstep;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAltstep(ExceptAltstepSpec newAltstep, NotificationChain msgs)
  {
    ExceptAltstepSpec oldAltstep = altstep;
    altstep = newAltstep;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__ALTSTEP, oldAltstep, newAltstep);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAltstep(ExceptAltstepSpec newAltstep)
  {
    if (newAltstep != altstep)
    {
      NotificationChain msgs = null;
      if (altstep != null)
        msgs = ((InternalEObject)altstep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__ALTSTEP, null, msgs);
      if (newAltstep != null)
        msgs = ((InternalEObject)newAltstep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__ALTSTEP, null, msgs);
      msgs = basicSetAltstep(newAltstep, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__ALTSTEP, newAltstep, newAltstep));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptFunctionSpec getFunction()
  {
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFunction(ExceptFunctionSpec newFunction, NotificationChain msgs)
  {
    ExceptFunctionSpec oldFunction = function;
    function = newFunction;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__FUNCTION, oldFunction, newFunction);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFunction(ExceptFunctionSpec newFunction)
  {
    if (newFunction != function)
    {
      NotificationChain msgs = null;
      if (function != null)
        msgs = ((InternalEObject)function).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__FUNCTION, null, msgs);
      if (newFunction != null)
        msgs = ((InternalEObject)newFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__FUNCTION, null, msgs);
      msgs = basicSetFunction(newFunction, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__FUNCTION, newFunction, newFunction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptSignatureSpec getSignature()
  {
    return signature;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSignature(ExceptSignatureSpec newSignature, NotificationChain msgs)
  {
    ExceptSignatureSpec oldSignature = signature;
    signature = newSignature;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__SIGNATURE, oldSignature, newSignature);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSignature(ExceptSignatureSpec newSignature)
  {
    if (newSignature != signature)
    {
      NotificationChain msgs = null;
      if (signature != null)
        msgs = ((InternalEObject)signature).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__SIGNATURE, null, msgs);
      if (newSignature != null)
        msgs = ((InternalEObject)newSignature).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__SIGNATURE, null, msgs);
      msgs = basicSetSignature(newSignature, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__SIGNATURE, newSignature, newSignature));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptModuleParSpec getModulePar()
  {
    return modulePar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetModulePar(ExceptModuleParSpec newModulePar, NotificationChain msgs)
  {
    ExceptModuleParSpec oldModulePar = modulePar;
    modulePar = newModulePar;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__MODULE_PAR, oldModulePar, newModulePar);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModulePar(ExceptModuleParSpec newModulePar)
  {
    if (newModulePar != modulePar)
    {
      NotificationChain msgs = null;
      if (modulePar != null)
        msgs = ((InternalEObject)modulePar).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__MODULE_PAR, null, msgs);
      if (newModulePar != null)
        msgs = ((InternalEObject)newModulePar).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.EXCEPT_ELEMENT__MODULE_PAR, null, msgs);
      msgs = basicSetModulePar(newModulePar, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.EXCEPT_ELEMENT__MODULE_PAR, newModulePar, newModulePar));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.EXCEPT_ELEMENT__GROUP:
        return basicSetGroup(null, msgs);
      case TTCN3Package.EXCEPT_ELEMENT__TYPE:
        return basicSetType(null, msgs);
      case TTCN3Package.EXCEPT_ELEMENT__TEMPLATE:
        return basicSetTemplate(null, msgs);
      case TTCN3Package.EXCEPT_ELEMENT__CONST:
        return basicSetConst(null, msgs);
      case TTCN3Package.EXCEPT_ELEMENT__TESTCASE:
        return basicSetTestcase(null, msgs);
      case TTCN3Package.EXCEPT_ELEMENT__ALTSTEP:
        return basicSetAltstep(null, msgs);
      case TTCN3Package.EXCEPT_ELEMENT__FUNCTION:
        return basicSetFunction(null, msgs);
      case TTCN3Package.EXCEPT_ELEMENT__SIGNATURE:
        return basicSetSignature(null, msgs);
      case TTCN3Package.EXCEPT_ELEMENT__MODULE_PAR:
        return basicSetModulePar(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.EXCEPT_ELEMENT__GROUP:
        return getGroup();
      case TTCN3Package.EXCEPT_ELEMENT__TYPE:
        return getType();
      case TTCN3Package.EXCEPT_ELEMENT__TEMPLATE:
        return getTemplate();
      case TTCN3Package.EXCEPT_ELEMENT__CONST:
        return getConst();
      case TTCN3Package.EXCEPT_ELEMENT__TESTCASE:
        return getTestcase();
      case TTCN3Package.EXCEPT_ELEMENT__ALTSTEP:
        return getAltstep();
      case TTCN3Package.EXCEPT_ELEMENT__FUNCTION:
        return getFunction();
      case TTCN3Package.EXCEPT_ELEMENT__SIGNATURE:
        return getSignature();
      case TTCN3Package.EXCEPT_ELEMENT__MODULE_PAR:
        return getModulePar();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.EXCEPT_ELEMENT__GROUP:
        setGroup((ExceptGroupSpec)newValue);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__TYPE:
        setType((ExceptTypeDefSpec)newValue);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__TEMPLATE:
        setTemplate((ExceptTemplateSpec)newValue);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__CONST:
        setConst((ExceptConstSpec)newValue);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__TESTCASE:
        setTestcase((ExceptTestcaseSpec)newValue);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__ALTSTEP:
        setAltstep((ExceptAltstepSpec)newValue);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__FUNCTION:
        setFunction((ExceptFunctionSpec)newValue);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__SIGNATURE:
        setSignature((ExceptSignatureSpec)newValue);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__MODULE_PAR:
        setModulePar((ExceptModuleParSpec)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.EXCEPT_ELEMENT__GROUP:
        setGroup((ExceptGroupSpec)null);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__TYPE:
        setType((ExceptTypeDefSpec)null);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__TEMPLATE:
        setTemplate((ExceptTemplateSpec)null);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__CONST:
        setConst((ExceptConstSpec)null);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__TESTCASE:
        setTestcase((ExceptTestcaseSpec)null);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__ALTSTEP:
        setAltstep((ExceptAltstepSpec)null);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__FUNCTION:
        setFunction((ExceptFunctionSpec)null);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__SIGNATURE:
        setSignature((ExceptSignatureSpec)null);
        return;
      case TTCN3Package.EXCEPT_ELEMENT__MODULE_PAR:
        setModulePar((ExceptModuleParSpec)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.EXCEPT_ELEMENT__GROUP:
        return group != null;
      case TTCN3Package.EXCEPT_ELEMENT__TYPE:
        return type != null;
      case TTCN3Package.EXCEPT_ELEMENT__TEMPLATE:
        return template != null;
      case TTCN3Package.EXCEPT_ELEMENT__CONST:
        return const_ != null;
      case TTCN3Package.EXCEPT_ELEMENT__TESTCASE:
        return testcase != null;
      case TTCN3Package.EXCEPT_ELEMENT__ALTSTEP:
        return altstep != null;
      case TTCN3Package.EXCEPT_ELEMENT__FUNCTION:
        return function != null;
      case TTCN3Package.EXCEPT_ELEMENT__SIGNATURE:
        return signature != null;
      case TTCN3Package.EXCEPT_ELEMENT__MODULE_PAR:
        return modulePar != null;
    }
    return super.eIsSet(featureID);
  }

} //ExceptElementImpl
