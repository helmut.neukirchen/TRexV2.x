/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ExtendedFieldReference;
import de.ugoe.cs.swe.tTCN3.MatchingSymbol;
import de.ugoe.cs.swe.tTCN3.SingleTemplateExpression;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateRefWithParList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Single Template Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SingleTemplateExpressionImpl#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SingleTemplateExpressionImpl#getList <em>List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SingleTemplateExpressionImpl#getExtended <em>Extended</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SingleTemplateExpressionImpl extends MinimalEObjectImpl.Container implements SingleTemplateExpression
{
  /**
   * The cached value of the '{@link #getSymbol() <em>Symbol</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSymbol()
   * @generated
   * @ordered
   */
  protected MatchingSymbol symbol;

  /**
   * The cached value of the '{@link #getList() <em>List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getList()
   * @generated
   * @ordered
   */
  protected TemplateRefWithParList list;

  /**
   * The cached value of the '{@link #getExtended() <em>Extended</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtended()
   * @generated
   * @ordered
   */
  protected ExtendedFieldReference extended;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SingleTemplateExpressionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getSingleTemplateExpression();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MatchingSymbol getSymbol()
  {
    return symbol;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSymbol(MatchingSymbol newSymbol, NotificationChain msgs)
  {
    MatchingSymbol oldSymbol = symbol;
    symbol = newSymbol;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__SYMBOL, oldSymbol, newSymbol);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSymbol(MatchingSymbol newSymbol)
  {
    if (newSymbol != symbol)
    {
      NotificationChain msgs = null;
      if (symbol != null)
        msgs = ((InternalEObject)symbol).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__SYMBOL, null, msgs);
      if (newSymbol != null)
        msgs = ((InternalEObject)newSymbol).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__SYMBOL, null, msgs);
      msgs = basicSetSymbol(newSymbol, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__SYMBOL, newSymbol, newSymbol));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateRefWithParList getList()
  {
    return list;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetList(TemplateRefWithParList newList, NotificationChain msgs)
  {
    TemplateRefWithParList oldList = list;
    list = newList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__LIST, oldList, newList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setList(TemplateRefWithParList newList)
  {
    if (newList != list)
    {
      NotificationChain msgs = null;
      if (list != null)
        msgs = ((InternalEObject)list).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__LIST, null, msgs);
      if (newList != null)
        msgs = ((InternalEObject)newList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__LIST, null, msgs);
      msgs = basicSetList(newList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__LIST, newList, newList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtendedFieldReference getExtended()
  {
    return extended;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExtended(ExtendedFieldReference newExtended, NotificationChain msgs)
  {
    ExtendedFieldReference oldExtended = extended;
    extended = newExtended;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__EXTENDED, oldExtended, newExtended);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExtended(ExtendedFieldReference newExtended)
  {
    if (newExtended != extended)
    {
      NotificationChain msgs = null;
      if (extended != null)
        msgs = ((InternalEObject)extended).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__EXTENDED, null, msgs);
      if (newExtended != null)
        msgs = ((InternalEObject)newExtended).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__EXTENDED, null, msgs);
      msgs = basicSetExtended(newExtended, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__EXTENDED, newExtended, newExtended));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__SYMBOL:
        return basicSetSymbol(null, msgs);
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__LIST:
        return basicSetList(null, msgs);
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__EXTENDED:
        return basicSetExtended(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__SYMBOL:
        return getSymbol();
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__LIST:
        return getList();
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__EXTENDED:
        return getExtended();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__SYMBOL:
        setSymbol((MatchingSymbol)newValue);
        return;
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__LIST:
        setList((TemplateRefWithParList)newValue);
        return;
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__EXTENDED:
        setExtended((ExtendedFieldReference)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__SYMBOL:
        setSymbol((MatchingSymbol)null);
        return;
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__LIST:
        setList((TemplateRefWithParList)null);
        return;
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__EXTENDED:
        setExtended((ExtendedFieldReference)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__SYMBOL:
        return symbol != null;
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__LIST:
        return list != null;
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION__EXTENDED:
        return extended != null;
    }
    return super.eIsSet(featureID);
  }

} //SingleTemplateExpressionImpl
