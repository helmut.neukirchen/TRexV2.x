/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.BooleanExpression;
import de.ugoe.cs.swe.tTCN3.ConditionalConstruct;
import de.ugoe.cs.swe.tTCN3.ElseClause;
import de.ugoe.cs.swe.tTCN3.ElseIfClause;
import de.ugoe.cs.swe.tTCN3.StatementBlock;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conditional Construct</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConditionalConstructImpl#getExpression <em>Expression</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConditionalConstructImpl#getStatement <em>Statement</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConditionalConstructImpl#getElseifs <em>Elseifs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConditionalConstructImpl#getElse <em>Else</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionalConstructImpl extends MinimalEObjectImpl.Container implements ConditionalConstruct
{
  /**
   * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpression()
   * @generated
   * @ordered
   */
  protected BooleanExpression expression;

  /**
   * The cached value of the '{@link #getStatement() <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatement()
   * @generated
   * @ordered
   */
  protected StatementBlock statement;

  /**
   * The cached value of the '{@link #getElseifs() <em>Elseifs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElseifs()
   * @generated
   * @ordered
   */
  protected EList<ElseIfClause> elseifs;

  /**
   * The cached value of the '{@link #getElse() <em>Else</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElse()
   * @generated
   * @ordered
   */
  protected ElseClause else_;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ConditionalConstructImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getConditionalConstruct();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanExpression getExpression()
  {
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpression(BooleanExpression newExpression, NotificationChain msgs)
  {
    BooleanExpression oldExpression = expression;
    expression = newExpression;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONDITIONAL_CONSTRUCT__EXPRESSION, oldExpression, newExpression);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpression(BooleanExpression newExpression)
  {
    if (newExpression != expression)
    {
      NotificationChain msgs = null;
      if (expression != null)
        msgs = ((InternalEObject)expression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONDITIONAL_CONSTRUCT__EXPRESSION, null, msgs);
      if (newExpression != null)
        msgs = ((InternalEObject)newExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONDITIONAL_CONSTRUCT__EXPRESSION, null, msgs);
      msgs = basicSetExpression(newExpression, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONDITIONAL_CONSTRUCT__EXPRESSION, newExpression, newExpression));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementBlock getStatement()
  {
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStatement(StatementBlock newStatement, NotificationChain msgs)
  {
    StatementBlock oldStatement = statement;
    statement = newStatement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONDITIONAL_CONSTRUCT__STATEMENT, oldStatement, newStatement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatement(StatementBlock newStatement)
  {
    if (newStatement != statement)
    {
      NotificationChain msgs = null;
      if (statement != null)
        msgs = ((InternalEObject)statement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONDITIONAL_CONSTRUCT__STATEMENT, null, msgs);
      if (newStatement != null)
        msgs = ((InternalEObject)newStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONDITIONAL_CONSTRUCT__STATEMENT, null, msgs);
      msgs = basicSetStatement(newStatement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONDITIONAL_CONSTRUCT__STATEMENT, newStatement, newStatement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ElseIfClause> getElseifs()
  {
    if (elseifs == null)
    {
      elseifs = new EObjectContainmentEList<ElseIfClause>(ElseIfClause.class, this, TTCN3Package.CONDITIONAL_CONSTRUCT__ELSEIFS);
    }
    return elseifs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ElseClause getElse()
  {
    return else_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetElse(ElseClause newElse, NotificationChain msgs)
  {
    ElseClause oldElse = else_;
    else_ = newElse;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONDITIONAL_CONSTRUCT__ELSE, oldElse, newElse);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setElse(ElseClause newElse)
  {
    if (newElse != else_)
    {
      NotificationChain msgs = null;
      if (else_ != null)
        msgs = ((InternalEObject)else_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONDITIONAL_CONSTRUCT__ELSE, null, msgs);
      if (newElse != null)
        msgs = ((InternalEObject)newElse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONDITIONAL_CONSTRUCT__ELSE, null, msgs);
      msgs = basicSetElse(newElse, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONDITIONAL_CONSTRUCT__ELSE, newElse, newElse));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.CONDITIONAL_CONSTRUCT__EXPRESSION:
        return basicSetExpression(null, msgs);
      case TTCN3Package.CONDITIONAL_CONSTRUCT__STATEMENT:
        return basicSetStatement(null, msgs);
      case TTCN3Package.CONDITIONAL_CONSTRUCT__ELSEIFS:
        return ((InternalEList<?>)getElseifs()).basicRemove(otherEnd, msgs);
      case TTCN3Package.CONDITIONAL_CONSTRUCT__ELSE:
        return basicSetElse(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.CONDITIONAL_CONSTRUCT__EXPRESSION:
        return getExpression();
      case TTCN3Package.CONDITIONAL_CONSTRUCT__STATEMENT:
        return getStatement();
      case TTCN3Package.CONDITIONAL_CONSTRUCT__ELSEIFS:
        return getElseifs();
      case TTCN3Package.CONDITIONAL_CONSTRUCT__ELSE:
        return getElse();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.CONDITIONAL_CONSTRUCT__EXPRESSION:
        setExpression((BooleanExpression)newValue);
        return;
      case TTCN3Package.CONDITIONAL_CONSTRUCT__STATEMENT:
        setStatement((StatementBlock)newValue);
        return;
      case TTCN3Package.CONDITIONAL_CONSTRUCT__ELSEIFS:
        getElseifs().clear();
        getElseifs().addAll((Collection<? extends ElseIfClause>)newValue);
        return;
      case TTCN3Package.CONDITIONAL_CONSTRUCT__ELSE:
        setElse((ElseClause)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONDITIONAL_CONSTRUCT__EXPRESSION:
        setExpression((BooleanExpression)null);
        return;
      case TTCN3Package.CONDITIONAL_CONSTRUCT__STATEMENT:
        setStatement((StatementBlock)null);
        return;
      case TTCN3Package.CONDITIONAL_CONSTRUCT__ELSEIFS:
        getElseifs().clear();
        return;
      case TTCN3Package.CONDITIONAL_CONSTRUCT__ELSE:
        setElse((ElseClause)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONDITIONAL_CONSTRUCT__EXPRESSION:
        return expression != null;
      case TTCN3Package.CONDITIONAL_CONSTRUCT__STATEMENT:
        return statement != null;
      case TTCN3Package.CONDITIONAL_CONSTRUCT__ELSEIFS:
        return elseifs != null && !elseifs.isEmpty();
      case TTCN3Package.CONDITIONAL_CONSTRUCT__ELSE:
        return else_ != null;
    }
    return super.eIsSet(featureID);
  }

} //ConditionalConstructImpl
