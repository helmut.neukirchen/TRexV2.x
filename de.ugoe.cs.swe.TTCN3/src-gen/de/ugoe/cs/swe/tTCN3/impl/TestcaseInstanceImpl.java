/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.Expression;
import de.ugoe.cs.swe.tTCN3.SingleExpression;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TestcaseActualParList;
import de.ugoe.cs.swe.tTCN3.TestcaseDef;
import de.ugoe.cs.swe.tTCN3.TestcaseInstance;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Testcase Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseInstanceImpl#getRef <em>Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseInstanceImpl#getTcParams <em>Tc Params</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseInstanceImpl#getExpr <em>Expr</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TestcaseInstanceImpl#getSexpr <em>Sexpr</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestcaseInstanceImpl extends MinimalEObjectImpl.Container implements TestcaseInstance
{
  /**
   * The cached value of the '{@link #getRef() <em>Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRef()
   * @generated
   * @ordered
   */
  protected TestcaseDef ref;

  /**
   * The cached value of the '{@link #getTcParams() <em>Tc Params</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTcParams()
   * @generated
   * @ordered
   */
  protected TestcaseActualParList tcParams;

  /**
   * The cached value of the '{@link #getExpr() <em>Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpr()
   * @generated
   * @ordered
   */
  protected Expression expr;

  /**
   * The cached value of the '{@link #getSexpr() <em>Sexpr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSexpr()
   * @generated
   * @ordered
   */
  protected SingleExpression sexpr;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TestcaseInstanceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTestcaseInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TestcaseDef getRef()
  {
    if (ref != null && ref.eIsProxy())
    {
      InternalEObject oldRef = (InternalEObject)ref;
      ref = (TestcaseDef)eResolveProxy(oldRef);
      if (ref != oldRef)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, TTCN3Package.TESTCASE_INSTANCE__REF, oldRef, ref));
      }
    }
    return ref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TestcaseDef basicGetRef()
  {
    return ref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRef(TestcaseDef newRef)
  {
    TestcaseDef oldRef = ref;
    ref = newRef;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_INSTANCE__REF, oldRef, ref));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TestcaseActualParList getTcParams()
  {
    return tcParams;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTcParams(TestcaseActualParList newTcParams, NotificationChain msgs)
  {
    TestcaseActualParList oldTcParams = tcParams;
    tcParams = newTcParams;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_INSTANCE__TC_PARAMS, oldTcParams, newTcParams);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTcParams(TestcaseActualParList newTcParams)
  {
    if (newTcParams != tcParams)
    {
      NotificationChain msgs = null;
      if (tcParams != null)
        msgs = ((InternalEObject)tcParams).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_INSTANCE__TC_PARAMS, null, msgs);
      if (newTcParams != null)
        msgs = ((InternalEObject)newTcParams).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_INSTANCE__TC_PARAMS, null, msgs);
      msgs = basicSetTcParams(newTcParams, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_INSTANCE__TC_PARAMS, newTcParams, newTcParams));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getExpr()
  {
    return expr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpr(Expression newExpr, NotificationChain msgs)
  {
    Expression oldExpr = expr;
    expr = newExpr;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_INSTANCE__EXPR, oldExpr, newExpr);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpr(Expression newExpr)
  {
    if (newExpr != expr)
    {
      NotificationChain msgs = null;
      if (expr != null)
        msgs = ((InternalEObject)expr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_INSTANCE__EXPR, null, msgs);
      if (newExpr != null)
        msgs = ((InternalEObject)newExpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_INSTANCE__EXPR, null, msgs);
      msgs = basicSetExpr(newExpr, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_INSTANCE__EXPR, newExpr, newExpr));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SingleExpression getSexpr()
  {
    return sexpr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSexpr(SingleExpression newSexpr, NotificationChain msgs)
  {
    SingleExpression oldSexpr = sexpr;
    sexpr = newSexpr;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_INSTANCE__SEXPR, oldSexpr, newSexpr);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSexpr(SingleExpression newSexpr)
  {
    if (newSexpr != sexpr)
    {
      NotificationChain msgs = null;
      if (sexpr != null)
        msgs = ((InternalEObject)sexpr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_INSTANCE__SEXPR, null, msgs);
      if (newSexpr != null)
        msgs = ((InternalEObject)newSexpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TESTCASE_INSTANCE__SEXPR, null, msgs);
      msgs = basicSetSexpr(newSexpr, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TESTCASE_INSTANCE__SEXPR, newSexpr, newSexpr));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_INSTANCE__TC_PARAMS:
        return basicSetTcParams(null, msgs);
      case TTCN3Package.TESTCASE_INSTANCE__EXPR:
        return basicSetExpr(null, msgs);
      case TTCN3Package.TESTCASE_INSTANCE__SEXPR:
        return basicSetSexpr(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_INSTANCE__REF:
        if (resolve) return getRef();
        return basicGetRef();
      case TTCN3Package.TESTCASE_INSTANCE__TC_PARAMS:
        return getTcParams();
      case TTCN3Package.TESTCASE_INSTANCE__EXPR:
        return getExpr();
      case TTCN3Package.TESTCASE_INSTANCE__SEXPR:
        return getSexpr();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_INSTANCE__REF:
        setRef((TestcaseDef)newValue);
        return;
      case TTCN3Package.TESTCASE_INSTANCE__TC_PARAMS:
        setTcParams((TestcaseActualParList)newValue);
        return;
      case TTCN3Package.TESTCASE_INSTANCE__EXPR:
        setExpr((Expression)newValue);
        return;
      case TTCN3Package.TESTCASE_INSTANCE__SEXPR:
        setSexpr((SingleExpression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_INSTANCE__REF:
        setRef((TestcaseDef)null);
        return;
      case TTCN3Package.TESTCASE_INSTANCE__TC_PARAMS:
        setTcParams((TestcaseActualParList)null);
        return;
      case TTCN3Package.TESTCASE_INSTANCE__EXPR:
        setExpr((Expression)null);
        return;
      case TTCN3Package.TESTCASE_INSTANCE__SEXPR:
        setSexpr((SingleExpression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TESTCASE_INSTANCE__REF:
        return ref != null;
      case TTCN3Package.TESTCASE_INSTANCE__TC_PARAMS:
        return tcParams != null;
      case TTCN3Package.TESTCASE_INSTANCE__EXPR:
        return expr != null;
      case TTCN3Package.TESTCASE_INSTANCE__SEXPR:
        return sexpr != null;
    }
    return super.eIsSet(featureID);
  }

} //TestcaseInstanceImpl
