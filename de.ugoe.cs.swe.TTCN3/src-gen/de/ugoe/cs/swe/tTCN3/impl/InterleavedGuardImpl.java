/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.GuardOp;
import de.ugoe.cs.swe.tTCN3.InterleavedGuard;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interleaved Guard</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.InterleavedGuardImpl#getGuardOp <em>Guard Op</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InterleavedGuardImpl extends MinimalEObjectImpl.Container implements InterleavedGuard
{
  /**
   * The cached value of the '{@link #getGuardOp() <em>Guard Op</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGuardOp()
   * @generated
   * @ordered
   */
  protected GuardOp guardOp;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InterleavedGuardImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getInterleavedGuard();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GuardOp getGuardOp()
  {
    return guardOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGuardOp(GuardOp newGuardOp, NotificationChain msgs)
  {
    GuardOp oldGuardOp = guardOp;
    guardOp = newGuardOp;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.INTERLEAVED_GUARD__GUARD_OP, oldGuardOp, newGuardOp);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGuardOp(GuardOp newGuardOp)
  {
    if (newGuardOp != guardOp)
    {
      NotificationChain msgs = null;
      if (guardOp != null)
        msgs = ((InternalEObject)guardOp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.INTERLEAVED_GUARD__GUARD_OP, null, msgs);
      if (newGuardOp != null)
        msgs = ((InternalEObject)newGuardOp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.INTERLEAVED_GUARD__GUARD_OP, null, msgs);
      msgs = basicSetGuardOp(newGuardOp, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.INTERLEAVED_GUARD__GUARD_OP, newGuardOp, newGuardOp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.INTERLEAVED_GUARD__GUARD_OP:
        return basicSetGuardOp(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.INTERLEAVED_GUARD__GUARD_OP:
        return getGuardOp();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.INTERLEAVED_GUARD__GUARD_OP:
        setGuardOp((GuardOp)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.INTERLEAVED_GUARD__GUARD_OP:
        setGuardOp((GuardOp)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.INTERLEAVED_GUARD__GUARD_OP:
        return guardOp != null;
    }
    return super.eIsSet(featureID);
  }

} //InterleavedGuardImpl
