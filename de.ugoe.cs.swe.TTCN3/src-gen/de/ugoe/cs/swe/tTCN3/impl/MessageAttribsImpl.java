/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AddressDecl;
import de.ugoe.cs.swe.tTCN3.ConfigParamDef;
import de.ugoe.cs.swe.tTCN3.MessageAttribs;
import de.ugoe.cs.swe.tTCN3.MessageList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Attribs</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MessageAttribsImpl#getAdresses <em>Adresses</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MessageAttribsImpl#getMessages <em>Messages</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.MessageAttribsImpl#getConfigs <em>Configs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MessageAttribsImpl extends MinimalEObjectImpl.Container implements MessageAttribs
{
  /**
   * The cached value of the '{@link #getAdresses() <em>Adresses</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdresses()
   * @generated
   * @ordered
   */
  protected EList<AddressDecl> adresses;

  /**
   * The cached value of the '{@link #getMessages() <em>Messages</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMessages()
   * @generated
   * @ordered
   */
  protected EList<MessageList> messages;

  /**
   * The cached value of the '{@link #getConfigs() <em>Configs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConfigs()
   * @generated
   * @ordered
   */
  protected EList<ConfigParamDef> configs;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MessageAttribsImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getMessageAttribs();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<AddressDecl> getAdresses()
  {
    if (adresses == null)
    {
      adresses = new EObjectContainmentEList<AddressDecl>(AddressDecl.class, this, TTCN3Package.MESSAGE_ATTRIBS__ADRESSES);
    }
    return adresses;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<MessageList> getMessages()
  {
    if (messages == null)
    {
      messages = new EObjectContainmentEList<MessageList>(MessageList.class, this, TTCN3Package.MESSAGE_ATTRIBS__MESSAGES);
    }
    return messages;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ConfigParamDef> getConfigs()
  {
    if (configs == null)
    {
      configs = new EObjectContainmentEList<ConfigParamDef>(ConfigParamDef.class, this, TTCN3Package.MESSAGE_ATTRIBS__CONFIGS);
    }
    return configs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.MESSAGE_ATTRIBS__ADRESSES:
        return ((InternalEList<?>)getAdresses()).basicRemove(otherEnd, msgs);
      case TTCN3Package.MESSAGE_ATTRIBS__MESSAGES:
        return ((InternalEList<?>)getMessages()).basicRemove(otherEnd, msgs);
      case TTCN3Package.MESSAGE_ATTRIBS__CONFIGS:
        return ((InternalEList<?>)getConfigs()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.MESSAGE_ATTRIBS__ADRESSES:
        return getAdresses();
      case TTCN3Package.MESSAGE_ATTRIBS__MESSAGES:
        return getMessages();
      case TTCN3Package.MESSAGE_ATTRIBS__CONFIGS:
        return getConfigs();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.MESSAGE_ATTRIBS__ADRESSES:
        getAdresses().clear();
        getAdresses().addAll((Collection<? extends AddressDecl>)newValue);
        return;
      case TTCN3Package.MESSAGE_ATTRIBS__MESSAGES:
        getMessages().clear();
        getMessages().addAll((Collection<? extends MessageList>)newValue);
        return;
      case TTCN3Package.MESSAGE_ATTRIBS__CONFIGS:
        getConfigs().clear();
        getConfigs().addAll((Collection<? extends ConfigParamDef>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MESSAGE_ATTRIBS__ADRESSES:
        getAdresses().clear();
        return;
      case TTCN3Package.MESSAGE_ATTRIBS__MESSAGES:
        getMessages().clear();
        return;
      case TTCN3Package.MESSAGE_ATTRIBS__CONFIGS:
        getConfigs().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.MESSAGE_ATTRIBS__ADRESSES:
        return adresses != null && !adresses.isEmpty();
      case TTCN3Package.MESSAGE_ATTRIBS__MESSAGES:
        return messages != null && !messages.isEmpty();
      case TTCN3Package.MESSAGE_ATTRIBS__CONFIGS:
        return configs != null && !configs.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //MessageAttribsImpl
