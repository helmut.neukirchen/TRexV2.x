/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ConstDef;
import de.ugoe.cs.swe.tTCN3.FunctionLocalDef;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateDef;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Local Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionLocalDefImpl#getConstDef <em>Const Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FunctionLocalDefImpl#getTemplateDef <em>Template Def</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionLocalDefImpl extends MinimalEObjectImpl.Container implements FunctionLocalDef
{
  /**
   * The cached value of the '{@link #getConstDef() <em>Const Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConstDef()
   * @generated
   * @ordered
   */
  protected ConstDef constDef;

  /**
   * The cached value of the '{@link #getTemplateDef() <em>Template Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplateDef()
   * @generated
   * @ordered
   */
  protected TemplateDef templateDef;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FunctionLocalDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFunctionLocalDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstDef getConstDef()
  {
    return constDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConstDef(ConstDef newConstDef, NotificationChain msgs)
  {
    ConstDef oldConstDef = constDef;
    constDef = newConstDef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_LOCAL_DEF__CONST_DEF, oldConstDef, newConstDef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConstDef(ConstDef newConstDef)
  {
    if (newConstDef != constDef)
    {
      NotificationChain msgs = null;
      if (constDef != null)
        msgs = ((InternalEObject)constDef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_LOCAL_DEF__CONST_DEF, null, msgs);
      if (newConstDef != null)
        msgs = ((InternalEObject)newConstDef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_LOCAL_DEF__CONST_DEF, null, msgs);
      msgs = basicSetConstDef(newConstDef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_LOCAL_DEF__CONST_DEF, newConstDef, newConstDef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateDef getTemplateDef()
  {
    return templateDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTemplateDef(TemplateDef newTemplateDef, NotificationChain msgs)
  {
    TemplateDef oldTemplateDef = templateDef;
    templateDef = newTemplateDef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_LOCAL_DEF__TEMPLATE_DEF, oldTemplateDef, newTemplateDef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTemplateDef(TemplateDef newTemplateDef)
  {
    if (newTemplateDef != templateDef)
    {
      NotificationChain msgs = null;
      if (templateDef != null)
        msgs = ((InternalEObject)templateDef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_LOCAL_DEF__TEMPLATE_DEF, null, msgs);
      if (newTemplateDef != null)
        msgs = ((InternalEObject)newTemplateDef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FUNCTION_LOCAL_DEF__TEMPLATE_DEF, null, msgs);
      msgs = basicSetTemplateDef(newTemplateDef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FUNCTION_LOCAL_DEF__TEMPLATE_DEF, newTemplateDef, newTemplateDef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_LOCAL_DEF__CONST_DEF:
        return basicSetConstDef(null, msgs);
      case TTCN3Package.FUNCTION_LOCAL_DEF__TEMPLATE_DEF:
        return basicSetTemplateDef(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_LOCAL_DEF__CONST_DEF:
        return getConstDef();
      case TTCN3Package.FUNCTION_LOCAL_DEF__TEMPLATE_DEF:
        return getTemplateDef();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_LOCAL_DEF__CONST_DEF:
        setConstDef((ConstDef)newValue);
        return;
      case TTCN3Package.FUNCTION_LOCAL_DEF__TEMPLATE_DEF:
        setTemplateDef((TemplateDef)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_LOCAL_DEF__CONST_DEF:
        setConstDef((ConstDef)null);
        return;
      case TTCN3Package.FUNCTION_LOCAL_DEF__TEMPLATE_DEF:
        setTemplateDef((TemplateDef)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FUNCTION_LOCAL_DEF__CONST_DEF:
        return constDef != null;
      case TTCN3Package.FUNCTION_LOCAL_DEF__TEMPLATE_DEF:
        return templateDef != null;
    }
    return super.eIsSet(featureID);
  }

} //FunctionLocalDefImpl
