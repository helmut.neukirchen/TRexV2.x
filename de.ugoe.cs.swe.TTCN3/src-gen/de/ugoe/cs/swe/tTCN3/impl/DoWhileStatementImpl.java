/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.BooleanExpression;
import de.ugoe.cs.swe.tTCN3.DoWhileStatement;
import de.ugoe.cs.swe.tTCN3.StatementBlock;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Do While Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.DoWhileStatementImpl#getKeyDo <em>Key Do</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.DoWhileStatementImpl#getStatement <em>Statement</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.DoWhileStatementImpl#getKeyWhile <em>Key While</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.DoWhileStatementImpl#getExpression <em>Expression</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DoWhileStatementImpl extends MinimalEObjectImpl.Container implements DoWhileStatement
{
  /**
   * The default value of the '{@link #getKeyDo() <em>Key Do</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKeyDo()
   * @generated
   * @ordered
   */
  protected static final String KEY_DO_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getKeyDo() <em>Key Do</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKeyDo()
   * @generated
   * @ordered
   */
  protected String keyDo = KEY_DO_EDEFAULT;

  /**
   * The cached value of the '{@link #getStatement() <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStatement()
   * @generated
   * @ordered
   */
  protected StatementBlock statement;

  /**
   * The default value of the '{@link #getKeyWhile() <em>Key While</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKeyWhile()
   * @generated
   * @ordered
   */
  protected static final String KEY_WHILE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getKeyWhile() <em>Key While</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKeyWhile()
   * @generated
   * @ordered
   */
  protected String keyWhile = KEY_WHILE_EDEFAULT;

  /**
   * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpression()
   * @generated
   * @ordered
   */
  protected BooleanExpression expression;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DoWhileStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getDoWhileStatement();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getKeyDo()
  {
    return keyDo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setKeyDo(String newKeyDo)
  {
    String oldKeyDo = keyDo;
    keyDo = newKeyDo;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.DO_WHILE_STATEMENT__KEY_DO, oldKeyDo, keyDo));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementBlock getStatement()
  {
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetStatement(StatementBlock newStatement, NotificationChain msgs)
  {
    StatementBlock oldStatement = statement;
    statement = newStatement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.DO_WHILE_STATEMENT__STATEMENT, oldStatement, newStatement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStatement(StatementBlock newStatement)
  {
    if (newStatement != statement)
    {
      NotificationChain msgs = null;
      if (statement != null)
        msgs = ((InternalEObject)statement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.DO_WHILE_STATEMENT__STATEMENT, null, msgs);
      if (newStatement != null)
        msgs = ((InternalEObject)newStatement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.DO_WHILE_STATEMENT__STATEMENT, null, msgs);
      msgs = basicSetStatement(newStatement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.DO_WHILE_STATEMENT__STATEMENT, newStatement, newStatement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getKeyWhile()
  {
    return keyWhile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setKeyWhile(String newKeyWhile)
  {
    String oldKeyWhile = keyWhile;
    keyWhile = newKeyWhile;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.DO_WHILE_STATEMENT__KEY_WHILE, oldKeyWhile, keyWhile));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanExpression getExpression()
  {
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpression(BooleanExpression newExpression, NotificationChain msgs)
  {
    BooleanExpression oldExpression = expression;
    expression = newExpression;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.DO_WHILE_STATEMENT__EXPRESSION, oldExpression, newExpression);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpression(BooleanExpression newExpression)
  {
    if (newExpression != expression)
    {
      NotificationChain msgs = null;
      if (expression != null)
        msgs = ((InternalEObject)expression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.DO_WHILE_STATEMENT__EXPRESSION, null, msgs);
      if (newExpression != null)
        msgs = ((InternalEObject)newExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.DO_WHILE_STATEMENT__EXPRESSION, null, msgs);
      msgs = basicSetExpression(newExpression, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.DO_WHILE_STATEMENT__EXPRESSION, newExpression, newExpression));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.DO_WHILE_STATEMENT__STATEMENT:
        return basicSetStatement(null, msgs);
      case TTCN3Package.DO_WHILE_STATEMENT__EXPRESSION:
        return basicSetExpression(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.DO_WHILE_STATEMENT__KEY_DO:
        return getKeyDo();
      case TTCN3Package.DO_WHILE_STATEMENT__STATEMENT:
        return getStatement();
      case TTCN3Package.DO_WHILE_STATEMENT__KEY_WHILE:
        return getKeyWhile();
      case TTCN3Package.DO_WHILE_STATEMENT__EXPRESSION:
        return getExpression();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.DO_WHILE_STATEMENT__KEY_DO:
        setKeyDo((String)newValue);
        return;
      case TTCN3Package.DO_WHILE_STATEMENT__STATEMENT:
        setStatement((StatementBlock)newValue);
        return;
      case TTCN3Package.DO_WHILE_STATEMENT__KEY_WHILE:
        setKeyWhile((String)newValue);
        return;
      case TTCN3Package.DO_WHILE_STATEMENT__EXPRESSION:
        setExpression((BooleanExpression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.DO_WHILE_STATEMENT__KEY_DO:
        setKeyDo(KEY_DO_EDEFAULT);
        return;
      case TTCN3Package.DO_WHILE_STATEMENT__STATEMENT:
        setStatement((StatementBlock)null);
        return;
      case TTCN3Package.DO_WHILE_STATEMENT__KEY_WHILE:
        setKeyWhile(KEY_WHILE_EDEFAULT);
        return;
      case TTCN3Package.DO_WHILE_STATEMENT__EXPRESSION:
        setExpression((BooleanExpression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.DO_WHILE_STATEMENT__KEY_DO:
        return KEY_DO_EDEFAULT == null ? keyDo != null : !KEY_DO_EDEFAULT.equals(keyDo);
      case TTCN3Package.DO_WHILE_STATEMENT__STATEMENT:
        return statement != null;
      case TTCN3Package.DO_WHILE_STATEMENT__KEY_WHILE:
        return KEY_WHILE_EDEFAULT == null ? keyWhile != null : !KEY_WHILE_EDEFAULT.equals(keyWhile);
      case TTCN3Package.DO_WHILE_STATEMENT__EXPRESSION:
        return expression != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (keyDo: ");
    result.append(keyDo);
    result.append(", keyWhile: ");
    result.append(keyWhile);
    result.append(')');
    return result.toString();
  }

} //DoWhileStatementImpl
