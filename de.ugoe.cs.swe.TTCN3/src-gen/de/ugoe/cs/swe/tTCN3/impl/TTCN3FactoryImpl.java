/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TTCN3FactoryImpl extends EFactoryImpl implements TTCN3Factory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static TTCN3Factory init()
  {
    try
    {
      TTCN3Factory theTTCN3Factory = (TTCN3Factory)EPackage.Registry.INSTANCE.getEFactory(TTCN3Package.eNS_URI);
      if (theTTCN3Factory != null)
      {
        return theTTCN3Factory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new TTCN3FactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3FactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case TTCN3Package.TTCN3_FILE: return createTTCN3File();
      case TTCN3Package.CONST_DEF: return createConstDef();
      case TTCN3Package.TYPE: return createType();
      case TTCN3Package.TYPE_REFERENCE_TAIL_TYPE: return createTypeReferenceTailType();
      case TTCN3Package.TYPE_REFERENCE_TAIL: return createTypeReferenceTail();
      case TTCN3Package.TYPE_REFERENCE: return createTypeReference();
      case TTCN3Package.SPEC_TYPE_ELEMENT: return createSpecTypeElement();
      case TTCN3Package.ARRAY_DEF: return createArrayDef();
      case TTCN3Package.TTCN3_MODULE: return createTTCN3Module();
      case TTCN3Package.LANGUAGE_SPEC: return createLanguageSpec();
      case TTCN3Package.MODULE_PAR_DEF: return createModuleParDef();
      case TTCN3Package.MODULE_PAR: return createModulePar();
      case TTCN3Package.MODULE_PAR_LIST: return createModuleParList();
      case TTCN3Package.MODULE_PARAMETER: return createModuleParameter();
      case TTCN3Package.MULTITYPED_MODULE_PAR_LIST: return createMultitypedModuleParList();
      case TTCN3Package.MODULE_DEFINITIONS_LIST: return createModuleDefinitionsList();
      case TTCN3Package.MODULE_DEFINITION: return createModuleDefinition();
      case TTCN3Package.FRIEND_MODULE_DEF: return createFriendModuleDef();
      case TTCN3Package.GROUP_DEF: return createGroupDef();
      case TTCN3Package.EXT_FUNCTION_DEF: return createExtFunctionDef();
      case TTCN3Package.EXT_CONST_DEF: return createExtConstDef();
      case TTCN3Package.IDENTIFIER_OBJECT_LIST: return createIdentifierObjectList();
      case TTCN3Package.NAMED_OBJECT: return createNamedObject();
      case TTCN3Package.IMPORT_DEF: return createImportDef();
      case TTCN3Package.ALL_WITH_EXCEPTS: return createAllWithExcepts();
      case TTCN3Package.EXCEPTS_DEF: return createExceptsDef();
      case TTCN3Package.EXCEPT_SPEC: return createExceptSpec();
      case TTCN3Package.EXCEPT_ELEMENT: return createExceptElement();
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL: return createIdentifierListOrAll();
      case TTCN3Package.EXCEPT_GROUP_SPEC: return createExceptGroupSpec();
      case TTCN3Package.EXCEPT_TYPE_DEF_SPEC: return createExceptTypeDefSpec();
      case TTCN3Package.EXCEPT_TEMPLATE_SPEC: return createExceptTemplateSpec();
      case TTCN3Package.EXCEPT_CONST_SPEC: return createExceptConstSpec();
      case TTCN3Package.EXCEPT_TESTCASE_SPEC: return createExceptTestcaseSpec();
      case TTCN3Package.EXCEPT_ALTSTEP_SPEC: return createExceptAltstepSpec();
      case TTCN3Package.EXCEPT_FUNCTION_SPEC: return createExceptFunctionSpec();
      case TTCN3Package.EXCEPT_SIGNATURE_SPEC: return createExceptSignatureSpec();
      case TTCN3Package.EXCEPT_MODULE_PAR_SPEC: return createExceptModuleParSpec();
      case TTCN3Package.IMPORT_SPEC: return createImportSpec();
      case TTCN3Package.IMPORT_ELEMENT: return createImportElement();
      case TTCN3Package.IMPORT_GROUP_SPEC: return createImportGroupSpec();
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT: return createIdentifierListOrAllWithExcept();
      case TTCN3Package.ALL_WITH_EXCEPT: return createAllWithExcept();
      case TTCN3Package.IMPORT_TYPE_DEF_SPEC: return createImportTypeDefSpec();
      case TTCN3Package.IMPORT_TEMPLATE_SPEC: return createImportTemplateSpec();
      case TTCN3Package.IMPORT_CONST_SPEC: return createImportConstSpec();
      case TTCN3Package.IMPORT_ALTSTEP_SPEC: return createImportAltstepSpec();
      case TTCN3Package.IMPORT_TESTCASE_SPEC: return createImportTestcaseSpec();
      case TTCN3Package.IMPORT_FUNCTION_SPEC: return createImportFunctionSpec();
      case TTCN3Package.IMPORT_SIGNATURE_SPEC: return createImportSignatureSpec();
      case TTCN3Package.IMPORT_MODULE_PAR_SPEC: return createImportModuleParSpec();
      case TTCN3Package.GROUP_REF_LIST_WITH_EXCEPT: return createGroupRefListWithExcept();
      case TTCN3Package.QUALIFIED_IDENTIFIER_WITH_EXCEPT: return createQualifiedIdentifierWithExcept();
      case TTCN3Package.ALL_GROUPS_WITH_EXCEPT: return createAllGroupsWithExcept();
      case TTCN3Package.ALTSTEP_DEF: return createAltstepDef();
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST: return createAltstepLocalDefList();
      case TTCN3Package.ALTSTEP_LOCAL_DEF: return createAltstepLocalDef();
      case TTCN3Package.TESTCASE_DEF: return createTestcaseDef();
      case TTCN3Package.TEMPLATE_OR_VALUE_FORMAL_PAR_LIST: return createTemplateOrValueFormalParList();
      case TTCN3Package.TEMPLATE_OR_VALUE_FORMAL_PAR: return createTemplateOrValueFormalPar();
      case TTCN3Package.CONFIG_SPEC: return createConfigSpec();
      case TTCN3Package.SYSTEM_SPEC: return createSystemSpec();
      case TTCN3Package.SIGNATURE_DEF: return createSignatureDef();
      case TTCN3Package.EXCEPTION_SPEC: return createExceptionSpec();
      case TTCN3Package.SIGNATURE_FORMAL_PAR_LIST: return createSignatureFormalParList();
      case TTCN3Package.MODULE_CONTROL_PART: return createModuleControlPart();
      case TTCN3Package.MODULE_CONTROL_BODY: return createModuleControlBody();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF_LIST: return createControlStatementOrDefList();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF: return createControlStatementOrDef();
      case TTCN3Package.CONTROL_STATEMENT: return createControlStatement();
      case TTCN3Package.SUT_STATEMENTS: return createSUTStatements();
      case TTCN3Package.ACTION_TEXT: return createActionText();
      case TTCN3Package.BEHAVIOUR_STATEMENTS: return createBehaviourStatements();
      case TTCN3Package.ACTIVATE_OP: return createActivateOp();
      case TTCN3Package.DEACTIVATE_STATEMENT: return createDeactivateStatement();
      case TTCN3Package.LABEL_STATEMENT: return createLabelStatement();
      case TTCN3Package.GOTO_STATEMENT: return createGotoStatement();
      case TTCN3Package.RETURN_STATEMENT: return createReturnStatement();
      case TTCN3Package.INTERLEAVED_CONSTRUCT: return createInterleavedConstruct();
      case TTCN3Package.INTERLEAVED_GUARD_LIST: return createInterleavedGuardList();
      case TTCN3Package.INTERLEAVED_GUARD_ELEMENT: return createInterleavedGuardElement();
      case TTCN3Package.INTERLEAVED_GUARD: return createInterleavedGuard();
      case TTCN3Package.ALT_CONSTRUCT: return createAltConstruct();
      case TTCN3Package.ALT_GUARD_LIST: return createAltGuardList();
      case TTCN3Package.ELSE_STATEMENT: return createElseStatement();
      case TTCN3Package.GUARD_STATEMENT: return createGuardStatement();
      case TTCN3Package.GUARD_OP: return createGuardOp();
      case TTCN3Package.DONE_STATEMENT: return createDoneStatement();
      case TTCN3Package.KILLED_STATEMENT: return createKilledStatement();
      case TTCN3Package.COMPONENT_OR_ANY: return createComponentOrAny();
      case TTCN3Package.INDEX_ASSIGNMENT: return createIndexAssignment();
      case TTCN3Package.INDEX_SPEC: return createIndexSpec();
      case TTCN3Package.GET_REPLY_STATEMENT: return createGetReplyStatement();
      case TTCN3Package.CHECK_STATEMENT: return createCheckStatement();
      case TTCN3Package.PORT_CHECK_OP: return createPortCheckOp();
      case TTCN3Package.CHECK_PARAMETER: return createCheckParameter();
      case TTCN3Package.REDIRECT_PRESENT: return createRedirectPresent();
      case TTCN3Package.FROM_CLAUSE_PRESENT: return createFromClausePresent();
      case TTCN3Package.CHECK_PORT_OPS_PRESENT: return createCheckPortOpsPresent();
      case TTCN3Package.PORT_GET_REPLY_OP: return createPortGetReplyOp();
      case TTCN3Package.PORT_REDIRECT_WITH_VALUE_AND_PARAM: return createPortRedirectWithValueAndParam();
      case TTCN3Package.REDIRECT_WITH_VALUE_AND_PARAM_SPEC: return createRedirectWithValueAndParamSpec();
      case TTCN3Package.VALUE_MATCH_SPEC: return createValueMatchSpec();
      case TTCN3Package.CATCH_STATEMENT: return createCatchStatement();
      case TTCN3Package.PORT_CATCH_OP: return createPortCatchOp();
      case TTCN3Package.CATCH_OP_PARAMETER: return createCatchOpParameter();
      case TTCN3Package.GET_CALL_STATEMENT: return createGetCallStatement();
      case TTCN3Package.PORT_GET_CALL_OP: return createPortGetCallOp();
      case TTCN3Package.PORT_REDIRECT_WITH_PARAM: return createPortRedirectWithParam();
      case TTCN3Package.REDIRECT_WITH_PARAM_SPEC: return createRedirectWithParamSpec();
      case TTCN3Package.PARAM_SPEC: return createParamSpec();
      case TTCN3Package.PARAM_ASSIGNMENT_LIST: return createParamAssignmentList();
      case TTCN3Package.ASSIGNMENT_LIST: return createAssignmentList();
      case TTCN3Package.VARIABLE_ASSIGNMENT: return createVariableAssignment();
      case TTCN3Package.VARIABLE_LIST: return createVariableList();
      case TTCN3Package.VARIABLE_ENTRY: return createVariableEntry();
      case TTCN3Package.TRIGGER_STATEMENT: return createTriggerStatement();
      case TTCN3Package.PORT_TRIGGER_OP: return createPortTriggerOp();
      case TTCN3Package.RECEIVE_STATEMENT: return createReceiveStatement();
      case TTCN3Package.PORT_OR_ANY: return createPortOrAny();
      case TTCN3Package.PORT_RECEIVE_OP: return createPortReceiveOp();
      case TTCN3Package.FROM_CLAUSE: return createFromClause();
      case TTCN3Package.ADDRESS_REF_LIST: return createAddressRefList();
      case TTCN3Package.PORT_REDIRECT: return createPortRedirect();
      case TTCN3Package.SENDER_SPEC: return createSenderSpec();
      case TTCN3Package.VALUE_SPEC: return createValueSpec();
      case TTCN3Package.SINGLE_VALUE_SPEC: return createSingleValueSpec();
      case TTCN3Package.ALT_GUARD_CHAR: return createAltGuardChar();
      case TTCN3Package.ALTSTEP_INSTANCE: return createAltstepInstance();
      case TTCN3Package.FUNCTION_INSTANCE: return createFunctionInstance();
      case TTCN3Package.FUNCTION_REF: return createFunctionRef();
      case TTCN3Package.FUNCTION_ACTUAL_PAR_LIST: return createFunctionActualParList();
      case TTCN3Package.FUNCTION_ACTUAL_PAR_ASSIGNMENT: return createFunctionActualParAssignment();
      case TTCN3Package.COMPONENT_REF_ASSIGNMENT: return createComponentRefAssignment();
      case TTCN3Package.FORMAL_PORT_AND_VALUE_PAR: return createFormalPortAndValuePar();
      case TTCN3Package.PORT_REF_ASSIGNMENT: return createPortRefAssignment();
      case TTCN3Package.TIMER_REF_ASSIGNMENT: return createTimerRefAssignment();
      case TTCN3Package.FUNCTION_ACTUAL_PAR: return createFunctionActualPar();
      case TTCN3Package.COMPONENT_REF: return createComponentRef();
      case TTCN3Package.COMPONENT_OR_DEFAULT_REFERENCE: return createComponentOrDefaultReference();
      case TTCN3Package.TESTCASE_INSTANCE: return createTestcaseInstance();
      case TTCN3Package.TESTCASE_ACTUAL_PAR_LIST: return createTestcaseActualParList();
      case TTCN3Package.TIMER_STATEMENTS: return createTimerStatements();
      case TTCN3Package.TIMEOUT_STATEMENT: return createTimeoutStatement();
      case TTCN3Package.START_TIMER_STATEMENT: return createStartTimerStatement();
      case TTCN3Package.STOP_TIMER_STATEMENT: return createStopTimerStatement();
      case TTCN3Package.TIMER_REF_OR_ANY: return createTimerRefOrAny();
      case TTCN3Package.TIMER_REF_OR_ALL: return createTimerRefOrAll();
      case TTCN3Package.BASIC_STATEMENTS: return createBasicStatements();
      case TTCN3Package.STATEMENT_BLOCK: return createStatementBlock();
      case TTCN3Package.FUNCTION_STATEMENT_LIST: return createFunctionStatementList();
      case TTCN3Package.FUNCTION_STATEMENT: return createFunctionStatement();
      case TTCN3Package.TESTCASE_OPERATION: return createTestcaseOperation();
      case TTCN3Package.SET_LOCAL_VERDICT: return createSetLocalVerdict();
      case TTCN3Package.CONFIGURATION_STATEMENTS: return createConfigurationStatements();
      case TTCN3Package.KILL_TC_STATEMENT: return createKillTCStatement();
      case TTCN3Package.STOP_TC_STATEMENT: return createStopTCStatement();
      case TTCN3Package.COMPONENT_REFERENCE_OR_LITERAL: return createComponentReferenceOrLiteral();
      case TTCN3Package.START_TC_STATEMENT: return createStartTCStatement();
      case TTCN3Package.UNMAP_STATEMENT: return createUnmapStatement();
      case TTCN3Package.DISCONNECT_STATEMENT: return createDisconnectStatement();
      case TTCN3Package.ALL_CONNECTIONS_SPEC: return createAllConnectionsSpec();
      case TTCN3Package.ALL_PORTS_SPEC: return createAllPortsSpec();
      case TTCN3Package.MAP_STATEMENT: return createMapStatement();
      case TTCN3Package.PARAM_CLAUSE: return createParamClause();
      case TTCN3Package.CONNECT_STATEMENT: return createConnectStatement();
      case TTCN3Package.SINGLE_CONNECTION_SPEC: return createSingleConnectionSpec();
      case TTCN3Package.PORT_REF: return createPortRef();
      case TTCN3Package.COMMUNICATION_STATEMENTS: return createCommunicationStatements();
      case TTCN3Package.CHECK_STATE_STATEMENT: return createCheckStateStatement();
      case TTCN3Package.PORT_OR_ALL_ANY: return createPortOrAllAny();
      case TTCN3Package.HALT_STATEMENT: return createHaltStatement();
      case TTCN3Package.START_STATEMENT: return createStartStatement();
      case TTCN3Package.STOP_STATEMENT: return createStopStatement();
      case TTCN3Package.CLEAR_STATEMENT: return createClearStatement();
      case TTCN3Package.PORT_OR_ALL: return createPortOrAll();
      case TTCN3Package.RAISE_STATEMENT: return createRaiseStatement();
      case TTCN3Package.PORT_RAISE_OP: return createPortRaiseOp();
      case TTCN3Package.REPLY_STATEMENT: return createReplyStatement();
      case TTCN3Package.PORT_REPLY_OP: return createPortReplyOp();
      case TTCN3Package.REPLY_VALUE: return createReplyValue();
      case TTCN3Package.CALL_STATEMENT: return createCallStatement();
      case TTCN3Package.PORT_CALL_OP: return createPortCallOp();
      case TTCN3Package.CALL_PARAMETERS: return createCallParameters();
      case TTCN3Package.CALL_TIMER_VALUE: return createCallTimerValue();
      case TTCN3Package.PORT_CALL_BODY: return createPortCallBody();
      case TTCN3Package.CALL_BODY_STATEMENT_LIST: return createCallBodyStatementList();
      case TTCN3Package.CALL_BODY_STATEMENT: return createCallBodyStatement();
      case TTCN3Package.CALL_BODY_GUARD: return createCallBodyGuard();
      case TTCN3Package.CALL_BODY_OPS: return createCallBodyOps();
      case TTCN3Package.SEND_STATEMENT: return createSendStatement();
      case TTCN3Package.PORT_SEND_OP: return createPortSendOp();
      case TTCN3Package.TO_CLAUSE: return createToClause();
      case TTCN3Package.FUNCTION_DEF_LIST: return createFunctionDefList();
      case TTCN3Package.FUNCTION_LOCAL_DEF: return createFunctionLocalDef();
      case TTCN3Package.FUNCTION_LOCAL_INST: return createFunctionLocalInst();
      case TTCN3Package.TIMER_INSTANCE: return createTimerInstance();
      case TTCN3Package.VAR_INSTANCE: return createVarInstance();
      case TTCN3Package.VAR_LIST: return createVarList();
      case TTCN3Package.MODULE_OR_GROUP: return createModuleOrGroup();
      case TTCN3Package.REFERENCED_TYPE: return createReferencedType();
      case TTCN3Package.TYPE_DEF: return createTypeDef();
      case TTCN3Package.TYPE_DEF_BODY: return createTypeDefBody();
      case TTCN3Package.SUB_TYPE_DEF: return createSubTypeDef();
      case TTCN3Package.SUB_TYPE_DEF_NAMED: return createSubTypeDefNamed();
      case TTCN3Package.STRUCTURED_TYPE_DEF: return createStructuredTypeDef();
      case TTCN3Package.RECORD_DEF: return createRecordDef();
      case TTCN3Package.RECORD_DEF_NAMED: return createRecordDefNamed();
      case TTCN3Package.RECORD_OF_DEF: return createRecordOfDef();
      case TTCN3Package.RECORD_OF_DEF_NAMED: return createRecordOfDefNamed();
      case TTCN3Package.STRUCT_DEF_BODY: return createStructDefBody();
      case TTCN3Package.STRUCT_FIELD_DEF: return createStructFieldDef();
      case TTCN3Package.SET_OF_DEF: return createSetOfDef();
      case TTCN3Package.SET_OF_DEF_NAMED: return createSetOfDefNamed();
      case TTCN3Package.PORT_DEF: return createPortDef();
      case TTCN3Package.PORT_DEF_BODY: return createPortDefBody();
      case TTCN3Package.PORT_DEF_ATTRIBS: return createPortDefAttribs();
      case TTCN3Package.MIXED_ATTRIBS: return createMixedAttribs();
      case TTCN3Package.MIXED_LIST: return createMixedList();
      case TTCN3Package.PROC_OR_TYPE_LIST: return createProcOrTypeList();
      case TTCN3Package.PROC_OR_TYPE: return createProcOrType();
      case TTCN3Package.MESSAGE_ATTRIBS: return createMessageAttribs();
      case TTCN3Package.CONFIG_PARAM_DEF: return createConfigParamDef();
      case TTCN3Package.MAP_PARAM_DEF: return createMapParamDef();
      case TTCN3Package.UNMAP_PARAM_DEF: return createUnmapParamDef();
      case TTCN3Package.ADDRESS_DECL: return createAddressDecl();
      case TTCN3Package.PROCEDURE_ATTRIBS: return createProcedureAttribs();
      case TTCN3Package.COMPONENT_DEF: return createComponentDef();
      case TTCN3Package.COMPONENT_DEF_LIST: return createComponentDefList();
      case TTCN3Package.PORT_INSTANCE: return createPortInstance();
      case TTCN3Package.PORT_ELEMENT: return createPortElement();
      case TTCN3Package.COMPONENT_ELEMENT_DEF: return createComponentElementDef();
      case TTCN3Package.PROCEDURE_LIST: return createProcedureList();
      case TTCN3Package.ALL_OR_SIGNATURE_LIST: return createAllOrSignatureList();
      case TTCN3Package.SIGNATURE_LIST: return createSignatureList();
      case TTCN3Package.ENUM_DEF: return createEnumDef();
      case TTCN3Package.ENUM_DEF_NAMED: return createEnumDefNamed();
      case TTCN3Package.NESTED_TYPE_DEF: return createNestedTypeDef();
      case TTCN3Package.NESTED_RECORD_DEF: return createNestedRecordDef();
      case TTCN3Package.NESTED_UNION_DEF: return createNestedUnionDef();
      case TTCN3Package.NESTED_SET_DEF: return createNestedSetDef();
      case TTCN3Package.NESTED_RECORD_OF_DEF: return createNestedRecordOfDef();
      case TTCN3Package.NESTED_SET_OF_DEF: return createNestedSetOfDef();
      case TTCN3Package.NESTED_ENUM_DEF: return createNestedEnumDef();
      case TTCN3Package.MESSAGE_LIST: return createMessageList();
      case TTCN3Package.ALL_OR_TYPE_LIST: return createAllOrTypeList();
      case TTCN3Package.TYPE_LIST: return createTypeList();
      case TTCN3Package.UNION_DEF: return createUnionDef();
      case TTCN3Package.UNION_DEF_NAMED: return createUnionDefNamed();
      case TTCN3Package.UNION_DEF_BODY: return createUnionDefBody();
      case TTCN3Package.ENUMERATION_LIST: return createEnumerationList();
      case TTCN3Package.ENUMERATION: return createEnumeration();
      case TTCN3Package.UNION_FIELD_DEF: return createUnionFieldDef();
      case TTCN3Package.SET_DEF: return createSetDef();
      case TTCN3Package.SET_DEF_NAMED: return createSetDefNamed();
      case TTCN3Package.SUB_TYPE_SPEC: return createSubTypeSpec();
      case TTCN3Package.ALLOWED_VALUES_SPEC: return createAllowedValuesSpec();
      case TTCN3Package.TEMPLATE_OR_RANGE: return createTemplateOrRange();
      case TTCN3Package.RANGE_DEF: return createRangeDef();
      case TTCN3Package.BOUND: return createBound();
      case TTCN3Package.STRING_LENGTH: return createStringLength();
      case TTCN3Package.CHAR_STRING_MATCH: return createCharStringMatch();
      case TTCN3Package.PATTERN_PARTICLE: return createPatternParticle();
      case TTCN3Package.TEMPLATE_DEF: return createTemplateDef();
      case TTCN3Package.DERIVED_DEF: return createDerivedDef();
      case TTCN3Package.BASE_TEMPLATE: return createBaseTemplate();
      case TTCN3Package.TEMP_VAR_LIST: return createTempVarList();
      case TTCN3Package.TIMER_VAR_INSTANCE: return createTimerVarInstance();
      case TTCN3Package.SINGLE_VAR_INSTANCE: return createSingleVarInstance();
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE: return createSingleTempVarInstance();
      case TTCN3Package.IN_LINE_TEMPLATE: return createInLineTemplate();
      case TTCN3Package.DERIVED_REF_WITH_PAR_LIST: return createDerivedRefWithParList();
      case TTCN3Package.TEMPLATE_BODY: return createTemplateBody();
      case TTCN3Package.EXTRA_MATCHING_ATTRIBUTES: return createExtraMatchingAttributes();
      case TTCN3Package.FIELD_SPEC_LIST: return createFieldSpecList();
      case TTCN3Package.FIELD_SPEC: return createFieldSpec();
      case TTCN3Package.SIMPLE_SPEC: return createSimpleSpec();
      case TTCN3Package.SIMPLE_TEMPLATE_SPEC: return createSimpleTemplateSpec();
      case TTCN3Package.SINGLE_TEMPLATE_EXPRESSION: return createSingleTemplateExpression();
      case TTCN3Package.TEMPLATE_REF_WITH_PAR_LIST: return createTemplateRefWithParList();
      case TTCN3Package.TEMPLATE_ACTUAL_PAR_LIST: return createTemplateActualParList();
      case TTCN3Package.MATCHING_SYMBOL: return createMatchingSymbol();
      case TTCN3Package.SUBSET_MATCH: return createSubsetMatch();
      case TTCN3Package.SUPERSET_MATCH: return createSupersetMatch();
      case TTCN3Package.RANGE: return createRange();
      case TTCN3Package.WILDCARD_LENGTH_MATCH: return createWildcardLengthMatch();
      case TTCN3Package.COMPLEMENT: return createComplement();
      case TTCN3Package.LIST_OF_TEMPLATES: return createListOfTemplates();
      case TTCN3Package.TEMPLATE_OPS: return createTemplateOps();
      case TTCN3Package.MATCH_OP: return createMatchOp();
      case TTCN3Package.VALUEOF_OP: return createValueofOp();
      case TTCN3Package.CONFIGURATION_OPS: return createConfigurationOps();
      case TTCN3Package.CREATE_OP: return createCreateOp();
      case TTCN3Package.RUNNING_OP: return createRunningOp();
      case TTCN3Package.OP_CALL: return createOpCall();
      case TTCN3Package.ALIVE_OP: return createAliveOp();
      case TTCN3Package.TEMPLATE_LIST_ITEM: return createTemplateListItem();
      case TTCN3Package.ALL_ELEMENTS_FROM: return createAllElementsFrom();
      case TTCN3Package.SIGNATURE: return createSignature();
      case TTCN3Package.TEMPLATE_INSTANCE_ASSIGNMENT: return createTemplateInstanceAssignment();
      case TTCN3Package.TEMPLATE_INSTANCE_ACTUAL_PAR: return createTemplateInstanceActualPar();
      case TTCN3Package.TEMPLATE_RESTRICTION: return createTemplateRestriction();
      case TTCN3Package.RESTRICTED_TEMPLATE: return createRestrictedTemplate();
      case TTCN3Package.LOG_STATEMENT: return createLogStatement();
      case TTCN3Package.LOG_ITEM: return createLogItem();
      case TTCN3Package.EXPRESSION: return createExpression();
      case TTCN3Package.WITH_STATEMENT: return createWithStatement();
      case TTCN3Package.WITH_ATTRIB_LIST: return createWithAttribList();
      case TTCN3Package.MULTI_WITH_ATTRIB: return createMultiWithAttrib();
      case TTCN3Package.SINGLE_WITH_ATTRIB: return createSingleWithAttrib();
      case TTCN3Package.ATTRIB_QUALIFIER: return createAttribQualifier();
      case TTCN3Package.IDENTIFIER_LIST: return createIdentifierList();
      case TTCN3Package.QUALIFIED_IDENTIFIER_LIST: return createQualifiedIdentifierList();
      case TTCN3Package.SINGLE_CONST_DEF: return createSingleConstDef();
      case TTCN3Package.COMPOUND_EXPRESSION: return createCompoundExpression();
      case TTCN3Package.ARRAY_EXPRESSION: return createArrayExpression();
      case TTCN3Package.FIELD_EXPRESSION_LIST: return createFieldExpressionList();
      case TTCN3Package.ARRAY_ELEMENT_EXPRESSION_LIST: return createArrayElementExpressionList();
      case TTCN3Package.FIELD_EXPRESSION_SPEC: return createFieldExpressionSpec();
      case TTCN3Package.NOT_USED_OR_EXPRESSION: return createNotUsedOrExpression();
      case TTCN3Package.CONSTANT_EXPRESSION: return createConstantExpression();
      case TTCN3Package.COMPOUND_CONST_EXPRESSION: return createCompoundConstExpression();
      case TTCN3Package.FIELD_CONST_EXPRESSION_LIST: return createFieldConstExpressionList();
      case TTCN3Package.FIELD_CONST_EXPRESSION_SPEC: return createFieldConstExpressionSpec();
      case TTCN3Package.ARRAY_CONST_EXPRESSION: return createArrayConstExpression();
      case TTCN3Package.ARRAY_ELEMENT_CONST_EXPRESSION_LIST: return createArrayElementConstExpressionList();
      case TTCN3Package.CONST_LIST: return createConstList();
      case TTCN3Package.VALUE: return createValue();
      case TTCN3Package.REFERENCED_VALUE: return createReferencedValue();
      case TTCN3Package.REF_VALUE_HEAD: return createRefValueHead();
      case TTCN3Package.REF_VALUE_ELEMENT: return createRefValueElement();
      case TTCN3Package.HEAD: return createHead();
      case TTCN3Package.REF_VALUE_TAIL: return createRefValueTail();
      case TTCN3Package.SPEC_ELEMENT: return createSpecElement();
      case TTCN3Package.EXTENDED_FIELD_REFERENCE: return createExtendedFieldReference();
      case TTCN3Package.REF_VALUE: return createRefValue();
      case TTCN3Package.PREDEFINED_VALUE: return createPredefinedValue();
      case TTCN3Package.BOOLEAN_EXPRESSION: return createBooleanExpression();
      case TTCN3Package.SINGLE_EXPRESSION: return createSingleExpression();
      case TTCN3Package.DEF_OR_FIELD_REF_LIST: return createDefOrFieldRefList();
      case TTCN3Package.DEF_OR_FIELD_REF: return createDefOrFieldRef();
      case TTCN3Package.ALL_REF: return createAllRef();
      case TTCN3Package.GROUP_REF_LIST: return createGroupRefList();
      case TTCN3Package.TTCN3_REFERENCE: return createTTCN3Reference();
      case TTCN3Package.TTCN3_REFERENCE_LIST: return createTTCN3ReferenceList();
      case TTCN3Package.FIELD_REFERENCE: return createFieldReference();
      case TTCN3Package.ARRAY_OR_BIT_REF: return createArrayOrBitRef();
      case TTCN3Package.FIELD_OR_BIT_NUMBER: return createFieldOrBitNumber();
      case TTCN3Package.ARRAY_VALUE_OR_ATTRIB: return createArrayValueOrAttrib();
      case TTCN3Package.ARRAY_ELEMENT_SPEC_LIST: return createArrayElementSpecList();
      case TTCN3Package.ARRAY_ELEMENT_SPEC: return createArrayElementSpec();
      case TTCN3Package.TIMER_OPS: return createTimerOps();
      case TTCN3Package.RUNNING_TIMER_OP: return createRunningTimerOp();
      case TTCN3Package.READ_TIMER_OP: return createReadTimerOp();
      case TTCN3Package.PERMUTATION_MATCH: return createPermutationMatch();
      case TTCN3Package.LOOP_CONSTRUCT: return createLoopConstruct();
      case TTCN3Package.FOR_STATEMENT: return createForStatement();
      case TTCN3Package.WHILE_STATEMENT: return createWhileStatement();
      case TTCN3Package.DO_WHILE_STATEMENT: return createDoWhileStatement();
      case TTCN3Package.CONDITIONAL_CONSTRUCT: return createConditionalConstruct();
      case TTCN3Package.ELSE_IF_CLAUSE: return createElseIfClause();
      case TTCN3Package.ELSE_CLAUSE: return createElseClause();
      case TTCN3Package.INITIAL: return createInitial();
      case TTCN3Package.SELECT_CASE_CONSTRUCT: return createSelectCaseConstruct();
      case TTCN3Package.SELECT_CASE_BODY: return createSelectCaseBody();
      case TTCN3Package.SELECT_CASE: return createSelectCase();
      case TTCN3Package.FUNCTION_DEF: return createFunctionDef();
      case TTCN3Package.MTC_SPEC: return createMtcSpec();
      case TTCN3Package.FUNCTION_FORMAL_PAR_LIST: return createFunctionFormalParList();
      case TTCN3Package.FUNCTION_FORMAL_PAR: return createFunctionFormalPar();
      case TTCN3Package.FORMAL_VALUE_PAR: return createFormalValuePar();
      case TTCN3Package.FORMAL_TIMER_PAR: return createFormalTimerPar();
      case TTCN3Package.FORMAL_PORT_PAR: return createFormalPortPar();
      case TTCN3Package.FORMAL_TEMPLATE_PAR: return createFormalTemplatePar();
      case TTCN3Package.RUNS_ON_SPEC: return createRunsOnSpec();
      case TTCN3Package.RETURN_TYPE: return createReturnType();
      case TTCN3Package.ASSIGNMENT: return createAssignment();
      case TTCN3Package.VARIABLE_REF: return createVariableRef();
      case TTCN3Package.PRE_DEF_FUNCTION: return createPreDefFunction();
      case TTCN3Package.FINT2CHAR: return createFint2char();
      case TTCN3Package.FINT2UNICHAR: return createFint2unichar();
      case TTCN3Package.FINT2BIT: return createFint2bit();
      case TTCN3Package.FINT2ENUM: return createFint2enum();
      case TTCN3Package.FINT2HEX: return createFint2hex();
      case TTCN3Package.FINT2OCT: return createFint2oct();
      case TTCN3Package.FINT2STR: return createFint2str();
      case TTCN3Package.FINT2FLOAT: return createFint2float();
      case TTCN3Package.FFLOAT2INT: return createFfloat2int();
      case TTCN3Package.FCHAR2INT: return createFchar2int();
      case TTCN3Package.FCHAR2OCT: return createFchar2oct();
      case TTCN3Package.FUNICHAR2INT: return createFunichar2int();
      case TTCN3Package.FUNICHAR2OCT: return createFunichar2oct();
      case TTCN3Package.FBIT2INT: return createFbit2int();
      case TTCN3Package.FBIT2HEX: return createFbit2hex();
      case TTCN3Package.FBIT2OCT: return createFbit2oct();
      case TTCN3Package.FBIT2STR: return createFbit2str();
      case TTCN3Package.FHEX2INT: return createFhex2int();
      case TTCN3Package.FHEX2BIT: return createFhex2bit();
      case TTCN3Package.FHEX2OCT: return createFhex2oct();
      case TTCN3Package.FHEX2STR: return createFhex2str();
      case TTCN3Package.FOCT2INT: return createFoct2int();
      case TTCN3Package.FOCT2BIT: return createFoct2bit();
      case TTCN3Package.FOCT2HEX: return createFoct2hex();
      case TTCN3Package.FOCT2STR: return createFoct2str();
      case TTCN3Package.FOCT2CHAR: return createFoct2char();
      case TTCN3Package.FOCT2UNICHAR: return createFoct2unichar();
      case TTCN3Package.FSTR2INT: return createFstr2int();
      case TTCN3Package.FSTR2HEX: return createFstr2hex();
      case TTCN3Package.FSTR2OCT: return createFstr2oct();
      case TTCN3Package.FSTR2FLOAT: return createFstr2float();
      case TTCN3Package.FENUM2INT: return createFenum2int();
      case TTCN3Package.FLENGTHOF: return createFlengthof();
      case TTCN3Package.FSIZEOF: return createFsizeof();
      case TTCN3Package.FISPRESENT: return createFispresent();
      case TTCN3Package.FISCHOSEN: return createFischosen();
      case TTCN3Package.FISVALUE: return createFisvalue();
      case TTCN3Package.FISBOUND: return createFisbound();
      case TTCN3Package.FREGEXP: return createFregexp();
      case TTCN3Package.FSUBSTR: return createFsubstr();
      case TTCN3Package.FREPLACE: return createFreplace();
      case TTCN3Package.FENCVALUE: return createFencvalue();
      case TTCN3Package.FDECVALUE: return createFdecvalue();
      case TTCN3Package.FENCVALUE_UNICHAR: return createFencvalueUnichar();
      case TTCN3Package.FDECVALUE_UNICHAR: return createFdecvalueUnichar();
      case TTCN3Package.FRND: return createFrnd();
      case TTCN3Package.FTESTCASENAME: return createFtestcasename();
      case TTCN3Package.XOR_EXPRESSION: return createXorExpression();
      case TTCN3Package.AND_EXPRESSION: return createAndExpression();
      case TTCN3Package.EQUAL_EXPRESSION: return createEqualExpression();
      case TTCN3Package.REL_EXPRESSION: return createRelExpression();
      case TTCN3Package.SHIFT_EXPRESSION: return createShiftExpression();
      case TTCN3Package.BIT_OR_EXPRESSION: return createBitOrExpression();
      case TTCN3Package.BIT_XOR_EXPRESSION: return createBitXorExpression();
      case TTCN3Package.BIT_AND_EXPRESSION: return createBitAndExpression();
      case TTCN3Package.ADD_EXPRESSION: return createAddExpression();
      case TTCN3Package.MUL_EXPRESSION: return createMulExpression();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case TTCN3Package.VISIBILITY:
        return createVisibilityFromString(eDataType, initialValue);
      case TTCN3Package.VERDICT_TYPE_VALUE:
        return createVerdictTypeValueFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case TTCN3Package.VISIBILITY:
        return convertVisibilityToString(eDataType, instanceValue);
      case TTCN3Package.VERDICT_TYPE_VALUE:
        return convertVerdictTypeValueToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3File createTTCN3File()
  {
    TTCN3FileImpl ttcn3File = new TTCN3FileImpl();
    return ttcn3File;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstDef createConstDef()
  {
    ConstDefImpl constDef = new ConstDefImpl();
    return constDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type createType()
  {
    TypeImpl type = new TypeImpl();
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeReferenceTailType createTypeReferenceTailType()
  {
    TypeReferenceTailTypeImpl typeReferenceTailType = new TypeReferenceTailTypeImpl();
    return typeReferenceTailType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeReferenceTail createTypeReferenceTail()
  {
    TypeReferenceTailImpl typeReferenceTail = new TypeReferenceTailImpl();
    return typeReferenceTail;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeReference createTypeReference()
  {
    TypeReferenceImpl typeReference = new TypeReferenceImpl();
    return typeReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SpecTypeElement createSpecTypeElement()
  {
    SpecTypeElementImpl specTypeElement = new SpecTypeElementImpl();
    return specTypeElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayDef createArrayDef()
  {
    ArrayDefImpl arrayDef = new ArrayDefImpl();
    return arrayDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3Module createTTCN3Module()
  {
    TTCN3ModuleImpl ttcn3Module = new TTCN3ModuleImpl();
    return ttcn3Module;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LanguageSpec createLanguageSpec()
  {
    LanguageSpecImpl languageSpec = new LanguageSpecImpl();
    return languageSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleParDef createModuleParDef()
  {
    ModuleParDefImpl moduleParDef = new ModuleParDefImpl();
    return moduleParDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModulePar createModulePar()
  {
    ModuleParImpl modulePar = new ModuleParImpl();
    return modulePar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleParList createModuleParList()
  {
    ModuleParListImpl moduleParList = new ModuleParListImpl();
    return moduleParList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleParameter createModuleParameter()
  {
    ModuleParameterImpl moduleParameter = new ModuleParameterImpl();
    return moduleParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MultitypedModuleParList createMultitypedModuleParList()
  {
    MultitypedModuleParListImpl multitypedModuleParList = new MultitypedModuleParListImpl();
    return multitypedModuleParList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleDefinitionsList createModuleDefinitionsList()
  {
    ModuleDefinitionsListImpl moduleDefinitionsList = new ModuleDefinitionsListImpl();
    return moduleDefinitionsList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleDefinition createModuleDefinition()
  {
    ModuleDefinitionImpl moduleDefinition = new ModuleDefinitionImpl();
    return moduleDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FriendModuleDef createFriendModuleDef()
  {
    FriendModuleDefImpl friendModuleDef = new FriendModuleDefImpl();
    return friendModuleDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GroupDef createGroupDef()
  {
    GroupDefImpl groupDef = new GroupDefImpl();
    return groupDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtFunctionDef createExtFunctionDef()
  {
    ExtFunctionDefImpl extFunctionDef = new ExtFunctionDefImpl();
    return extFunctionDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtConstDef createExtConstDef()
  {
    ExtConstDefImpl extConstDef = new ExtConstDefImpl();
    return extConstDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifierObjectList createIdentifierObjectList()
  {
    IdentifierObjectListImpl identifierObjectList = new IdentifierObjectListImpl();
    return identifierObjectList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NamedObject createNamedObject()
  {
    NamedObjectImpl namedObject = new NamedObjectImpl();
    return namedObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportDef createImportDef()
  {
    ImportDefImpl importDef = new ImportDefImpl();
    return importDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllWithExcepts createAllWithExcepts()
  {
    AllWithExceptsImpl allWithExcepts = new AllWithExceptsImpl();
    return allWithExcepts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptsDef createExceptsDef()
  {
    ExceptsDefImpl exceptsDef = new ExceptsDefImpl();
    return exceptsDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptSpec createExceptSpec()
  {
    ExceptSpecImpl exceptSpec = new ExceptSpecImpl();
    return exceptSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptElement createExceptElement()
  {
    ExceptElementImpl exceptElement = new ExceptElementImpl();
    return exceptElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifierListOrAll createIdentifierListOrAll()
  {
    IdentifierListOrAllImpl identifierListOrAll = new IdentifierListOrAllImpl();
    return identifierListOrAll;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptGroupSpec createExceptGroupSpec()
  {
    ExceptGroupSpecImpl exceptGroupSpec = new ExceptGroupSpecImpl();
    return exceptGroupSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptTypeDefSpec createExceptTypeDefSpec()
  {
    ExceptTypeDefSpecImpl exceptTypeDefSpec = new ExceptTypeDefSpecImpl();
    return exceptTypeDefSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptTemplateSpec createExceptTemplateSpec()
  {
    ExceptTemplateSpecImpl exceptTemplateSpec = new ExceptTemplateSpecImpl();
    return exceptTemplateSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptConstSpec createExceptConstSpec()
  {
    ExceptConstSpecImpl exceptConstSpec = new ExceptConstSpecImpl();
    return exceptConstSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptTestcaseSpec createExceptTestcaseSpec()
  {
    ExceptTestcaseSpecImpl exceptTestcaseSpec = new ExceptTestcaseSpecImpl();
    return exceptTestcaseSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptAltstepSpec createExceptAltstepSpec()
  {
    ExceptAltstepSpecImpl exceptAltstepSpec = new ExceptAltstepSpecImpl();
    return exceptAltstepSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptFunctionSpec createExceptFunctionSpec()
  {
    ExceptFunctionSpecImpl exceptFunctionSpec = new ExceptFunctionSpecImpl();
    return exceptFunctionSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptSignatureSpec createExceptSignatureSpec()
  {
    ExceptSignatureSpecImpl exceptSignatureSpec = new ExceptSignatureSpecImpl();
    return exceptSignatureSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptModuleParSpec createExceptModuleParSpec()
  {
    ExceptModuleParSpecImpl exceptModuleParSpec = new ExceptModuleParSpecImpl();
    return exceptModuleParSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportSpec createImportSpec()
  {
    ImportSpecImpl importSpec = new ImportSpecImpl();
    return importSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportElement createImportElement()
  {
    ImportElementImpl importElement = new ImportElementImpl();
    return importElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportGroupSpec createImportGroupSpec()
  {
    ImportGroupSpecImpl importGroupSpec = new ImportGroupSpecImpl();
    return importGroupSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifierListOrAllWithExcept createIdentifierListOrAllWithExcept()
  {
    IdentifierListOrAllWithExceptImpl identifierListOrAllWithExcept = new IdentifierListOrAllWithExceptImpl();
    return identifierListOrAllWithExcept;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllWithExcept createAllWithExcept()
  {
    AllWithExceptImpl allWithExcept = new AllWithExceptImpl();
    return allWithExcept;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportTypeDefSpec createImportTypeDefSpec()
  {
    ImportTypeDefSpecImpl importTypeDefSpec = new ImportTypeDefSpecImpl();
    return importTypeDefSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportTemplateSpec createImportTemplateSpec()
  {
    ImportTemplateSpecImpl importTemplateSpec = new ImportTemplateSpecImpl();
    return importTemplateSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportConstSpec createImportConstSpec()
  {
    ImportConstSpecImpl importConstSpec = new ImportConstSpecImpl();
    return importConstSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportAltstepSpec createImportAltstepSpec()
  {
    ImportAltstepSpecImpl importAltstepSpec = new ImportAltstepSpecImpl();
    return importAltstepSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportTestcaseSpec createImportTestcaseSpec()
  {
    ImportTestcaseSpecImpl importTestcaseSpec = new ImportTestcaseSpecImpl();
    return importTestcaseSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportFunctionSpec createImportFunctionSpec()
  {
    ImportFunctionSpecImpl importFunctionSpec = new ImportFunctionSpecImpl();
    return importFunctionSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportSignatureSpec createImportSignatureSpec()
  {
    ImportSignatureSpecImpl importSignatureSpec = new ImportSignatureSpecImpl();
    return importSignatureSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportModuleParSpec createImportModuleParSpec()
  {
    ImportModuleParSpecImpl importModuleParSpec = new ImportModuleParSpecImpl();
    return importModuleParSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GroupRefListWithExcept createGroupRefListWithExcept()
  {
    GroupRefListWithExceptImpl groupRefListWithExcept = new GroupRefListWithExceptImpl();
    return groupRefListWithExcept;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QualifiedIdentifierWithExcept createQualifiedIdentifierWithExcept()
  {
    QualifiedIdentifierWithExceptImpl qualifiedIdentifierWithExcept = new QualifiedIdentifierWithExceptImpl();
    return qualifiedIdentifierWithExcept;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllGroupsWithExcept createAllGroupsWithExcept()
  {
    AllGroupsWithExceptImpl allGroupsWithExcept = new AllGroupsWithExceptImpl();
    return allGroupsWithExcept;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltstepDef createAltstepDef()
  {
    AltstepDefImpl altstepDef = new AltstepDefImpl();
    return altstepDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltstepLocalDefList createAltstepLocalDefList()
  {
    AltstepLocalDefListImpl altstepLocalDefList = new AltstepLocalDefListImpl();
    return altstepLocalDefList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltstepLocalDef createAltstepLocalDef()
  {
    AltstepLocalDefImpl altstepLocalDef = new AltstepLocalDefImpl();
    return altstepLocalDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TestcaseDef createTestcaseDef()
  {
    TestcaseDefImpl testcaseDef = new TestcaseDefImpl();
    return testcaseDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateOrValueFormalParList createTemplateOrValueFormalParList()
  {
    TemplateOrValueFormalParListImpl templateOrValueFormalParList = new TemplateOrValueFormalParListImpl();
    return templateOrValueFormalParList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateOrValueFormalPar createTemplateOrValueFormalPar()
  {
    TemplateOrValueFormalParImpl templateOrValueFormalPar = new TemplateOrValueFormalParImpl();
    return templateOrValueFormalPar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConfigSpec createConfigSpec()
  {
    ConfigSpecImpl configSpec = new ConfigSpecImpl();
    return configSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SystemSpec createSystemSpec()
  {
    SystemSpecImpl systemSpec = new SystemSpecImpl();
    return systemSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SignatureDef createSignatureDef()
  {
    SignatureDefImpl signatureDef = new SignatureDefImpl();
    return signatureDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExceptionSpec createExceptionSpec()
  {
    ExceptionSpecImpl exceptionSpec = new ExceptionSpecImpl();
    return exceptionSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SignatureFormalParList createSignatureFormalParList()
  {
    SignatureFormalParListImpl signatureFormalParList = new SignatureFormalParListImpl();
    return signatureFormalParList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleControlPart createModuleControlPart()
  {
    ModuleControlPartImpl moduleControlPart = new ModuleControlPartImpl();
    return moduleControlPart;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleControlBody createModuleControlBody()
  {
    ModuleControlBodyImpl moduleControlBody = new ModuleControlBodyImpl();
    return moduleControlBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ControlStatementOrDefList createControlStatementOrDefList()
  {
    ControlStatementOrDefListImpl controlStatementOrDefList = new ControlStatementOrDefListImpl();
    return controlStatementOrDefList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ControlStatementOrDef createControlStatementOrDef()
  {
    ControlStatementOrDefImpl controlStatementOrDef = new ControlStatementOrDefImpl();
    return controlStatementOrDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ControlStatement createControlStatement()
  {
    ControlStatementImpl controlStatement = new ControlStatementImpl();
    return controlStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SUTStatements createSUTStatements()
  {
    SUTStatementsImpl sutStatements = new SUTStatementsImpl();
    return sutStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActionText createActionText()
  {
    ActionTextImpl actionText = new ActionTextImpl();
    return actionText;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BehaviourStatements createBehaviourStatements()
  {
    BehaviourStatementsImpl behaviourStatements = new BehaviourStatementsImpl();
    return behaviourStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActivateOp createActivateOp()
  {
    ActivateOpImpl activateOp = new ActivateOpImpl();
    return activateOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DeactivateStatement createDeactivateStatement()
  {
    DeactivateStatementImpl deactivateStatement = new DeactivateStatementImpl();
    return deactivateStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LabelStatement createLabelStatement()
  {
    LabelStatementImpl labelStatement = new LabelStatementImpl();
    return labelStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GotoStatement createGotoStatement()
  {
    GotoStatementImpl gotoStatement = new GotoStatementImpl();
    return gotoStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReturnStatement createReturnStatement()
  {
    ReturnStatementImpl returnStatement = new ReturnStatementImpl();
    return returnStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InterleavedConstruct createInterleavedConstruct()
  {
    InterleavedConstructImpl interleavedConstruct = new InterleavedConstructImpl();
    return interleavedConstruct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InterleavedGuardList createInterleavedGuardList()
  {
    InterleavedGuardListImpl interleavedGuardList = new InterleavedGuardListImpl();
    return interleavedGuardList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InterleavedGuardElement createInterleavedGuardElement()
  {
    InterleavedGuardElementImpl interleavedGuardElement = new InterleavedGuardElementImpl();
    return interleavedGuardElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InterleavedGuard createInterleavedGuard()
  {
    InterleavedGuardImpl interleavedGuard = new InterleavedGuardImpl();
    return interleavedGuard;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltConstruct createAltConstruct()
  {
    AltConstructImpl altConstruct = new AltConstructImpl();
    return altConstruct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltGuardList createAltGuardList()
  {
    AltGuardListImpl altGuardList = new AltGuardListImpl();
    return altGuardList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ElseStatement createElseStatement()
  {
    ElseStatementImpl elseStatement = new ElseStatementImpl();
    return elseStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GuardStatement createGuardStatement()
  {
    GuardStatementImpl guardStatement = new GuardStatementImpl();
    return guardStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GuardOp createGuardOp()
  {
    GuardOpImpl guardOp = new GuardOpImpl();
    return guardOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DoneStatement createDoneStatement()
  {
    DoneStatementImpl doneStatement = new DoneStatementImpl();
    return doneStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public KilledStatement createKilledStatement()
  {
    KilledStatementImpl killedStatement = new KilledStatementImpl();
    return killedStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentOrAny createComponentOrAny()
  {
    ComponentOrAnyImpl componentOrAny = new ComponentOrAnyImpl();
    return componentOrAny;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IndexAssignment createIndexAssignment()
  {
    IndexAssignmentImpl indexAssignment = new IndexAssignmentImpl();
    return indexAssignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IndexSpec createIndexSpec()
  {
    IndexSpecImpl indexSpec = new IndexSpecImpl();
    return indexSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GetReplyStatement createGetReplyStatement()
  {
    GetReplyStatementImpl getReplyStatement = new GetReplyStatementImpl();
    return getReplyStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CheckStatement createCheckStatement()
  {
    CheckStatementImpl checkStatement = new CheckStatementImpl();
    return checkStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortCheckOp createPortCheckOp()
  {
    PortCheckOpImpl portCheckOp = new PortCheckOpImpl();
    return portCheckOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CheckParameter createCheckParameter()
  {
    CheckParameterImpl checkParameter = new CheckParameterImpl();
    return checkParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RedirectPresent createRedirectPresent()
  {
    RedirectPresentImpl redirectPresent = new RedirectPresentImpl();
    return redirectPresent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FromClausePresent createFromClausePresent()
  {
    FromClausePresentImpl fromClausePresent = new FromClausePresentImpl();
    return fromClausePresent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CheckPortOpsPresent createCheckPortOpsPresent()
  {
    CheckPortOpsPresentImpl checkPortOpsPresent = new CheckPortOpsPresentImpl();
    return checkPortOpsPresent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortGetReplyOp createPortGetReplyOp()
  {
    PortGetReplyOpImpl portGetReplyOp = new PortGetReplyOpImpl();
    return portGetReplyOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRedirectWithValueAndParam createPortRedirectWithValueAndParam()
  {
    PortRedirectWithValueAndParamImpl portRedirectWithValueAndParam = new PortRedirectWithValueAndParamImpl();
    return portRedirectWithValueAndParam;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RedirectWithValueAndParamSpec createRedirectWithValueAndParamSpec()
  {
    RedirectWithValueAndParamSpecImpl redirectWithValueAndParamSpec = new RedirectWithValueAndParamSpecImpl();
    return redirectWithValueAndParamSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ValueMatchSpec createValueMatchSpec()
  {
    ValueMatchSpecImpl valueMatchSpec = new ValueMatchSpecImpl();
    return valueMatchSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CatchStatement createCatchStatement()
  {
    CatchStatementImpl catchStatement = new CatchStatementImpl();
    return catchStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortCatchOp createPortCatchOp()
  {
    PortCatchOpImpl portCatchOp = new PortCatchOpImpl();
    return portCatchOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CatchOpParameter createCatchOpParameter()
  {
    CatchOpParameterImpl catchOpParameter = new CatchOpParameterImpl();
    return catchOpParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GetCallStatement createGetCallStatement()
  {
    GetCallStatementImpl getCallStatement = new GetCallStatementImpl();
    return getCallStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortGetCallOp createPortGetCallOp()
  {
    PortGetCallOpImpl portGetCallOp = new PortGetCallOpImpl();
    return portGetCallOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRedirectWithParam createPortRedirectWithParam()
  {
    PortRedirectWithParamImpl portRedirectWithParam = new PortRedirectWithParamImpl();
    return portRedirectWithParam;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RedirectWithParamSpec createRedirectWithParamSpec()
  {
    RedirectWithParamSpecImpl redirectWithParamSpec = new RedirectWithParamSpecImpl();
    return redirectWithParamSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParamSpec createParamSpec()
  {
    ParamSpecImpl paramSpec = new ParamSpecImpl();
    return paramSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParamAssignmentList createParamAssignmentList()
  {
    ParamAssignmentListImpl paramAssignmentList = new ParamAssignmentListImpl();
    return paramAssignmentList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AssignmentList createAssignmentList()
  {
    AssignmentListImpl assignmentList = new AssignmentListImpl();
    return assignmentList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableAssignment createVariableAssignment()
  {
    VariableAssignmentImpl variableAssignment = new VariableAssignmentImpl();
    return variableAssignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableList createVariableList()
  {
    VariableListImpl variableList = new VariableListImpl();
    return variableList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableEntry createVariableEntry()
  {
    VariableEntryImpl variableEntry = new VariableEntryImpl();
    return variableEntry;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TriggerStatement createTriggerStatement()
  {
    TriggerStatementImpl triggerStatement = new TriggerStatementImpl();
    return triggerStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortTriggerOp createPortTriggerOp()
  {
    PortTriggerOpImpl portTriggerOp = new PortTriggerOpImpl();
    return portTriggerOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReceiveStatement createReceiveStatement()
  {
    ReceiveStatementImpl receiveStatement = new ReceiveStatementImpl();
    return receiveStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortOrAny createPortOrAny()
  {
    PortOrAnyImpl portOrAny = new PortOrAnyImpl();
    return portOrAny;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortReceiveOp createPortReceiveOp()
  {
    PortReceiveOpImpl portReceiveOp = new PortReceiveOpImpl();
    return portReceiveOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FromClause createFromClause()
  {
    FromClauseImpl fromClause = new FromClauseImpl();
    return fromClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddressRefList createAddressRefList()
  {
    AddressRefListImpl addressRefList = new AddressRefListImpl();
    return addressRefList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRedirect createPortRedirect()
  {
    PortRedirectImpl portRedirect = new PortRedirectImpl();
    return portRedirect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SenderSpec createSenderSpec()
  {
    SenderSpecImpl senderSpec = new SenderSpecImpl();
    return senderSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ValueSpec createValueSpec()
  {
    ValueSpecImpl valueSpec = new ValueSpecImpl();
    return valueSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SingleValueSpec createSingleValueSpec()
  {
    SingleValueSpecImpl singleValueSpec = new SingleValueSpecImpl();
    return singleValueSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltGuardChar createAltGuardChar()
  {
    AltGuardCharImpl altGuardChar = new AltGuardCharImpl();
    return altGuardChar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AltstepInstance createAltstepInstance()
  {
    AltstepInstanceImpl altstepInstance = new AltstepInstanceImpl();
    return altstepInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionInstance createFunctionInstance()
  {
    FunctionInstanceImpl functionInstance = new FunctionInstanceImpl();
    return functionInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionRef createFunctionRef()
  {
    FunctionRefImpl functionRef = new FunctionRefImpl();
    return functionRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionActualParList createFunctionActualParList()
  {
    FunctionActualParListImpl functionActualParList = new FunctionActualParListImpl();
    return functionActualParList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionActualParAssignment createFunctionActualParAssignment()
  {
    FunctionActualParAssignmentImpl functionActualParAssignment = new FunctionActualParAssignmentImpl();
    return functionActualParAssignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentRefAssignment createComponentRefAssignment()
  {
    ComponentRefAssignmentImpl componentRefAssignment = new ComponentRefAssignmentImpl();
    return componentRefAssignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalPortAndValuePar createFormalPortAndValuePar()
  {
    FormalPortAndValueParImpl formalPortAndValuePar = new FormalPortAndValueParImpl();
    return formalPortAndValuePar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRefAssignment createPortRefAssignment()
  {
    PortRefAssignmentImpl portRefAssignment = new PortRefAssignmentImpl();
    return portRefAssignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerRefAssignment createTimerRefAssignment()
  {
    TimerRefAssignmentImpl timerRefAssignment = new TimerRefAssignmentImpl();
    return timerRefAssignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionActualPar createFunctionActualPar()
  {
    FunctionActualParImpl functionActualPar = new FunctionActualParImpl();
    return functionActualPar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentRef createComponentRef()
  {
    ComponentRefImpl componentRef = new ComponentRefImpl();
    return componentRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentOrDefaultReference createComponentOrDefaultReference()
  {
    ComponentOrDefaultReferenceImpl componentOrDefaultReference = new ComponentOrDefaultReferenceImpl();
    return componentOrDefaultReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TestcaseInstance createTestcaseInstance()
  {
    TestcaseInstanceImpl testcaseInstance = new TestcaseInstanceImpl();
    return testcaseInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TestcaseActualParList createTestcaseActualParList()
  {
    TestcaseActualParListImpl testcaseActualParList = new TestcaseActualParListImpl();
    return testcaseActualParList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerStatements createTimerStatements()
  {
    TimerStatementsImpl timerStatements = new TimerStatementsImpl();
    return timerStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimeoutStatement createTimeoutStatement()
  {
    TimeoutStatementImpl timeoutStatement = new TimeoutStatementImpl();
    return timeoutStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StartTimerStatement createStartTimerStatement()
  {
    StartTimerStatementImpl startTimerStatement = new StartTimerStatementImpl();
    return startTimerStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StopTimerStatement createStopTimerStatement()
  {
    StopTimerStatementImpl stopTimerStatement = new StopTimerStatementImpl();
    return stopTimerStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerRefOrAny createTimerRefOrAny()
  {
    TimerRefOrAnyImpl timerRefOrAny = new TimerRefOrAnyImpl();
    return timerRefOrAny;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerRefOrAll createTimerRefOrAll()
  {
    TimerRefOrAllImpl timerRefOrAll = new TimerRefOrAllImpl();
    return timerRefOrAll;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BasicStatements createBasicStatements()
  {
    BasicStatementsImpl basicStatements = new BasicStatementsImpl();
    return basicStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StatementBlock createStatementBlock()
  {
    StatementBlockImpl statementBlock = new StatementBlockImpl();
    return statementBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionStatementList createFunctionStatementList()
  {
    FunctionStatementListImpl functionStatementList = new FunctionStatementListImpl();
    return functionStatementList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionStatement createFunctionStatement()
  {
    FunctionStatementImpl functionStatement = new FunctionStatementImpl();
    return functionStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TestcaseOperation createTestcaseOperation()
  {
    TestcaseOperationImpl testcaseOperation = new TestcaseOperationImpl();
    return testcaseOperation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SetLocalVerdict createSetLocalVerdict()
  {
    SetLocalVerdictImpl setLocalVerdict = new SetLocalVerdictImpl();
    return setLocalVerdict;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConfigurationStatements createConfigurationStatements()
  {
    ConfigurationStatementsImpl configurationStatements = new ConfigurationStatementsImpl();
    return configurationStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public KillTCStatement createKillTCStatement()
  {
    KillTCStatementImpl killTCStatement = new KillTCStatementImpl();
    return killTCStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StopTCStatement createStopTCStatement()
  {
    StopTCStatementImpl stopTCStatement = new StopTCStatementImpl();
    return stopTCStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentReferenceOrLiteral createComponentReferenceOrLiteral()
  {
    ComponentReferenceOrLiteralImpl componentReferenceOrLiteral = new ComponentReferenceOrLiteralImpl();
    return componentReferenceOrLiteral;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StartTCStatement createStartTCStatement()
  {
    StartTCStatementImpl startTCStatement = new StartTCStatementImpl();
    return startTCStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnmapStatement createUnmapStatement()
  {
    UnmapStatementImpl unmapStatement = new UnmapStatementImpl();
    return unmapStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DisconnectStatement createDisconnectStatement()
  {
    DisconnectStatementImpl disconnectStatement = new DisconnectStatementImpl();
    return disconnectStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllConnectionsSpec createAllConnectionsSpec()
  {
    AllConnectionsSpecImpl allConnectionsSpec = new AllConnectionsSpecImpl();
    return allConnectionsSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllPortsSpec createAllPortsSpec()
  {
    AllPortsSpecImpl allPortsSpec = new AllPortsSpecImpl();
    return allPortsSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MapStatement createMapStatement()
  {
    MapStatementImpl mapStatement = new MapStatementImpl();
    return mapStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ParamClause createParamClause()
  {
    ParamClauseImpl paramClause = new ParamClauseImpl();
    return paramClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConnectStatement createConnectStatement()
  {
    ConnectStatementImpl connectStatement = new ConnectStatementImpl();
    return connectStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SingleConnectionSpec createSingleConnectionSpec()
  {
    SingleConnectionSpecImpl singleConnectionSpec = new SingleConnectionSpecImpl();
    return singleConnectionSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRef createPortRef()
  {
    PortRefImpl portRef = new PortRefImpl();
    return portRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommunicationStatements createCommunicationStatements()
  {
    CommunicationStatementsImpl communicationStatements = new CommunicationStatementsImpl();
    return communicationStatements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CheckStateStatement createCheckStateStatement()
  {
    CheckStateStatementImpl checkStateStatement = new CheckStateStatementImpl();
    return checkStateStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortOrAllAny createPortOrAllAny()
  {
    PortOrAllAnyImpl portOrAllAny = new PortOrAllAnyImpl();
    return portOrAllAny;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HaltStatement createHaltStatement()
  {
    HaltStatementImpl haltStatement = new HaltStatementImpl();
    return haltStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StartStatement createStartStatement()
  {
    StartStatementImpl startStatement = new StartStatementImpl();
    return startStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StopStatement createStopStatement()
  {
    StopStatementImpl stopStatement = new StopStatementImpl();
    return stopStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClearStatement createClearStatement()
  {
    ClearStatementImpl clearStatement = new ClearStatementImpl();
    return clearStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortOrAll createPortOrAll()
  {
    PortOrAllImpl portOrAll = new PortOrAllImpl();
    return portOrAll;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RaiseStatement createRaiseStatement()
  {
    RaiseStatementImpl raiseStatement = new RaiseStatementImpl();
    return raiseStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortRaiseOp createPortRaiseOp()
  {
    PortRaiseOpImpl portRaiseOp = new PortRaiseOpImpl();
    return portRaiseOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReplyStatement createReplyStatement()
  {
    ReplyStatementImpl replyStatement = new ReplyStatementImpl();
    return replyStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortReplyOp createPortReplyOp()
  {
    PortReplyOpImpl portReplyOp = new PortReplyOpImpl();
    return portReplyOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReplyValue createReplyValue()
  {
    ReplyValueImpl replyValue = new ReplyValueImpl();
    return replyValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallStatement createCallStatement()
  {
    CallStatementImpl callStatement = new CallStatementImpl();
    return callStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortCallOp createPortCallOp()
  {
    PortCallOpImpl portCallOp = new PortCallOpImpl();
    return portCallOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallParameters createCallParameters()
  {
    CallParametersImpl callParameters = new CallParametersImpl();
    return callParameters;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallTimerValue createCallTimerValue()
  {
    CallTimerValueImpl callTimerValue = new CallTimerValueImpl();
    return callTimerValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortCallBody createPortCallBody()
  {
    PortCallBodyImpl portCallBody = new PortCallBodyImpl();
    return portCallBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallBodyStatementList createCallBodyStatementList()
  {
    CallBodyStatementListImpl callBodyStatementList = new CallBodyStatementListImpl();
    return callBodyStatementList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallBodyStatement createCallBodyStatement()
  {
    CallBodyStatementImpl callBodyStatement = new CallBodyStatementImpl();
    return callBodyStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallBodyGuard createCallBodyGuard()
  {
    CallBodyGuardImpl callBodyGuard = new CallBodyGuardImpl();
    return callBodyGuard;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallBodyOps createCallBodyOps()
  {
    CallBodyOpsImpl callBodyOps = new CallBodyOpsImpl();
    return callBodyOps;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SendStatement createSendStatement()
  {
    SendStatementImpl sendStatement = new SendStatementImpl();
    return sendStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortSendOp createPortSendOp()
  {
    PortSendOpImpl portSendOp = new PortSendOpImpl();
    return portSendOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ToClause createToClause()
  {
    ToClauseImpl toClause = new ToClauseImpl();
    return toClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionDefList createFunctionDefList()
  {
    FunctionDefListImpl functionDefList = new FunctionDefListImpl();
    return functionDefList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionLocalDef createFunctionLocalDef()
  {
    FunctionLocalDefImpl functionLocalDef = new FunctionLocalDefImpl();
    return functionLocalDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionLocalInst createFunctionLocalInst()
  {
    FunctionLocalInstImpl functionLocalInst = new FunctionLocalInstImpl();
    return functionLocalInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerInstance createTimerInstance()
  {
    TimerInstanceImpl timerInstance = new TimerInstanceImpl();
    return timerInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarInstance createVarInstance()
  {
    VarInstanceImpl varInstance = new VarInstanceImpl();
    return varInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarList createVarList()
  {
    VarListImpl varList = new VarListImpl();
    return varList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModuleOrGroup createModuleOrGroup()
  {
    ModuleOrGroupImpl moduleOrGroup = new ModuleOrGroupImpl();
    return moduleOrGroup;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReferencedType createReferencedType()
  {
    ReferencedTypeImpl referencedType = new ReferencedTypeImpl();
    return referencedType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeDef createTypeDef()
  {
    TypeDefImpl typeDef = new TypeDefImpl();
    return typeDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeDefBody createTypeDefBody()
  {
    TypeDefBodyImpl typeDefBody = new TypeDefBodyImpl();
    return typeDefBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubTypeDef createSubTypeDef()
  {
    SubTypeDefImpl subTypeDef = new SubTypeDefImpl();
    return subTypeDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubTypeDefNamed createSubTypeDefNamed()
  {
    SubTypeDefNamedImpl subTypeDefNamed = new SubTypeDefNamedImpl();
    return subTypeDefNamed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructuredTypeDef createStructuredTypeDef()
  {
    StructuredTypeDefImpl structuredTypeDef = new StructuredTypeDefImpl();
    return structuredTypeDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordDef createRecordDef()
  {
    RecordDefImpl recordDef = new RecordDefImpl();
    return recordDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordDefNamed createRecordDefNamed()
  {
    RecordDefNamedImpl recordDefNamed = new RecordDefNamedImpl();
    return recordDefNamed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordOfDef createRecordOfDef()
  {
    RecordOfDefImpl recordOfDef = new RecordOfDefImpl();
    return recordOfDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RecordOfDefNamed createRecordOfDefNamed()
  {
    RecordOfDefNamedImpl recordOfDefNamed = new RecordOfDefNamedImpl();
    return recordOfDefNamed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructDefBody createStructDefBody()
  {
    StructDefBodyImpl structDefBody = new StructDefBodyImpl();
    return structDefBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructFieldDef createStructFieldDef()
  {
    StructFieldDefImpl structFieldDef = new StructFieldDefImpl();
    return structFieldDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SetOfDef createSetOfDef()
  {
    SetOfDefImpl setOfDef = new SetOfDefImpl();
    return setOfDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SetOfDefNamed createSetOfDefNamed()
  {
    SetOfDefNamedImpl setOfDefNamed = new SetOfDefNamedImpl();
    return setOfDefNamed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortDef createPortDef()
  {
    PortDefImpl portDef = new PortDefImpl();
    return portDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortDefBody createPortDefBody()
  {
    PortDefBodyImpl portDefBody = new PortDefBodyImpl();
    return portDefBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortDefAttribs createPortDefAttribs()
  {
    PortDefAttribsImpl portDefAttribs = new PortDefAttribsImpl();
    return portDefAttribs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MixedAttribs createMixedAttribs()
  {
    MixedAttribsImpl mixedAttribs = new MixedAttribsImpl();
    return mixedAttribs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MixedList createMixedList()
  {
    MixedListImpl mixedList = new MixedListImpl();
    return mixedList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcOrTypeList createProcOrTypeList()
  {
    ProcOrTypeListImpl procOrTypeList = new ProcOrTypeListImpl();
    return procOrTypeList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcOrType createProcOrType()
  {
    ProcOrTypeImpl procOrType = new ProcOrTypeImpl();
    return procOrType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MessageAttribs createMessageAttribs()
  {
    MessageAttribsImpl messageAttribs = new MessageAttribsImpl();
    return messageAttribs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConfigParamDef createConfigParamDef()
  {
    ConfigParamDefImpl configParamDef = new ConfigParamDefImpl();
    return configParamDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MapParamDef createMapParamDef()
  {
    MapParamDefImpl mapParamDef = new MapParamDefImpl();
    return mapParamDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnmapParamDef createUnmapParamDef()
  {
    UnmapParamDefImpl unmapParamDef = new UnmapParamDefImpl();
    return unmapParamDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddressDecl createAddressDecl()
  {
    AddressDeclImpl addressDecl = new AddressDeclImpl();
    return addressDecl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcedureAttribs createProcedureAttribs()
  {
    ProcedureAttribsImpl procedureAttribs = new ProcedureAttribsImpl();
    return procedureAttribs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentDef createComponentDef()
  {
    ComponentDefImpl componentDef = new ComponentDefImpl();
    return componentDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentDefList createComponentDefList()
  {
    ComponentDefListImpl componentDefList = new ComponentDefListImpl();
    return componentDefList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortInstance createPortInstance()
  {
    PortInstanceImpl portInstance = new PortInstanceImpl();
    return portInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PortElement createPortElement()
  {
    PortElementImpl portElement = new PortElementImpl();
    return portElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComponentElementDef createComponentElementDef()
  {
    ComponentElementDefImpl componentElementDef = new ComponentElementDefImpl();
    return componentElementDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcedureList createProcedureList()
  {
    ProcedureListImpl procedureList = new ProcedureListImpl();
    return procedureList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllOrSignatureList createAllOrSignatureList()
  {
    AllOrSignatureListImpl allOrSignatureList = new AllOrSignatureListImpl();
    return allOrSignatureList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SignatureList createSignatureList()
  {
    SignatureListImpl signatureList = new SignatureListImpl();
    return signatureList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumDef createEnumDef()
  {
    EnumDefImpl enumDef = new EnumDefImpl();
    return enumDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumDefNamed createEnumDefNamed()
  {
    EnumDefNamedImpl enumDefNamed = new EnumDefNamedImpl();
    return enumDefNamed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedTypeDef createNestedTypeDef()
  {
    NestedTypeDefImpl nestedTypeDef = new NestedTypeDefImpl();
    return nestedTypeDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedRecordDef createNestedRecordDef()
  {
    NestedRecordDefImpl nestedRecordDef = new NestedRecordDefImpl();
    return nestedRecordDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedUnionDef createNestedUnionDef()
  {
    NestedUnionDefImpl nestedUnionDef = new NestedUnionDefImpl();
    return nestedUnionDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedSetDef createNestedSetDef()
  {
    NestedSetDefImpl nestedSetDef = new NestedSetDefImpl();
    return nestedSetDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedRecordOfDef createNestedRecordOfDef()
  {
    NestedRecordOfDefImpl nestedRecordOfDef = new NestedRecordOfDefImpl();
    return nestedRecordOfDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedSetOfDef createNestedSetOfDef()
  {
    NestedSetOfDefImpl nestedSetOfDef = new NestedSetOfDefImpl();
    return nestedSetOfDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedEnumDef createNestedEnumDef()
  {
    NestedEnumDefImpl nestedEnumDef = new NestedEnumDefImpl();
    return nestedEnumDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MessageList createMessageList()
  {
    MessageListImpl messageList = new MessageListImpl();
    return messageList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllOrTypeList createAllOrTypeList()
  {
    AllOrTypeListImpl allOrTypeList = new AllOrTypeListImpl();
    return allOrTypeList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeList createTypeList()
  {
    TypeListImpl typeList = new TypeListImpl();
    return typeList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionDef createUnionDef()
  {
    UnionDefImpl unionDef = new UnionDefImpl();
    return unionDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionDefNamed createUnionDefNamed()
  {
    UnionDefNamedImpl unionDefNamed = new UnionDefNamedImpl();
    return unionDefNamed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionDefBody createUnionDefBody()
  {
    UnionDefBodyImpl unionDefBody = new UnionDefBodyImpl();
    return unionDefBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumerationList createEnumerationList()
  {
    EnumerationListImpl enumerationList = new EnumerationListImpl();
    return enumerationList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Enumeration createEnumeration()
  {
    EnumerationImpl enumeration = new EnumerationImpl();
    return enumeration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnionFieldDef createUnionFieldDef()
  {
    UnionFieldDefImpl unionFieldDef = new UnionFieldDefImpl();
    return unionFieldDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SetDef createSetDef()
  {
    SetDefImpl setDef = new SetDefImpl();
    return setDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SetDefNamed createSetDefNamed()
  {
    SetDefNamedImpl setDefNamed = new SetDefNamedImpl();
    return setDefNamed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubTypeSpec createSubTypeSpec()
  {
    SubTypeSpecImpl subTypeSpec = new SubTypeSpecImpl();
    return subTypeSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllowedValuesSpec createAllowedValuesSpec()
  {
    AllowedValuesSpecImpl allowedValuesSpec = new AllowedValuesSpecImpl();
    return allowedValuesSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateOrRange createTemplateOrRange()
  {
    TemplateOrRangeImpl templateOrRange = new TemplateOrRangeImpl();
    return templateOrRange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RangeDef createRangeDef()
  {
    RangeDefImpl rangeDef = new RangeDefImpl();
    return rangeDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Bound createBound()
  {
    BoundImpl bound = new BoundImpl();
    return bound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StringLength createStringLength()
  {
    StringLengthImpl stringLength = new StringLengthImpl();
    return stringLength;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CharStringMatch createCharStringMatch()
  {
    CharStringMatchImpl charStringMatch = new CharStringMatchImpl();
    return charStringMatch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PatternParticle createPatternParticle()
  {
    PatternParticleImpl patternParticle = new PatternParticleImpl();
    return patternParticle;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateDef createTemplateDef()
  {
    TemplateDefImpl templateDef = new TemplateDefImpl();
    return templateDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DerivedDef createDerivedDef()
  {
    DerivedDefImpl derivedDef = new DerivedDefImpl();
    return derivedDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BaseTemplate createBaseTemplate()
  {
    BaseTemplateImpl baseTemplate = new BaseTemplateImpl();
    return baseTemplate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TempVarList createTempVarList()
  {
    TempVarListImpl tempVarList = new TempVarListImpl();
    return tempVarList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerVarInstance createTimerVarInstance()
  {
    TimerVarInstanceImpl timerVarInstance = new TimerVarInstanceImpl();
    return timerVarInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SingleVarInstance createSingleVarInstance()
  {
    SingleVarInstanceImpl singleVarInstance = new SingleVarInstanceImpl();
    return singleVarInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SingleTempVarInstance createSingleTempVarInstance()
  {
    SingleTempVarInstanceImpl singleTempVarInstance = new SingleTempVarInstanceImpl();
    return singleTempVarInstance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InLineTemplate createInLineTemplate()
  {
    InLineTemplateImpl inLineTemplate = new InLineTemplateImpl();
    return inLineTemplate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DerivedRefWithParList createDerivedRefWithParList()
  {
    DerivedRefWithParListImpl derivedRefWithParList = new DerivedRefWithParListImpl();
    return derivedRefWithParList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateBody createTemplateBody()
  {
    TemplateBodyImpl templateBody = new TemplateBodyImpl();
    return templateBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtraMatchingAttributes createExtraMatchingAttributes()
  {
    ExtraMatchingAttributesImpl extraMatchingAttributes = new ExtraMatchingAttributesImpl();
    return extraMatchingAttributes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldSpecList createFieldSpecList()
  {
    FieldSpecListImpl fieldSpecList = new FieldSpecListImpl();
    return fieldSpecList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldSpec createFieldSpec()
  {
    FieldSpecImpl fieldSpec = new FieldSpecImpl();
    return fieldSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleSpec createSimpleSpec()
  {
    SimpleSpecImpl simpleSpec = new SimpleSpecImpl();
    return simpleSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleTemplateSpec createSimpleTemplateSpec()
  {
    SimpleTemplateSpecImpl simpleTemplateSpec = new SimpleTemplateSpecImpl();
    return simpleTemplateSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SingleTemplateExpression createSingleTemplateExpression()
  {
    SingleTemplateExpressionImpl singleTemplateExpression = new SingleTemplateExpressionImpl();
    return singleTemplateExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateRefWithParList createTemplateRefWithParList()
  {
    TemplateRefWithParListImpl templateRefWithParList = new TemplateRefWithParListImpl();
    return templateRefWithParList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateActualParList createTemplateActualParList()
  {
    TemplateActualParListImpl templateActualParList = new TemplateActualParListImpl();
    return templateActualParList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MatchingSymbol createMatchingSymbol()
  {
    MatchingSymbolImpl matchingSymbol = new MatchingSymbolImpl();
    return matchingSymbol;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubsetMatch createSubsetMatch()
  {
    SubsetMatchImpl subsetMatch = new SubsetMatchImpl();
    return subsetMatch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SupersetMatch createSupersetMatch()
  {
    SupersetMatchImpl supersetMatch = new SupersetMatchImpl();
    return supersetMatch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Range createRange()
  {
    RangeImpl range = new RangeImpl();
    return range;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WildcardLengthMatch createWildcardLengthMatch()
  {
    WildcardLengthMatchImpl wildcardLengthMatch = new WildcardLengthMatchImpl();
    return wildcardLengthMatch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Complement createComplement()
  {
    ComplementImpl complement = new ComplementImpl();
    return complement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ListOfTemplates createListOfTemplates()
  {
    ListOfTemplatesImpl listOfTemplates = new ListOfTemplatesImpl();
    return listOfTemplates;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateOps createTemplateOps()
  {
    TemplateOpsImpl templateOps = new TemplateOpsImpl();
    return templateOps;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MatchOp createMatchOp()
  {
    MatchOpImpl matchOp = new MatchOpImpl();
    return matchOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ValueofOp createValueofOp()
  {
    ValueofOpImpl valueofOp = new ValueofOpImpl();
    return valueofOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConfigurationOps createConfigurationOps()
  {
    ConfigurationOpsImpl configurationOps = new ConfigurationOpsImpl();
    return configurationOps;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CreateOp createCreateOp()
  {
    CreateOpImpl createOp = new CreateOpImpl();
    return createOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RunningOp createRunningOp()
  {
    RunningOpImpl runningOp = new RunningOpImpl();
    return runningOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OpCall createOpCall()
  {
    OpCallImpl opCall = new OpCallImpl();
    return opCall;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AliveOp createAliveOp()
  {
    AliveOpImpl aliveOp = new AliveOpImpl();
    return aliveOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateListItem createTemplateListItem()
  {
    TemplateListItemImpl templateListItem = new TemplateListItemImpl();
    return templateListItem;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllElementsFrom createAllElementsFrom()
  {
    AllElementsFromImpl allElementsFrom = new AllElementsFromImpl();
    return allElementsFrom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Signature createSignature()
  {
    SignatureImpl signature = new SignatureImpl();
    return signature;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateInstanceAssignment createTemplateInstanceAssignment()
  {
    TemplateInstanceAssignmentImpl templateInstanceAssignment = new TemplateInstanceAssignmentImpl();
    return templateInstanceAssignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateInstanceActualPar createTemplateInstanceActualPar()
  {
    TemplateInstanceActualParImpl templateInstanceActualPar = new TemplateInstanceActualParImpl();
    return templateInstanceActualPar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateRestriction createTemplateRestriction()
  {
    TemplateRestrictionImpl templateRestriction = new TemplateRestrictionImpl();
    return templateRestriction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RestrictedTemplate createRestrictedTemplate()
  {
    RestrictedTemplateImpl restrictedTemplate = new RestrictedTemplateImpl();
    return restrictedTemplate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LogStatement createLogStatement()
  {
    LogStatementImpl logStatement = new LogStatementImpl();
    return logStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LogItem createLogItem()
  {
    LogItemImpl logItem = new LogItemImpl();
    return logItem;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WithStatement createWithStatement()
  {
    WithStatementImpl withStatement = new WithStatementImpl();
    return withStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WithAttribList createWithAttribList()
  {
    WithAttribListImpl withAttribList = new WithAttribListImpl();
    return withAttribList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MultiWithAttrib createMultiWithAttrib()
  {
    MultiWithAttribImpl multiWithAttrib = new MultiWithAttribImpl();
    return multiWithAttrib;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SingleWithAttrib createSingleWithAttrib()
  {
    SingleWithAttribImpl singleWithAttrib = new SingleWithAttribImpl();
    return singleWithAttrib;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AttribQualifier createAttribQualifier()
  {
    AttribQualifierImpl attribQualifier = new AttribQualifierImpl();
    return attribQualifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifierList createIdentifierList()
  {
    IdentifierListImpl identifierList = new IdentifierListImpl();
    return identifierList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public QualifiedIdentifierList createQualifiedIdentifierList()
  {
    QualifiedIdentifierListImpl qualifiedIdentifierList = new QualifiedIdentifierListImpl();
    return qualifiedIdentifierList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SingleConstDef createSingleConstDef()
  {
    SingleConstDefImpl singleConstDef = new SingleConstDefImpl();
    return singleConstDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompoundExpression createCompoundExpression()
  {
    CompoundExpressionImpl compoundExpression = new CompoundExpressionImpl();
    return compoundExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayExpression createArrayExpression()
  {
    ArrayExpressionImpl arrayExpression = new ArrayExpressionImpl();
    return arrayExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldExpressionList createFieldExpressionList()
  {
    FieldExpressionListImpl fieldExpressionList = new FieldExpressionListImpl();
    return fieldExpressionList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayElementExpressionList createArrayElementExpressionList()
  {
    ArrayElementExpressionListImpl arrayElementExpressionList = new ArrayElementExpressionListImpl();
    return arrayElementExpressionList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldExpressionSpec createFieldExpressionSpec()
  {
    FieldExpressionSpecImpl fieldExpressionSpec = new FieldExpressionSpecImpl();
    return fieldExpressionSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotUsedOrExpression createNotUsedOrExpression()
  {
    NotUsedOrExpressionImpl notUsedOrExpression = new NotUsedOrExpressionImpl();
    return notUsedOrExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstantExpression createConstantExpression()
  {
    ConstantExpressionImpl constantExpression = new ConstantExpressionImpl();
    return constantExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompoundConstExpression createCompoundConstExpression()
  {
    CompoundConstExpressionImpl compoundConstExpression = new CompoundConstExpressionImpl();
    return compoundConstExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldConstExpressionList createFieldConstExpressionList()
  {
    FieldConstExpressionListImpl fieldConstExpressionList = new FieldConstExpressionListImpl();
    return fieldConstExpressionList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldConstExpressionSpec createFieldConstExpressionSpec()
  {
    FieldConstExpressionSpecImpl fieldConstExpressionSpec = new FieldConstExpressionSpecImpl();
    return fieldConstExpressionSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayConstExpression createArrayConstExpression()
  {
    ArrayConstExpressionImpl arrayConstExpression = new ArrayConstExpressionImpl();
    return arrayConstExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayElementConstExpressionList createArrayElementConstExpressionList()
  {
    ArrayElementConstExpressionListImpl arrayElementConstExpressionList = new ArrayElementConstExpressionListImpl();
    return arrayElementConstExpressionList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstList createConstList()
  {
    ConstListImpl constList = new ConstListImpl();
    return constList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Value createValue()
  {
    ValueImpl value = new ValueImpl();
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReferencedValue createReferencedValue()
  {
    ReferencedValueImpl referencedValue = new ReferencedValueImpl();
    return referencedValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RefValueHead createRefValueHead()
  {
    RefValueHeadImpl refValueHead = new RefValueHeadImpl();
    return refValueHead;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RefValueElement createRefValueElement()
  {
    RefValueElementImpl refValueElement = new RefValueElementImpl();
    return refValueElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Head createHead()
  {
    HeadImpl head = new HeadImpl();
    return head;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RefValueTail createRefValueTail()
  {
    RefValueTailImpl refValueTail = new RefValueTailImpl();
    return refValueTail;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SpecElement createSpecElement()
  {
    SpecElementImpl specElement = new SpecElementImpl();
    return specElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtendedFieldReference createExtendedFieldReference()
  {
    ExtendedFieldReferenceImpl extendedFieldReference = new ExtendedFieldReferenceImpl();
    return extendedFieldReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RefValue createRefValue()
  {
    RefValueImpl refValue = new RefValueImpl();
    return refValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PredefinedValue createPredefinedValue()
  {
    PredefinedValueImpl predefinedValue = new PredefinedValueImpl();
    return predefinedValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanExpression createBooleanExpression()
  {
    BooleanExpressionImpl booleanExpression = new BooleanExpressionImpl();
    return booleanExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SingleExpression createSingleExpression()
  {
    SingleExpressionImpl singleExpression = new SingleExpressionImpl();
    return singleExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DefOrFieldRefList createDefOrFieldRefList()
  {
    DefOrFieldRefListImpl defOrFieldRefList = new DefOrFieldRefListImpl();
    return defOrFieldRefList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DefOrFieldRef createDefOrFieldRef()
  {
    DefOrFieldRefImpl defOrFieldRef = new DefOrFieldRefImpl();
    return defOrFieldRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllRef createAllRef()
  {
    AllRefImpl allRef = new AllRefImpl();
    return allRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GroupRefList createGroupRefList()
  {
    GroupRefListImpl groupRefList = new GroupRefListImpl();
    return groupRefList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3Reference createTTCN3Reference()
  {
    TTCN3ReferenceImpl ttcn3Reference = new TTCN3ReferenceImpl();
    return ttcn3Reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3ReferenceList createTTCN3ReferenceList()
  {
    TTCN3ReferenceListImpl ttcn3ReferenceList = new TTCN3ReferenceListImpl();
    return ttcn3ReferenceList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldReference createFieldReference()
  {
    FieldReferenceImpl fieldReference = new FieldReferenceImpl();
    return fieldReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayOrBitRef createArrayOrBitRef()
  {
    ArrayOrBitRefImpl arrayOrBitRef = new ArrayOrBitRefImpl();
    return arrayOrBitRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldOrBitNumber createFieldOrBitNumber()
  {
    FieldOrBitNumberImpl fieldOrBitNumber = new FieldOrBitNumberImpl();
    return fieldOrBitNumber;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayValueOrAttrib createArrayValueOrAttrib()
  {
    ArrayValueOrAttribImpl arrayValueOrAttrib = new ArrayValueOrAttribImpl();
    return arrayValueOrAttrib;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayElementSpecList createArrayElementSpecList()
  {
    ArrayElementSpecListImpl arrayElementSpecList = new ArrayElementSpecListImpl();
    return arrayElementSpecList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayElementSpec createArrayElementSpec()
  {
    ArrayElementSpecImpl arrayElementSpec = new ArrayElementSpecImpl();
    return arrayElementSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerOps createTimerOps()
  {
    TimerOpsImpl timerOps = new TimerOpsImpl();
    return timerOps;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RunningTimerOp createRunningTimerOp()
  {
    RunningTimerOpImpl runningTimerOp = new RunningTimerOpImpl();
    return runningTimerOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReadTimerOp createReadTimerOp()
  {
    ReadTimerOpImpl readTimerOp = new ReadTimerOpImpl();
    return readTimerOp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PermutationMatch createPermutationMatch()
  {
    PermutationMatchImpl permutationMatch = new PermutationMatchImpl();
    return permutationMatch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LoopConstruct createLoopConstruct()
  {
    LoopConstructImpl loopConstruct = new LoopConstructImpl();
    return loopConstruct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ForStatement createForStatement()
  {
    ForStatementImpl forStatement = new ForStatementImpl();
    return forStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WhileStatement createWhileStatement()
  {
    WhileStatementImpl whileStatement = new WhileStatementImpl();
    return whileStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DoWhileStatement createDoWhileStatement()
  {
    DoWhileStatementImpl doWhileStatement = new DoWhileStatementImpl();
    return doWhileStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConditionalConstruct createConditionalConstruct()
  {
    ConditionalConstructImpl conditionalConstruct = new ConditionalConstructImpl();
    return conditionalConstruct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ElseIfClause createElseIfClause()
  {
    ElseIfClauseImpl elseIfClause = new ElseIfClauseImpl();
    return elseIfClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ElseClause createElseClause()
  {
    ElseClauseImpl elseClause = new ElseClauseImpl();
    return elseClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Initial createInitial()
  {
    InitialImpl initial = new InitialImpl();
    return initial;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectCaseConstruct createSelectCaseConstruct()
  {
    SelectCaseConstructImpl selectCaseConstruct = new SelectCaseConstructImpl();
    return selectCaseConstruct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectCaseBody createSelectCaseBody()
  {
    SelectCaseBodyImpl selectCaseBody = new SelectCaseBodyImpl();
    return selectCaseBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectCase createSelectCase()
  {
    SelectCaseImpl selectCase = new SelectCaseImpl();
    return selectCase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionDef createFunctionDef()
  {
    FunctionDefImpl functionDef = new FunctionDefImpl();
    return functionDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MtcSpec createMtcSpec()
  {
    MtcSpecImpl mtcSpec = new MtcSpecImpl();
    return mtcSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionFormalParList createFunctionFormalParList()
  {
    FunctionFormalParListImpl functionFormalParList = new FunctionFormalParListImpl();
    return functionFormalParList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionFormalPar createFunctionFormalPar()
  {
    FunctionFormalParImpl functionFormalPar = new FunctionFormalParImpl();
    return functionFormalPar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalValuePar createFormalValuePar()
  {
    FormalValueParImpl formalValuePar = new FormalValueParImpl();
    return formalValuePar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalTimerPar createFormalTimerPar()
  {
    FormalTimerParImpl formalTimerPar = new FormalTimerParImpl();
    return formalTimerPar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalPortPar createFormalPortPar()
  {
    FormalPortParImpl formalPortPar = new FormalPortParImpl();
    return formalPortPar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormalTemplatePar createFormalTemplatePar()
  {
    FormalTemplateParImpl formalTemplatePar = new FormalTemplateParImpl();
    return formalTemplatePar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RunsOnSpec createRunsOnSpec()
  {
    RunsOnSpecImpl runsOnSpec = new RunsOnSpecImpl();
    return runsOnSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReturnType createReturnType()
  {
    ReturnTypeImpl returnType = new ReturnTypeImpl();
    return returnType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Assignment createAssignment()
  {
    AssignmentImpl assignment = new AssignmentImpl();
    return assignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableRef createVariableRef()
  {
    VariableRefImpl variableRef = new VariableRefImpl();
    return variableRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PreDefFunction createPreDefFunction()
  {
    PreDefFunctionImpl preDefFunction = new PreDefFunctionImpl();
    return preDefFunction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fint2char createFint2char()
  {
    Fint2charImpl fint2char = new Fint2charImpl();
    return fint2char;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fint2unichar createFint2unichar()
  {
    Fint2unicharImpl fint2unichar = new Fint2unicharImpl();
    return fint2unichar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fint2bit createFint2bit()
  {
    Fint2bitImpl fint2bit = new Fint2bitImpl();
    return fint2bit;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fint2enum createFint2enum()
  {
    Fint2enumImpl fint2enum = new Fint2enumImpl();
    return fint2enum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fint2hex createFint2hex()
  {
    Fint2hexImpl fint2hex = new Fint2hexImpl();
    return fint2hex;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fint2oct createFint2oct()
  {
    Fint2octImpl fint2oct = new Fint2octImpl();
    return fint2oct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fint2str createFint2str()
  {
    Fint2strImpl fint2str = new Fint2strImpl();
    return fint2str;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fint2float createFint2float()
  {
    Fint2floatImpl fint2float = new Fint2floatImpl();
    return fint2float;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Ffloat2int createFfloat2int()
  {
    Ffloat2intImpl ffloat2int = new Ffloat2intImpl();
    return ffloat2int;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fchar2int createFchar2int()
  {
    Fchar2intImpl fchar2int = new Fchar2intImpl();
    return fchar2int;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fchar2oct createFchar2oct()
  {
    Fchar2octImpl fchar2oct = new Fchar2octImpl();
    return fchar2oct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Funichar2int createFunichar2int()
  {
    Funichar2intImpl funichar2int = new Funichar2intImpl();
    return funichar2int;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Funichar2oct createFunichar2oct()
  {
    Funichar2octImpl funichar2oct = new Funichar2octImpl();
    return funichar2oct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fbit2int createFbit2int()
  {
    Fbit2intImpl fbit2int = new Fbit2intImpl();
    return fbit2int;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fbit2hex createFbit2hex()
  {
    Fbit2hexImpl fbit2hex = new Fbit2hexImpl();
    return fbit2hex;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fbit2oct createFbit2oct()
  {
    Fbit2octImpl fbit2oct = new Fbit2octImpl();
    return fbit2oct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fbit2str createFbit2str()
  {
    Fbit2strImpl fbit2str = new Fbit2strImpl();
    return fbit2str;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fhex2int createFhex2int()
  {
    Fhex2intImpl fhex2int = new Fhex2intImpl();
    return fhex2int;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fhex2bit createFhex2bit()
  {
    Fhex2bitImpl fhex2bit = new Fhex2bitImpl();
    return fhex2bit;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fhex2oct createFhex2oct()
  {
    Fhex2octImpl fhex2oct = new Fhex2octImpl();
    return fhex2oct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fhex2str createFhex2str()
  {
    Fhex2strImpl fhex2str = new Fhex2strImpl();
    return fhex2str;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Foct2int createFoct2int()
  {
    Foct2intImpl foct2int = new Foct2intImpl();
    return foct2int;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Foct2bit createFoct2bit()
  {
    Foct2bitImpl foct2bit = new Foct2bitImpl();
    return foct2bit;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Foct2hex createFoct2hex()
  {
    Foct2hexImpl foct2hex = new Foct2hexImpl();
    return foct2hex;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Foct2str createFoct2str()
  {
    Foct2strImpl foct2str = new Foct2strImpl();
    return foct2str;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Foct2char createFoct2char()
  {
    Foct2charImpl foct2char = new Foct2charImpl();
    return foct2char;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Foct2unichar createFoct2unichar()
  {
    Foct2unicharImpl foct2unichar = new Foct2unicharImpl();
    return foct2unichar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fstr2int createFstr2int()
  {
    Fstr2intImpl fstr2int = new Fstr2intImpl();
    return fstr2int;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fstr2hex createFstr2hex()
  {
    Fstr2hexImpl fstr2hex = new Fstr2hexImpl();
    return fstr2hex;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fstr2oct createFstr2oct()
  {
    Fstr2octImpl fstr2oct = new Fstr2octImpl();
    return fstr2oct;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fstr2float createFstr2float()
  {
    Fstr2floatImpl fstr2float = new Fstr2floatImpl();
    return fstr2float;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fenum2int createFenum2int()
  {
    Fenum2intImpl fenum2int = new Fenum2intImpl();
    return fenum2int;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Flengthof createFlengthof()
  {
    FlengthofImpl flengthof = new FlengthofImpl();
    return flengthof;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fsizeof createFsizeof()
  {
    FsizeofImpl fsizeof = new FsizeofImpl();
    return fsizeof;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fispresent createFispresent()
  {
    FispresentImpl fispresent = new FispresentImpl();
    return fispresent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fischosen createFischosen()
  {
    FischosenImpl fischosen = new FischosenImpl();
    return fischosen;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fisvalue createFisvalue()
  {
    FisvalueImpl fisvalue = new FisvalueImpl();
    return fisvalue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fisbound createFisbound()
  {
    FisboundImpl fisbound = new FisboundImpl();
    return fisbound;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fregexp createFregexp()
  {
    FregexpImpl fregexp = new FregexpImpl();
    return fregexp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fsubstr createFsubstr()
  {
    FsubstrImpl fsubstr = new FsubstrImpl();
    return fsubstr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Freplace createFreplace()
  {
    FreplaceImpl freplace = new FreplaceImpl();
    return freplace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fencvalue createFencvalue()
  {
    FencvalueImpl fencvalue = new FencvalueImpl();
    return fencvalue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fdecvalue createFdecvalue()
  {
    FdecvalueImpl fdecvalue = new FdecvalueImpl();
    return fdecvalue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FencvalueUnichar createFencvalueUnichar()
  {
    FencvalueUnicharImpl fencvalueUnichar = new FencvalueUnicharImpl();
    return fencvalueUnichar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FdecvalueUnichar createFdecvalueUnichar()
  {
    FdecvalueUnicharImpl fdecvalueUnichar = new FdecvalueUnicharImpl();
    return fdecvalueUnichar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Frnd createFrnd()
  {
    FrndImpl frnd = new FrndImpl();
    return frnd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Ftestcasename createFtestcasename()
  {
    FtestcasenameImpl ftestcasename = new FtestcasenameImpl();
    return ftestcasename;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XorExpression createXorExpression()
  {
    XorExpressionImpl xorExpression = new XorExpressionImpl();
    return xorExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AndExpression createAndExpression()
  {
    AndExpressionImpl andExpression = new AndExpressionImpl();
    return andExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EqualExpression createEqualExpression()
  {
    EqualExpressionImpl equalExpression = new EqualExpressionImpl();
    return equalExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RelExpression createRelExpression()
  {
    RelExpressionImpl relExpression = new RelExpressionImpl();
    return relExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ShiftExpression createShiftExpression()
  {
    ShiftExpressionImpl shiftExpression = new ShiftExpressionImpl();
    return shiftExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BitOrExpression createBitOrExpression()
  {
    BitOrExpressionImpl bitOrExpression = new BitOrExpressionImpl();
    return bitOrExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BitXorExpression createBitXorExpression()
  {
    BitXorExpressionImpl bitXorExpression = new BitXorExpressionImpl();
    return bitXorExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BitAndExpression createBitAndExpression()
  {
    BitAndExpressionImpl bitAndExpression = new BitAndExpressionImpl();
    return bitAndExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AddExpression createAddExpression()
  {
    AddExpressionImpl addExpression = new AddExpressionImpl();
    return addExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MulExpression createMulExpression()
  {
    MulExpressionImpl mulExpression = new MulExpressionImpl();
    return mulExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Visibility createVisibilityFromString(EDataType eDataType, String initialValue)
  {
    Visibility result = Visibility.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertVisibilityToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VerdictTypeValue createVerdictTypeValueFromString(EDataType eDataType, String initialValue)
  {
    VerdictTypeValue result = VerdictTypeValue.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertVerdictTypeValueToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TTCN3Package getTTCN3Package()
  {
    return (TTCN3Package)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static TTCN3Package getPackage()
  {
    return TTCN3Package.eINSTANCE;
  }

} //TTCN3FactoryImpl
