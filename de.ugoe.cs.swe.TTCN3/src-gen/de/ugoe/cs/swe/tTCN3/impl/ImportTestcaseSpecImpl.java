/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept;
import de.ugoe.cs.swe.tTCN3.ImportTestcaseSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Import Testcase Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportTestcaseSpecImpl#getIdOrAll <em>Id Or All</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ImportTestcaseSpecImpl extends MinimalEObjectImpl.Container implements ImportTestcaseSpec
{
  /**
   * The cached value of the '{@link #getIdOrAll() <em>Id Or All</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdOrAll()
   * @generated
   * @ordered
   */
  protected IdentifierListOrAllWithExcept idOrAll;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ImportTestcaseSpecImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getImportTestcaseSpec();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifierListOrAllWithExcept getIdOrAll()
  {
    return idOrAll;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIdOrAll(IdentifierListOrAllWithExcept newIdOrAll, NotificationChain msgs)
  {
    IdentifierListOrAllWithExcept oldIdOrAll = idOrAll;
    idOrAll = newIdOrAll;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_TESTCASE_SPEC__ID_OR_ALL, oldIdOrAll, newIdOrAll);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIdOrAll(IdentifierListOrAllWithExcept newIdOrAll)
  {
    if (newIdOrAll != idOrAll)
    {
      NotificationChain msgs = null;
      if (idOrAll != null)
        msgs = ((InternalEObject)idOrAll).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_TESTCASE_SPEC__ID_OR_ALL, null, msgs);
      if (newIdOrAll != null)
        msgs = ((InternalEObject)newIdOrAll).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_TESTCASE_SPEC__ID_OR_ALL, null, msgs);
      msgs = basicSetIdOrAll(newIdOrAll, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_TESTCASE_SPEC__ID_OR_ALL, newIdOrAll, newIdOrAll));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_TESTCASE_SPEC__ID_OR_ALL:
        return basicSetIdOrAll(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_TESTCASE_SPEC__ID_OR_ALL:
        return getIdOrAll();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_TESTCASE_SPEC__ID_OR_ALL:
        setIdOrAll((IdentifierListOrAllWithExcept)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_TESTCASE_SPEC__ID_OR_ALL:
        setIdOrAll((IdentifierListOrAllWithExcept)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_TESTCASE_SPEC__ID_OR_ALL:
        return idOrAll != null;
    }
    return super.eIsSet(featureID);
  }

} //ImportTestcaseSpecImpl
