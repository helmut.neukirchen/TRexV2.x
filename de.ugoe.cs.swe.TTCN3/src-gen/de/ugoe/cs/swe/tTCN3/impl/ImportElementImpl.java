/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ImportAltstepSpec;
import de.ugoe.cs.swe.tTCN3.ImportConstSpec;
import de.ugoe.cs.swe.tTCN3.ImportElement;
import de.ugoe.cs.swe.tTCN3.ImportFunctionSpec;
import de.ugoe.cs.swe.tTCN3.ImportGroupSpec;
import de.ugoe.cs.swe.tTCN3.ImportModuleParSpec;
import de.ugoe.cs.swe.tTCN3.ImportSignatureSpec;
import de.ugoe.cs.swe.tTCN3.ImportTemplateSpec;
import de.ugoe.cs.swe.tTCN3.ImportTestcaseSpec;
import de.ugoe.cs.swe.tTCN3.ImportTypeDefSpec;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Import Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl#getConst <em>Const</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl#getTestcase <em>Testcase</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl#getAltstep <em>Altstep</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl#getFunction <em>Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl#getSignature <em>Signature</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl#getModulePar <em>Module Par</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ImportElementImpl#getImportSpec <em>Import Spec</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ImportElementImpl extends MinimalEObjectImpl.Container implements ImportElement
{
  /**
   * The cached value of the '{@link #getGroup() <em>Group</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroup()
   * @generated
   * @ordered
   */
  protected ImportGroupSpec group;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected ImportTypeDefSpec type;

  /**
   * The cached value of the '{@link #getTemplate() <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplate()
   * @generated
   * @ordered
   */
  protected ImportTemplateSpec template;

  /**
   * The cached value of the '{@link #getConst() <em>Const</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConst()
   * @generated
   * @ordered
   */
  protected ImportConstSpec const_;

  /**
   * The cached value of the '{@link #getTestcase() <em>Testcase</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTestcase()
   * @generated
   * @ordered
   */
  protected ImportTestcaseSpec testcase;

  /**
   * The cached value of the '{@link #getAltstep() <em>Altstep</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAltstep()
   * @generated
   * @ordered
   */
  protected ImportAltstepSpec altstep;

  /**
   * The cached value of the '{@link #getFunction() <em>Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunction()
   * @generated
   * @ordered
   */
  protected ImportFunctionSpec function;

  /**
   * The cached value of the '{@link #getSignature() <em>Signature</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSignature()
   * @generated
   * @ordered
   */
  protected ImportSignatureSpec signature;

  /**
   * The cached value of the '{@link #getModulePar() <em>Module Par</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModulePar()
   * @generated
   * @ordered
   */
  protected ImportModuleParSpec modulePar;

  /**
   * The default value of the '{@link #getImportSpec() <em>Import Spec</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getImportSpec()
   * @generated
   * @ordered
   */
  protected static final String IMPORT_SPEC_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getImportSpec() <em>Import Spec</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getImportSpec()
   * @generated
   * @ordered
   */
  protected String importSpec = IMPORT_SPEC_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ImportElementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getImportElement();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportGroupSpec getGroup()
  {
    return group;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGroup(ImportGroupSpec newGroup, NotificationChain msgs)
  {
    ImportGroupSpec oldGroup = group;
    group = newGroup;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__GROUP, oldGroup, newGroup);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGroup(ImportGroupSpec newGroup)
  {
    if (newGroup != group)
    {
      NotificationChain msgs = null;
      if (group != null)
        msgs = ((InternalEObject)group).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__GROUP, null, msgs);
      if (newGroup != null)
        msgs = ((InternalEObject)newGroup).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__GROUP, null, msgs);
      msgs = basicSetGroup(newGroup, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__GROUP, newGroup, newGroup));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportTypeDefSpec getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(ImportTypeDefSpec newType, NotificationChain msgs)
  {
    ImportTypeDefSpec oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(ImportTypeDefSpec newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportTemplateSpec getTemplate()
  {
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTemplate(ImportTemplateSpec newTemplate, NotificationChain msgs)
  {
    ImportTemplateSpec oldTemplate = template;
    template = newTemplate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__TEMPLATE, oldTemplate, newTemplate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTemplate(ImportTemplateSpec newTemplate)
  {
    if (newTemplate != template)
    {
      NotificationChain msgs = null;
      if (template != null)
        msgs = ((InternalEObject)template).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__TEMPLATE, null, msgs);
      if (newTemplate != null)
        msgs = ((InternalEObject)newTemplate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__TEMPLATE, null, msgs);
      msgs = basicSetTemplate(newTemplate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__TEMPLATE, newTemplate, newTemplate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportConstSpec getConst()
  {
    return const_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetConst(ImportConstSpec newConst, NotificationChain msgs)
  {
    ImportConstSpec oldConst = const_;
    const_ = newConst;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__CONST, oldConst, newConst);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConst(ImportConstSpec newConst)
  {
    if (newConst != const_)
    {
      NotificationChain msgs = null;
      if (const_ != null)
        msgs = ((InternalEObject)const_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__CONST, null, msgs);
      if (newConst != null)
        msgs = ((InternalEObject)newConst).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__CONST, null, msgs);
      msgs = basicSetConst(newConst, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__CONST, newConst, newConst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportTestcaseSpec getTestcase()
  {
    return testcase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTestcase(ImportTestcaseSpec newTestcase, NotificationChain msgs)
  {
    ImportTestcaseSpec oldTestcase = testcase;
    testcase = newTestcase;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__TESTCASE, oldTestcase, newTestcase);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTestcase(ImportTestcaseSpec newTestcase)
  {
    if (newTestcase != testcase)
    {
      NotificationChain msgs = null;
      if (testcase != null)
        msgs = ((InternalEObject)testcase).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__TESTCASE, null, msgs);
      if (newTestcase != null)
        msgs = ((InternalEObject)newTestcase).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__TESTCASE, null, msgs);
      msgs = basicSetTestcase(newTestcase, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__TESTCASE, newTestcase, newTestcase));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportAltstepSpec getAltstep()
  {
    return altstep;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAltstep(ImportAltstepSpec newAltstep, NotificationChain msgs)
  {
    ImportAltstepSpec oldAltstep = altstep;
    altstep = newAltstep;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__ALTSTEP, oldAltstep, newAltstep);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAltstep(ImportAltstepSpec newAltstep)
  {
    if (newAltstep != altstep)
    {
      NotificationChain msgs = null;
      if (altstep != null)
        msgs = ((InternalEObject)altstep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__ALTSTEP, null, msgs);
      if (newAltstep != null)
        msgs = ((InternalEObject)newAltstep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__ALTSTEP, null, msgs);
      msgs = basicSetAltstep(newAltstep, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__ALTSTEP, newAltstep, newAltstep));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportFunctionSpec getFunction()
  {
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFunction(ImportFunctionSpec newFunction, NotificationChain msgs)
  {
    ImportFunctionSpec oldFunction = function;
    function = newFunction;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__FUNCTION, oldFunction, newFunction);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFunction(ImportFunctionSpec newFunction)
  {
    if (newFunction != function)
    {
      NotificationChain msgs = null;
      if (function != null)
        msgs = ((InternalEObject)function).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__FUNCTION, null, msgs);
      if (newFunction != null)
        msgs = ((InternalEObject)newFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__FUNCTION, null, msgs);
      msgs = basicSetFunction(newFunction, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__FUNCTION, newFunction, newFunction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportSignatureSpec getSignature()
  {
    return signature;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSignature(ImportSignatureSpec newSignature, NotificationChain msgs)
  {
    ImportSignatureSpec oldSignature = signature;
    signature = newSignature;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__SIGNATURE, oldSignature, newSignature);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSignature(ImportSignatureSpec newSignature)
  {
    if (newSignature != signature)
    {
      NotificationChain msgs = null;
      if (signature != null)
        msgs = ((InternalEObject)signature).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__SIGNATURE, null, msgs);
      if (newSignature != null)
        msgs = ((InternalEObject)newSignature).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__SIGNATURE, null, msgs);
      msgs = basicSetSignature(newSignature, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__SIGNATURE, newSignature, newSignature));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImportModuleParSpec getModulePar()
  {
    return modulePar;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetModulePar(ImportModuleParSpec newModulePar, NotificationChain msgs)
  {
    ImportModuleParSpec oldModulePar = modulePar;
    modulePar = newModulePar;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__MODULE_PAR, oldModulePar, newModulePar);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModulePar(ImportModuleParSpec newModulePar)
  {
    if (newModulePar != modulePar)
    {
      NotificationChain msgs = null;
      if (modulePar != null)
        msgs = ((InternalEObject)modulePar).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__MODULE_PAR, null, msgs);
      if (newModulePar != null)
        msgs = ((InternalEObject)newModulePar).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IMPORT_ELEMENT__MODULE_PAR, null, msgs);
      msgs = basicSetModulePar(newModulePar, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__MODULE_PAR, newModulePar, newModulePar));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getImportSpec()
  {
    return importSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setImportSpec(String newImportSpec)
  {
    String oldImportSpec = importSpec;
    importSpec = newImportSpec;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IMPORT_ELEMENT__IMPORT_SPEC, oldImportSpec, importSpec));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_ELEMENT__GROUP:
        return basicSetGroup(null, msgs);
      case TTCN3Package.IMPORT_ELEMENT__TYPE:
        return basicSetType(null, msgs);
      case TTCN3Package.IMPORT_ELEMENT__TEMPLATE:
        return basicSetTemplate(null, msgs);
      case TTCN3Package.IMPORT_ELEMENT__CONST:
        return basicSetConst(null, msgs);
      case TTCN3Package.IMPORT_ELEMENT__TESTCASE:
        return basicSetTestcase(null, msgs);
      case TTCN3Package.IMPORT_ELEMENT__ALTSTEP:
        return basicSetAltstep(null, msgs);
      case TTCN3Package.IMPORT_ELEMENT__FUNCTION:
        return basicSetFunction(null, msgs);
      case TTCN3Package.IMPORT_ELEMENT__SIGNATURE:
        return basicSetSignature(null, msgs);
      case TTCN3Package.IMPORT_ELEMENT__MODULE_PAR:
        return basicSetModulePar(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_ELEMENT__GROUP:
        return getGroup();
      case TTCN3Package.IMPORT_ELEMENT__TYPE:
        return getType();
      case TTCN3Package.IMPORT_ELEMENT__TEMPLATE:
        return getTemplate();
      case TTCN3Package.IMPORT_ELEMENT__CONST:
        return getConst();
      case TTCN3Package.IMPORT_ELEMENT__TESTCASE:
        return getTestcase();
      case TTCN3Package.IMPORT_ELEMENT__ALTSTEP:
        return getAltstep();
      case TTCN3Package.IMPORT_ELEMENT__FUNCTION:
        return getFunction();
      case TTCN3Package.IMPORT_ELEMENT__SIGNATURE:
        return getSignature();
      case TTCN3Package.IMPORT_ELEMENT__MODULE_PAR:
        return getModulePar();
      case TTCN3Package.IMPORT_ELEMENT__IMPORT_SPEC:
        return getImportSpec();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_ELEMENT__GROUP:
        setGroup((ImportGroupSpec)newValue);
        return;
      case TTCN3Package.IMPORT_ELEMENT__TYPE:
        setType((ImportTypeDefSpec)newValue);
        return;
      case TTCN3Package.IMPORT_ELEMENT__TEMPLATE:
        setTemplate((ImportTemplateSpec)newValue);
        return;
      case TTCN3Package.IMPORT_ELEMENT__CONST:
        setConst((ImportConstSpec)newValue);
        return;
      case TTCN3Package.IMPORT_ELEMENT__TESTCASE:
        setTestcase((ImportTestcaseSpec)newValue);
        return;
      case TTCN3Package.IMPORT_ELEMENT__ALTSTEP:
        setAltstep((ImportAltstepSpec)newValue);
        return;
      case TTCN3Package.IMPORT_ELEMENT__FUNCTION:
        setFunction((ImportFunctionSpec)newValue);
        return;
      case TTCN3Package.IMPORT_ELEMENT__SIGNATURE:
        setSignature((ImportSignatureSpec)newValue);
        return;
      case TTCN3Package.IMPORT_ELEMENT__MODULE_PAR:
        setModulePar((ImportModuleParSpec)newValue);
        return;
      case TTCN3Package.IMPORT_ELEMENT__IMPORT_SPEC:
        setImportSpec((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_ELEMENT__GROUP:
        setGroup((ImportGroupSpec)null);
        return;
      case TTCN3Package.IMPORT_ELEMENT__TYPE:
        setType((ImportTypeDefSpec)null);
        return;
      case TTCN3Package.IMPORT_ELEMENT__TEMPLATE:
        setTemplate((ImportTemplateSpec)null);
        return;
      case TTCN3Package.IMPORT_ELEMENT__CONST:
        setConst((ImportConstSpec)null);
        return;
      case TTCN3Package.IMPORT_ELEMENT__TESTCASE:
        setTestcase((ImportTestcaseSpec)null);
        return;
      case TTCN3Package.IMPORT_ELEMENT__ALTSTEP:
        setAltstep((ImportAltstepSpec)null);
        return;
      case TTCN3Package.IMPORT_ELEMENT__FUNCTION:
        setFunction((ImportFunctionSpec)null);
        return;
      case TTCN3Package.IMPORT_ELEMENT__SIGNATURE:
        setSignature((ImportSignatureSpec)null);
        return;
      case TTCN3Package.IMPORT_ELEMENT__MODULE_PAR:
        setModulePar((ImportModuleParSpec)null);
        return;
      case TTCN3Package.IMPORT_ELEMENT__IMPORT_SPEC:
        setImportSpec(IMPORT_SPEC_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.IMPORT_ELEMENT__GROUP:
        return group != null;
      case TTCN3Package.IMPORT_ELEMENT__TYPE:
        return type != null;
      case TTCN3Package.IMPORT_ELEMENT__TEMPLATE:
        return template != null;
      case TTCN3Package.IMPORT_ELEMENT__CONST:
        return const_ != null;
      case TTCN3Package.IMPORT_ELEMENT__TESTCASE:
        return testcase != null;
      case TTCN3Package.IMPORT_ELEMENT__ALTSTEP:
        return altstep != null;
      case TTCN3Package.IMPORT_ELEMENT__FUNCTION:
        return function != null;
      case TTCN3Package.IMPORT_ELEMENT__SIGNATURE:
        return signature != null;
      case TTCN3Package.IMPORT_ELEMENT__MODULE_PAR:
        return modulePar != null;
      case TTCN3Package.IMPORT_ELEMENT__IMPORT_SPEC:
        return IMPORT_SPEC_EDEFAULT == null ? importSpec != null : !IMPORT_SPEC_EDEFAULT.equals(importSpec);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (importSpec: ");
    result.append(importSpec);
    result.append(')');
    return result.toString();
  }

} //ImportElementImpl
