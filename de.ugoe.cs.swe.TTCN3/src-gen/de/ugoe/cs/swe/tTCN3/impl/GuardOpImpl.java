/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.CatchStatement;
import de.ugoe.cs.swe.tTCN3.CheckStatement;
import de.ugoe.cs.swe.tTCN3.DoneStatement;
import de.ugoe.cs.swe.tTCN3.GetCallStatement;
import de.ugoe.cs.swe.tTCN3.GetReplyStatement;
import de.ugoe.cs.swe.tTCN3.GuardOp;
import de.ugoe.cs.swe.tTCN3.KilledStatement;
import de.ugoe.cs.swe.tTCN3.ReceiveStatement;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TimeoutStatement;
import de.ugoe.cs.swe.tTCN3.TriggerStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Guard Op</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl#getTimeout <em>Timeout</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl#getReceive <em>Receive</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl#getGetCall <em>Get Call</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl#getCatch <em>Catch</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl#getCheck <em>Check</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl#getGetReply <em>Get Reply</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl#getDone <em>Done</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.GuardOpImpl#getKilled <em>Killed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GuardOpImpl extends MinimalEObjectImpl.Container implements GuardOp
{
  /**
   * The cached value of the '{@link #getTimeout() <em>Timeout</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimeout()
   * @generated
   * @ordered
   */
  protected TimeoutStatement timeout;

  /**
   * The cached value of the '{@link #getReceive() <em>Receive</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReceive()
   * @generated
   * @ordered
   */
  protected ReceiveStatement receive;

  /**
   * The cached value of the '{@link #getTrigger() <em>Trigger</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTrigger()
   * @generated
   * @ordered
   */
  protected TriggerStatement trigger;

  /**
   * The cached value of the '{@link #getGetCall() <em>Get Call</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGetCall()
   * @generated
   * @ordered
   */
  protected GetCallStatement getCall;

  /**
   * The cached value of the '{@link #getCatch() <em>Catch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCatch()
   * @generated
   * @ordered
   */
  protected CatchStatement catch_;

  /**
   * The cached value of the '{@link #getCheck() <em>Check</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCheck()
   * @generated
   * @ordered
   */
  protected CheckStatement check;

  /**
   * The cached value of the '{@link #getGetReply() <em>Get Reply</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGetReply()
   * @generated
   * @ordered
   */
  protected GetReplyStatement getReply;

  /**
   * The cached value of the '{@link #getDone() <em>Done</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDone()
   * @generated
   * @ordered
   */
  protected DoneStatement done;

  /**
   * The cached value of the '{@link #getKilled() <em>Killed</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKilled()
   * @generated
   * @ordered
   */
  protected KilledStatement killed;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GuardOpImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getGuardOp();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimeoutStatement getTimeout()
  {
    return timeout;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTimeout(TimeoutStatement newTimeout, NotificationChain msgs)
  {
    TimeoutStatement oldTimeout = timeout;
    timeout = newTimeout;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__TIMEOUT, oldTimeout, newTimeout);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimeout(TimeoutStatement newTimeout)
  {
    if (newTimeout != timeout)
    {
      NotificationChain msgs = null;
      if (timeout != null)
        msgs = ((InternalEObject)timeout).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__TIMEOUT, null, msgs);
      if (newTimeout != null)
        msgs = ((InternalEObject)newTimeout).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__TIMEOUT, null, msgs);
      msgs = basicSetTimeout(newTimeout, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__TIMEOUT, newTimeout, newTimeout));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReceiveStatement getReceive()
  {
    return receive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReceive(ReceiveStatement newReceive, NotificationChain msgs)
  {
    ReceiveStatement oldReceive = receive;
    receive = newReceive;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__RECEIVE, oldReceive, newReceive);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReceive(ReceiveStatement newReceive)
  {
    if (newReceive != receive)
    {
      NotificationChain msgs = null;
      if (receive != null)
        msgs = ((InternalEObject)receive).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__RECEIVE, null, msgs);
      if (newReceive != null)
        msgs = ((InternalEObject)newReceive).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__RECEIVE, null, msgs);
      msgs = basicSetReceive(newReceive, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__RECEIVE, newReceive, newReceive));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TriggerStatement getTrigger()
  {
    return trigger;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTrigger(TriggerStatement newTrigger, NotificationChain msgs)
  {
    TriggerStatement oldTrigger = trigger;
    trigger = newTrigger;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__TRIGGER, oldTrigger, newTrigger);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTrigger(TriggerStatement newTrigger)
  {
    if (newTrigger != trigger)
    {
      NotificationChain msgs = null;
      if (trigger != null)
        msgs = ((InternalEObject)trigger).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__TRIGGER, null, msgs);
      if (newTrigger != null)
        msgs = ((InternalEObject)newTrigger).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__TRIGGER, null, msgs);
      msgs = basicSetTrigger(newTrigger, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__TRIGGER, newTrigger, newTrigger));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GetCallStatement getGetCall()
  {
    return getCall;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGetCall(GetCallStatement newGetCall, NotificationChain msgs)
  {
    GetCallStatement oldGetCall = getCall;
    getCall = newGetCall;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__GET_CALL, oldGetCall, newGetCall);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGetCall(GetCallStatement newGetCall)
  {
    if (newGetCall != getCall)
    {
      NotificationChain msgs = null;
      if (getCall != null)
        msgs = ((InternalEObject)getCall).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__GET_CALL, null, msgs);
      if (newGetCall != null)
        msgs = ((InternalEObject)newGetCall).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__GET_CALL, null, msgs);
      msgs = basicSetGetCall(newGetCall, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__GET_CALL, newGetCall, newGetCall));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CatchStatement getCatch()
  {
    return catch_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCatch(CatchStatement newCatch, NotificationChain msgs)
  {
    CatchStatement oldCatch = catch_;
    catch_ = newCatch;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__CATCH, oldCatch, newCatch);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCatch(CatchStatement newCatch)
  {
    if (newCatch != catch_)
    {
      NotificationChain msgs = null;
      if (catch_ != null)
        msgs = ((InternalEObject)catch_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__CATCH, null, msgs);
      if (newCatch != null)
        msgs = ((InternalEObject)newCatch).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__CATCH, null, msgs);
      msgs = basicSetCatch(newCatch, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__CATCH, newCatch, newCatch));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CheckStatement getCheck()
  {
    return check;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCheck(CheckStatement newCheck, NotificationChain msgs)
  {
    CheckStatement oldCheck = check;
    check = newCheck;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__CHECK, oldCheck, newCheck);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCheck(CheckStatement newCheck)
  {
    if (newCheck != check)
    {
      NotificationChain msgs = null;
      if (check != null)
        msgs = ((InternalEObject)check).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__CHECK, null, msgs);
      if (newCheck != null)
        msgs = ((InternalEObject)newCheck).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__CHECK, null, msgs);
      msgs = basicSetCheck(newCheck, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__CHECK, newCheck, newCheck));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GetReplyStatement getGetReply()
  {
    return getReply;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetGetReply(GetReplyStatement newGetReply, NotificationChain msgs)
  {
    GetReplyStatement oldGetReply = getReply;
    getReply = newGetReply;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__GET_REPLY, oldGetReply, newGetReply);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGetReply(GetReplyStatement newGetReply)
  {
    if (newGetReply != getReply)
    {
      NotificationChain msgs = null;
      if (getReply != null)
        msgs = ((InternalEObject)getReply).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__GET_REPLY, null, msgs);
      if (newGetReply != null)
        msgs = ((InternalEObject)newGetReply).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__GET_REPLY, null, msgs);
      msgs = basicSetGetReply(newGetReply, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__GET_REPLY, newGetReply, newGetReply));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DoneStatement getDone()
  {
    return done;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDone(DoneStatement newDone, NotificationChain msgs)
  {
    DoneStatement oldDone = done;
    done = newDone;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__DONE, oldDone, newDone);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDone(DoneStatement newDone)
  {
    if (newDone != done)
    {
      NotificationChain msgs = null;
      if (done != null)
        msgs = ((InternalEObject)done).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__DONE, null, msgs);
      if (newDone != null)
        msgs = ((InternalEObject)newDone).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__DONE, null, msgs);
      msgs = basicSetDone(newDone, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__DONE, newDone, newDone));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public KilledStatement getKilled()
  {
    return killed;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetKilled(KilledStatement newKilled, NotificationChain msgs)
  {
    KilledStatement oldKilled = killed;
    killed = newKilled;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__KILLED, oldKilled, newKilled);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setKilled(KilledStatement newKilled)
  {
    if (newKilled != killed)
    {
      NotificationChain msgs = null;
      if (killed != null)
        msgs = ((InternalEObject)killed).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__KILLED, null, msgs);
      if (newKilled != null)
        msgs = ((InternalEObject)newKilled).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.GUARD_OP__KILLED, null, msgs);
      msgs = basicSetKilled(newKilled, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.GUARD_OP__KILLED, newKilled, newKilled));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.GUARD_OP__TIMEOUT:
        return basicSetTimeout(null, msgs);
      case TTCN3Package.GUARD_OP__RECEIVE:
        return basicSetReceive(null, msgs);
      case TTCN3Package.GUARD_OP__TRIGGER:
        return basicSetTrigger(null, msgs);
      case TTCN3Package.GUARD_OP__GET_CALL:
        return basicSetGetCall(null, msgs);
      case TTCN3Package.GUARD_OP__CATCH:
        return basicSetCatch(null, msgs);
      case TTCN3Package.GUARD_OP__CHECK:
        return basicSetCheck(null, msgs);
      case TTCN3Package.GUARD_OP__GET_REPLY:
        return basicSetGetReply(null, msgs);
      case TTCN3Package.GUARD_OP__DONE:
        return basicSetDone(null, msgs);
      case TTCN3Package.GUARD_OP__KILLED:
        return basicSetKilled(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.GUARD_OP__TIMEOUT:
        return getTimeout();
      case TTCN3Package.GUARD_OP__RECEIVE:
        return getReceive();
      case TTCN3Package.GUARD_OP__TRIGGER:
        return getTrigger();
      case TTCN3Package.GUARD_OP__GET_CALL:
        return getGetCall();
      case TTCN3Package.GUARD_OP__CATCH:
        return getCatch();
      case TTCN3Package.GUARD_OP__CHECK:
        return getCheck();
      case TTCN3Package.GUARD_OP__GET_REPLY:
        return getGetReply();
      case TTCN3Package.GUARD_OP__DONE:
        return getDone();
      case TTCN3Package.GUARD_OP__KILLED:
        return getKilled();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.GUARD_OP__TIMEOUT:
        setTimeout((TimeoutStatement)newValue);
        return;
      case TTCN3Package.GUARD_OP__RECEIVE:
        setReceive((ReceiveStatement)newValue);
        return;
      case TTCN3Package.GUARD_OP__TRIGGER:
        setTrigger((TriggerStatement)newValue);
        return;
      case TTCN3Package.GUARD_OP__GET_CALL:
        setGetCall((GetCallStatement)newValue);
        return;
      case TTCN3Package.GUARD_OP__CATCH:
        setCatch((CatchStatement)newValue);
        return;
      case TTCN3Package.GUARD_OP__CHECK:
        setCheck((CheckStatement)newValue);
        return;
      case TTCN3Package.GUARD_OP__GET_REPLY:
        setGetReply((GetReplyStatement)newValue);
        return;
      case TTCN3Package.GUARD_OP__DONE:
        setDone((DoneStatement)newValue);
        return;
      case TTCN3Package.GUARD_OP__KILLED:
        setKilled((KilledStatement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.GUARD_OP__TIMEOUT:
        setTimeout((TimeoutStatement)null);
        return;
      case TTCN3Package.GUARD_OP__RECEIVE:
        setReceive((ReceiveStatement)null);
        return;
      case TTCN3Package.GUARD_OP__TRIGGER:
        setTrigger((TriggerStatement)null);
        return;
      case TTCN3Package.GUARD_OP__GET_CALL:
        setGetCall((GetCallStatement)null);
        return;
      case TTCN3Package.GUARD_OP__CATCH:
        setCatch((CatchStatement)null);
        return;
      case TTCN3Package.GUARD_OP__CHECK:
        setCheck((CheckStatement)null);
        return;
      case TTCN3Package.GUARD_OP__GET_REPLY:
        setGetReply((GetReplyStatement)null);
        return;
      case TTCN3Package.GUARD_OP__DONE:
        setDone((DoneStatement)null);
        return;
      case TTCN3Package.GUARD_OP__KILLED:
        setKilled((KilledStatement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.GUARD_OP__TIMEOUT:
        return timeout != null;
      case TTCN3Package.GUARD_OP__RECEIVE:
        return receive != null;
      case TTCN3Package.GUARD_OP__TRIGGER:
        return trigger != null;
      case TTCN3Package.GUARD_OP__GET_CALL:
        return getCall != null;
      case TTCN3Package.GUARD_OP__CATCH:
        return catch_ != null;
      case TTCN3Package.GUARD_OP__CHECK:
        return check != null;
      case TTCN3Package.GUARD_OP__GET_REPLY:
        return getReply != null;
      case TTCN3Package.GUARD_OP__DONE:
        return done != null;
      case TTCN3Package.GUARD_OP__KILLED:
        return killed != null;
    }
    return super.eIsSet(featureID);
  }

} //GuardOpImpl
