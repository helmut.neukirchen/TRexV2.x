/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ArrayOrBitRef;
import de.ugoe.cs.swe.tTCN3.FieldExpressionSpec;
import de.ugoe.cs.swe.tTCN3.FieldReference;
import de.ugoe.cs.swe.tTCN3.NotUsedOrExpression;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Field Expression Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FieldExpressionSpecImpl#getFieldRef <em>Field Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FieldExpressionSpecImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FieldExpressionSpecImpl#getArray <em>Array</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.FieldExpressionSpecImpl#getExpr <em>Expr</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FieldExpressionSpecImpl extends MinimalEObjectImpl.Container implements FieldExpressionSpec
{
  /**
   * The cached value of the '{@link #getFieldRef() <em>Field Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFieldRef()
   * @generated
   * @ordered
   */
  protected FieldReference fieldRef;

  /**
   * The default value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected static final String TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected String type = TYPE_EDEFAULT;

  /**
   * The cached value of the '{@link #getArray() <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArray()
   * @generated
   * @ordered
   */
  protected ArrayOrBitRef array;

  /**
   * The cached value of the '{@link #getExpr() <em>Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpr()
   * @generated
   * @ordered
   */
  protected NotUsedOrExpression expr;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FieldExpressionSpecImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getFieldExpressionSpec();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldReference getFieldRef()
  {
    if (fieldRef != null && fieldRef.eIsProxy())
    {
      InternalEObject oldFieldRef = (InternalEObject)fieldRef;
      fieldRef = (FieldReference)eResolveProxy(oldFieldRef);
      if (fieldRef != oldFieldRef)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, TTCN3Package.FIELD_EXPRESSION_SPEC__FIELD_REF, oldFieldRef, fieldRef));
      }
    }
    return fieldRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldReference basicGetFieldRef()
  {
    return fieldRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFieldRef(FieldReference newFieldRef)
  {
    FieldReference oldFieldRef = fieldRef;
    fieldRef = newFieldRef;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FIELD_EXPRESSION_SPEC__FIELD_REF, oldFieldRef, fieldRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(String newType)
  {
    String oldType = type;
    type = newType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FIELD_EXPRESSION_SPEC__TYPE, oldType, type));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayOrBitRef getArray()
  {
    return array;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetArray(ArrayOrBitRef newArray, NotificationChain msgs)
  {
    ArrayOrBitRef oldArray = array;
    array = newArray;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FIELD_EXPRESSION_SPEC__ARRAY, oldArray, newArray);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setArray(ArrayOrBitRef newArray)
  {
    if (newArray != array)
    {
      NotificationChain msgs = null;
      if (array != null)
        msgs = ((InternalEObject)array).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FIELD_EXPRESSION_SPEC__ARRAY, null, msgs);
      if (newArray != null)
        msgs = ((InternalEObject)newArray).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FIELD_EXPRESSION_SPEC__ARRAY, null, msgs);
      msgs = basicSetArray(newArray, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FIELD_EXPRESSION_SPEC__ARRAY, newArray, newArray));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotUsedOrExpression getExpr()
  {
    return expr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpr(NotUsedOrExpression newExpr, NotificationChain msgs)
  {
    NotUsedOrExpression oldExpr = expr;
    expr = newExpr;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.FIELD_EXPRESSION_SPEC__EXPR, oldExpr, newExpr);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpr(NotUsedOrExpression newExpr)
  {
    if (newExpr != expr)
    {
      NotificationChain msgs = null;
      if (expr != null)
        msgs = ((InternalEObject)expr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FIELD_EXPRESSION_SPEC__EXPR, null, msgs);
      if (newExpr != null)
        msgs = ((InternalEObject)newExpr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.FIELD_EXPRESSION_SPEC__EXPR, null, msgs);
      msgs = basicSetExpr(newExpr, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.FIELD_EXPRESSION_SPEC__EXPR, newExpr, newExpr));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.FIELD_EXPRESSION_SPEC__ARRAY:
        return basicSetArray(null, msgs);
      case TTCN3Package.FIELD_EXPRESSION_SPEC__EXPR:
        return basicSetExpr(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.FIELD_EXPRESSION_SPEC__FIELD_REF:
        if (resolve) return getFieldRef();
        return basicGetFieldRef();
      case TTCN3Package.FIELD_EXPRESSION_SPEC__TYPE:
        return getType();
      case TTCN3Package.FIELD_EXPRESSION_SPEC__ARRAY:
        return getArray();
      case TTCN3Package.FIELD_EXPRESSION_SPEC__EXPR:
        return getExpr();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.FIELD_EXPRESSION_SPEC__FIELD_REF:
        setFieldRef((FieldReference)newValue);
        return;
      case TTCN3Package.FIELD_EXPRESSION_SPEC__TYPE:
        setType((String)newValue);
        return;
      case TTCN3Package.FIELD_EXPRESSION_SPEC__ARRAY:
        setArray((ArrayOrBitRef)newValue);
        return;
      case TTCN3Package.FIELD_EXPRESSION_SPEC__EXPR:
        setExpr((NotUsedOrExpression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FIELD_EXPRESSION_SPEC__FIELD_REF:
        setFieldRef((FieldReference)null);
        return;
      case TTCN3Package.FIELD_EXPRESSION_SPEC__TYPE:
        setType(TYPE_EDEFAULT);
        return;
      case TTCN3Package.FIELD_EXPRESSION_SPEC__ARRAY:
        setArray((ArrayOrBitRef)null);
        return;
      case TTCN3Package.FIELD_EXPRESSION_SPEC__EXPR:
        setExpr((NotUsedOrExpression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.FIELD_EXPRESSION_SPEC__FIELD_REF:
        return fieldRef != null;
      case TTCN3Package.FIELD_EXPRESSION_SPEC__TYPE:
        return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
      case TTCN3Package.FIELD_EXPRESSION_SPEC__ARRAY:
        return array != null;
      case TTCN3Package.FIELD_EXPRESSION_SPEC__EXPR:
        return expr != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (type: ");
    result.append(type);
    result.append(')');
    return result.toString();
  }

} //FieldExpressionSpecImpl
