/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ArrayDef;
import de.ugoe.cs.swe.tTCN3.SingleTempVarInstance;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateBody;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Single Temp Var Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SingleTempVarInstanceImpl#getArray <em>Array</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SingleTempVarInstanceImpl#getAc <em>Ac</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.SingleTempVarInstanceImpl#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SingleTempVarInstanceImpl extends RefValueImpl implements SingleTempVarInstance
{
  /**
   * The cached value of the '{@link #getArray() <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArray()
   * @generated
   * @ordered
   */
  protected ArrayDef array;

  /**
   * The default value of the '{@link #getAc() <em>Ac</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAc()
   * @generated
   * @ordered
   */
  protected static final String AC_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAc() <em>Ac</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAc()
   * @generated
   * @ordered
   */
  protected String ac = AC_EDEFAULT;

  /**
   * The cached value of the '{@link #getTemplate() <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTemplate()
   * @generated
   * @ordered
   */
  protected TemplateBody template;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SingleTempVarInstanceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getSingleTempVarInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ArrayDef getArray()
  {
    return array;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetArray(ArrayDef newArray, NotificationChain msgs)
  {
    ArrayDef oldArray = array;
    array = newArray;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__ARRAY, oldArray, newArray);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setArray(ArrayDef newArray)
  {
    if (newArray != array)
    {
      NotificationChain msgs = null;
      if (array != null)
        msgs = ((InternalEObject)array).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__ARRAY, null, msgs);
      if (newArray != null)
        msgs = ((InternalEObject)newArray).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__ARRAY, null, msgs);
      msgs = basicSetArray(newArray, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__ARRAY, newArray, newArray));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAc()
  {
    return ac;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAc(String newAc)
  {
    String oldAc = ac;
    ac = newAc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__AC, oldAc, ac));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateBody getTemplate()
  {
    return template;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTemplate(TemplateBody newTemplate, NotificationChain msgs)
  {
    TemplateBody oldTemplate = template;
    template = newTemplate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__TEMPLATE, oldTemplate, newTemplate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTemplate(TemplateBody newTemplate)
  {
    if (newTemplate != template)
    {
      NotificationChain msgs = null;
      if (template != null)
        msgs = ((InternalEObject)template).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__TEMPLATE, null, msgs);
      if (newTemplate != null)
        msgs = ((InternalEObject)newTemplate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__TEMPLATE, null, msgs);
      msgs = basicSetTemplate(newTemplate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__TEMPLATE, newTemplate, newTemplate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__ARRAY:
        return basicSetArray(null, msgs);
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__TEMPLATE:
        return basicSetTemplate(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__ARRAY:
        return getArray();
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__AC:
        return getAc();
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__TEMPLATE:
        return getTemplate();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__ARRAY:
        setArray((ArrayDef)newValue);
        return;
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__AC:
        setAc((String)newValue);
        return;
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__TEMPLATE:
        setTemplate((TemplateBody)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__ARRAY:
        setArray((ArrayDef)null);
        return;
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__AC:
        setAc(AC_EDEFAULT);
        return;
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__TEMPLATE:
        setTemplate((TemplateBody)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__ARRAY:
        return array != null;
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__AC:
        return AC_EDEFAULT == null ? ac != null : !AC_EDEFAULT.equals(ac);
      case TTCN3Package.SINGLE_TEMP_VAR_INSTANCE__TEMPLATE:
        return template != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (ac: ");
    result.append(ac);
    result.append(')');
    return result.toString();
  }

} //SingleTempVarInstanceImpl
