/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ControlStatement;
import de.ugoe.cs.swe.tTCN3.ControlStatementOrDef;
import de.ugoe.cs.swe.tTCN3.FunctionLocalDef;
import de.ugoe.cs.swe.tTCN3.FunctionLocalInst;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.WithStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Statement Or Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefImpl#getLocalDef <em>Local Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefImpl#getLocalInst <em>Local Inst</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefImpl#getWithstm <em>Withstm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ControlStatementOrDefImpl#getControl <em>Control</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ControlStatementOrDefImpl extends MinimalEObjectImpl.Container implements ControlStatementOrDef
{
  /**
   * The cached value of the '{@link #getLocalDef() <em>Local Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocalDef()
   * @generated
   * @ordered
   */
  protected FunctionLocalDef localDef;

  /**
   * The cached value of the '{@link #getLocalInst() <em>Local Inst</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocalInst()
   * @generated
   * @ordered
   */
  protected FunctionLocalInst localInst;

  /**
   * The cached value of the '{@link #getWithstm() <em>Withstm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWithstm()
   * @generated
   * @ordered
   */
  protected WithStatement withstm;

  /**
   * The cached value of the '{@link #getControl() <em>Control</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getControl()
   * @generated
   * @ordered
   */
  protected ControlStatement control;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ControlStatementOrDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getControlStatementOrDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionLocalDef getLocalDef()
  {
    return localDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLocalDef(FunctionLocalDef newLocalDef, NotificationChain msgs)
  {
    FunctionLocalDef oldLocalDef = localDef;
    localDef = newLocalDef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_DEF, oldLocalDef, newLocalDef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLocalDef(FunctionLocalDef newLocalDef)
  {
    if (newLocalDef != localDef)
    {
      NotificationChain msgs = null;
      if (localDef != null)
        msgs = ((InternalEObject)localDef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_DEF, null, msgs);
      if (newLocalDef != null)
        msgs = ((InternalEObject)newLocalDef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_DEF, null, msgs);
      msgs = basicSetLocalDef(newLocalDef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_DEF, newLocalDef, newLocalDef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FunctionLocalInst getLocalInst()
  {
    return localInst;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLocalInst(FunctionLocalInst newLocalInst, NotificationChain msgs)
  {
    FunctionLocalInst oldLocalInst = localInst;
    localInst = newLocalInst;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_INST, oldLocalInst, newLocalInst);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLocalInst(FunctionLocalInst newLocalInst)
  {
    if (newLocalInst != localInst)
    {
      NotificationChain msgs = null;
      if (localInst != null)
        msgs = ((InternalEObject)localInst).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_INST, null, msgs);
      if (newLocalInst != null)
        msgs = ((InternalEObject)newLocalInst).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_INST, null, msgs);
      msgs = basicSetLocalInst(newLocalInst, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_INST, newLocalInst, newLocalInst));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WithStatement getWithstm()
  {
    return withstm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetWithstm(WithStatement newWithstm, NotificationChain msgs)
  {
    WithStatement oldWithstm = withstm;
    withstm = newWithstm;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT_OR_DEF__WITHSTM, oldWithstm, newWithstm);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWithstm(WithStatement newWithstm)
  {
    if (newWithstm != withstm)
    {
      NotificationChain msgs = null;
      if (withstm != null)
        msgs = ((InternalEObject)withstm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT_OR_DEF__WITHSTM, null, msgs);
      if (newWithstm != null)
        msgs = ((InternalEObject)newWithstm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT_OR_DEF__WITHSTM, null, msgs);
      msgs = basicSetWithstm(newWithstm, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT_OR_DEF__WITHSTM, newWithstm, newWithstm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ControlStatement getControl()
  {
    return control;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetControl(ControlStatement newControl, NotificationChain msgs)
  {
    ControlStatement oldControl = control;
    control = newControl;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT_OR_DEF__CONTROL, oldControl, newControl);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setControl(ControlStatement newControl)
  {
    if (newControl != control)
    {
      NotificationChain msgs = null;
      if (control != null)
        msgs = ((InternalEObject)control).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT_OR_DEF__CONTROL, null, msgs);
      if (newControl != null)
        msgs = ((InternalEObject)newControl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONTROL_STATEMENT_OR_DEF__CONTROL, null, msgs);
      msgs = basicSetControl(newControl, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONTROL_STATEMENT_OR_DEF__CONTROL, newControl, newControl));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_DEF:
        return basicSetLocalDef(null, msgs);
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_INST:
        return basicSetLocalInst(null, msgs);
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__WITHSTM:
        return basicSetWithstm(null, msgs);
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__CONTROL:
        return basicSetControl(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_DEF:
        return getLocalDef();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_INST:
        return getLocalInst();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__WITHSTM:
        return getWithstm();
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__CONTROL:
        return getControl();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_DEF:
        setLocalDef((FunctionLocalDef)newValue);
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_INST:
        setLocalInst((FunctionLocalInst)newValue);
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__WITHSTM:
        setWithstm((WithStatement)newValue);
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__CONTROL:
        setControl((ControlStatement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_DEF:
        setLocalDef((FunctionLocalDef)null);
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_INST:
        setLocalInst((FunctionLocalInst)null);
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__WITHSTM:
        setWithstm((WithStatement)null);
        return;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__CONTROL:
        setControl((ControlStatement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_DEF:
        return localDef != null;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__LOCAL_INST:
        return localInst != null;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__WITHSTM:
        return withstm != null;
      case TTCN3Package.CONTROL_STATEMENT_OR_DEF__CONTROL:
        return control != null;
    }
    return super.eIsSet(featureID);
  }

} //ControlStatementOrDefImpl
