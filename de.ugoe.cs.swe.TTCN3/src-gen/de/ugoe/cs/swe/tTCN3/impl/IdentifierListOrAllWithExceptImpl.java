/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AllWithExcept;
import de.ugoe.cs.swe.tTCN3.IdentifierList;
import de.ugoe.cs.swe.tTCN3.IdentifierListOrAllWithExcept;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Identifier List Or All With Except</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.IdentifierListOrAllWithExceptImpl#getIdList <em>Id List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.IdentifierListOrAllWithExceptImpl#getAll <em>All</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IdentifierListOrAllWithExceptImpl extends MinimalEObjectImpl.Container implements IdentifierListOrAllWithExcept
{
  /**
   * The cached value of the '{@link #getIdList() <em>Id List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdList()
   * @generated
   * @ordered
   */
  protected IdentifierList idList;

  /**
   * The cached value of the '{@link #getAll() <em>All</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAll()
   * @generated
   * @ordered
   */
  protected AllWithExcept all;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IdentifierListOrAllWithExceptImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getIdentifierListOrAllWithExcept();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifierList getIdList()
  {
    return idList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIdList(IdentifierList newIdList, NotificationChain msgs)
  {
    IdentifierList oldIdList = idList;
    idList = newIdList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ID_LIST, oldIdList, newIdList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIdList(IdentifierList newIdList)
  {
    if (newIdList != idList)
    {
      NotificationChain msgs = null;
      if (idList != null)
        msgs = ((InternalEObject)idList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ID_LIST, null, msgs);
      if (newIdList != null)
        msgs = ((InternalEObject)newIdList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ID_LIST, null, msgs);
      msgs = basicSetIdList(newIdList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ID_LIST, newIdList, newIdList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllWithExcept getAll()
  {
    return all;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAll(AllWithExcept newAll, NotificationChain msgs)
  {
    AllWithExcept oldAll = all;
    all = newAll;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ALL, oldAll, newAll);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAll(AllWithExcept newAll)
  {
    if (newAll != all)
    {
      NotificationChain msgs = null;
      if (all != null)
        msgs = ((InternalEObject)all).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ALL, null, msgs);
      if (newAll != null)
        msgs = ((InternalEObject)newAll).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ALL, null, msgs);
      msgs = basicSetAll(newAll, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ALL, newAll, newAll));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ID_LIST:
        return basicSetIdList(null, msgs);
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ALL:
        return basicSetAll(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ID_LIST:
        return getIdList();
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ALL:
        return getAll();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ID_LIST:
        setIdList((IdentifierList)newValue);
        return;
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ALL:
        setAll((AllWithExcept)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ID_LIST:
        setIdList((IdentifierList)null);
        return;
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ALL:
        setAll((AllWithExcept)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ID_LIST:
        return idList != null;
      case TTCN3Package.IDENTIFIER_LIST_OR_ALL_WITH_EXCEPT__ALL:
        return all != null;
    }
    return super.eIsSet(featureID);
  }

} //IdentifierListOrAllWithExceptImpl
