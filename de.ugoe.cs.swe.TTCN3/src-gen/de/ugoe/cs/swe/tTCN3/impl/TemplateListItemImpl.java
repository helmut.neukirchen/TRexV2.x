/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AllElementsFrom;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TemplateBody;
import de.ugoe.cs.swe.tTCN3.TemplateListItem;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template List Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateListItemImpl#getBody <em>Body</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.TemplateListItemImpl#getAll <em>All</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TemplateListItemImpl extends MinimalEObjectImpl.Container implements TemplateListItem
{
  /**
   * The cached value of the '{@link #getBody() <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBody()
   * @generated
   * @ordered
   */
  protected TemplateBody body;

  /**
   * The cached value of the '{@link #getAll() <em>All</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAll()
   * @generated
   * @ordered
   */
  protected AllElementsFrom all;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TemplateListItemImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getTemplateListItem();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TemplateBody getBody()
  {
    return body;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBody(TemplateBody newBody, NotificationChain msgs)
  {
    TemplateBody oldBody = body;
    body = newBody;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_LIST_ITEM__BODY, oldBody, newBody);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBody(TemplateBody newBody)
  {
    if (newBody != body)
    {
      NotificationChain msgs = null;
      if (body != null)
        msgs = ((InternalEObject)body).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_LIST_ITEM__BODY, null, msgs);
      if (newBody != null)
        msgs = ((InternalEObject)newBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_LIST_ITEM__BODY, null, msgs);
      msgs = basicSetBody(newBody, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_LIST_ITEM__BODY, newBody, newBody));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AllElementsFrom getAll()
  {
    return all;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAll(AllElementsFrom newAll, NotificationChain msgs)
  {
    AllElementsFrom oldAll = all;
    all = newAll;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_LIST_ITEM__ALL, oldAll, newAll);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAll(AllElementsFrom newAll)
  {
    if (newAll != all)
    {
      NotificationChain msgs = null;
      if (all != null)
        msgs = ((InternalEObject)all).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_LIST_ITEM__ALL, null, msgs);
      if (newAll != null)
        msgs = ((InternalEObject)newAll).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.TEMPLATE_LIST_ITEM__ALL, null, msgs);
      msgs = basicSetAll(newAll, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.TEMPLATE_LIST_ITEM__ALL, newAll, newAll));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_LIST_ITEM__BODY:
        return basicSetBody(null, msgs);
      case TTCN3Package.TEMPLATE_LIST_ITEM__ALL:
        return basicSetAll(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_LIST_ITEM__BODY:
        return getBody();
      case TTCN3Package.TEMPLATE_LIST_ITEM__ALL:
        return getAll();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_LIST_ITEM__BODY:
        setBody((TemplateBody)newValue);
        return;
      case TTCN3Package.TEMPLATE_LIST_ITEM__ALL:
        setAll((AllElementsFrom)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_LIST_ITEM__BODY:
        setBody((TemplateBody)null);
        return;
      case TTCN3Package.TEMPLATE_LIST_ITEM__ALL:
        setAll((AllElementsFrom)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.TEMPLATE_LIST_ITEM__BODY:
        return body != null;
      case TTCN3Package.TEMPLATE_LIST_ITEM__ALL:
        return all != null;
    }
    return super.eIsSet(featureID);
  }

} //TemplateListItemImpl
