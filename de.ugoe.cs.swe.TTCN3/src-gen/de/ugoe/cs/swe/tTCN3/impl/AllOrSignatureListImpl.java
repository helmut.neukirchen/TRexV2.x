/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AllOrSignatureList;
import de.ugoe.cs.swe.tTCN3.SignatureList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>All Or Signature List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AllOrSignatureListImpl#getAll <em>All</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AllOrSignatureListImpl#getSignatureList <em>Signature List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AllOrSignatureListImpl extends MinimalEObjectImpl.Container implements AllOrSignatureList
{
  /**
   * The default value of the '{@link #getAll() <em>All</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAll()
   * @generated
   * @ordered
   */
  protected static final String ALL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAll() <em>All</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAll()
   * @generated
   * @ordered
   */
  protected String all = ALL_EDEFAULT;

  /**
   * The cached value of the '{@link #getSignatureList() <em>Signature List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSignatureList()
   * @generated
   * @ordered
   */
  protected SignatureList signatureList;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AllOrSignatureListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getAllOrSignatureList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAll()
  {
    return all;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAll(String newAll)
  {
    String oldAll = all;
    all = newAll;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALL_OR_SIGNATURE_LIST__ALL, oldAll, all));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SignatureList getSignatureList()
  {
    return signatureList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSignatureList(SignatureList newSignatureList, NotificationChain msgs)
  {
    SignatureList oldSignatureList = signatureList;
    signatureList = newSignatureList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.ALL_OR_SIGNATURE_LIST__SIGNATURE_LIST, oldSignatureList, newSignatureList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSignatureList(SignatureList newSignatureList)
  {
    if (newSignatureList != signatureList)
    {
      NotificationChain msgs = null;
      if (signatureList != null)
        msgs = ((InternalEObject)signatureList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALL_OR_SIGNATURE_LIST__SIGNATURE_LIST, null, msgs);
      if (newSignatureList != null)
        msgs = ((InternalEObject)newSignatureList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.ALL_OR_SIGNATURE_LIST__SIGNATURE_LIST, null, msgs);
      msgs = basicSetSignatureList(newSignatureList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.ALL_OR_SIGNATURE_LIST__SIGNATURE_LIST, newSignatureList, newSignatureList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.ALL_OR_SIGNATURE_LIST__SIGNATURE_LIST:
        return basicSetSignatureList(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.ALL_OR_SIGNATURE_LIST__ALL:
        return getAll();
      case TTCN3Package.ALL_OR_SIGNATURE_LIST__SIGNATURE_LIST:
        return getSignatureList();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.ALL_OR_SIGNATURE_LIST__ALL:
        setAll((String)newValue);
        return;
      case TTCN3Package.ALL_OR_SIGNATURE_LIST__SIGNATURE_LIST:
        setSignatureList((SignatureList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALL_OR_SIGNATURE_LIST__ALL:
        setAll(ALL_EDEFAULT);
        return;
      case TTCN3Package.ALL_OR_SIGNATURE_LIST__SIGNATURE_LIST:
        setSignatureList((SignatureList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALL_OR_SIGNATURE_LIST__ALL:
        return ALL_EDEFAULT == null ? all != null : !ALL_EDEFAULT.equals(all);
      case TTCN3Package.ALL_OR_SIGNATURE_LIST__SIGNATURE_LIST:
        return signatureList != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (all: ");
    result.append(all);
    result.append(')');
    return result.toString();
  }

} //AllOrSignatureListImpl
