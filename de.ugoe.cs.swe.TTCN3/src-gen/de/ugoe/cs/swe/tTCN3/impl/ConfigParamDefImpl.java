/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.ConfigParamDef;
import de.ugoe.cs.swe.tTCN3.MapParamDef;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.UnmapParamDef;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Config Param Def</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigParamDefImpl#getMap <em>Map</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.ConfigParamDefImpl#getUnmap <em>Unmap</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConfigParamDefImpl extends MinimalEObjectImpl.Container implements ConfigParamDef
{
  /**
   * The cached value of the '{@link #getMap() <em>Map</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMap()
   * @generated
   * @ordered
   */
  protected MapParamDef map;

  /**
   * The cached value of the '{@link #getUnmap() <em>Unmap</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnmap()
   * @generated
   * @ordered
   */
  protected UnmapParamDef unmap;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ConfigParamDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getConfigParamDef();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MapParamDef getMap()
  {
    return map;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMap(MapParamDef newMap, NotificationChain msgs)
  {
    MapParamDef oldMap = map;
    map = newMap;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIG_PARAM_DEF__MAP, oldMap, newMap);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMap(MapParamDef newMap)
  {
    if (newMap != map)
    {
      NotificationChain msgs = null;
      if (map != null)
        msgs = ((InternalEObject)map).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIG_PARAM_DEF__MAP, null, msgs);
      if (newMap != null)
        msgs = ((InternalEObject)newMap).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIG_PARAM_DEF__MAP, null, msgs);
      msgs = basicSetMap(newMap, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIG_PARAM_DEF__MAP, newMap, newMap));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnmapParamDef getUnmap()
  {
    return unmap;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetUnmap(UnmapParamDef newUnmap, NotificationChain msgs)
  {
    UnmapParamDef oldUnmap = unmap;
    unmap = newUnmap;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIG_PARAM_DEF__UNMAP, oldUnmap, newUnmap);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUnmap(UnmapParamDef newUnmap)
  {
    if (newUnmap != unmap)
    {
      NotificationChain msgs = null;
      if (unmap != null)
        msgs = ((InternalEObject)unmap).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIG_PARAM_DEF__UNMAP, null, msgs);
      if (newUnmap != null)
        msgs = ((InternalEObject)newUnmap).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.CONFIG_PARAM_DEF__UNMAP, null, msgs);
      msgs = basicSetUnmap(newUnmap, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.CONFIG_PARAM_DEF__UNMAP, newUnmap, newUnmap));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIG_PARAM_DEF__MAP:
        return basicSetMap(null, msgs);
      case TTCN3Package.CONFIG_PARAM_DEF__UNMAP:
        return basicSetUnmap(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIG_PARAM_DEF__MAP:
        return getMap();
      case TTCN3Package.CONFIG_PARAM_DEF__UNMAP:
        return getUnmap();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIG_PARAM_DEF__MAP:
        setMap((MapParamDef)newValue);
        return;
      case TTCN3Package.CONFIG_PARAM_DEF__UNMAP:
        setUnmap((UnmapParamDef)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIG_PARAM_DEF__MAP:
        setMap((MapParamDef)null);
        return;
      case TTCN3Package.CONFIG_PARAM_DEF__UNMAP:
        setUnmap((UnmapParamDef)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.CONFIG_PARAM_DEF__MAP:
        return map != null;
      case TTCN3Package.CONFIG_PARAM_DEF__UNMAP:
        return unmap != null;
    }
    return super.eIsSet(featureID);
  }

} //ConfigParamDefImpl
