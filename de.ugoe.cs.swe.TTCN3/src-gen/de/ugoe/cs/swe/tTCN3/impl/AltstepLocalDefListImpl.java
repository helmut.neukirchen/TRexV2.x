/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.AltstepLocalDef;
import de.ugoe.cs.swe.tTCN3.AltstepLocalDefList;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.WithStatement;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Altstep Local Def List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltstepLocalDefListImpl#getDefs <em>Defs</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltstepLocalDefListImpl#getWithstats <em>Withstats</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.AltstepLocalDefListImpl#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AltstepLocalDefListImpl extends MinimalEObjectImpl.Container implements AltstepLocalDefList
{
  /**
   * The cached value of the '{@link #getDefs() <em>Defs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefs()
   * @generated
   * @ordered
   */
  protected EList<AltstepLocalDef> defs;

  /**
   * The cached value of the '{@link #getWithstats() <em>Withstats</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWithstats()
   * @generated
   * @ordered
   */
  protected EList<WithStatement> withstats;

  /**
   * The cached value of the '{@link #getSc() <em>Sc</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSc()
   * @generated
   * @ordered
   */
  protected EList<String> sc;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AltstepLocalDefListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getAltstepLocalDefList();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<AltstepLocalDef> getDefs()
  {
    if (defs == null)
    {
      defs = new EObjectContainmentEList<AltstepLocalDef>(AltstepLocalDef.class, this, TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__DEFS);
    }
    return defs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<WithStatement> getWithstats()
  {
    if (withstats == null)
    {
      withstats = new EObjectContainmentEList<WithStatement>(WithStatement.class, this, TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__WITHSTATS);
    }
    return withstats;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getSc()
  {
    if (sc == null)
    {
      sc = new EDataTypeEList<String>(String.class, this, TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__SC);
    }
    return sc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__DEFS:
        return ((InternalEList<?>)getDefs()).basicRemove(otherEnd, msgs);
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__WITHSTATS:
        return ((InternalEList<?>)getWithstats()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__DEFS:
        return getDefs();
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__WITHSTATS:
        return getWithstats();
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__SC:
        return getSc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__DEFS:
        getDefs().clear();
        getDefs().addAll((Collection<? extends AltstepLocalDef>)newValue);
        return;
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__WITHSTATS:
        getWithstats().clear();
        getWithstats().addAll((Collection<? extends WithStatement>)newValue);
        return;
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__SC:
        getSc().clear();
        getSc().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__DEFS:
        getDefs().clear();
        return;
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__WITHSTATS:
        getWithstats().clear();
        return;
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__SC:
        getSc().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__DEFS:
        return defs != null && !defs.isEmpty();
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__WITHSTATS:
        return withstats != null && !withstats.isEmpty();
      case TTCN3Package.ALTSTEP_LOCAL_DEF_LIST__SC:
        return sc != null && !sc.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (sc: ");
    result.append(sc);
    result.append(')');
    return result.toString();
  }

} //AltstepLocalDefListImpl
