/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.IndexAssignment;
import de.ugoe.cs.swe.tTCN3.RunningTimerOp;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.TimerRefOrAny;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Running Timer Op</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RunningTimerOpImpl#getTimerRef <em>Timer Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.RunningTimerOpImpl#getIndex <em>Index</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RunningTimerOpImpl extends MinimalEObjectImpl.Container implements RunningTimerOp
{
  /**
   * The cached value of the '{@link #getTimerRef() <em>Timer Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimerRef()
   * @generated
   * @ordered
   */
  protected TimerRefOrAny timerRef;

  /**
   * The cached value of the '{@link #getIndex() <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIndex()
   * @generated
   * @ordered
   */
  protected IndexAssignment index;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RunningTimerOpImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getRunningTimerOp();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimerRefOrAny getTimerRef()
  {
    return timerRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTimerRef(TimerRefOrAny newTimerRef, NotificationChain msgs)
  {
    TimerRefOrAny oldTimerRef = timerRef;
    timerRef = newTimerRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.RUNNING_TIMER_OP__TIMER_REF, oldTimerRef, newTimerRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimerRef(TimerRefOrAny newTimerRef)
  {
    if (newTimerRef != timerRef)
    {
      NotificationChain msgs = null;
      if (timerRef != null)
        msgs = ((InternalEObject)timerRef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RUNNING_TIMER_OP__TIMER_REF, null, msgs);
      if (newTimerRef != null)
        msgs = ((InternalEObject)newTimerRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RUNNING_TIMER_OP__TIMER_REF, null, msgs);
      msgs = basicSetTimerRef(newTimerRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.RUNNING_TIMER_OP__TIMER_REF, newTimerRef, newTimerRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IndexAssignment getIndex()
  {
    return index;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIndex(IndexAssignment newIndex, NotificationChain msgs)
  {
    IndexAssignment oldIndex = index;
    index = newIndex;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.RUNNING_TIMER_OP__INDEX, oldIndex, newIndex);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIndex(IndexAssignment newIndex)
  {
    if (newIndex != index)
    {
      NotificationChain msgs = null;
      if (index != null)
        msgs = ((InternalEObject)index).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RUNNING_TIMER_OP__INDEX, null, msgs);
      if (newIndex != null)
        msgs = ((InternalEObject)newIndex).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.RUNNING_TIMER_OP__INDEX, null, msgs);
      msgs = basicSetIndex(newIndex, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.RUNNING_TIMER_OP__INDEX, newIndex, newIndex));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.RUNNING_TIMER_OP__TIMER_REF:
        return basicSetTimerRef(null, msgs);
      case TTCN3Package.RUNNING_TIMER_OP__INDEX:
        return basicSetIndex(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.RUNNING_TIMER_OP__TIMER_REF:
        return getTimerRef();
      case TTCN3Package.RUNNING_TIMER_OP__INDEX:
        return getIndex();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.RUNNING_TIMER_OP__TIMER_REF:
        setTimerRef((TimerRefOrAny)newValue);
        return;
      case TTCN3Package.RUNNING_TIMER_OP__INDEX:
        setIndex((IndexAssignment)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.RUNNING_TIMER_OP__TIMER_REF:
        setTimerRef((TimerRefOrAny)null);
        return;
      case TTCN3Package.RUNNING_TIMER_OP__INDEX:
        setIndex((IndexAssignment)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.RUNNING_TIMER_OP__TIMER_REF:
        return timerRef != null;
      case TTCN3Package.RUNNING_TIMER_OP__INDEX:
        return index != null;
    }
    return super.eIsSet(featureID);
  }

} //RunningTimerOpImpl
