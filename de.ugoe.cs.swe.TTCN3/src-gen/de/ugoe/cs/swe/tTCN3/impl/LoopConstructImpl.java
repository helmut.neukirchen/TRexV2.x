/**
 */
package de.ugoe.cs.swe.tTCN3.impl;

import de.ugoe.cs.swe.tTCN3.DoWhileStatement;
import de.ugoe.cs.swe.tTCN3.ForStatement;
import de.ugoe.cs.swe.tTCN3.LoopConstruct;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.tTCN3.WhileStatement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Loop Construct</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.LoopConstructImpl#getForStm <em>For Stm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.LoopConstructImpl#getWhileStm <em>While Stm</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.impl.LoopConstructImpl#getDoStm <em>Do Stm</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LoopConstructImpl extends MinimalEObjectImpl.Container implements LoopConstruct
{
  /**
   * The cached value of the '{@link #getForStm() <em>For Stm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getForStm()
   * @generated
   * @ordered
   */
  protected ForStatement forStm;

  /**
   * The cached value of the '{@link #getWhileStm() <em>While Stm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWhileStm()
   * @generated
   * @ordered
   */
  protected WhileStatement whileStm;

  /**
   * The cached value of the '{@link #getDoStm() <em>Do Stm</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDoStm()
   * @generated
   * @ordered
   */
  protected DoWhileStatement doStm;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LoopConstructImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return TTCN3Package.eINSTANCE.getLoopConstruct();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ForStatement getForStm()
  {
    return forStm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetForStm(ForStatement newForStm, NotificationChain msgs)
  {
    ForStatement oldForStm = forStm;
    forStm = newForStm;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.LOOP_CONSTRUCT__FOR_STM, oldForStm, newForStm);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setForStm(ForStatement newForStm)
  {
    if (newForStm != forStm)
    {
      NotificationChain msgs = null;
      if (forStm != null)
        msgs = ((InternalEObject)forStm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.LOOP_CONSTRUCT__FOR_STM, null, msgs);
      if (newForStm != null)
        msgs = ((InternalEObject)newForStm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.LOOP_CONSTRUCT__FOR_STM, null, msgs);
      msgs = basicSetForStm(newForStm, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.LOOP_CONSTRUCT__FOR_STM, newForStm, newForStm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WhileStatement getWhileStm()
  {
    return whileStm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetWhileStm(WhileStatement newWhileStm, NotificationChain msgs)
  {
    WhileStatement oldWhileStm = whileStm;
    whileStm = newWhileStm;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.LOOP_CONSTRUCT__WHILE_STM, oldWhileStm, newWhileStm);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWhileStm(WhileStatement newWhileStm)
  {
    if (newWhileStm != whileStm)
    {
      NotificationChain msgs = null;
      if (whileStm != null)
        msgs = ((InternalEObject)whileStm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.LOOP_CONSTRUCT__WHILE_STM, null, msgs);
      if (newWhileStm != null)
        msgs = ((InternalEObject)newWhileStm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.LOOP_CONSTRUCT__WHILE_STM, null, msgs);
      msgs = basicSetWhileStm(newWhileStm, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.LOOP_CONSTRUCT__WHILE_STM, newWhileStm, newWhileStm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DoWhileStatement getDoStm()
  {
    return doStm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDoStm(DoWhileStatement newDoStm, NotificationChain msgs)
  {
    DoWhileStatement oldDoStm = doStm;
    doStm = newDoStm;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TTCN3Package.LOOP_CONSTRUCT__DO_STM, oldDoStm, newDoStm);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDoStm(DoWhileStatement newDoStm)
  {
    if (newDoStm != doStm)
    {
      NotificationChain msgs = null;
      if (doStm != null)
        msgs = ((InternalEObject)doStm).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.LOOP_CONSTRUCT__DO_STM, null, msgs);
      if (newDoStm != null)
        msgs = ((InternalEObject)newDoStm).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TTCN3Package.LOOP_CONSTRUCT__DO_STM, null, msgs);
      msgs = basicSetDoStm(newDoStm, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, TTCN3Package.LOOP_CONSTRUCT__DO_STM, newDoStm, newDoStm));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case TTCN3Package.LOOP_CONSTRUCT__FOR_STM:
        return basicSetForStm(null, msgs);
      case TTCN3Package.LOOP_CONSTRUCT__WHILE_STM:
        return basicSetWhileStm(null, msgs);
      case TTCN3Package.LOOP_CONSTRUCT__DO_STM:
        return basicSetDoStm(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case TTCN3Package.LOOP_CONSTRUCT__FOR_STM:
        return getForStm();
      case TTCN3Package.LOOP_CONSTRUCT__WHILE_STM:
        return getWhileStm();
      case TTCN3Package.LOOP_CONSTRUCT__DO_STM:
        return getDoStm();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case TTCN3Package.LOOP_CONSTRUCT__FOR_STM:
        setForStm((ForStatement)newValue);
        return;
      case TTCN3Package.LOOP_CONSTRUCT__WHILE_STM:
        setWhileStm((WhileStatement)newValue);
        return;
      case TTCN3Package.LOOP_CONSTRUCT__DO_STM:
        setDoStm((DoWhileStatement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.LOOP_CONSTRUCT__FOR_STM:
        setForStm((ForStatement)null);
        return;
      case TTCN3Package.LOOP_CONSTRUCT__WHILE_STM:
        setWhileStm((WhileStatement)null);
        return;
      case TTCN3Package.LOOP_CONSTRUCT__DO_STM:
        setDoStm((DoWhileStatement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case TTCN3Package.LOOP_CONSTRUCT__FOR_STM:
        return forStm != null;
      case TTCN3Package.LOOP_CONSTRUCT__WHILE_STM:
        return whileStm != null;
      case TTCN3Package.LOOP_CONSTRUCT__DO_STM:
        return doStm != null;
    }
    return super.eIsSet(featureID);
  }

} //LoopConstructImpl
