/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Read Timer Op</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ReadTimerOp#getTimer <em>Timer</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getReadTimerOp()
 * @model
 * @generated
 */
public interface ReadTimerOp extends EObject
{
  /**
   * Returns the value of the '<em><b>Timer</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' reference.
   * @see #setTimer(TimerVarInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getReadTimerOp_Timer()
   * @model
   * @generated
   */
  TimerVarInstance getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ReadTimerOp#getTimer <em>Timer</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' reference.
   * @see #getTimer()
   * @generated
   */
  void setTimer(TimerVarInstance value);

} // ReadTimerOp
