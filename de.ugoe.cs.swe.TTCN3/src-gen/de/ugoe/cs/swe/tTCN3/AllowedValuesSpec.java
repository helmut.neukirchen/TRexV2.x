/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Allowed Values Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AllowedValuesSpec#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllowedValuesSpec()
 * @model
 * @generated
 */
public interface AllowedValuesSpec extends SubTypeSpec
{
  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.TemplateOrRange}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllowedValuesSpec_Template()
   * @model containment="true"
   * @generated
   */
  EList<TemplateOrRange> getTemplate();

} // AllowedValuesSpec
