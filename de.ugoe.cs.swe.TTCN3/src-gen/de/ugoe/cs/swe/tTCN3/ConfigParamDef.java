/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Config Param Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigParamDef#getMap <em>Map</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ConfigParamDef#getUnmap <em>Unmap</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigParamDef()
 * @model
 * @generated
 */
public interface ConfigParamDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Map</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Map</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Map</em>' containment reference.
   * @see #setMap(MapParamDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigParamDef_Map()
   * @model containment="true"
   * @generated
   */
  MapParamDef getMap();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigParamDef#getMap <em>Map</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Map</em>' containment reference.
   * @see #getMap()
   * @generated
   */
  void setMap(MapParamDef value);

  /**
   * Returns the value of the '<em><b>Unmap</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Unmap</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Unmap</em>' containment reference.
   * @see #setUnmap(UnmapParamDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getConfigParamDef_Unmap()
   * @model containment="true"
   * @generated
   */
  UnmapParamDef getUnmap();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ConfigParamDef#getUnmap <em>Unmap</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Unmap</em>' containment reference.
   * @see #getUnmap()
   * @generated
   */
  void setUnmap(UnmapParamDef value);

} // ConfigParamDef
