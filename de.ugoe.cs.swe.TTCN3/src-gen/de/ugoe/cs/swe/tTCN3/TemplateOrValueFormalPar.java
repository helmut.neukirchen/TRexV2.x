/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Or Value Formal Par</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar#getValue <em>Value</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateOrValueFormalPar()
 * @model
 * @generated
 */
public interface TemplateOrValueFormalPar extends EObject
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(FormalValuePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateOrValueFormalPar_Value()
   * @model containment="true"
   * @generated
   */
  FormalValuePar getValue();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(FormalValuePar value);

  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(FormalTemplatePar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateOrValueFormalPar_Template()
   * @model containment="true"
   * @generated
   */
  FormalTemplatePar getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(FormalTemplatePar value);

} // TemplateOrValueFormalPar
