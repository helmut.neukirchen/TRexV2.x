/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SUT Statements</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SUTStatements#getTxt <em>Txt</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSUTStatements()
 * @model
 * @generated
 */
public interface SUTStatements extends EObject
{
  /**
   * Returns the value of the '<em><b>Txt</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ActionText}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Txt</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Txt</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSUTStatements_Txt()
   * @model containment="true"
   * @generated
   */
  EList<ActionText> getTxt();

} // SUTStatements
