/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getBasic <em>Basic</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getBehavior <em>Behavior</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getVerdict <em>Verdict</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getCommunication <em>Communication</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getSut <em>Sut</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getTest <em>Test</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatement()
 * @model
 * @generated
 */
public interface FunctionStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Timer</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' containment reference.
   * @see #setTimer(TimerStatements)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatement_Timer()
   * @model containment="true"
   * @generated
   */
  TimerStatements getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getTimer <em>Timer</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' containment reference.
   * @see #getTimer()
   * @generated
   */
  void setTimer(TimerStatements value);

  /**
   * Returns the value of the '<em><b>Basic</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Basic</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Basic</em>' containment reference.
   * @see #setBasic(BasicStatements)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatement_Basic()
   * @model containment="true"
   * @generated
   */
  BasicStatements getBasic();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getBasic <em>Basic</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Basic</em>' containment reference.
   * @see #getBasic()
   * @generated
   */
  void setBasic(BasicStatements value);

  /**
   * Returns the value of the '<em><b>Behavior</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Behavior</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Behavior</em>' containment reference.
   * @see #setBehavior(BehaviourStatements)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatement_Behavior()
   * @model containment="true"
   * @generated
   */
  BehaviourStatements getBehavior();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getBehavior <em>Behavior</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Behavior</em>' containment reference.
   * @see #getBehavior()
   * @generated
   */
  void setBehavior(BehaviourStatements value);

  /**
   * Returns the value of the '<em><b>Verdict</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Verdict</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Verdict</em>' containment reference.
   * @see #setVerdict(SetLocalVerdict)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatement_Verdict()
   * @model containment="true"
   * @generated
   */
  SetLocalVerdict getVerdict();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getVerdict <em>Verdict</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Verdict</em>' containment reference.
   * @see #getVerdict()
   * @generated
   */
  void setVerdict(SetLocalVerdict value);

  /**
   * Returns the value of the '<em><b>Communication</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Communication</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Communication</em>' containment reference.
   * @see #setCommunication(CommunicationStatements)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatement_Communication()
   * @model containment="true"
   * @generated
   */
  CommunicationStatements getCommunication();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getCommunication <em>Communication</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Communication</em>' containment reference.
   * @see #getCommunication()
   * @generated
   */
  void setCommunication(CommunicationStatements value);

  /**
   * Returns the value of the '<em><b>Configuration</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Configuration</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Configuration</em>' containment reference.
   * @see #setConfiguration(ConfigurationStatements)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatement_Configuration()
   * @model containment="true"
   * @generated
   */
  ConfigurationStatements getConfiguration();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getConfiguration <em>Configuration</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Configuration</em>' containment reference.
   * @see #getConfiguration()
   * @generated
   */
  void setConfiguration(ConfigurationStatements value);

  /**
   * Returns the value of the '<em><b>Sut</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sut</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sut</em>' containment reference.
   * @see #setSut(SUTStatements)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatement_Sut()
   * @model containment="true"
   * @generated
   */
  SUTStatements getSut();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getSut <em>Sut</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sut</em>' containment reference.
   * @see #getSut()
   * @generated
   */
  void setSut(SUTStatements value);

  /**
   * Returns the value of the '<em><b>Test</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Test</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Test</em>' containment reference.
   * @see #setTest(TestcaseOperation)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatement_Test()
   * @model containment="true"
   * @generated
   */
  TestcaseOperation getTest();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionStatement#getTest <em>Test</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Test</em>' containment reference.
   * @see #getTest()
   * @generated
   */
  void setTest(TestcaseOperation value);

} // FunctionStatement
