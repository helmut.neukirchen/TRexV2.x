/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Connection Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleConnectionSpec#getPort1 <em>Port1</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleConnectionSpec#getPort2 <em>Port2</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleConnectionSpec()
 * @model
 * @generated
 */
public interface SingleConnectionSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Port1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port1</em>' containment reference.
   * @see #setPort1(PortRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleConnectionSpec_Port1()
   * @model containment="true"
   * @generated
   */
  PortRef getPort1();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleConnectionSpec#getPort1 <em>Port1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port1</em>' containment reference.
   * @see #getPort1()
   * @generated
   */
  void setPort1(PortRef value);

  /**
   * Returns the value of the '<em><b>Port2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port2</em>' containment reference.
   * @see #setPort2(PortRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleConnectionSpec_Port2()
   * @model containment="true"
   * @generated
   */
  PortRef getPort2();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleConnectionSpec#getPort2 <em>Port2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port2</em>' containment reference.
   * @see #getPort2()
   * @generated
   */
  void setPort2(PortRef value);

} // SingleConnectionSpec
