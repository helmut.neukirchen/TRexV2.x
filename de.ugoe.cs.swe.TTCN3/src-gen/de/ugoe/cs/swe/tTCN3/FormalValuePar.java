/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Value Par</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getInOut <em>In Out</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getMod <em>Mod</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getExpression <em>Expression</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalValuePar()
 * @model
 * @generated
 */
public interface FormalValuePar extends FormalPortAndValuePar, FieldReference
{
  /**
   * Returns the value of the '<em><b>In Out</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>In Out</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>In Out</em>' attribute.
   * @see #setInOut(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalValuePar_InOut()
   * @model
   * @generated
   */
  String getInOut();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getInOut <em>In Out</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>In Out</em>' attribute.
   * @see #getInOut()
   * @generated
   */
  void setInOut(String value);

  /**
   * Returns the value of the '<em><b>Mod</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mod</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mod</em>' attribute.
   * @see #setMod(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalValuePar_Mod()
   * @model
   * @generated
   */
  String getMod();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getMod <em>Mod</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mod</em>' attribute.
   * @see #getMod()
   * @generated
   */
  void setMod(String value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalValuePar_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression</em>' containment reference.
   * @see #setExpression(Expression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalValuePar_Expression()
   * @model containment="true"
   * @generated
   */
  Expression getExpression();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FormalValuePar#getExpression <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression</em>' containment reference.
   * @see #getExpression()
   * @generated
   */
  void setExpression(Expression value);

} // FormalValuePar
