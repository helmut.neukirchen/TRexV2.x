/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Behaviour Statements</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getTestcase <em>Testcase</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getFunction <em>Function</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getReturn <em>Return</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getAlt <em>Alt</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getInterleaved <em>Interleaved</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getLabel <em>Label</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getGoto <em>Goto</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getDeactivate <em>Deactivate</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getAltstep <em>Altstep</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getActivate <em>Activate</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements()
 * @model
 * @generated
 */
public interface BehaviourStatements extends EObject
{
  /**
   * Returns the value of the '<em><b>Testcase</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Testcase</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Testcase</em>' containment reference.
   * @see #setTestcase(TestcaseInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements_Testcase()
   * @model containment="true"
   * @generated
   */
  TestcaseInstance getTestcase();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getTestcase <em>Testcase</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Testcase</em>' containment reference.
   * @see #getTestcase()
   * @generated
   */
  void setTestcase(TestcaseInstance value);

  /**
   * Returns the value of the '<em><b>Function</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function</em>' containment reference.
   * @see #setFunction(FunctionInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements_Function()
   * @model containment="true"
   * @generated
   */
  FunctionInstance getFunction();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getFunction <em>Function</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Function</em>' containment reference.
   * @see #getFunction()
   * @generated
   */
  void setFunction(FunctionInstance value);

  /**
   * Returns the value of the '<em><b>Return</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Return</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return</em>' containment reference.
   * @see #setReturn(ReturnStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements_Return()
   * @model containment="true"
   * @generated
   */
  ReturnStatement getReturn();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getReturn <em>Return</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Return</em>' containment reference.
   * @see #getReturn()
   * @generated
   */
  void setReturn(ReturnStatement value);

  /**
   * Returns the value of the '<em><b>Alt</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Alt</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Alt</em>' containment reference.
   * @see #setAlt(AltConstruct)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements_Alt()
   * @model containment="true"
   * @generated
   */
  AltConstruct getAlt();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getAlt <em>Alt</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Alt</em>' containment reference.
   * @see #getAlt()
   * @generated
   */
  void setAlt(AltConstruct value);

  /**
   * Returns the value of the '<em><b>Interleaved</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Interleaved</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Interleaved</em>' containment reference.
   * @see #setInterleaved(InterleavedConstruct)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements_Interleaved()
   * @model containment="true"
   * @generated
   */
  InterleavedConstruct getInterleaved();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getInterleaved <em>Interleaved</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Interleaved</em>' containment reference.
   * @see #getInterleaved()
   * @generated
   */
  void setInterleaved(InterleavedConstruct value);

  /**
   * Returns the value of the '<em><b>Label</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Label</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Label</em>' containment reference.
   * @see #setLabel(LabelStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements_Label()
   * @model containment="true"
   * @generated
   */
  LabelStatement getLabel();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getLabel <em>Label</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Label</em>' containment reference.
   * @see #getLabel()
   * @generated
   */
  void setLabel(LabelStatement value);

  /**
   * Returns the value of the '<em><b>Goto</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Goto</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Goto</em>' containment reference.
   * @see #setGoto(GotoStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements_Goto()
   * @model containment="true"
   * @generated
   */
  GotoStatement getGoto();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getGoto <em>Goto</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Goto</em>' containment reference.
   * @see #getGoto()
   * @generated
   */
  void setGoto(GotoStatement value);

  /**
   * Returns the value of the '<em><b>Deactivate</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Deactivate</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Deactivate</em>' containment reference.
   * @see #setDeactivate(DeactivateStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements_Deactivate()
   * @model containment="true"
   * @generated
   */
  DeactivateStatement getDeactivate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getDeactivate <em>Deactivate</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Deactivate</em>' containment reference.
   * @see #getDeactivate()
   * @generated
   */
  void setDeactivate(DeactivateStatement value);

  /**
   * Returns the value of the '<em><b>Altstep</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Altstep</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Altstep</em>' containment reference.
   * @see #setAltstep(AltstepInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements_Altstep()
   * @model containment="true"
   * @generated
   */
  AltstepInstance getAltstep();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getAltstep <em>Altstep</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Altstep</em>' containment reference.
   * @see #getAltstep()
   * @generated
   */
  void setAltstep(AltstepInstance value);

  /**
   * Returns the value of the '<em><b>Activate</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Activate</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Activate</em>' containment reference.
   * @see #setActivate(ActivateOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getBehaviourStatements_Activate()
   * @model containment="true"
   * @generated
   */
  ActivateOp getActivate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.BehaviourStatements#getActivate <em>Activate</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Activate</em>' containment reference.
   * @see #getActivate()
   * @generated
   */
  void setActivate(ActivateOp value);

} // BehaviourStatements
