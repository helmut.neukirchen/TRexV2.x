/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Redirect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortRedirect#getValue <em>Value</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortRedirect#getSender <em>Sender</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortRedirect#getIndex <em>Index</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRedirect()
 * @model
 * @generated
 */
public interface PortRedirect extends EObject
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' containment reference.
   * @see #setValue(ValueSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRedirect_Value()
   * @model containment="true"
   * @generated
   */
  ValueSpec getValue();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortRedirect#getValue <em>Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' containment reference.
   * @see #getValue()
   * @generated
   */
  void setValue(ValueSpec value);

  /**
   * Returns the value of the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sender</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sender</em>' containment reference.
   * @see #setSender(SenderSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRedirect_Sender()
   * @model containment="true"
   * @generated
   */
  SenderSpec getSender();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortRedirect#getSender <em>Sender</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sender</em>' containment reference.
   * @see #getSender()
   * @generated
   */
  void setSender(SenderSpec value);

  /**
   * Returns the value of the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Index</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Index</em>' containment reference.
   * @see #setIndex(IndexSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRedirect_Index()
   * @model containment="true"
   * @generated
   */
  IndexSpec getIndex();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortRedirect#getIndex <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Index</em>' containment reference.
   * @see #getIndex()
   * @generated
   */
  void setIndex(IndexSpec value);

} // PortRedirect
