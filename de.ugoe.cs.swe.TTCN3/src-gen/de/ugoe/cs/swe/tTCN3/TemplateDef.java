/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getRestriction <em>Restriction</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getFuzzy <em>Fuzzy</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getBase <em>Base</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getDerived <em>Derived</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateDef()
 * @model
 * @generated
 */
public interface TemplateDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Restriction</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Restriction</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Restriction</em>' containment reference.
   * @see #setRestriction(TemplateRestriction)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateDef_Restriction()
   * @model containment="true"
   * @generated
   */
  TemplateRestriction getRestriction();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getRestriction <em>Restriction</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Restriction</em>' containment reference.
   * @see #getRestriction()
   * @generated
   */
  void setRestriction(TemplateRestriction value);

  /**
   * Returns the value of the '<em><b>Fuzzy</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fuzzy</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fuzzy</em>' attribute.
   * @see #setFuzzy(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateDef_Fuzzy()
   * @model
   * @generated
   */
  String getFuzzy();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getFuzzy <em>Fuzzy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fuzzy</em>' attribute.
   * @see #getFuzzy()
   * @generated
   */
  void setFuzzy(String value);

  /**
   * Returns the value of the '<em><b>Base</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Base</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Base</em>' containment reference.
   * @see #setBase(BaseTemplate)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateDef_Base()
   * @model containment="true"
   * @generated
   */
  BaseTemplate getBase();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getBase <em>Base</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Base</em>' containment reference.
   * @see #getBase()
   * @generated
   */
  void setBase(BaseTemplate value);

  /**
   * Returns the value of the '<em><b>Derived</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Derived</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Derived</em>' containment reference.
   * @see #setDerived(DerivedDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateDef_Derived()
   * @model containment="true"
   * @generated
   */
  DerivedDef getDerived();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getDerived <em>Derived</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Derived</em>' containment reference.
   * @see #getDerived()
   * @generated
   */
  void setDerived(DerivedDef value);

  /**
   * Returns the value of the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Body</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Body</em>' containment reference.
   * @see #setBody(TemplateBody)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTemplateDef_Body()
   * @model containment="true"
   * @generated
   */
  TemplateBody getBody();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TemplateDef#getBody <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Body</em>' containment reference.
   * @see #getBody()
   * @generated
   */
  void setBody(TemplateBody value);

} // TemplateDef
