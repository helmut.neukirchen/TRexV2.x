/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>From Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FromClause#getSender <em>Sender</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FromClause#getIndex <em>Index</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FromClause#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FromClause#getAddresses <em>Addresses</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFromClause()
 * @model
 * @generated
 */
public interface FromClause extends FromClausePresent
{
  /**
   * Returns the value of the '<em><b>Sender</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sender</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sender</em>' containment reference.
   * @see #setSender(SenderSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFromClause_Sender()
   * @model containment="true"
   * @generated
   */
  SenderSpec getSender();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FromClause#getSender <em>Sender</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sender</em>' containment reference.
   * @see #getSender()
   * @generated
   */
  void setSender(SenderSpec value);

  /**
   * Returns the value of the '<em><b>Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Index</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Index</em>' containment reference.
   * @see #setIndex(IndexSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFromClause_Index()
   * @model containment="true"
   * @generated
   */
  IndexSpec getIndex();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FromClause#getIndex <em>Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Index</em>' containment reference.
   * @see #getIndex()
   * @generated
   */
  void setIndex(IndexSpec value);

  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(InLineTemplate)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFromClause_Template()
   * @model containment="true"
   * @generated
   */
  InLineTemplate getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FromClause#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(InLineTemplate value);

  /**
   * Returns the value of the '<em><b>Addresses</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Addresses</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Addresses</em>' containment reference.
   * @see #setAddresses(AddressRefList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFromClause_Addresses()
   * @model containment="true"
   * @generated
   */
  AddressRefList getAddresses();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FromClause#getAddresses <em>Addresses</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Addresses</em>' containment reference.
   * @see #getAddresses()
   * @generated
   */
  void setAddresses(AddressRefList value);

} // FromClause
