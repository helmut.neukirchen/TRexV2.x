/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Reference Or Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentReferenceOrLiteral#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentReferenceOrLiteral()
 * @model
 * @generated
 */
public interface ComponentReferenceOrLiteral extends KillTCStatement, StopTCStatement
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(ComponentOrDefaultReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentReferenceOrLiteral_Ref()
   * @model containment="true"
   * @generated
   */
  ComponentOrDefaultReference getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentReferenceOrLiteral#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(ComponentOrDefaultReference value);

} // ComponentReferenceOrLiteral
