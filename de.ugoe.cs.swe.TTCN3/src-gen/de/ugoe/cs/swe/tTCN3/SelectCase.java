/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SelectCase#getTemplate <em>Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SelectCase#getElse <em>Else</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SelectCase#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSelectCase()
 * @model
 * @generated
 */
public interface SelectCase extends EObject
{
  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.InLineTemplate}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSelectCase_Template()
   * @model containment="true"
   * @generated
   */
  EList<InLineTemplate> getTemplate();

  /**
   * Returns the value of the '<em><b>Else</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Else</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Else</em>' attribute.
   * @see #setElse(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSelectCase_Else()
   * @model
   * @generated
   */
  String getElse();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SelectCase#getElse <em>Else</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Else</em>' attribute.
   * @see #getElse()
   * @generated
   */
  void setElse(String value);

  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(StatementBlock)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSelectCase_Statement()
   * @model containment="true"
   * @generated
   */
  StatementBlock getStatement();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SelectCase#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(StatementBlock value);

} // SelectCase
