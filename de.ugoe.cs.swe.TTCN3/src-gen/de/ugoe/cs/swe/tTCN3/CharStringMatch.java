/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Char String Match</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CharStringMatch#getPattern <em>Pattern</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCharStringMatch()
 * @model
 * @generated
 */
public interface CharStringMatch extends AllowedValuesSpec
{
  /**
   * Returns the value of the '<em><b>Pattern</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.PatternParticle}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pattern</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pattern</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCharStringMatch_Pattern()
   * @model containment="true"
   * @generated
   */
  EList<PatternParticle> getPattern();

} // CharStringMatch
