/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>All With Excepts</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AllWithExcepts#getDef <em>Def</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllWithExcepts()
 * @model
 * @generated
 */
public interface AllWithExcepts extends EObject
{
  /**
   * Returns the value of the '<em><b>Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Def</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Def</em>' containment reference.
   * @see #setDef(ExceptsDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllWithExcepts_Def()
   * @model containment="true"
   * @generated
   */
  ExceptsDef getDef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AllWithExcepts#getDef <em>Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Def</em>' containment reference.
   * @see #getDef()
   * @generated
   */
  void setDef(ExceptsDef value);

} // AllWithExcepts
