/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signature Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SignatureDef#getParamList <em>Param List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SignatureDef#getReturn <em>Return</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SignatureDef#getSprec <em>Sprec</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSignatureDef()
 * @model
 * @generated
 */
public interface SignatureDef extends ReferencedType
{
  /**
   * Returns the value of the '<em><b>Param List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Param List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Param List</em>' containment reference.
   * @see #setParamList(SignatureFormalParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSignatureDef_ParamList()
   * @model containment="true"
   * @generated
   */
  SignatureFormalParList getParamList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SignatureDef#getParamList <em>Param List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Param List</em>' containment reference.
   * @see #getParamList()
   * @generated
   */
  void setParamList(SignatureFormalParList value);

  /**
   * Returns the value of the '<em><b>Return</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Return</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return</em>' containment reference.
   * @see #setReturn(ReturnType)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSignatureDef_Return()
   * @model containment="true"
   * @generated
   */
  ReturnType getReturn();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SignatureDef#getReturn <em>Return</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Return</em>' containment reference.
   * @see #getReturn()
   * @generated
   */
  void setReturn(ReturnType value);

  /**
   * Returns the value of the '<em><b>Sprec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sprec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sprec</em>' containment reference.
   * @see #setSprec(ExceptionSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSignatureDef_Sprec()
   * @model containment="true"
   * @generated
   */
  ExceptionSpec getSprec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SignatureDef#getSprec <em>Sprec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sprec</em>' containment reference.
   * @see #getSprec()
   * @generated
   */
  void setSprec(ExceptionSpec value);

} // SignatureDef
