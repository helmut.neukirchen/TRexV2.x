/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Body Ops</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CallBodyOps#getReply <em>Reply</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CallBodyOps#getCatch <em>Catch</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCallBodyOps()
 * @model
 * @generated
 */
public interface CallBodyOps extends EObject
{
  /**
   * Returns the value of the '<em><b>Reply</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reply</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reply</em>' containment reference.
   * @see #setReply(GetReplyStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCallBodyOps_Reply()
   * @model containment="true"
   * @generated
   */
  GetReplyStatement getReply();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CallBodyOps#getReply <em>Reply</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reply</em>' containment reference.
   * @see #getReply()
   * @generated
   */
  void setReply(GetReplyStatement value);

  /**
   * Returns the value of the '<em><b>Catch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Catch</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Catch</em>' containment reference.
   * @see #setCatch(CatchStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCallBodyOps_Catch()
   * @model containment="true"
   * @generated
   */
  CatchStatement getCatch();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CallBodyOps#getCatch <em>Catch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Catch</em>' containment reference.
   * @see #getCatch()
   * @generated
   */
  void setCatch(CatchStatement value);

} // CallBodyOps
