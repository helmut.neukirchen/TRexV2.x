/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Template Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getSymbol <em>Symbol</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getList <em>List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getExtended <em>Extended</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleTemplateExpression()
 * @model
 * @generated
 */
public interface SingleTemplateExpression extends EObject
{
  /**
   * Returns the value of the '<em><b>Symbol</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Symbol</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Symbol</em>' containment reference.
   * @see #setSymbol(MatchingSymbol)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleTemplateExpression_Symbol()
   * @model containment="true"
   * @generated
   */
  MatchingSymbol getSymbol();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getSymbol <em>Symbol</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Symbol</em>' containment reference.
   * @see #getSymbol()
   * @generated
   */
  void setSymbol(MatchingSymbol value);

  /**
   * Returns the value of the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List</em>' containment reference.
   * @see #setList(TemplateRefWithParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleTemplateExpression_List()
   * @model containment="true"
   * @generated
   */
  TemplateRefWithParList getList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getList <em>List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>List</em>' containment reference.
   * @see #getList()
   * @generated
   */
  void setList(TemplateRefWithParList value);

  /**
   * Returns the value of the '<em><b>Extended</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Extended</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Extended</em>' containment reference.
   * @see #setExtended(ExtendedFieldReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleTemplateExpression_Extended()
   * @model containment="true"
   * @generated
   */
  ExtendedFieldReference getExtended();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleTemplateExpression#getExtended <em>Extended</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Extended</em>' containment reference.
   * @see #getExtended()
   * @generated
   */
  void setExtended(ExtendedFieldReference value);

} // SingleTemplateExpression
