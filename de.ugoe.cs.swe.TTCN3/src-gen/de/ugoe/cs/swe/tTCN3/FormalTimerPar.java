/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Timer Par</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFormalTimerPar()
 * @model
 * @generated
 */
public interface FormalTimerPar extends TimerVarInstance
{
} // FormalTimerPar
