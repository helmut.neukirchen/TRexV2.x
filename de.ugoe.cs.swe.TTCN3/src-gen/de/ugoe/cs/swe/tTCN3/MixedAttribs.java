/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mixed Attribs</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MixedAttribs#getAddressDecls <em>Address Decls</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MixedAttribs#getMixedLists <em>Mixed Lists</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.MixedAttribs#getConfigDefs <em>Config Defs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMixedAttribs()
 * @model
 * @generated
 */
public interface MixedAttribs extends EObject
{
  /**
   * Returns the value of the '<em><b>Address Decls</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.AddressDecl}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Address Decls</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Address Decls</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMixedAttribs_AddressDecls()
   * @model containment="true"
   * @generated
   */
  EList<AddressDecl> getAddressDecls();

  /**
   * Returns the value of the '<em><b>Mixed Lists</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.MixedList}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mixed Lists</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mixed Lists</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMixedAttribs_MixedLists()
   * @model containment="true"
   * @generated
   */
  EList<MixedList> getMixedLists();

  /**
   * Returns the value of the '<em><b>Config Defs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ConfigParamDef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Config Defs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Config Defs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getMixedAttribs_ConfigDefs()
   * @model containment="true"
   * @generated
   */
  EList<ConfigParamDef> getConfigDefs();

} // MixedAttribs
