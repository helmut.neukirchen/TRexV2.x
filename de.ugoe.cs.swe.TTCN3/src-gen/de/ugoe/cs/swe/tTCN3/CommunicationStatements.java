/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Statements</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getSend <em>Send</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCall <em>Call</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getReply <em>Reply</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getRaise <em>Raise</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getReceive <em>Receive</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getGetCall <em>Get Call</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getGetReply <em>Get Reply</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCatch <em>Catch</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCheck <em>Check</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getClear <em>Clear</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getStart <em>Start</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getStop <em>Stop</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getHalt <em>Halt</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCheckState <em>Check State</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements()
 * @model
 * @generated
 */
public interface CommunicationStatements extends EObject
{
  /**
   * Returns the value of the '<em><b>Send</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Send</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Send</em>' containment reference.
   * @see #setSend(SendStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Send()
   * @model containment="true"
   * @generated
   */
  SendStatement getSend();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getSend <em>Send</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Send</em>' containment reference.
   * @see #getSend()
   * @generated
   */
  void setSend(SendStatement value);

  /**
   * Returns the value of the '<em><b>Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Call</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Call</em>' containment reference.
   * @see #setCall(CallStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Call()
   * @model containment="true"
   * @generated
   */
  CallStatement getCall();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCall <em>Call</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Call</em>' containment reference.
   * @see #getCall()
   * @generated
   */
  void setCall(CallStatement value);

  /**
   * Returns the value of the '<em><b>Reply</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reply</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reply</em>' containment reference.
   * @see #setReply(ReplyStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Reply()
   * @model containment="true"
   * @generated
   */
  ReplyStatement getReply();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getReply <em>Reply</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reply</em>' containment reference.
   * @see #getReply()
   * @generated
   */
  void setReply(ReplyStatement value);

  /**
   * Returns the value of the '<em><b>Raise</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Raise</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Raise</em>' containment reference.
   * @see #setRaise(RaiseStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Raise()
   * @model containment="true"
   * @generated
   */
  RaiseStatement getRaise();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getRaise <em>Raise</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Raise</em>' containment reference.
   * @see #getRaise()
   * @generated
   */
  void setRaise(RaiseStatement value);

  /**
   * Returns the value of the '<em><b>Receive</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Receive</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Receive</em>' containment reference.
   * @see #setReceive(ReceiveStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Receive()
   * @model containment="true"
   * @generated
   */
  ReceiveStatement getReceive();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getReceive <em>Receive</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Receive</em>' containment reference.
   * @see #getReceive()
   * @generated
   */
  void setReceive(ReceiveStatement value);

  /**
   * Returns the value of the '<em><b>Trigger</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Trigger</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Trigger</em>' containment reference.
   * @see #setTrigger(TriggerStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Trigger()
   * @model containment="true"
   * @generated
   */
  TriggerStatement getTrigger();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getTrigger <em>Trigger</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Trigger</em>' containment reference.
   * @see #getTrigger()
   * @generated
   */
  void setTrigger(TriggerStatement value);

  /**
   * Returns the value of the '<em><b>Get Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Get Call</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Get Call</em>' containment reference.
   * @see #setGetCall(GetCallStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_GetCall()
   * @model containment="true"
   * @generated
   */
  GetCallStatement getGetCall();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getGetCall <em>Get Call</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Get Call</em>' containment reference.
   * @see #getGetCall()
   * @generated
   */
  void setGetCall(GetCallStatement value);

  /**
   * Returns the value of the '<em><b>Get Reply</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Get Reply</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Get Reply</em>' containment reference.
   * @see #setGetReply(GetReplyStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_GetReply()
   * @model containment="true"
   * @generated
   */
  GetReplyStatement getGetReply();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getGetReply <em>Get Reply</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Get Reply</em>' containment reference.
   * @see #getGetReply()
   * @generated
   */
  void setGetReply(GetReplyStatement value);

  /**
   * Returns the value of the '<em><b>Catch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Catch</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Catch</em>' containment reference.
   * @see #setCatch(CatchStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Catch()
   * @model containment="true"
   * @generated
   */
  CatchStatement getCatch();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCatch <em>Catch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Catch</em>' containment reference.
   * @see #getCatch()
   * @generated
   */
  void setCatch(CatchStatement value);

  /**
   * Returns the value of the '<em><b>Check</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Check</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Check</em>' containment reference.
   * @see #setCheck(CheckStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Check()
   * @model containment="true"
   * @generated
   */
  CheckStatement getCheck();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCheck <em>Check</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Check</em>' containment reference.
   * @see #getCheck()
   * @generated
   */
  void setCheck(CheckStatement value);

  /**
   * Returns the value of the '<em><b>Clear</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Clear</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Clear</em>' containment reference.
   * @see #setClear(ClearStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Clear()
   * @model containment="true"
   * @generated
   */
  ClearStatement getClear();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getClear <em>Clear</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Clear</em>' containment reference.
   * @see #getClear()
   * @generated
   */
  void setClear(ClearStatement value);

  /**
   * Returns the value of the '<em><b>Start</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Start</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Start</em>' containment reference.
   * @see #setStart(StartStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Start()
   * @model containment="true"
   * @generated
   */
  StartStatement getStart();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getStart <em>Start</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Start</em>' containment reference.
   * @see #getStart()
   * @generated
   */
  void setStart(StartStatement value);

  /**
   * Returns the value of the '<em><b>Stop</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Stop</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Stop</em>' containment reference.
   * @see #setStop(StopStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Stop()
   * @model containment="true"
   * @generated
   */
  StopStatement getStop();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getStop <em>Stop</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Stop</em>' containment reference.
   * @see #getStop()
   * @generated
   */
  void setStop(StopStatement value);

  /**
   * Returns the value of the '<em><b>Halt</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Halt</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Halt</em>' containment reference.
   * @see #setHalt(HaltStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_Halt()
   * @model containment="true"
   * @generated
   */
  HaltStatement getHalt();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getHalt <em>Halt</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Halt</em>' containment reference.
   * @see #getHalt()
   * @generated
   */
  void setHalt(HaltStatement value);

  /**
   * Returns the value of the '<em><b>Check State</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Check State</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Check State</em>' containment reference.
   * @see #setCheckState(CheckStateStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getCommunicationStatements_CheckState()
   * @model containment="true"
   * @generated
   */
  CheckStateStatement getCheckState();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.CommunicationStatements#getCheckState <em>Check State</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Check State</em>' containment reference.
   * @see #getCheckState()
   * @generated
   */
  void setCheckState(CheckStateStatement value);

} // CommunicationStatements
