/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guard Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getGuard <em>Guard</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getStep <em>Step</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getBlock <em>Block</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getOp <em>Op</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardStatement()
 * @model
 * @generated
 */
public interface GuardStatement extends EObject
{
  /**
   * Returns the value of the '<em><b>Guard</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Guard</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Guard</em>' containment reference.
   * @see #setGuard(AltGuardChar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardStatement_Guard()
   * @model containment="true"
   * @generated
   */
  AltGuardChar getGuard();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getGuard <em>Guard</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Guard</em>' containment reference.
   * @see #getGuard()
   * @generated
   */
  void setGuard(AltGuardChar value);

  /**
   * Returns the value of the '<em><b>Step</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Step</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Step</em>' containment reference.
   * @see #setStep(AltstepInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardStatement_Step()
   * @model containment="true"
   * @generated
   */
  AltstepInstance getStep();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getStep <em>Step</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Step</em>' containment reference.
   * @see #getStep()
   * @generated
   */
  void setStep(AltstepInstance value);

  /**
   * Returns the value of the '<em><b>Block</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Block</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Block</em>' containment reference.
   * @see #setBlock(StatementBlock)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardStatement_Block()
   * @model containment="true"
   * @generated
   */
  StatementBlock getBlock();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getBlock <em>Block</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Block</em>' containment reference.
   * @see #getBlock()
   * @generated
   */
  void setBlock(StatementBlock value);

  /**
   * Returns the value of the '<em><b>Op</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' containment reference.
   * @see #setOp(GuardOp)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardStatement_Op()
   * @model containment="true"
   * @generated
   */
  GuardOp getOp();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardStatement#getOp <em>Op</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Op</em>' containment reference.
   * @see #getOp()
   * @generated
   */
  void setOp(GuardOp value);

} // GuardStatement
