/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guard Op</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardOp#getTimeout <em>Timeout</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardOp#getReceive <em>Receive</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardOp#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardOp#getGetCall <em>Get Call</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardOp#getCatch <em>Catch</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardOp#getCheck <em>Check</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardOp#getGetReply <em>Get Reply</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardOp#getDone <em>Done</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.GuardOp#getKilled <em>Killed</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardOp()
 * @model
 * @generated
 */
public interface GuardOp extends EObject
{
  /**
   * Returns the value of the '<em><b>Timeout</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timeout</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timeout</em>' containment reference.
   * @see #setTimeout(TimeoutStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardOp_Timeout()
   * @model containment="true"
   * @generated
   */
  TimeoutStatement getTimeout();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getTimeout <em>Timeout</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timeout</em>' containment reference.
   * @see #getTimeout()
   * @generated
   */
  void setTimeout(TimeoutStatement value);

  /**
   * Returns the value of the '<em><b>Receive</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Receive</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Receive</em>' containment reference.
   * @see #setReceive(ReceiveStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardOp_Receive()
   * @model containment="true"
   * @generated
   */
  ReceiveStatement getReceive();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getReceive <em>Receive</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Receive</em>' containment reference.
   * @see #getReceive()
   * @generated
   */
  void setReceive(ReceiveStatement value);

  /**
   * Returns the value of the '<em><b>Trigger</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Trigger</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Trigger</em>' containment reference.
   * @see #setTrigger(TriggerStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardOp_Trigger()
   * @model containment="true"
   * @generated
   */
  TriggerStatement getTrigger();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getTrigger <em>Trigger</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Trigger</em>' containment reference.
   * @see #getTrigger()
   * @generated
   */
  void setTrigger(TriggerStatement value);

  /**
   * Returns the value of the '<em><b>Get Call</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Get Call</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Get Call</em>' containment reference.
   * @see #setGetCall(GetCallStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardOp_GetCall()
   * @model containment="true"
   * @generated
   */
  GetCallStatement getGetCall();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getGetCall <em>Get Call</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Get Call</em>' containment reference.
   * @see #getGetCall()
   * @generated
   */
  void setGetCall(GetCallStatement value);

  /**
   * Returns the value of the '<em><b>Catch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Catch</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Catch</em>' containment reference.
   * @see #setCatch(CatchStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardOp_Catch()
   * @model containment="true"
   * @generated
   */
  CatchStatement getCatch();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getCatch <em>Catch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Catch</em>' containment reference.
   * @see #getCatch()
   * @generated
   */
  void setCatch(CatchStatement value);

  /**
   * Returns the value of the '<em><b>Check</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Check</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Check</em>' containment reference.
   * @see #setCheck(CheckStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardOp_Check()
   * @model containment="true"
   * @generated
   */
  CheckStatement getCheck();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getCheck <em>Check</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Check</em>' containment reference.
   * @see #getCheck()
   * @generated
   */
  void setCheck(CheckStatement value);

  /**
   * Returns the value of the '<em><b>Get Reply</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Get Reply</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Get Reply</em>' containment reference.
   * @see #setGetReply(GetReplyStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardOp_GetReply()
   * @model containment="true"
   * @generated
   */
  GetReplyStatement getGetReply();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getGetReply <em>Get Reply</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Get Reply</em>' containment reference.
   * @see #getGetReply()
   * @generated
   */
  void setGetReply(GetReplyStatement value);

  /**
   * Returns the value of the '<em><b>Done</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Done</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Done</em>' containment reference.
   * @see #setDone(DoneStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardOp_Done()
   * @model containment="true"
   * @generated
   */
  DoneStatement getDone();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getDone <em>Done</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Done</em>' containment reference.
   * @see #getDone()
   * @generated
   */
  void setDone(DoneStatement value);

  /**
   * Returns the value of the '<em><b>Killed</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Killed</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Killed</em>' containment reference.
   * @see #setKilled(KilledStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getGuardOp_Killed()
   * @model containment="true"
   * @generated
   */
  KilledStatement getKilled();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.GuardOp#getKilled <em>Killed</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Killed</em>' containment reference.
   * @see #getKilled()
   * @generated
   */
  void setKilled(KilledStatement value);

} // GuardOp
