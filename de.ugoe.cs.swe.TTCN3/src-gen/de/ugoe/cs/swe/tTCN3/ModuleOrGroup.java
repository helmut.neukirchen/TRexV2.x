/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module Or Group</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleOrGroup()
 * @model
 * @generated
 */
public interface ModuleOrGroup extends ReferencedType, RefValueHead
{
} // ModuleOrGroup
