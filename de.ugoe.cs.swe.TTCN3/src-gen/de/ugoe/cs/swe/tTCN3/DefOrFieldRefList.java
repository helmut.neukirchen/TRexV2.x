/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Def Or Field Ref List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRefList#getRefs <em>Refs</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getDefOrFieldRefList()
 * @model
 * @generated
 */
public interface DefOrFieldRefList extends EObject
{
  /**
   * Returns the value of the '<em><b>Refs</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Refs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Refs</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getDefOrFieldRefList_Refs()
   * @model containment="true"
   * @generated
   */
  EList<DefOrFieldRef> getRefs();

} // DefOrFieldRefList
