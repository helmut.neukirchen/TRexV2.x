/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Var Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.VarInstance#getListMod <em>List Mod</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.VarInstance#getListType <em>List Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.VarInstance#getList <em>List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.VarInstance#getResTemplate <em>Res Template</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.VarInstance#getTempMod <em>Temp Mod</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.VarInstance#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.VarInstance#getTempList <em>Temp List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getVarInstance()
 * @model
 * @generated
 */
public interface VarInstance extends EObject
{
  /**
   * Returns the value of the '<em><b>List Mod</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List Mod</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List Mod</em>' attribute.
   * @see #setListMod(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getVarInstance_ListMod()
   * @model
   * @generated
   */
  String getListMod();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getListMod <em>List Mod</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>List Mod</em>' attribute.
   * @see #getListMod()
   * @generated
   */
  void setListMod(String value);

  /**
   * Returns the value of the '<em><b>List Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List Type</em>' containment reference.
   * @see #setListType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getVarInstance_ListType()
   * @model containment="true"
   * @generated
   */
  Type getListType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getListType <em>List Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>List Type</em>' containment reference.
   * @see #getListType()
   * @generated
   */
  void setListType(Type value);

  /**
   * Returns the value of the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List</em>' containment reference.
   * @see #setList(VarList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getVarInstance_List()
   * @model containment="true"
   * @generated
   */
  VarList getList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getList <em>List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>List</em>' containment reference.
   * @see #getList()
   * @generated
   */
  void setList(VarList value);

  /**
   * Returns the value of the '<em><b>Res Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Res Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Res Template</em>' containment reference.
   * @see #setResTemplate(RestrictedTemplate)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getVarInstance_ResTemplate()
   * @model containment="true"
   * @generated
   */
  RestrictedTemplate getResTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getResTemplate <em>Res Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Res Template</em>' containment reference.
   * @see #getResTemplate()
   * @generated
   */
  void setResTemplate(RestrictedTemplate value);

  /**
   * Returns the value of the '<em><b>Temp Mod</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Temp Mod</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Temp Mod</em>' attribute.
   * @see #setTempMod(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getVarInstance_TempMod()
   * @model
   * @generated
   */
  String getTempMod();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getTempMod <em>Temp Mod</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Temp Mod</em>' attribute.
   * @see #getTempMod()
   * @generated
   */
  void setTempMod(String value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Type)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getVarInstance_Type()
   * @model containment="true"
   * @generated
   */
  Type getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Type value);

  /**
   * Returns the value of the '<em><b>Temp List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Temp List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Temp List</em>' containment reference.
   * @see #setTempList(TempVarList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getVarInstance_TempList()
   * @model containment="true"
   * @generated
   */
  TempVarList getTempList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.VarInstance#getTempList <em>Temp List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Temp List</em>' containment reference.
   * @see #getTempList()
   * @generated
   */
  void setTempList(TempVarList value);

} // VarInstance
