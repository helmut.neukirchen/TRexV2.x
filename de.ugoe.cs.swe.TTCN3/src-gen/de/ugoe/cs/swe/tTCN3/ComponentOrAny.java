/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Or Any</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentOrAny#getCompOrDefault <em>Comp Or Default</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ComponentOrAny#getRef <em>Ref</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentOrAny()
 * @model
 * @generated
 */
public interface ComponentOrAny extends EObject
{
  /**
   * Returns the value of the '<em><b>Comp Or Default</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Comp Or Default</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Comp Or Default</em>' containment reference.
   * @see #setCompOrDefault(ComponentOrDefaultReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentOrAny_CompOrDefault()
   * @model containment="true"
   * @generated
   */
  ComponentOrDefaultReference getCompOrDefault();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentOrAny#getCompOrDefault <em>Comp Or Default</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Comp Or Default</em>' containment reference.
   * @see #getCompOrDefault()
   * @generated
   */
  void setCompOrDefault(ComponentOrDefaultReference value);

  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(VariableRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getComponentOrAny_Ref()
   * @model containment="true"
   * @generated
   */
  VariableRef getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ComponentOrAny#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(VariableRef value);

} // ComponentOrAny
