/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Testcase Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getRef <em>Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getTcParams <em>Tc Params</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getExpr <em>Expr</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getSexpr <em>Sexpr</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseInstance()
 * @model
 * @generated
 */
public interface TestcaseInstance extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' reference.
   * @see #setRef(TestcaseDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseInstance_Ref()
   * @model
   * @generated
   */
  TestcaseDef getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getRef <em>Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' reference.
   * @see #getRef()
   * @generated
   */
  void setRef(TestcaseDef value);

  /**
   * Returns the value of the '<em><b>Tc Params</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tc Params</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tc Params</em>' containment reference.
   * @see #setTcParams(TestcaseActualParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseInstance_TcParams()
   * @model containment="true"
   * @generated
   */
  TestcaseActualParList getTcParams();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getTcParams <em>Tc Params</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Tc Params</em>' containment reference.
   * @see #getTcParams()
   * @generated
   */
  void setTcParams(TestcaseActualParList value);

  /**
   * Returns the value of the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr</em>' containment reference.
   * @see #setExpr(Expression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseInstance_Expr()
   * @model containment="true"
   * @generated
   */
  Expression getExpr();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getExpr <em>Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expr</em>' containment reference.
   * @see #getExpr()
   * @generated
   */
  void setExpr(Expression value);

  /**
   * Returns the value of the '<em><b>Sexpr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sexpr</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sexpr</em>' containment reference.
   * @see #setSexpr(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseInstance_Sexpr()
   * @model containment="true"
   * @generated
   */
  SingleExpression getSexpr();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TestcaseInstance#getSexpr <em>Sexpr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sexpr</em>' containment reference.
   * @see #getSexpr()
   * @generated
   */
  void setSexpr(SingleExpression value);

} // TestcaseInstance
