/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Except Group Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptGroupSpec#getIdList <em>Id List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExceptGroupSpec#getAll <em>All</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptGroupSpec()
 * @model
 * @generated
 */
public interface ExceptGroupSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Id List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id List</em>' containment reference.
   * @see #setIdList(QualifiedIdentifierList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptGroupSpec_IdList()
   * @model containment="true"
   * @generated
   */
  QualifiedIdentifierList getIdList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptGroupSpec#getIdList <em>Id List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id List</em>' containment reference.
   * @see #getIdList()
   * @generated
   */
  void setIdList(QualifiedIdentifierList value);

  /**
   * Returns the value of the '<em><b>All</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All</em>' attribute.
   * @see #setAll(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExceptGroupSpec_All()
   * @model
   * @generated
   */
  String getAll();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExceptGroupSpec#getAll <em>All</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>All</em>' attribute.
   * @see #getAll()
   * @generated
   */
  void setAll(String value);

} // ExceptGroupSpec
