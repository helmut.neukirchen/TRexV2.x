/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Def Or Field Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getId <em>Id</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getField <em>Field</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getExtended <em>Extended</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getAll <em>All</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getDefOrFieldRef()
 * @model
 * @generated
 */
public interface DefOrFieldRef extends EObject
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' reference.
   * @see #setId(TTCN3Reference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getDefOrFieldRef_Id()
   * @model
   * @generated
   */
  TTCN3Reference getId();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getId <em>Id</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' reference.
   * @see #getId()
   * @generated
   */
  void setId(TTCN3Reference value);

  /**
   * Returns the value of the '<em><b>Field</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field</em>' containment reference.
   * @see #setField(FieldReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getDefOrFieldRef_Field()
   * @model containment="true"
   * @generated
   */
  FieldReference getField();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getField <em>Field</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field</em>' containment reference.
   * @see #getField()
   * @generated
   */
  void setField(FieldReference value);

  /**
   * Returns the value of the '<em><b>Extended</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Extended</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Extended</em>' containment reference.
   * @see #setExtended(ExtendedFieldReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getDefOrFieldRef_Extended()
   * @model containment="true"
   * @generated
   */
  ExtendedFieldReference getExtended();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getExtended <em>Extended</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Extended</em>' containment reference.
   * @see #getExtended()
   * @generated
   */
  void setExtended(ExtendedFieldReference value);

  /**
   * Returns the value of the '<em><b>All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All</em>' containment reference.
   * @see #setAll(AllRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getDefOrFieldRef_All()
   * @model containment="true"
   * @generated
   */
  AllRef getAll();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.DefOrFieldRef#getAll <em>All</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>All</em>' containment reference.
   * @see #getAll()
   * @generated
   */
  void setAll(AllRef value);

} // DefOrFieldRef
