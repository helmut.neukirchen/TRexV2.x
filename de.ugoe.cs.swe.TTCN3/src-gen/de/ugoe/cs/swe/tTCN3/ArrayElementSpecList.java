/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Element Spec List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpecList#getSpec <em>Spec</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getArrayElementSpecList()
 * @model
 * @generated
 */
public interface ArrayElementSpecList extends EObject
{
  /**
   * Returns the value of the '<em><b>Spec</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ArrayElementSpec}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spec</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spec</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getArrayElementSpecList_Spec()
   * @model containment="true"
   * @generated
   */
  EList<ArrayElementSpec> getSpec();

} // ArrayElementSpecList
