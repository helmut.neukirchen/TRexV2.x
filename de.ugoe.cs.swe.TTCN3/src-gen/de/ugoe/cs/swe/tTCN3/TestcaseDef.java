/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Testcase Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseDef#getParList <em>Par List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseDef#getSpec <em>Spec</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseDef#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseDef()
 * @model
 * @generated
 */
public interface TestcaseDef extends TTCN3Reference
{
  /**
   * Returns the value of the '<em><b>Par List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Par List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Par List</em>' containment reference.
   * @see #setParList(TemplateOrValueFormalParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseDef_ParList()
   * @model containment="true"
   * @generated
   */
  TemplateOrValueFormalParList getParList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TestcaseDef#getParList <em>Par List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Par List</em>' containment reference.
   * @see #getParList()
   * @generated
   */
  void setParList(TemplateOrValueFormalParList value);

  /**
   * Returns the value of the '<em><b>Spec</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Spec</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Spec</em>' containment reference.
   * @see #setSpec(ConfigSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseDef_Spec()
   * @model containment="true"
   * @generated
   */
  ConfigSpec getSpec();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TestcaseDef#getSpec <em>Spec</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Spec</em>' containment reference.
   * @see #getSpec()
   * @generated
   */
  void setSpec(ConfigSpec value);

  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(StatementBlock)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseDef_Statement()
   * @model containment="true"
   * @generated
   */
  StatementBlock getStatement();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TestcaseDef#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(StatementBlock value);

} // TestcaseDef
