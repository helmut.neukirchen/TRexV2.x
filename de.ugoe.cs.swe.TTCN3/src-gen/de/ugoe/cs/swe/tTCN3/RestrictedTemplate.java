/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Restricted Template</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.RestrictedTemplate#getRestriction <em>Restriction</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRestrictedTemplate()
 * @model
 * @generated
 */
public interface RestrictedTemplate extends ReturnType
{
  /**
   * Returns the value of the '<em><b>Restriction</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Restriction</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Restriction</em>' containment reference.
   * @see #setRestriction(TemplateRestriction)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getRestrictedTemplate_Restriction()
   * @model containment="true"
   * @generated
   */
  TemplateRestriction getRestriction();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.RestrictedTemplate#getRestriction <em>Restriction</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Restriction</em>' containment reference.
   * @see #getRestriction()
   * @generated
   */
  void setRestriction(TemplateRestriction value);

} // RestrictedTemplate
