/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interleaved Construct</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getInterleavedConstruct()
 * @model
 * @generated
 */
public interface InterleavedConstruct extends EObject
{
} // InterleavedConstruct
