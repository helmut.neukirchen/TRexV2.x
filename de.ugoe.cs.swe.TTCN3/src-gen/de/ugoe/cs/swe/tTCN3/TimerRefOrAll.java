/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timer Ref Or All</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAll#getRef <em>Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAll#getTimer <em>Timer</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerRefOrAll()
 * @model
 * @generated
 */
public interface TimerRefOrAll extends EObject
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' reference.
   * @see #setRef(TimerVarInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerRefOrAll_Ref()
   * @model
   * @generated
   */
  TimerVarInstance getRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAll#getRef <em>Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' reference.
   * @see #getRef()
   * @generated
   */
  void setRef(TimerVarInstance value);

  /**
   * Returns the value of the '<em><b>Timer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' attribute.
   * @see #setTimer(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerRefOrAll_Timer()
   * @model
   * @generated
   */
  String getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerRefOrAll#getTimer <em>Timer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' attribute.
   * @see #getTimer()
   * @generated
   */
  void setTimer(String value);

} // TimerRefOrAll
