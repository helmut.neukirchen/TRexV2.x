/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Else If Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getKeyElse <em>Key Else</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getKeyIf <em>Key If</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getExpression <em>Expression</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getElseIfClause()
 * @model
 * @generated
 */
public interface ElseIfClause extends EObject
{
  /**
   * Returns the value of the '<em><b>Key Else</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Key Else</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Key Else</em>' attribute.
   * @see #setKeyElse(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getElseIfClause_KeyElse()
   * @model
   * @generated
   */
  String getKeyElse();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getKeyElse <em>Key Else</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Key Else</em>' attribute.
   * @see #getKeyElse()
   * @generated
   */
  void setKeyElse(String value);

  /**
   * Returns the value of the '<em><b>Key If</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Key If</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Key If</em>' attribute.
   * @see #setKeyIf(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getElseIfClause_KeyIf()
   * @model
   * @generated
   */
  String getKeyIf();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getKeyIf <em>Key If</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Key If</em>' attribute.
   * @see #getKeyIf()
   * @generated
   */
  void setKeyIf(String value);

  /**
   * Returns the value of the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression</em>' containment reference.
   * @see #setExpression(BooleanExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getElseIfClause_Expression()
   * @model containment="true"
   * @generated
   */
  BooleanExpression getExpression();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getExpression <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression</em>' containment reference.
   * @see #getExpression()
   * @generated
   */
  void setExpression(BooleanExpression value);

  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(StatementBlock)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getElseIfClause_Statement()
   * @model containment="true"
   * @generated
   */
  StatementBlock getStatement();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ElseIfClause#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(StatementBlock value);

} // ElseIfClause
