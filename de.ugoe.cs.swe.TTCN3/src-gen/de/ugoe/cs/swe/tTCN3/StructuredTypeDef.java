/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structured Type Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getRecord <em>Record</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getUnion <em>Union</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getSet <em>Set</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getRecordOf <em>Record Of</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getSetOf <em>Set Of</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getEnumDef <em>Enum Def</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getPort <em>Port</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getComponent <em>Component</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructuredTypeDef()
 * @model
 * @generated
 */
public interface StructuredTypeDef extends EObject
{
  /**
   * Returns the value of the '<em><b>Record</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Record</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Record</em>' containment reference.
   * @see #setRecord(RecordDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructuredTypeDef_Record()
   * @model containment="true"
   * @generated
   */
  RecordDef getRecord();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getRecord <em>Record</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Record</em>' containment reference.
   * @see #getRecord()
   * @generated
   */
  void setRecord(RecordDef value);

  /**
   * Returns the value of the '<em><b>Union</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Union</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Union</em>' containment reference.
   * @see #setUnion(UnionDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructuredTypeDef_Union()
   * @model containment="true"
   * @generated
   */
  UnionDef getUnion();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getUnion <em>Union</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Union</em>' containment reference.
   * @see #getUnion()
   * @generated
   */
  void setUnion(UnionDef value);

  /**
   * Returns the value of the '<em><b>Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Set</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Set</em>' containment reference.
   * @see #setSet(SetDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructuredTypeDef_Set()
   * @model containment="true"
   * @generated
   */
  SetDef getSet();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getSet <em>Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Set</em>' containment reference.
   * @see #getSet()
   * @generated
   */
  void setSet(SetDef value);

  /**
   * Returns the value of the '<em><b>Record Of</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Record Of</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Record Of</em>' containment reference.
   * @see #setRecordOf(RecordOfDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructuredTypeDef_RecordOf()
   * @model containment="true"
   * @generated
   */
  RecordOfDef getRecordOf();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getRecordOf <em>Record Of</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Record Of</em>' containment reference.
   * @see #getRecordOf()
   * @generated
   */
  void setRecordOf(RecordOfDef value);

  /**
   * Returns the value of the '<em><b>Set Of</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Set Of</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Set Of</em>' containment reference.
   * @see #setSetOf(SetOfDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructuredTypeDef_SetOf()
   * @model containment="true"
   * @generated
   */
  SetOfDef getSetOf();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getSetOf <em>Set Of</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Set Of</em>' containment reference.
   * @see #getSetOf()
   * @generated
   */
  void setSetOf(SetOfDef value);

  /**
   * Returns the value of the '<em><b>Enum Def</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Enum Def</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Enum Def</em>' containment reference.
   * @see #setEnumDef(EnumDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructuredTypeDef_EnumDef()
   * @model containment="true"
   * @generated
   */
  EnumDef getEnumDef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getEnumDef <em>Enum Def</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Enum Def</em>' containment reference.
   * @see #getEnumDef()
   * @generated
   */
  void setEnumDef(EnumDef value);

  /**
   * Returns the value of the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' containment reference.
   * @see #setPort(PortDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructuredTypeDef_Port()
   * @model containment="true"
   * @generated
   */
  PortDef getPort();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getPort <em>Port</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' containment reference.
   * @see #getPort()
   * @generated
   */
  void setPort(PortDef value);

  /**
   * Returns the value of the '<em><b>Component</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Component</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Component</em>' containment reference.
   * @see #setComponent(ComponentDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getStructuredTypeDef_Component()
   * @model containment="true"
   * @generated
   */
  ComponentDef getComponent();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.StructuredTypeDef#getComponent <em>Component</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component</em>' containment reference.
   * @see #getComponent()
   * @generated
   */
  void setComponent(ComponentDef value);

} // StructuredTypeDef
