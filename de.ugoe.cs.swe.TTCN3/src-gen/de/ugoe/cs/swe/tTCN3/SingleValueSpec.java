/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Value Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleValueSpec#getVariable <em>Variable</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleValueSpec#getField <em>Field</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleValueSpec#getExt <em>Ext</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleValueSpec()
 * @model
 * @generated
 */
public interface SingleValueSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable</em>' containment reference.
   * @see #setVariable(VariableRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleValueSpec_Variable()
   * @model containment="true"
   * @generated
   */
  VariableRef getVariable();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleValueSpec#getVariable <em>Variable</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable</em>' containment reference.
   * @see #getVariable()
   * @generated
   */
  void setVariable(VariableRef value);

  /**
   * Returns the value of the '<em><b>Field</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field</em>' reference.
   * @see #setField(FieldReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleValueSpec_Field()
   * @model
   * @generated
   */
  FieldReference getField();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleValueSpec#getField <em>Field</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field</em>' reference.
   * @see #getField()
   * @generated
   */
  void setField(FieldReference value);

  /**
   * Returns the value of the '<em><b>Ext</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ExtendedFieldReference}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ext</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ext</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleValueSpec_Ext()
   * @model containment="true"
   * @generated
   */
  EList<ExtendedFieldReference> getExt();

} // SingleValueSpec
