/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Statement List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionStatementList#getStatements <em>Statements</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionStatementList#getSc <em>Sc</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatementList()
 * @model
 * @generated
 */
public interface FunctionStatementList extends EObject
{
  /**
   * Returns the value of the '<em><b>Statements</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.FunctionStatement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statements</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatementList_Statements()
   * @model containment="true"
   * @generated
   */
  EList<FunctionStatement> getStatements();

  /**
   * Returns the value of the '<em><b>Sc</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sc</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sc</em>' attribute list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionStatementList_Sc()
   * @model unique="false"
   * @generated
   */
  EList<String> getSc();

} // FunctionStatementList
