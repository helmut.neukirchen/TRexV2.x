/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timer Statements</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerStatements#getStart <em>Start</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerStatements#getStop <em>Stop</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerStatements#getTimeout <em>Timeout</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerStatements()
 * @model
 * @generated
 */
public interface TimerStatements extends EObject
{
  /**
   * Returns the value of the '<em><b>Start</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Start</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Start</em>' containment reference.
   * @see #setStart(StartTimerStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerStatements_Start()
   * @model containment="true"
   * @generated
   */
  StartTimerStatement getStart();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerStatements#getStart <em>Start</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Start</em>' containment reference.
   * @see #getStart()
   * @generated
   */
  void setStart(StartTimerStatement value);

  /**
   * Returns the value of the '<em><b>Stop</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Stop</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Stop</em>' containment reference.
   * @see #setStop(StopTimerStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerStatements_Stop()
   * @model containment="true"
   * @generated
   */
  StopTimerStatement getStop();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerStatements#getStop <em>Stop</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Stop</em>' containment reference.
   * @see #getStop()
   * @generated
   */
  void setStop(StopTimerStatement value);

  /**
   * Returns the value of the '<em><b>Timeout</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timeout</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timeout</em>' containment reference.
   * @see #setTimeout(TimeoutStatement)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerStatements_Timeout()
   * @model containment="true"
   * @generated
   */
  TimeoutStatement getTimeout();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerStatements#getTimeout <em>Timeout</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timeout</em>' containment reference.
   * @see #getTimeout()
   * @generated
   */
  void setTimeout(TimeoutStatement value);

} // TimerStatements
