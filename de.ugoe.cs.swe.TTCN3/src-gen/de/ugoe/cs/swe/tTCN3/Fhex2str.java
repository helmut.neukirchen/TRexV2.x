/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fhex2str</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.Fhex2str#getE1 <em>E1</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFhex2str()
 * @model
 * @generated
 */
public interface Fhex2str extends PreDefFunction
{
  /**
   * Returns the value of the '<em><b>E1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>E1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>E1</em>' containment reference.
   * @see #setE1(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFhex2str_E1()
   * @model containment="true"
   * @generated
   */
  SingleExpression getE1();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.Fhex2str#getE1 <em>E1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>E1</em>' containment reference.
   * @see #getE1()
   * @generated
   */
  void setE1(SingleExpression value);

} // Fhex2str
