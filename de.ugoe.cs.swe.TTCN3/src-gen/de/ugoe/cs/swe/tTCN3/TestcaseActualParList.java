/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Testcase Actual Par List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseActualParList#getTemplParam <em>Templ Param</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TestcaseActualParList#getTemplAssign <em>Templ Assign</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseActualParList()
 * @model
 * @generated
 */
public interface TestcaseActualParList extends EObject
{
  /**
   * Returns the value of the '<em><b>Templ Param</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.TemplateInstanceActualPar}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Templ Param</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Templ Param</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseActualParList_TemplParam()
   * @model containment="true"
   * @generated
   */
  EList<TemplateInstanceActualPar> getTemplParam();

  /**
   * Returns the value of the '<em><b>Templ Assign</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.TemplateInstanceAssignment}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Templ Assign</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Templ Assign</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTestcaseActualParList_TemplAssign()
   * @model containment="true"
   * @generated
   */
  EList<TemplateInstanceAssignment> getTemplAssign();

} // TestcaseActualParList
