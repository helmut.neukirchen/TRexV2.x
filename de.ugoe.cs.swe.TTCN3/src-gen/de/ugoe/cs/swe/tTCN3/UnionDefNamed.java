/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Union Def Named</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getUnionDefNamed()
 * @model
 * @generated
 */
public interface UnionDefNamed extends ReferencedType, UnionDef
{
} // UnionDefNamed
