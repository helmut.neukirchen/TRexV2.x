/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module Definitions List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ModuleDefinitionsList#getDefinitions <em>Definitions</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleDefinitionsList()
 * @model
 * @generated
 */
public interface ModuleDefinitionsList extends EObject
{
  /**
   * Returns the value of the '<em><b>Definitions</b></em>' containment reference list.
   * The list contents are of type {@link de.ugoe.cs.swe.tTCN3.ModuleDefinition}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Definitions</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Definitions</em>' containment reference list.
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getModuleDefinitionsList_Definitions()
   * @model containment="true"
   * @generated
   */
  EList<ModuleDefinition> getDefinitions();

} // ModuleDefinitionsList
