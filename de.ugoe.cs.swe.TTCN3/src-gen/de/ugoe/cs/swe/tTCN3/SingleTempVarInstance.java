/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Temp Var Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getArray <em>Array</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getAc <em>Ac</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getTemplate <em>Template</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleTempVarInstance()
 * @model
 * @generated
 */
public interface SingleTempVarInstance extends RefValue
{
  /**
   * Returns the value of the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array</em>' containment reference.
   * @see #setArray(ArrayDef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleTempVarInstance_Array()
   * @model containment="true"
   * @generated
   */
  ArrayDef getArray();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getArray <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Array</em>' containment reference.
   * @see #getArray()
   * @generated
   */
  void setArray(ArrayDef value);

  /**
   * Returns the value of the '<em><b>Ac</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ac</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ac</em>' attribute.
   * @see #setAc(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleTempVarInstance_Ac()
   * @model
   * @generated
   */
  String getAc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getAc <em>Ac</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ac</em>' attribute.
   * @see #getAc()
   * @generated
   */
  void setAc(String value);

  /**
   * Returns the value of the '<em><b>Template</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Template</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Template</em>' containment reference.
   * @see #setTemplate(TemplateBody)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSingleTempVarInstance_Template()
   * @model containment="true"
   * @generated
   */
  TemplateBody getTemplate();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SingleTempVarInstance#getTemplate <em>Template</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Template</em>' containment reference.
   * @see #getTemplate()
   * @generated
   */
  void setTemplate(TemplateBody value);

} // SingleTempVarInstance
