/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Expression Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getFieldRef <em>Field Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getArray <em>Array</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getExpr <em>Expr</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldExpressionSpec()
 * @model
 * @generated
 */
public interface FieldExpressionSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Field Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field Ref</em>' reference.
   * @see #setFieldRef(FieldReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldExpressionSpec_FieldRef()
   * @model
   * @generated
   */
  FieldReference getFieldRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getFieldRef <em>Field Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field Ref</em>' reference.
   * @see #getFieldRef()
   * @generated
   */
  void setFieldRef(FieldReference value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldExpressionSpec_Type()
   * @model
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

  /**
   * Returns the value of the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array</em>' containment reference.
   * @see #setArray(ArrayOrBitRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldExpressionSpec_Array()
   * @model containment="true"
   * @generated
   */
  ArrayOrBitRef getArray();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getArray <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Array</em>' containment reference.
   * @see #getArray()
   * @generated
   */
  void setArray(ArrayOrBitRef value);

  /**
   * Returns the value of the '<em><b>Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr</em>' containment reference.
   * @see #setExpr(NotUsedOrExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldExpressionSpec_Expr()
   * @model containment="true"
   * @generated
   */
  NotUsedOrExpression getExpr();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FieldExpressionSpec#getExpr <em>Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expr</em>' containment reference.
   * @see #getExpr()
   * @generated
   */
  void setExpr(NotUsedOrExpression value);

} // FieldExpressionSpec
