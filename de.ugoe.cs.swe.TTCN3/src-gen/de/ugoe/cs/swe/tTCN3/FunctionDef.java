/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getDet <em>Det</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getParameterList <em>Parameter List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getRunsOn <em>Runs On</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getMtc <em>Mtc</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getSystem <em>System</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getStatement <em>Statement</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDef()
 * @model
 * @generated
 */
public interface FunctionDef extends FunctionRef
{
  /**
   * Returns the value of the '<em><b>Det</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Det</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Det</em>' attribute.
   * @see #setDet(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDef_Det()
   * @model
   * @generated
   */
  String getDet();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getDet <em>Det</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Det</em>' attribute.
   * @see #getDet()
   * @generated
   */
  void setDet(String value);

  /**
   * Returns the value of the '<em><b>Parameter List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parameter List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parameter List</em>' containment reference.
   * @see #setParameterList(FunctionFormalParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDef_ParameterList()
   * @model containment="true"
   * @generated
   */
  FunctionFormalParList getParameterList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getParameterList <em>Parameter List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Parameter List</em>' containment reference.
   * @see #getParameterList()
   * @generated
   */
  void setParameterList(FunctionFormalParList value);

  /**
   * Returns the value of the '<em><b>Runs On</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Runs On</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Runs On</em>' containment reference.
   * @see #setRunsOn(RunsOnSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDef_RunsOn()
   * @model containment="true"
   * @generated
   */
  RunsOnSpec getRunsOn();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getRunsOn <em>Runs On</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Runs On</em>' containment reference.
   * @see #getRunsOn()
   * @generated
   */
  void setRunsOn(RunsOnSpec value);

  /**
   * Returns the value of the '<em><b>Mtc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mtc</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mtc</em>' containment reference.
   * @see #setMtc(MtcSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDef_Mtc()
   * @model containment="true"
   * @generated
   */
  MtcSpec getMtc();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getMtc <em>Mtc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mtc</em>' containment reference.
   * @see #getMtc()
   * @generated
   */
  void setMtc(MtcSpec value);

  /**
   * Returns the value of the '<em><b>System</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>System</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>System</em>' containment reference.
   * @see #setSystem(SystemSpec)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDef_System()
   * @model containment="true"
   * @generated
   */
  SystemSpec getSystem();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getSystem <em>System</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>System</em>' containment reference.
   * @see #getSystem()
   * @generated
   */
  void setSystem(SystemSpec value);

  /**
   * Returns the value of the '<em><b>Return Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Return Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return Type</em>' containment reference.
   * @see #setReturnType(ReturnType)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDef_ReturnType()
   * @model containment="true"
   * @generated
   */
  ReturnType getReturnType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getReturnType <em>Return Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Return Type</em>' containment reference.
   * @see #getReturnType()
   * @generated
   */
  void setReturnType(ReturnType value);

  /**
   * Returns the value of the '<em><b>Statement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Statement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Statement</em>' containment reference.
   * @see #setStatement(StatementBlock)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFunctionDef_Statement()
   * @model containment="true"
   * @generated
   */
  StatementBlock getStatement();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FunctionDef#getStatement <em>Statement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Statement</em>' containment reference.
   * @see #getStatement()
   * @generated
   */
  void setStatement(StatementBlock value);

} // FunctionDef
