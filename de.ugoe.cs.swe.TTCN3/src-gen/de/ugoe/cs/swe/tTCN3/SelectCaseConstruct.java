/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select Case Construct</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SelectCaseConstruct#getExpression <em>Expression</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.SelectCaseConstruct#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSelectCaseConstruct()
 * @model
 * @generated
 */
public interface SelectCaseConstruct extends EObject
{
  /**
   * Returns the value of the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression</em>' containment reference.
   * @see #setExpression(SingleExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSelectCaseConstruct_Expression()
   * @model containment="true"
   * @generated
   */
  SingleExpression getExpression();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SelectCaseConstruct#getExpression <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression</em>' containment reference.
   * @see #getExpression()
   * @generated
   */
  void setExpression(SingleExpression value);

  /**
   * Returns the value of the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Body</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Body</em>' containment reference.
   * @see #setBody(SelectCaseBody)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getSelectCaseConstruct_Body()
   * @model containment="true"
   * @generated
   */
  SelectCaseBody getBody();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.SelectCaseConstruct#getBody <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Body</em>' containment reference.
   * @see #getBody()
   * @generated
   */
  void setBody(SelectCaseBody value);

} // SelectCaseConstruct
