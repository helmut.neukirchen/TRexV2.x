/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>All Groups With Except</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AllGroupsWithExcept#getIdList <em>Id List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllGroupsWithExcept()
 * @model
 * @generated
 */
public interface AllGroupsWithExcept extends EObject
{
  /**
   * Returns the value of the '<em><b>Id List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id List</em>' containment reference.
   * @see #setIdList(QualifiedIdentifierList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllGroupsWithExcept_IdList()
   * @model containment="true"
   * @generated
   */
  QualifiedIdentifierList getIdList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AllGroupsWithExcept#getIdList <em>Id List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id List</em>' containment reference.
   * @see #getIdList()
   * @generated
   */
  void setIdList(QualifiedIdentifierList value);

} // AllGroupsWithExcept
