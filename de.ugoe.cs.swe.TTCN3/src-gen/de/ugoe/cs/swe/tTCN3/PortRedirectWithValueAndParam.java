/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Redirect With Value And Param</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortRedirectWithValueAndParam()
 * @model
 * @generated
 */
public interface PortRedirectWithValueAndParam extends EObject
{
} // PortRedirectWithValueAndParam
