/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Const Expression Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getFieldRef <em>Field Ref</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getType <em>Type</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getArray <em>Array</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getConstExpr <em>Const Expr</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldConstExpressionSpec()
 * @model
 * @generated
 */
public interface FieldConstExpressionSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Field Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Field Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Field Ref</em>' reference.
   * @see #setFieldRef(FieldReference)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldConstExpressionSpec_FieldRef()
   * @model
   * @generated
   */
  FieldReference getFieldRef();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getFieldRef <em>Field Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Field Ref</em>' reference.
   * @see #getFieldRef()
   * @generated
   */
  void setFieldRef(FieldReference value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldConstExpressionSpec_Type()
   * @model
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

  /**
   * Returns the value of the '<em><b>Array</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Array</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Array</em>' containment reference.
   * @see #setArray(ArrayOrBitRef)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldConstExpressionSpec_Array()
   * @model containment="true"
   * @generated
   */
  ArrayOrBitRef getArray();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getArray <em>Array</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Array</em>' containment reference.
   * @see #getArray()
   * @generated
   */
  void setArray(ArrayOrBitRef value);

  /**
   * Returns the value of the '<em><b>Const Expr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Const Expr</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Const Expr</em>' containment reference.
   * @see #setConstExpr(ConstantExpression)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getFieldConstExpressionSpec_ConstExpr()
   * @model containment="true"
   * @generated
   */
  ConstantExpression getConstExpr();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec#getConstExpr <em>Const Expr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Const Expr</em>' containment reference.
   * @see #getConstExpr()
   * @generated
   */
  void setConstExpr(ConstantExpression value);

} // FieldConstExpressionSpec
