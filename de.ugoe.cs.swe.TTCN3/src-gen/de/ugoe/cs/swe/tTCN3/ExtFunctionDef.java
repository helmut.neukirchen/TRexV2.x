/**
 */
package de.ugoe.cs.swe.tTCN3;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ext Function Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getDet <em>Det</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getList <em>List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getReturn <em>Return</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExtFunctionDef()
 * @model
 * @generated
 */
public interface ExtFunctionDef extends FunctionRef
{
  /**
   * Returns the value of the '<em><b>Det</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Det</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Det</em>' attribute.
   * @see #setDet(String)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExtFunctionDef_Det()
   * @model
   * @generated
   */
  String getDet();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getDet <em>Det</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Det</em>' attribute.
   * @see #getDet()
   * @generated
   */
  void setDet(String value);

  /**
   * Returns the value of the '<em><b>List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List</em>' containment reference.
   * @see #setList(FunctionFormalParList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExtFunctionDef_List()
   * @model containment="true"
   * @generated
   */
  FunctionFormalParList getList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getList <em>List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>List</em>' containment reference.
   * @see #getList()
   * @generated
   */
  void setList(FunctionFormalParList value);

  /**
   * Returns the value of the '<em><b>Return</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Return</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return</em>' containment reference.
   * @see #setReturn(ReturnType)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getExtFunctionDef_Return()
   * @model containment="true"
   * @generated
   */
  ReturnType getReturn();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ExtFunctionDef#getReturn <em>Return</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Return</em>' containment reference.
   * @see #getReturn()
   * @generated
   */
  void setReturn(ReturnType value);

} // ExtFunctionDef
