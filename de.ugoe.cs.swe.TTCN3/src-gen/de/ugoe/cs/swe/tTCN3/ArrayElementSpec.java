/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Element Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpec#getMatch <em>Match</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpec#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getArrayElementSpec()
 * @model
 * @generated
 */
public interface ArrayElementSpec extends EObject
{
  /**
   * Returns the value of the '<em><b>Match</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Match</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Match</em>' containment reference.
   * @see #setMatch(PermutationMatch)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getArrayElementSpec_Match()
   * @model containment="true"
   * @generated
   */
  PermutationMatch getMatch();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpec#getMatch <em>Match</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Match</em>' containment reference.
   * @see #getMatch()
   * @generated
   */
  void setMatch(PermutationMatch value);

  /**
   * Returns the value of the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Body</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Body</em>' containment reference.
   * @see #setBody(TemplateBody)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getArrayElementSpec_Body()
   * @model containment="true"
   * @generated
   */
  TemplateBody getBody();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.ArrayElementSpec#getBody <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Body</em>' containment reference.
   * @see #getBody()
   * @generated
   */
  void setBody(TemplateBody value);

} // ArrayElementSpec
