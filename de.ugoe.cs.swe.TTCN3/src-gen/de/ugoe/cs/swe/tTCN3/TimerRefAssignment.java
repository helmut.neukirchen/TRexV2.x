/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timer Ref Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerRefAssignment#getTimer <em>Timer</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.TimerRefAssignment#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerRefAssignment()
 * @model
 * @generated
 */
public interface TimerRefAssignment extends EObject
{
  /**
   * Returns the value of the '<em><b>Timer</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Timer</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Timer</em>' reference.
   * @see #setTimer(FormalTimerPar)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerRefAssignment_Timer()
   * @model
   * @generated
   */
  FormalTimerPar getTimer();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerRefAssignment#getTimer <em>Timer</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Timer</em>' reference.
   * @see #getTimer()
   * @generated
   */
  void setTimer(FormalTimerPar value);

  /**
   * Returns the value of the '<em><b>Value</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' reference.
   * @see #setValue(TimerVarInstance)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getTimerRefAssignment_Value()
   * @model
   * @generated
   */
  TimerVarInstance getValue();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.TimerRefAssignment#getValue <em>Value</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' reference.
   * @see #getValue()
   * @generated
   */
  void setValue(TimerVarInstance value);

} // TimerRefAssignment
