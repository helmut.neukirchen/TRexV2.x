/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>All Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AllRef#getGroupList <em>Group List</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.AllRef#getIdList <em>Id List</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllRef()
 * @model
 * @generated
 */
public interface AllRef extends EObject
{
  /**
   * Returns the value of the '<em><b>Group List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group List</em>' containment reference.
   * @see #setGroupList(GroupRefList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllRef_GroupList()
   * @model containment="true"
   * @generated
   */
  GroupRefList getGroupList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AllRef#getGroupList <em>Group List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Group List</em>' containment reference.
   * @see #getGroupList()
   * @generated
   */
  void setGroupList(GroupRefList value);

  /**
   * Returns the value of the '<em><b>Id List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id List</em>' containment reference.
   * @see #setIdList(TTCN3ReferenceList)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getAllRef_IdList()
   * @model containment="true"
   * @generated
   */
  TTCN3ReferenceList getIdList();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.AllRef#getIdList <em>Id List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id List</em>' containment reference.
   * @see #getIdList()
   * @generated
   */
  void setIdList(TTCN3ReferenceList value);

} // AllRef
