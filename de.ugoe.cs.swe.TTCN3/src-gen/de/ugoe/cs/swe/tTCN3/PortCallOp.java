/**
 */
package de.ugoe.cs.swe.tTCN3;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Call Op</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortCallOp#getParams <em>Params</em>}</li>
 *   <li>{@link de.ugoe.cs.swe.tTCN3.PortCallOp#getTo <em>To</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortCallOp()
 * @model
 * @generated
 */
public interface PortCallOp extends EObject
{
  /**
   * Returns the value of the '<em><b>Params</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Params</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Params</em>' containment reference.
   * @see #setParams(CallParameters)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortCallOp_Params()
   * @model containment="true"
   * @generated
   */
  CallParameters getParams();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortCallOp#getParams <em>Params</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Params</em>' containment reference.
   * @see #getParams()
   * @generated
   */
  void setParams(CallParameters value);

  /**
   * Returns the value of the '<em><b>To</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>To</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>To</em>' containment reference.
   * @see #setTo(ToClause)
   * @see de.ugoe.cs.swe.tTCN3.TTCN3Package#getPortCallOp_To()
   * @model containment="true"
   * @generated
   */
  ToClause getTo();

  /**
   * Sets the value of the '{@link de.ugoe.cs.swe.tTCN3.PortCallOp#getTo <em>To</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>To</em>' containment reference.
   * @see #getTo()
   * @generated
   */
  void setTo(ToClause value);

} // PortCallOp
