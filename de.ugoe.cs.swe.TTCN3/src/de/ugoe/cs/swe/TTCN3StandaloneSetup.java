/*
* generated by Xtext
*/
package de.ugoe.cs.swe;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class TTCN3StandaloneSetup extends TTCN3StandaloneSetupGenerated{

	public static void doSetup() {
		new TTCN3StandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

