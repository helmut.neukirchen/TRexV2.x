package de.ugoe.cs.swe.scoping;

import org.apache.log4j.Logger;
import org.eclipse.xtext.util.PolymorphicDispatcher.ErrorHandler;

public class TTCN3ScopeErrorHandler<IScope> implements ErrorHandler<IScope> {
	
    private Logger logger;

    public TTCN3ScopeErrorHandler( Logger logger ) {
        this.logger = logger;
    }
    
    @Override
    public IScope handle( Object[] params, Throwable throwable ) {
        
        if( !(throwable instanceof NoSuchMethodException ) ) {
            logger.warn("Error in polymorphic dispatcher : "+throwable.getMessage(), throwable);
        }
        
        return null;
    }	
	
}
