package de.ugoe.cs.swe.scoping;

import org.eclipse.xtext.generator.scoping.AbstractScopingFragment;
import org.eclipse.xtext.scoping.IGlobalScopeProvider;
import org.eclipse.xtext.scoping.IScopeProvider;

/**
 * @author Daniel Honsel
 */

public class TTCN3ImportNamespaceScopingFragment extends AbstractScopingFragment {

	@Override
	protected Class<? extends IScopeProvider> getLocalScopeProvider() {
		return TTCN3LocalScopeProvider.class;
	}

	@Override
	protected Class<? extends IGlobalScopeProvider> getGlobalScopeProvider() {
		return TTCN3GlobalScopeProvider.class;
	}

}
