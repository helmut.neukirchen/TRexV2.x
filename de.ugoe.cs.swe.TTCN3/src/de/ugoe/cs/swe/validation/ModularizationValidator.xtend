package de.ugoe.cs.swe.validation

import com.google.inject.Inject
import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile
import de.ugoe.cs.swe.common.ConfigTools
import de.ugoe.cs.swe.common.MiscTools
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass
import de.ugoe.cs.swe.scoping.TTCN3GlobalScopeProvider
import de.ugoe.cs.swe.tTCN3.FunctionDef
import de.ugoe.cs.swe.tTCN3.FunctionInstance
import de.ugoe.cs.swe.tTCN3.ImportDef
import de.ugoe.cs.swe.tTCN3.ModuleDefinition
import de.ugoe.cs.swe.tTCN3.StartTCStatement
import de.ugoe.cs.swe.tTCN3.StartTimerStatement
import de.ugoe.cs.swe.tTCN3.TTCN3Module
import de.ugoe.cs.swe.tTCN3.TTCN3Package
import de.ugoe.cs.swe.tTCN3.TimerInstance
import de.ugoe.cs.swe.tTCN3.TypeDef
import java.util.List
import java.util.regex.Matcher
import java.util.regex.Pattern
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.scoping.IGlobalScopeProvider
import org.eclipse.xtext.validation.AbstractDeclarativeValidator
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.EValidatorRegistrar

import static de.ugoe.cs.swe.scoping.TTCN3GlobalScopeProvider.*

import static extension de.ugoe.cs.swe.common.TTCN3ScopeHelper.*
import static extension org.eclipse.xtext.EcoreUtil2.*

class ModularizationValidator extends AbstractDeclarativeValidator {
	val ConfigTools configTools = ConfigTools.getInstance;
	var QualityCheckProfile activeProfile = configTools.selectedProfile as QualityCheckProfile
	var ModuleContentHelper moduleHelper = new ModuleContentHelper(activeProfile)

	@Inject
	private IGlobalScopeProvider globalScopeProvider;

	@Check
	def checkModuleContainment(TTCN3Module module) {
		if (activeProfile.checkTypesAndValuesModuleContainmentCheck &&
			module.name.contains(ModuleContentHelper.TypesAndValuesModuleIDSubstring)) {
			module.checkModuleContainment(ModuleContentHelper.TypesAndValuesModuleIDSubstring)
		} else if (activeProfile.checkTemplatesModuleContainmentCheck &&
			module.name.contains(ModuleContentHelper.TemplatesModuleIDSubstring) && module.name.contains(ModuleContentHelper.FunctionsModuleIDSubstring )) {
			module.checkModuleContainment(ModuleContentHelper.TemplatesModuleIDSubstring, ModuleContentHelper.FunctionsModuleIDSubstring)			
		} else if (activeProfile.checkTemplatesModuleContainmentCheck &&
			module.name.contains(ModuleContentHelper.TemplatesModuleIDSubstring)) {
			module.checkModuleContainment(ModuleContentHelper.TemplatesModuleIDSubstring)
		} else if (activeProfile.checkFunctionsModuleContainmentCheck &&
			module.name.contains(ModuleContentHelper.FunctionsModuleIDSubstring)) {
			module.checkModuleContainment(ModuleContentHelper.FunctionsModuleIDSubstring)
		} else if (activeProfile.checkTestcasesModuleContainmentCheck &&
			module.name.contains(ModuleContentHelper.TestcasesModuleIDSubstring)) {
			module.checkModuleContainment(ModuleContentHelper.TestcasesModuleIDSubstring)
		} else if (activeProfile.checkModuleParamsModuleContainmentCheck &&
			module.name.contains(ModuleContentHelper.ModuleParamsModuleIDSubstring)) {
			module.checkModuleContainment(ModuleContentHelper.ModuleParamsModuleIDSubstring)
		} else if (activeProfile.checkInterfaceModuleContainmentCheck &&
			module.name.contains(ModuleContentHelper.InterfaceModuleIDSubstring)) {
			module.checkModuleContainment(ModuleContentHelper.InterfaceModuleIDSubstring)
		} else if (activeProfile.checkTestSystemModuleContainmentCheck &&
			module.name.contains(ModuleContentHelper.TestSystemModuleIDSubstring)) {
			module.checkModuleContainment(ModuleContentHelper.TestSystemModuleIDSubstring)
		} else if (activeProfile.checkTestControlModuleContainmentCheck &&
			module.name.contains(ModuleContentHelper.TestControlModuleIDSubstring)) {
			module.checkModuleContainment(ModuleContentHelper.TestControlModuleIDSubstring)
		}
	}

	private def checkModuleContainment(TTCN3Module module, String moduleRestriction) {
		for (d : module.getAllContentsOfType(ModuleDefinition)) {
			var EObject definition = null
			if (moduleRestriction == ModuleContentHelper.TestSystemModuleIDSubstring) {
				definition = d.def
				if (definition instanceof TypeDef) {
					if (definition.body.structured !== null && definition.body.structured.component !== null) {
						definition = definition.body.structured.component
					}
				}
			} else {
				definition = d.def
			}
			if (!moduleHelper.typePermittedInRestrictedModule(moduleRestriction, definition.eClass)) {
				TTCN3StatisticsProvider.getInstance.incrementCountModularization
				val message = "Definition in " + moduleRestriction +
					" module is not of the permissible definition type(s)!"
				val INode node = NodeModelUtils.getNode(definition)
				warning(
					message,
					null,
					MessageClass.MODULARIZATION.toString,
					node.startLine.toString,
					node.endLine.toString,
					"7.x, " + MiscTools.getMethodName()
				);
			} else {
				if (moduleRestriction == ModuleContentHelper.TestcasesModuleIDSubstring &&
					definition instanceof FunctionDef) {
					if (!(definition as FunctionDef).functionStartBehavior(module)) {
						TTCN3StatisticsProvider.getInstance.incrementCountModularization
						val message = "Definition in " + moduleRestriction +
							" module is not of the permissible definition type(s)! Function " +
							(definition as FunctionDef).name + " is never referenced in a start statement!"
						val INode node = NodeModelUtils.getNode(definition)
						warning(
							message,
							null,
							MessageClass.MODULARIZATION.toString,
							node.startLine.toString,
							node.endLine.toString,
							"7.x, " + MiscTools.getMethodName()
						);
					}
				}
			}
		}
	}

	// Special case for TemplatesAndTestcases modules
	private def checkModuleContainment(TTCN3Module module, String moduleRestriction1, String moduleRestriction2) {
		for (d : module.getAllContentsOfType(ModuleDefinition)) {
			var EObject definition = d.def
			if (!(moduleHelper.typePermittedInRestrictedModule(moduleRestriction1, definition.eClass)
				|| moduleHelper.typePermittedInRestrictedModule(moduleRestriction2, definition.eClass))
			) {
				TTCN3StatisticsProvider.getInstance.incrementCountModularization
				val message = "Definition in " + moduleRestriction1 + " and " + moduleRestriction2 +
					" module is not of the permissible definition type(s)!"
				val INode node = NodeModelUtils.getNode(definition)
				warning(
					message,
					null,
					MessageClass.MODULARIZATION.toString,
					node.startLine.toString,
					node.endLine.toString,
					"7.x, " + MiscTools.getMethodName()
				);
			}
		}
	}

	private def boolean functionStartBehavior(FunctionDef function, TTCN3Module module) {
		val List<FunctionInstance> list = newArrayList
		
		val TTCN3GlobalScopeProvider scopeProvider = globalScopeProvider as TTCN3GlobalScopeProvider
		if (scopeProvider === null) {
			return false
		}
	
		synchronized ( IMPORTED_FROM ) {
			for (m : IMPORTED_FROM.get(module)) {
				for (instance : m.eAllOfType(FunctionInstance)) {
					val reference = instance.ref
					if (reference instanceof FunctionDef) {
						if (reference.equals(function)) {
							list.add(instance)
						}
					}
				}
			}			
		}
		
		for (r : list) {
			val startTC = r.findDesiredParent(StartTCStatement)
			val startTimer = r.findDesiredParent(StartTimerStatement)
			val nestedParameter = r.findDesiredParent(FunctionInstance)
			var timer = false
			if (startTimer !== null && startTimer.ref.findDesiredParent(TimerInstance) !== null) {
				timer = true
			}

			if (startTC !== null || startTimer !== null && !(nestedParameter !== null || timer)) {
				return true
			}
		}
		return false
	}

	@Check
	def checkModuleSize(TTCN3Module module) {
		if (!activeProfile.checkModuleSize) {
			return
		}

		val INode node = NodeModelUtils.getNode(module)
		val moduleSize = node.getEndOffset() - node.getOffset();
		val referenceSize = activeProfile.getMaximumAllowedModuleSizeInBytes();
		if (moduleSize > referenceSize) {
			TTCN3StatisticsProvider.getInstance.incrementCountModularization
			val message = "Module \"" + module.name + "\" is larger (" + moduleSize +
				" bytes) than the maximum allowed module size (" + referenceSize + " bytes)!"
			warning(
				message,
				TTCN3Package.eINSTANCE.TTCN3Reference_Name,
				MessageClass.MODULARIZATION.toString,
				node.startLine.toString,
				node.endLine.toString,
				"7.21, " + MiscTools.getMethodName()
			);
		}
	}

	@Check
	def checkTypesAndValuesModuleImportsLibNames(TTCN3Module module) {
		if (!activeProfile.checkTypesAndValuesModuleImportsLibNames)
			return;

		val Pattern typesAndValuesImportsLibNamesExcludedPattern = Pattern.compile(
			activeProfile.getTypesAndValuesImportsLibNamesExcludedRegExp())
		val Matcher typesAndValuesImportsLibNamesExcludedMatcher = typesAndValuesImportsLibNamesExcludedPattern.matcher(
			module.name)

		if (!module.name.contains(ModuleContentHelper.TypesAndValuesModuleIDSubstring) ||
			typesAndValuesImportsLibNamesExcludedMatcher.matches())
			return;

		val Pattern typesAndValuesImportsLibNamesPattern = Pattern.compile(
			activeProfile.getTypesAndValuesImportsLibNamesRegExp());
		var Matcher typesAndValuesImportsLibNamesMatcher = null;
		var libCommonReferenceFound = false;

		for (i : module.getAllContentsOfType(ImportDef)) {
			typesAndValuesImportsLibNamesMatcher = typesAndValuesImportsLibNamesPattern.matcher(i.name)
			if (typesAndValuesImportsLibNamesMatcher.matches()) {
				libCommonReferenceFound = true
			}
		}

		if (!libCommonReferenceFound) {
			TTCN3StatisticsProvider.getInstance.incrementCountModularization
			val message = "Required import from \"" + activeProfile.getTypesAndValuesImportsLibNamesRegExp() +
				"\" not found!!"
			val INode node = NodeModelUtils.getNode(module)
			warning(
				message,
				TTCN3Package.eINSTANCE.TTCN3Reference_Name,
				MessageClass.MODULARIZATION.toString,
				node.startLine.toString,
				node.startLine.toString,
				"7.3, " + MiscTools.getMethodName()
			);
		}
	}

	@Check
	def checkTestcasesModuleImportsLibCommon_Sync(TTCN3Module module) {
		if (!activeProfile.checkTestcasesModuleImportsLibCommon_Sync)
			return;

		if ((!module.name.contains("Testcases")) || module.name.contains("LibCommon_Sync")) {
			return
		}

		var libCommonReferenceFound = false;

		for (i : module.getAllContentsOfType(ImportDef)) {
			if (i.name.contains("LibCommon_Sync")) {
				libCommonReferenceFound = true
			}
		}

		if (!libCommonReferenceFound) {
			TTCN3StatisticsProvider.getInstance.incrementCountModularization
			val message = "Required import from \"LibCommon_Sync\" not found!"
			val INode node = NodeModelUtils.getNode(module)
			warning(
				message,
				TTCN3Package.eINSTANCE.TTCN3Reference_Name,
				MessageClass.MODULARIZATION.toString,
				node.startLine.toString,
				node.startLine.toString,
				"7.11, " + MiscTools.getMethodName()
			);
		}
	}

	override register(EValidatorRegistrar registrar) {
		// not needed for classes used as ComposedCheck
	}
}
