package de.ugoe.cs.swe.validation;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.emf.ecore.EClass;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;

public class ModuleContentHelper {

	public static final String TypesAndValuesModuleIDSubstring = "TypesAndValues";
	public static final String TemplatesModuleIDSubstring = "Templates";
	public static final String FunctionsModuleIDSubstring = "Functions";
	public static final String TestcasesModuleIDSubstring = "Testcases";
	public static final String ModuleParamsModuleIDSubstring = "ModuleParams";
	public static final String InterfaceModuleIDSubstring = "Interface";
	public static final String TestSystemModuleIDSubstring = "TestSystem";
	public static final String TestControlModuleIDSubstring = "TestControl";

	private HashMap<String, ArrayList<EClass>> permissibleModuleContentsMap;
	private QualityCheckProfile activeProfile;

	public ModuleContentHelper(QualityCheckProfile activeProfile) {
		this.activeProfile = activeProfile;
		permissibleModuleContentsMap = Maps.newHashMap();
		addTypesAndValuesModulePermissibleTypes();
		addTemplatesModulePermissibleTypes();
		addFunctionsModulePermissibleTypes();
		addTestcasesModulePermissibleTypes();
		addModuleParamsModulePermissibleTypes();
		addInterfaceModulePermissibleTypes();
		addTestSystemModulePermissibleTypes();
		addTestControlModulePermissibleTypes();
	}

	private void addPermissibleListForModuleID(String moduleIDSubstring, ArrayList<EClass> permissibleTypesList) {
		this.permissibleModuleContentsMap.put(moduleIDSubstring, permissibleTypesList);
	}

	private void addTypesAndValuesModulePermissibleTypes() {
		ArrayList<EClass> permissibleTypesList = Lists.newArrayList();

		permissibleTypesList.add(TTCN3Package.eINSTANCE.getTypeDef());
		permissibleTypesList.add(TTCN3Package.eINSTANCE.getConstDef());

		addPermissibleListForModuleID(TypesAndValuesModuleIDSubstring, permissibleTypesList);
	}

	private void addTemplatesModulePermissibleTypes() {
		ArrayList<EClass> permissibleTypesList = Lists.newArrayList();

		permissibleTypesList.add(TTCN3Package.eINSTANCE.getTemplateDef());

		addPermissibleListForModuleID(TemplatesModuleIDSubstring, permissibleTypesList);

	}

	private void addFunctionsModulePermissibleTypes() {
		ArrayList<EClass> permissibleTypesList = Lists.newArrayList();

		permissibleTypesList.add(TTCN3Package.eINSTANCE.getFunctionDef());
		permissibleTypesList.add(TTCN3Package.eINSTANCE.getAltstepDef());
		if (activeProfile.isCheckFunctionsModuleContainmentCheckAllowExtFunction()) {
			permissibleTypesList.add(TTCN3Package.eINSTANCE.getExtFunctionDef());
		}

		addPermissibleListForModuleID(FunctionsModuleIDSubstring, permissibleTypesList);

	}

	private void addTestcasesModulePermissibleTypes() {
		ArrayList<EClass> permissibleTypesList = Lists.newArrayList();

		permissibleTypesList.add(TTCN3Package.eINSTANCE.getTestcaseDef());
		permissibleTypesList.add(TTCN3Package.eINSTANCE.getFunctionDef());
		addPermissibleListForModuleID(TestcasesModuleIDSubstring, permissibleTypesList);

	}

	private void addModuleParamsModulePermissibleTypes() {
		ArrayList<EClass> permissibleTypesList = Lists.newArrayList();

		permissibleTypesList.add(TTCN3Package.eINSTANCE.getModuleParDef());
		addPermissibleListForModuleID(ModuleParamsModuleIDSubstring, permissibleTypesList);

	}

	private void addInterfaceModulePermissibleTypes() {
		ArrayList<EClass> permissibleTypesList = Lists.newArrayList();

		// permissibleTypesList.add(TTCN3ParserTokenTypes.ComponentDef);
		// permissibleTypesList.add(TTCN3ParserTokenTypes.PortDef);
		// TODO: the above can then be considered redundant..
		permissibleTypesList.add(TTCN3Package.eINSTANCE.getTypeDef());
		addPermissibleListForModuleID(InterfaceModuleIDSubstring, permissibleTypesList);

	}

	private void addTestSystemModulePermissibleTypes() {
		ArrayList<EClass> permissibleTypesList = Lists.newArrayList();

		permissibleTypesList.add(TTCN3Package.eINSTANCE.getComponentDef());
		addPermissibleListForModuleID(TestSystemModuleIDSubstring, permissibleTypesList);

	}

	private void addTestControlModulePermissibleTypes() {
		ArrayList<EClass> permissibleTypesList = Lists.newArrayList();

		// No definitions are permitted
		addPermissibleListForModuleID(TestControlModuleIDSubstring, permissibleTypesList);

	}

	public boolean typePermittedInRestrictedModule(String moduleRestriction, EClass type) {
		boolean permitted = false;
		if (this.permissibleModuleContentsMap.get(moduleRestriction).contains(type) || type == TTCN3Package.eINSTANCE.getImportDef()
				|| type == TTCN3Package.eINSTANCE.getGroupDef()) {
			permitted = true;
		}
		return permitted;
	}

}
