package de.ugoe.cs.swe.validation

import com.google.inject.Inject
import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile
import de.ugoe.cs.swe.common.ConfigTools
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass
import de.ugoe.cs.swe.tTCN3.AltConstruct
import de.ugoe.cs.swe.tTCN3.AltstepDef
import de.ugoe.cs.swe.tTCN3.AltstepLocalDef
import de.ugoe.cs.swe.tTCN3.Assignment
import de.ugoe.cs.swe.tTCN3.BasicStatements
import de.ugoe.cs.swe.tTCN3.BehaviourStatements
import de.ugoe.cs.swe.tTCN3.CallStatement
import de.ugoe.cs.swe.tTCN3.CommunicationStatements
import de.ugoe.cs.swe.tTCN3.ComponentDef
import de.ugoe.cs.swe.tTCN3.ComponentElementDef
import de.ugoe.cs.swe.tTCN3.ConditionalConstruct
import de.ugoe.cs.swe.tTCN3.ControlStatement
import de.ugoe.cs.swe.tTCN3.ExtFunctionDef
import de.ugoe.cs.swe.tTCN3.FunctionDef
import de.ugoe.cs.swe.tTCN3.FunctionDefList
import de.ugoe.cs.swe.tTCN3.FunctionFormalPar
import de.ugoe.cs.swe.tTCN3.FunctionInstance
import de.ugoe.cs.swe.tTCN3.FunctionLocalDef
import de.ugoe.cs.swe.tTCN3.FunctionLocalInst
import de.ugoe.cs.swe.tTCN3.FunctionStatement
import de.ugoe.cs.swe.tTCN3.FunctionStatementList
import de.ugoe.cs.swe.tTCN3.InterleavedConstruct
import de.ugoe.cs.swe.tTCN3.InterleavedGuardList
import de.ugoe.cs.swe.tTCN3.LoopConstruct
import de.ugoe.cs.swe.tTCN3.ModuleControlBody
import de.ugoe.cs.swe.tTCN3.ReferencedValue
import de.ugoe.cs.swe.tTCN3.SelectCaseConstruct
import de.ugoe.cs.swe.tTCN3.SingleTempVarInstance
import de.ugoe.cs.swe.tTCN3.SingleVarInstance
import de.ugoe.cs.swe.tTCN3.StatementBlock
import de.ugoe.cs.swe.tTCN3.TimerInstance
import de.ugoe.cs.swe.tTCN3.TimerStatements
import de.ugoe.cs.swe.tTCN3.Value
import de.ugoe.cs.swe.tTCN3.VarInstance
import de.ugoe.cs.swe.tTCN3.VariableRef
import de.ugoe.cs.swe.tTCN3.WithStatement
import de.ugoe.cs.swe.validation.DataFlowHelper.STATUS
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.nodemodel.impl.CompositeNode
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.scoping.IGlobalScopeProvider
import org.eclipse.xtext.validation.AbstractDeclarativeValidator
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.EValidatorRegistrar

import static extension de.ugoe.cs.swe.common.TTCN3ScopeHelper.*
import static extension org.eclipse.xtext.EcoreUtil2.*

class DataFlowValidator extends AbstractDeclarativeValidator {
    val ConfigTools configTools = ConfigTools.getInstance;
    var QualityCheckProfile activeProfile = configTools.selectedProfile as QualityCheckProfile

    @Inject
    private IGlobalScopeProvider globalScopeProvider;

    override register(EValidatorRegistrar registrar) {
        // not needed for classes used as ComposedCheck
    }

    @Check(NORMAL)
    def checkNoUninitialisedLocalDefinitions(StatementBlock block) {
        if (!activeProfile.checkNoUninitialisedVariables) {
            return
        }

        var parentBlock = block.findDesiredParent(StatementBlock)
        var parentControl = block.findDesiredParent(ModuleControlBody)
        var parentAltstep = block.findDesiredParent(AltstepDef)
        if (parentBlock === null 
            && parentControl === null
            && parentAltstep === null
        ) { //top-level
            val DataFlowHelper dfh = new DataFlowHelper()
            
            //debugging
            try {
                block.traverseChildren(dfh)
            } catch (Exception e) {
                println(block.URI)
                e.printStackTrace
            }
            //live
//            block.traverseChildren(dfh)
        }        

    }

    @Check(NORMAL)
    def checkNoUninitialisedLocalDefinitions(ModuleControlBody control) {
        if (!activeProfile.checkNoUninitialisedVariables) {
            return
        }

        val DataFlowHelper dfh = new DataFlowHelper()
        
        //debugging
        try {
            control.traverseChildren(dfh)
        } catch (Exception e) {
            println(control.URI)
            e.printStackTrace
        }
        //live
//         control.traverseChildren(dfh)

    }

    @Check(NORMAL)
    def checkNoUninitialisedLocalDefinitions(AltstepDef altstep) {
        if (!activeProfile.checkNoUninitialisedVariables) {
            return
        }

        val DataFlowHelper dfh = new DataFlowHelper()
        
        //debugging
        try {
            altstep.traverseChildren(dfh)
        } catch (Exception e) {
            println(altstep.URI)
            e.printStackTrace
        }
        //live
//         altstep.traverseChildren(dfh)

    }


    private def void traverseChildren(AltstepDef altstep, DataFlowHelper dfh) {
        //defs and statements in order
        for (cse : altstep.local.defs) {
            cse.processAltstepLocalDef(dfh)
        }
        for (b : altstep.guard.guardList) {
            dfh.checkVariableStatus(b.guard.eAllOfType(ReferencedValue))
            var bdfh = new DataFlowHelper(dfh)
            //op 
            if (b.op !== null) {
                val bValues = b.op.eAllOfType(ReferencedValue)
                dfh.checkVariableStatus(bValues.filter[eContainer instanceof Value])
                for (v : bValues.filter[eContainer instanceof VariableRef]) {
                    //valid only within -> bdfh for the branch, propagate if in all branches
                    bdfh.updateVariableStatus(v)
                }
            //step
            } else if (b.step !== null) {
                val bValues = b.step.eAllOfType(ReferencedValue)
                dfh.checkVariableStatus(bValues.filter[eContainer instanceof Value])
            }
            if (b.block !== null) {
                b.block.traverseChildren(bdfh)
            }
        }
                
        //else?
        for (b : altstep.guard.elseList) {
            var bdfh = new DataFlowHelper(dfh)
            b.block.traverseChildren(bdfh)
        }
    }


    private def traverseChildren(ModuleControlBody control, DataFlowHelper dfh) {
        //defs and statements in order
        for (c : NodeModelUtils.getNode(control.list).children.filter[hasDirectSemanticElement]) {
                val cse = c.semanticElement
//                println("###  "+c.semanticElement.eClass.name + "  "+c.text)

                if (cse instanceof FunctionLocalDef) {
                    //constDef=ConstDef -> always defined
                    //templateDef=TemplateDef -> always defined
                } else if (cse instanceof FunctionLocalInst) {
                    cse.processFunctionLocalInst(dfh)
                } else if (cse instanceof ControlStatement) {
                    cse.processControlStatement(dfh)
                }
        }
    }

    private def traverseChildren(StatementBlock block, DataFlowHelper dfh) {
        
        //defs and statements in order
        for (c : NodeModelUtils.getNode(block).children.filter[hasDirectSemanticElement]) {
            if (c instanceof CompositeNode) {
                val e = c.semanticElement
                if (e instanceof FunctionDefList) {
                    //handle defs
                    for (ce : c.children.filter[hasDirectSemanticElement]) {
//                        println("###  "+ce.semanticElement.eClass.name + "  "+ce.text)

                        val cse = ce.semanticElement
                        if (cse instanceof FunctionLocalDef) {
                            //constDef=ConstDef -> always defined
                            //templateDef=TemplateDef -> always defined
                        } else if (cse instanceof FunctionLocalInst) {
                            cse.processFunctionLocalInst(dfh)
                        } else if (cse instanceof WithStatement) {
                            //expression / free text
                            dfh.checkVariableStatus(cse.eAllOfType(ReferencedValue))
                        }

                    }
                } else if (e instanceof FunctionStatementList) {
                    //handle statements
                    for (ce : c.children.filter[hasDirectSemanticElement]) {
//                        println("###  "+ce.semanticElement.eClass.name + "  "+ce.text)

                        val cse = ce.semanticElement
                        if (cse instanceof FunctionStatement) {
                            cse.processFunctionStatement(dfh)                        
                        }

                    }
                }
            }
        }
    }
    
    protected def void processControlStatement(ControlStatement cse, DataFlowHelper dfh) {
//        basic=BasicStatements
//        behavior=BehaviourStatements
//        timer=TimerStatements
//        sut=SUTStatements
        if (cse.basic !== null) {
            
            cse.basic.processBasicStatement(dfh)
            
        } else if (cse.behavior !== null) {
    
            cse.behavior.processBehaviourStatement(dfh)
    
        } else if (cse.timer !== null) {
            
            cse.timer.processTimerStatement(dfh)

        } else if (cse.sut !== null) {
            
            for (t : cse.sut.txt) {
                dfh.checkVariableStatus(t.expr.eAllOfType(ReferencedValue))
            }
        }
    }

    protected def void processFunctionLocalInst(FunctionLocalInst inst, DataFlowHelper dfh) {
            //variable=VarInstance 
            //timer=TimerInstance -> always defined
            if (inst.variable !== null) {
                inst.variable.processVarInstance(dfh)
            } else if (inst.timer !== null) {
                for (target : inst.timer.list.variables) {
                    dfh.checkVariableStatus(target.eAllOfType(ReferencedValue))
                }                               
            }
    }

    protected def void processAltstepLocalDef(AltstepLocalDef inst, DataFlowHelper dfh) {
            //variable=VarInstance 
            //timer=TimerInstance -> always defined
            if (inst.variable !== null) {
                inst.variable.processVarInstance(dfh)
            } else if (inst.timer !== null) {
                for (target : inst.timer.list.variables) {
                    dfh.checkVariableStatus(target.eAllOfType(ReferencedValue))
                }                               
            } else if (inst.const !== null) {
                dfh.checkVariableStatus(inst.const.eAllOfType(ReferencedValue))
            } else if (inst.template !== null) {
                dfh.checkVariableStatus(inst.template.eAllOfType(ReferencedValue))
            }
    }
    
    protected def void processVarInstance(VarInstance variable, DataFlowHelper dfh) {
        if (variable.list !== null) {
            for (target : variable.list.variables) {
                dfh.checkVariableStatus(target.eAllOfType(ReferencedValue))
                dfh.updateVariableStatus(target)
            }
        } else if (variable.tempList !== null) {
            for (target : variable.tempList.variables) {
                dfh.checkVariableStatus(target.eAllOfType(ReferencedValue))
                dfh.updateVariableStatus(target)
            }                    
        }
    }

    
    protected def void processFunctionStatement(FunctionStatement cse, DataFlowHelper dfh) {
        if (cse.basic !== null) {
            
            cse.basic.processBasicStatement(dfh)
            
        } else if (cse.behavior !== null) {
    
            cse.behavior.processBehaviourStatement(dfh)
    
        } else if (cse.communication !== null) {
            //call=CallStatement
            cse.communication.processCommunicationStatement(dfh)
            
        } else if (cse.configuration !== null) {
            dfh.checkVariableStatus(cse.configuration.eAllOfType(ReferencedValue))
    
            //special handling for components due to ambiguous parsing
            //see timers below..
    
        } else if (cse.sut !== null) {
            
            for (t : cse.sut.txt) {
                dfh.checkVariableStatus(t.expr.eAllOfType(ReferencedValue))
            }
            
        } else if (cse.test !== null) {
            
            for (t : cse.test.template) {
                dfh.checkVariableStatus(t.eAllOfType(ReferencedValue))
            }
            
        } else if (cse.timer !== null) {
            
            cse.timer.processTimerStatement(dfh)
            
        } else if (cse.verdict !== null) {
            dfh.checkVariableStatus(cse.verdict.expression.eAllOfType(ReferencedValue))
            for (l : cse.verdict.log) {
                dfh.checkVariableStatus(l.eAllOfType(ReferencedValue))
            }
        }                           
    }
    
    protected def void processTimerStatement(TimerStatements timer, DataFlowHelper dfh) {
        dfh.checkVariableStatus(timer.eAllOfType(ReferencedValue))
        //should timers be evaluated as variables? what constitutes initialisation then? 
                
        //special handling for components due to ambiguous parsing
        if (timer.start !== null) {
            val target = timer.start.ref
            if (target instanceof SingleVarInstance) {
                dfh.checkVariableStatus(target,timer);
            }
        } else if (timer.stop !== null && timer.stop.ref !== null && timer.stop.ref.ref !== null) {
            val target = timer.stop.ref.ref
            if (target instanceof SingleVarInstance) {
                dfh.checkVariableStatus(target,timer);
            }
        }
    }
    
    protected def void processCommunicationStatement(CommunicationStatements communication, DataFlowHelper dfh) {
        if (communication.call !== null) {
            
            communication.call.processCallStatement(dfh)
            
        } else {
            //not supported due to ambiguity
            //start=StartStatement
            //stop=StopStatement
                    
            //should be covered below
            //send=SendStatement
            //receive=ReceiveStatement
            //trigger=TriggerStatement
            //check=CheckStatement
            //checkState=CheckStateStatement;
            //halt=HaltStatement
            //clear=ClearStatement
                
            //getReply=GetReplyStatement
            //getCall=GetCallStatement
            //reply=ReplyStatement
            //raise=RaiseStatement
            //catch=CatchStatement
            
            val values = communication.eAllOfType(ReferencedValue)
            dfh.checkVariableStatus(values.filter[eContainer instanceof Value])
            for (v : values.filter[eContainer instanceof VariableRef]) {
                dfh.updateVariableStatus(v)
            }
        }
    }
    
    protected def void processCallStatement(CallStatement call, DataFlowHelper dfh) {
        val oValues = call.op.eAllOfType(ReferencedValue)
        dfh.checkVariableStatus(oValues)
        if (call.body !== null) {
            val bdfhs = newArrayList()
            val bdfhkeys = newHashSet()
            for (b : call.body.callBody.body) {
                var bdfh = new DataFlowHelper(dfh)
                val bValues = b.call.eAllOfType(ReferencedValue)
                dfh.checkVariableStatus(bValues.filter[eContainer instanceof Value])
                for (v : bValues.filter[eContainer instanceof VariableRef]) {
                    //valid only within -> bdfh for the branch, propagate if in all branches
                    bdfh.updateVariableStatus(v)
                }
                
                b.block.traverseChildren(bdfh)
                bdfhs.add(bdfh)
                bdfhkeys.addAll(bdfh.states.keySet)
                    
            }
            for (k : bdfhkeys) {
                if (dfh.getState(k) === STATUS.DECLARED && 
                        bdfhs.forall[states.containsKey(k)]) {
                    dfh.setState(k,bdfhs.get(0).states.get(k))
                }
            }
        }
    }
    
    protected def void processBehaviourStatement(BehaviourStatements behavior, DataFlowHelper dfh) {
            //DONE: also test and handle component variables and timers..
            //NOTE: variables (and possibly other constructs) cross over in scope (resolve to foreign variables)
                //DONE: data flow analysis checks for variables and reports them not declared when they are outside the scope
        if (behavior.alt !== null) {
            behavior.alt.processAltStatement(dfh)

        } else if (behavior.function !== null) {
            //cse.behavior.function -> might be covered twice? -> no
            behavior.function.processFunctionInstance(dfh)
                
        } else if (behavior.interleaved !== null) {
            behavior.interleaved.processInterleavedStatement(dfh)
                
        } else if (behavior.goto !== null) {
            //not supported
        } else if (behavior.label !== null) {
            //not supported
        } else {
            //should cover
            //cse.behavior.altstep
            //cse.behavior.activate
            //cse.behavior.deactivate -> no filtering as it uses VariableRef
            //cse.behavior.testcase
            val values = behavior.eAllOfType(ReferencedValue)
            dfh.checkVariableStatus(values)
            
        }
    }
    
    protected def void processFunctionInstance(FunctionInstance function, DataFlowHelper dfh) {
            val r = function.ref;
            //TODO: also external functions?
            if (r instanceof FunctionDef) {
                if (r.parameterList !== null) {
                    function.processParameters(r.parameterList.params, dfh)
                }
            } else if (r instanceof ExtFunctionDef) {
                if (r.list !== null) {
                    function.processParameters(r.list.params, dfh)
                }
            } else if (r instanceof AltstepDef) {
                if (r.params !== null) {
                    function.processParameters(r.params.params, dfh)
                }
            } else {
                //handle other cases
                val values = function.eAllOfType(ReferencedValue)
                dfh.checkVariableStatus(values)
            }
    }
    
    protected def void processParameters(FunctionInstance function, EList<FunctionFormalPar> formalParameters, DataFlowHelper dfh) {
        var i = 0;
        if (function.params!==null) {
            val actualParameters = function.params.params
            //actual parameters
            for (p : actualParameters) {
                if (p.template!==null) {
                    //get corresponding formal parameter
                    val fp = formalParameters.get(i);
                    if (fp.value !== null && fp.value.inOut !== null) {
                        if (fp.value.inOut.equals("out")) {
                            //update
                            dfh.updateVariableStatus(p.eAllOfType(ReferencedValue).get(0))
                        } else if (fp.value.inOut.equals("in")) {
                            //check
                            dfh.checkVariableStatus(p.eAllOfType(ReferencedValue))
                        }
                        //TODO: how to handle inout?
                    }
                }
                i++;
            }
        }
    }

    protected def void processInterleavedStatement(InterleavedConstruct interleaved, DataFlowHelper dfh) {
        val igl = interleaved.eContents.get(0)
        if (igl instanceof InterleavedGuardList) {
            val bdfhs = newArrayList()
            val bdfhkeys = newHashSet()
            
            for (b : igl.elements) {
                dfh.checkVariableStatus(b.guard.eAllOfType(ReferencedValue))
                var bdfh = new DataFlowHelper(dfh)
                //op 
                val bValues = b.guard.guardOp.eAllOfType(ReferencedValue)
                dfh.checkVariableStatus(bValues.filter[eContainer instanceof Value])
                for (v : bValues.filter[eContainer instanceof VariableRef]) {
                    //valid only within -> bdfh
                    bdfh.updateVariableStatus(v)
                }
                    
                b.statement.traverseChildren(bdfh)
                bdfhs.add(bdfh)
                bdfhkeys.addAll(bdfh.states.keySet)
                    
            }
                
            for (k : bdfhkeys) {
                if (dfh.getState(k) === STATUS.DECLARED && 
                        bdfhs.forall[states.containsKey(k)]) {
                    dfh.setState(k,bdfhs.get(0).states.get(k))
                }
            }
        }
    }
    
    protected def void processAltStatement(AltConstruct alt, DataFlowHelper dfh) {
        val bdfhs = newArrayList()
        val bdfhkeys = newHashSet()
        //agList.elseList+?
        for (b : alt.agList.guardList) {
            dfh.checkVariableStatus(b.guard.eAllOfType(ReferencedValue))
            var bdfh = new DataFlowHelper(dfh)
            //op 
            if (b.op !== null) {
                val bValues = b.op.eAllOfType(ReferencedValue)
                dfh.checkVariableStatus(bValues.filter[eContainer instanceof Value])
                for (v : bValues.filter[eContainer instanceof VariableRef]) {
                    //valid only within -> bdfh for the branch, propagate if in all branches
                    bdfh.updateVariableStatus(v)
                }
            //step
            } else if (b.step !== null) {
                val bValues = b.step.eAllOfType(ReferencedValue)
                dfh.checkVariableStatus(bValues.filter[eContainer instanceof Value])
            }
            if (b.block !== null) {
                b.block.traverseChildren(bdfh)
            }
            bdfhs.add(bdfh)
            bdfhkeys.addAll(bdfh.states.keySet)
        }
                
        //else
        for (b : alt.agList.elseList) {
            var bdfh = new DataFlowHelper(dfh)
            b.block.traverseChildren(bdfh)
            bdfhs.add(bdfh)
            bdfhkeys.addAll(bdfh.states.keySet)
        }
                
        for (k : bdfhkeys) {
            if (dfh.getState(k) === STATUS.DECLARED && 
                    bdfhs.forall[states.containsKey(k)]) {
                dfh.setState(k,bdfhs.get(0).states.get(k))
            }
        }
    }
    
    protected def void processBasicStatement(BasicStatements basic, DataFlowHelper dfh) {
        if (basic.assign !== null) {
        
            //only for assignments or other statements that are not within nested blocks
            dfh.checkVariableStatus(basic.assign.eAllOfType(ReferencedValue))
            dfh.updateVariableStatus(basic.assign.ref.ref)
        
        } else if (basic.block !== null) {
                
            basic.block.processBlock(dfh)
                
        } else if (basic.conditional !== null) {
                
            basic.conditional.processConditionalStatement(dfh)
            
        } else if (basic.log !== null) {
                
            dfh.checkVariableStatus(basic.log.eAllOfType(ReferencedValue))
                
        } else if (basic.loop !== null) {
        
            basic.loop.processLoopStatement(dfh)
            
        } else if (basic.select !== null) {
            
            basic.select.processSelectStatement(dfh)
            
        }
    }
    
    protected def void processBlock(StatementBlock block, DataFlowHelper dfh) {
        var bdfh = new DataFlowHelper(dfh)
        block.traverseChildren(bdfh)
                
        for (k : bdfh.states.keySet) {
            if (dfh.getState(k) === STATUS.DECLARED) {
                dfh.setState(k,bdfh.states.get(k))
            }
        }
    }
    
    protected def void processConditionalStatement(ConditionalConstruct conditional, DataFlowHelper dfh) {
        //condition
        dfh.checkVariableStatus(conditional.expression.eAllOfType(ReferencedValue))
        
        //block
        var sdfh = new DataFlowHelper(dfh)
        conditional.statement.traverseChildren(sdfh)
                
        //elseif
        val eidfhs = newArrayList()
        for (elseIf : conditional.elseifs) {
            //condition
            dfh.checkVariableStatus(elseIf.expression.eAllOfType(ReferencedValue))
                
            var eidfh = new DataFlowHelper(dfh)
            conditional.statement.traverseChildren(eidfh)
            eidfhs.add(eidfh)
        }
        
        //else
        if (conditional.^else !== null) {
            var edfh = new DataFlowHelper(dfh)
            conditional.^else.statement.traverseChildren(edfh)
            
            for (k : edfh.states.keySet) {
                if (sdfh.states.containsKey(k) && 
                        dfh.getState(k) === STATUS.DECLARED && 
                        eidfhs.forall[states.containsKey(k)]) {
                    dfh.setState(k,edfh.states.get(k))
                }
            }
        }
    }
    
    protected def void processSelectStatement(SelectCaseConstruct select, DataFlowHelper dfh) {
        dfh.checkVariableStatus(select.expression.eAllOfType(ReferencedValue))
        
        val bdfhs = newArrayList()
        val bdfhkeys = newHashSet()
        for (sc : select.body.cases) {
            
            //case conditions
            for (t : sc.template) {
                dfh.checkVariableStatus(t.eAllOfType(ReferencedValue))
            }
            
            var bdfh = new DataFlowHelper(dfh)
            sc.statement.traverseChildren(bdfh)
            bdfhs.add(bdfh)
            bdfhkeys.addAll(bdfh.states.keySet)
            
        }
        for (k : bdfhkeys) {
            if (dfh.getState(k) === STATUS.DECLARED && 
                    bdfhs.forall[states.containsKey(k)]) {
                dfh.setState(k,bdfhs.get(0).states.get(k))
            }
        }
    }
    
    protected def void processLoopStatement(LoopConstruct loop, DataFlowHelper dfh) {
        if (loop.forStm !== null) {
                
            var fdfh = new DataFlowHelper(dfh)
                
            if (loop.forStm.init.assignment !== null) {
                fdfh.checkVariableStatus(loop.forStm.init.assignment.eAllOfType(ReferencedValue))
                //TODO: Not sure if this applies to outer scope, assuming yes
                dfh.updateVariableStatus(loop.forStm.init.assignment.ref.ref)
            } else if (loop.forStm.init.variable !== null) {
                for (target : loop.forStm.init.variable.list.variables) {
                    fdfh.checkVariableStatus(target.eAllOfType(ReferencedValue))
                    //this should definitely be in the scope of the loop
                    fdfh.updateVariableStatus(target)
                }
            } 
            
            fdfh.checkVariableStatus(loop.forStm.expression.eAllOfType(ReferencedValue))
            fdfh.checkVariableStatus(loop.forStm.assign.eAllOfType(ReferencedValue))
            fdfh.updateVariableStatus(loop.forStm.assign.ref.ref)
                 
            loop.forStm.statement.traverseChildren(fdfh)
                
        } else if (loop.whileStm !== null) {
        
             dfh.checkVariableStatus(loop.whileStm.expression.eAllOfType(ReferencedValue))
             loop.whileStm.statement.traverseChildren(new DataFlowHelper(dfh))
        
        } else if (loop.doStm !== null) {
                
             dfh.checkVariableStatus(loop.doStm.expression.eAllOfType(ReferencedValue))
             loop.doStm.statement.traverseChildren(new DataFlowHelper(dfh))
            
        }
    }
    

    protected def void updateVariableStatus(DataFlowHelper dfh, SingleVarInstance target) {
        if (dfh.getState(target) === STATUS.UNDECLARED) {
            if (target.expr !== null) {
                dfh.states.put(target,STATUS.INITIALISED)
            } else {
                dfh.states.put(target,STATUS.DECLARED)
            }
//            println("###     "+USE.DEFINE+"->"+dfh.states.get(target)+": "+target.name)
        } else {
//            println("###   ERROR: "+target.name+" already declared!")
        }
    }

    protected def void updateVariableStatus(DataFlowHelper dfh, SingleTempVarInstance target) {
        if (dfh.getState(target) === STATUS.UNDECLARED) {
            if (target.template !== null) {
                dfh.states.put(target,STATUS.INITIALISED)
            } else {
                dfh.states.put(target,STATUS.DECLARED)
            }
//            println("###     "+USE.DEFINE+"->"+dfh.states.get(target)+": "+target.name)
        } else {
//            println("###   ERROR: "+target.name+" already declared!")
        }
    }


    
    protected def void updateVariableStatus(DataFlowHelper dfh, ReferencedValue value) {
        val target = value.head.target
        if (target instanceof SingleVarInstance) {
            if (dfh.getState(target) !== STATUS.UNDECLARED) {
                if (dfh.getState(target) ===  STATUS.DECLARED) {
                    dfh.setState(target,STATUS.INITIALISED)
                }
//                println("###     "+USE.DEFINE+"->"+dfh.getState(target)+": "+target.name)
            } else {
//                println("###   ERROR: "+target.name+" not declared!")

                if (!(target.eContainer.eContainer.eContainer instanceof ComponentElementDef)) {
//                    println("###   ERROR: "+target.name+" not declared!")
                    target.uninitialisedVariableWarning("declared", value)
                }
            }
        }
    }
    
    protected def void checkVariableStatus(DataFlowHelper dfh, Iterable<ReferencedValue> values) {
        for (value : values.filter[eContainer instanceof Value
            || (eContainer instanceof VariableRef //what was this for again?
                && !(eContainer.eContainer instanceof Assignment)
            )
        ]) {
            //check states
            val target = value.head.target
            if (target instanceof SingleVarInstance) {
                if (dfh.getState(target) !== STATUS.UNDECLARED) {
                    //println("###     "+dfh.getState(target)+"->"+USE.REFERENCE+": "+target.name)                                       

                    if (dfh.getState(target) === STATUS.DECLARED) {
                        target.uninitialisedVariableWarning("initialised", value)
                    }

                } else {
//                    println("###   ERROR: "+target.name+" not declared!")

                    //special handling of timers and components due to ambiguity
                    val parent = target.eContainer.eContainer
                    if (!(parent instanceof TimerInstance) && !(parent.eContainer instanceof ComponentElementDef)) {
                        target.uninitialisedVariableWarning("declared", value)
                    }
                }
            }
        }
    }

    protected def void checkVariableStatus(DataFlowHelper dfh, SingleVarInstance target, EObject context) {
        //special handling of component variables due to incorrect parsing and peculiar design decisions
        val parent = target.eContainer.eContainer
        if (parent instanceof VarInstance) {
            if (parent.listType.ref.head instanceof ComponentDef) {
                if (dfh.getState(target) !== STATUS.UNDECLARED) {
                    //println("###     "+dfh.getState(target)+"->"+USE.REFERENCE+": "+target.name)                                       

                    if (dfh.getState(target) === STATUS.DECLARED) {
                        target.uninitialisedVariableWarning("initialised", context)
                    }

                } else {
//                    println("###   ERROR: "+target.name+" not declared!")
                    if (!(parent.eContainer instanceof ComponentElementDef)) {
                        target.uninitialisedVariableWarning("declared", context)
                    }
                }
            }
        }
    }
    
    protected def void uninitialisedVariableWarning(SingleVarInstance target, String status, EObject context) {
        TTCN3StatisticsProvider.getInstance.incrementCountStyle
        var parentName = "Module Control Part"
        val parent = context.findDesiredParent(FunctionDef)
        if (parent !== null) {
            parentName = parent.name 
        } else {
            val asd = context.findDesiredParent(AltstepDef)
            if (asd !== null) {
                parentName = asd.name
            } 
        }            
        val message = "Local definition for \"" + target.name + "\" in definition of \"" + parentName +
            "\" is not "+status+"!"
        val INode node = NodeModelUtils.getNode(context)
        warning(
            message,
            context,
            null,
            MessageClass.STYLE.toString,
            node.startLine.toString,
            node.endLine.toString,
            "6.10, " + "checkNoUninitialisedVariables"
        )
    }


    
}