package de.ugoe.cs.swe.common.exceptions;

public class TerminationException extends Exception {

	private static final long serialVersionUID = -2717985520434639235L;

	public TerminationException(String message) {
		super("Errors occurred during execution! Could not continue! \n\t" + message);
		System.err.println(super.getMessage());
		System.exit(1);
	}

}
