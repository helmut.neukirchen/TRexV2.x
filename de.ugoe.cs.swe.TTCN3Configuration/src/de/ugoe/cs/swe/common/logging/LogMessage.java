package de.ugoe.cs.swe.common.logging;

import de.ugoe.cs.swe.common.logging.LoggingInterface.LogLevel;
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass;

public class LogMessage {
	private String prefix;
	private String filename;
	private int startLine;
	private int endLine;
	private LogLevel logLevel;
	private MessageClass messageClass;
	private String message;
	private String details;
	
	public LogMessage(){
		
	}
	
	public LogMessage(String filename, int startLine, int endLine, LogLevel logLevel, MessageClass messageClass, String message, String details){
		this.setFilename(filename);
		this.setStartLine(startLine);
		this.setEndLine(endLine);
		this.setLogLevel(logLevel);
		this.setMessageClass(messageClass);
		this.setMessage(message);
		this.setDetails(details);
	}
	
	public String toString(){
		String output = "";
		output += getPrefix();
		if (getFilename() != null) {
			output += getFilename() + ": ";
		}

		if (getStartLine() >= getEndLine()) {
			output += getStartLine() + ": ";
		} else {
			output += getStartLine() + "-" + getEndLine() + ": ";
		}

		output += getLogLevel() + ": ";

		if (getMessageClass() != null) {
			output += getMessageClass().getDescription() + ": ";
		}

		output += getMessage();

		if (getDetails() != null) {
			output += " (" + getDetails() + ")";
		}

		return output;
	}
	
	
	public int getStartLine() {
		return startLine;
	}
	public void setStartLine(int startLine) {
		this.startLine = startLine;
	}
	public int getEndLine() {
		return endLine;
	}
	public void setEndLine(int endLine) {
		this.endLine = endLine;
	}
	public LogLevel getLogLevel() {
		return logLevel;
	}
	public void setLogLevel(LogLevel logLevel) {
		this.logLevel = logLevel;
	}
	public MessageClass getMessageClass() {
		return messageClass;
	}
	public void setMessageClass(MessageClass messageClass) {
		this.messageClass = messageClass;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}


	public String getFilename() {
		return filename;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getPrefix() {
		return prefix;
	}
	
}
