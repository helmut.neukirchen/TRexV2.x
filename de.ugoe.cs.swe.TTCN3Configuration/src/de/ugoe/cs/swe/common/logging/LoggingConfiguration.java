package de.ugoe.cs.swe.common.logging;

public class LoggingConfiguration {
	private boolean showFullPath = false;
	private boolean showFilename = true;
	private boolean showMessageClass = true;
	private boolean showDetails = true;
	private String logOutputPrefix = "   ";

	public boolean isShowFullPath() {
		return showFullPath;
	}

	public void setShowFullPath(boolean showFullPath) {
		this.showFullPath = showFullPath;
	}

	public boolean isShowFilename() {
		return showFilename;
	}

	public void setShowFilename(boolean showFilename) {
		this.showFilename = showFilename;
	}

	public boolean isShowMessageClass() {
		return showMessageClass;
	}

	public void setShowMessageClass(boolean showMessageClass) {
		this.showMessageClass = showMessageClass;
	}

	public boolean isShowDetails() {
		return showDetails;
	}

	public void setShowDetails(boolean showDetails) {
		this.showDetails = showDetails;
	}

	public void setLogOutputPrefix(String logOutputPrefix) {
		this.logOutputPrefix = logOutputPrefix;
	}

	public String getLogOutputPrefix() {
		return logOutputPrefix;
	}

}
