package de.ugoe.cs.swe.TTCN3Configuration;

public class NamingConventionsConfig {

	private String moduleRegExp = "[A-Z].*";
	private String groupRegExp = "[a-z].*";
	private String dataTypeRegExp = "[A-Z].*";
	private String messageTemplateRegExp = "m_[a-z].*";
	private String messageTemplateWithWildcardsRegExp = "mw_[a-z].*";
	private String derivedMessageTemplateRegExp = "md_[a-z].*";
	private String derivedMessageTemplateWithWildcardsRegExp = "mdw_[a-z].*";
	private String stf160sendTemplateRegExp = "cs_[a-z].*";
	private String stf160receiveTemplateRegExp = "cr_[a-z].*";
	private String derivedStf160sendTemplateRegExp = "dcs_[a-z].*";
	private String derivedStf160receiveTemplateRegExp = "dcr_[a-z].*";
	private String signatureTemplateRegExp = "s_[a-z].*";
	private String portInstanceRegExp = "[a-z].*";
	private String componentInstanceRegExp = "[a-z].*";
	private String constantRegExp = "c_[a-z].*";
	private String localConstantRegExp = "cl_[a-z].*";
	private String extConstantRegExp = "cx_[a-z].*";
	private String functionRegExp = "f_[a-z].*";
	private String extFunctionRegExp = "fx_[a-z].*";
	private String altstepRegExp = "a_[a-z].*";
	private String testcaseRegExp = "TC_.*";
	private String variableRegExp = "v_[a-z].*";
	private String componentVariableRegExp = "vc_[a-z].*";
	private String timerRegExp = "t_[a-z].*";
	private String componentTimerRegExp = "tc_[a-z].*";
	private String moduleParameterRegExp = "[A-Z][A-Z_1-9]*";
	private String formalParameterRegExp = "p_[a-z].*";
	private String enumeratedValueRegExp = "e_[a-z].*";

	public String getModuleRegExp() {
		return moduleRegExp;
	}

	public void setModuleRegExp(String moduleRegExp) {
		this.moduleRegExp = moduleRegExp;
	}

	public String getGroupRegExp() {
		return groupRegExp;
	}

	public void setGroupRegExp(String groupRegExp) {
		this.groupRegExp = groupRegExp;
	}

	public String getDataTypeRegExp() {
		return dataTypeRegExp;
	}

	public void setDataTypeRegExp(String dataTypeRegExp) {
		this.dataTypeRegExp = dataTypeRegExp;
	}

	public String getMessageTemplateRegExp() {
		return messageTemplateRegExp;
	}

	public void setMessageTemplateRegExp(String messageTemplateRegExp) {
		this.messageTemplateRegExp = messageTemplateRegExp;
	}

	public String getMessageTemplateWithWildcardsRegExp() {
		return messageTemplateWithWildcardsRegExp;
	}

	public void setMessageTemplateWithWildcardsRegExp(
			String messageTemplateWithWildcardsRegExp) {
		this.messageTemplateWithWildcardsRegExp = messageTemplateWithWildcardsRegExp;
	}

	public String getSignatureTemplateRegExp() {
		return signatureTemplateRegExp;
	}

	public void setSignatureTemplateRegExp(String signatureTemplateRegExp) {
		this.signatureTemplateRegExp = signatureTemplateRegExp;
	}

	public String getPortInstanceRegExp() {
		return portInstanceRegExp;
	}

	public void setPortInstanceRegExp(String portInstanceRegExp) {
		this.portInstanceRegExp = portInstanceRegExp;
	}

	public String getComponentInstanceRegExp() {
		return componentInstanceRegExp;
	}

	public void setComponentInstanceRegExp(String componentInstanceRegExp) {
		this.componentInstanceRegExp = componentInstanceRegExp;
	}

	public String getConstantRegExp() {
		return constantRegExp;
	}

	public void setConstantRegExp(String constantRegExp) {
		this.constantRegExp = constantRegExp;
	}

	public String getExtConstantRegExp() {
		return extConstantRegExp;
	}

	public void setExtConstantRegExp(String extConstantRegExp) {
		this.extConstantRegExp = extConstantRegExp;
	}

	public String getAltstepRegExp() {
		return altstepRegExp;
	}

	public void setAltstepRegExp(String altstepRegExp) {
		this.altstepRegExp = altstepRegExp;
	}

	public String getTestcaseRegExp() {
		return testcaseRegExp;
	}

	public void setTestcaseRegExp(String testcaseRegExp) {
		this.testcaseRegExp = testcaseRegExp;
	}

	public String getVariableRegExp() {
		return variableRegExp;
	}

	public void setVariableRegExp(String variableRegExp) {
		this.variableRegExp = variableRegExp;
	}

	public String getComponentVariableRegExp() {
		return componentVariableRegExp;
	}

	public void setComponentVariableRegExp(String componentVariableRegExp) {
		this.componentVariableRegExp = componentVariableRegExp;
	}

	public String getTimerRegExp() {
		return timerRegExp;
	}

	public void setTimerRegExp(String timerRegExp) {
		this.timerRegExp = timerRegExp;
	}

	public String getComponentTimerRegExp() {
		return componentTimerRegExp;
	}

	public void setComponentTimerRegExp(String componentTimerRegExp) {
		this.componentTimerRegExp = componentTimerRegExp;
	}

	public String getModuleParameterRegExp() {
		return moduleParameterRegExp;
	}

	public void setModuleParameterRegExp(String moduleParameterRegExp) {
		this.moduleParameterRegExp = moduleParameterRegExp;
	}

	public String getFormalParameterRegExp() {
		return formalParameterRegExp;
	}

	public void setFormalParameterRegExp(String formalParameterRegExp) {
		this.formalParameterRegExp = formalParameterRegExp;
	}

	public String getEnumeratedValueRegExp() {
		return enumeratedValueRegExp;
	}

	public void setEnumeratedValueRegExp(String enumeratedValueRegExp) {
		this.enumeratedValueRegExp = enumeratedValueRegExp;
	}

	public void setFunctionRegExp(String functionRegExp) {
		this.functionRegExp = functionRegExp;
	}

	public void setFunctionsRegExp(String functionRegExp) {
		this.functionRegExp = functionRegExp;
	}

	public String getFunctionRegExp() {
		return functionRegExp;
	}

	public void setExtFunctionRegExp(String extFunctionRegExp) {
		this.extFunctionRegExp = extFunctionRegExp;
	}

	public String getExtFunctionRegExp() {
		return extFunctionRegExp;
	}

	public void setLocalConstantRegExp(String localConstantRegExp) {
		this.localConstantRegExp = localConstantRegExp;
	}

	public String getLocalConstantRegExp() {
		return localConstantRegExp;
	}

	public void setDerivedMessageTemplateRegExp(
			String derivedMessageTemplateRegExp) {
		this.derivedMessageTemplateRegExp = derivedMessageTemplateRegExp;
	}

	public String getDerivedMessageTemplateRegExp() {
		return derivedMessageTemplateRegExp;
	}

	public void setDerivedMeessageTeemplateWithWildcardsRegExp(
			String derivedMessageTemplateWithWildcardsRegExp) {
		this.derivedMessageTemplateWithWildcardsRegExp = derivedMessageTemplateWithWildcardsRegExp;
	}

	public String getDerivedMessageTemplateWithWildcardsRegExp() {
		return derivedMessageTemplateWithWildcardsRegExp;
	}

	public void setStf160sendTemplateRegExp(String stf160sendTemplateRegExp) {
		this.stf160sendTemplateRegExp = stf160sendTemplateRegExp;
	}

	public String getStf160sendTemplateRegExp() {
		return stf160sendTemplateRegExp;
	}

	public void setStf160receiveTemplateRegExp(
			String stf160receiveTemplateRegExp) {
		this.stf160receiveTemplateRegExp = stf160receiveTemplateRegExp;
	}

	public String getStf160receiveTemplateRegExp() {
		return stf160receiveTemplateRegExp;
	}

	public void setDerivedStf160sendTemplateRegExp(
			String derivedStf160sendTemplateRegExp) {
		this.derivedStf160sendTemplateRegExp = derivedStf160sendTemplateRegExp;
	}

	public String getDerivedStf160sendTemplateRegExp() {
		return derivedStf160sendTemplateRegExp;
	}

	public void setDerivedStf160receiveTemplateRegExp(
			String derivedStf160receiveTemplateRegExp) {
		this.derivedStf160receiveTemplateRegExp = derivedStf160receiveTemplateRegExp;
	}

	public String getDerivedStf160receiveTemplateRegExp() {
		return derivedStf160receiveTemplateRegExp;
	}

}
