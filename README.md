TRex - the TTCN-3 Refactoring and Metrics Tool

The comprehensive test of modern communication systems leads to large and complex 
test suites which have to be maintained throughout the system life-cycle. 
Experience with those written in the standardised Testing and Test Control Notation (TTCN-3) 
has shown that the maintenance of test suites is a non-trivial task and can often be a burden. 
The time and effort required for maintenance tasks can be reduced with appropriate tool support. 
To this aim, we have developed the TRex tool, published as an open-source Eclipse plugin under 
the Eclipse Public License, which provides IDE functionality for the TTCN-3 core notation and 
supports the assessment and automatic restructuring of TTCN-3 test suites by providing suitable 
metrics and refactorings.

We invite interested Java programmers to contribute as well to the development of TRex.

Wiki page: https://gitlab.com/helmut.neukirchen/TRexV2.x/wikis/home

Note: TRex does not include a TTCN-3 compiler or interpreter!
