package de.ugoe.cs.swe.ui.outline

import org.eclipse.xtext.ui.editor.ISourceViewerAware

/** 
 * Customization of the default outline structure.
 * see http://www.eclipse.org/Xtext/documentation.html#outline
 */
class TTCN3OutlinePage extends org.eclipse.xtext.ui.editor.outline.impl.OutlinePage implements ISourceViewerAware {
	
	// Opens the outline fully expanded by default.
	// If the outline should be minimized by default
	// then change Integer.MAX_VALUE to 1
	override protected int getDefaultExpansionLevel() {
		return Integer.MAX_VALUE
	}
}
