package de.ugoe.cs.swe.ui.labeling;

import org.eclipse.swt.graphics.Image;

import de.ugoe.cs.swe.ui.TTCN3UIPluginImages;

public class TTCN3OutlineTreeNodeLabelingUtils  {
	
	// Whole thing needs to be improved by making each case more explicit.
	// An example where this could break is in tree nodes with text: "record : RecordDefNamed : >nameOfRecord<"
	// >nameOfRecord< being any name that user defined. And >nameOfRecord< could be for example "StructFieldDef"
	// in which case this would break and possibly display wrong icons.
	
	// Whenever there are three sections (textParts) we are checking for the middle part only.
	// Idea: expand contains check to first part plus second part (or just the full amount of sections except where something user named can exist).
	// Example: XYZ.contains("component : ComponentDef") instead of just XYZ.contains("ComponentDef")
	
	// modelElement textParts we consider representing "default" elements
	// i.e. returning the "Default" image from the getIcon method
	private static String[] defaultTypes = {"TTCN3Module", "ModuleDefinitionsList", "ControlPart", "StructFieldDef"};
	
	// modelElement textParts we consider representing "type" elements
	// i.e. returning the "Type" image from the getIcon method
	// --Note: "PortDef :" intentionally includes the " :" part to separate it from modelElements
	// --      with textParts of the form " abc: def : PortDef"
	private static String[] typeTypes = {"RecordDefNamed", "PortDef :", "ComponentDef"};
	
	// modelElement textParts we consider representing "template" elements
	// i.e. returning the "Template" image from the getIcon method
	private static String[] templateTypes = {"BaseTemplate"};
	
	// modelElement textParts we consider representing "testcase" elements
	// i.e. returning the "Testcase" image from the getIcon method
	private static String[] testcaseTypes = {"TestcaseDef"};
	
	// Fetches the icon to display for an element
	public static Image getIcon(Object modelElement, String elementText) {
		
		// Sanity check, if we receive null-data, return the 
		// "Other" image rather than an error
		if(modelElement == null || elementText == null) {
    		return TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_OTHER);    		
    	}
		
		// Use the "Default" image for modelElements having textParts
		// stated in the defaultTypes array
		for(String type : defaultTypes) {
			if(elementText.contains(type)) return TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_MISC_DEFAULT);
		}
		
		// Use the "Type" image for modelElements having textParts
		// stated in the typeTypes array
		for(String type : typeTypes) {
			if( elementText.contains(type)) return TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_TYPE);
		}
	
		// Use "Template" image for modelElements having textParts 
		// stated in the templateTypes array
		for(String type : templateTypes) {
			if(elementText.contains(type)) return TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_TEMPLATE);
		}
		
		// Use "Testcase" image for modelElements having textParts 
		// stated in the testcaseTypes array
		for(String type : testcaseTypes) {
			if(elementText.contains(type)) return TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_TESTCASE);
		}
		
		return TTCN3UIPluginImages.imageCacheGet(TTCN3UIPluginImages.DESC_OTHER);
	}
	
	// Same problem as for getImage(...) above but affecting the text of the nodes
	// so some nodes could have the wrong text displayed.
	
	// Fetches the text to display for an element
	public static Object getNodeText(Object elementText) {
		
		String[] textStringParts = elementText.toString().split(" : ");
		
		if ( elementText.toString().contains("TTCN3Module")) return textStringParts[2];
		if ( elementText.toString().contains("ModuleDefinitionsList")) return "Module Declarations";
		if ( elementText.toString().contains("ControlPart")) return "ControlPart";
		if ( elementText.toString().contains("StructFieldDef")) { return textStringParts[2].concat(" : ".concat(textStringParts[1]));}
		
		if ( elementText.toString().contains("RecordDefNamed")) return textStringParts[2].concat(" : ".concat(textStringParts[1]));
		if ( elementText.toString().contains("PortDef :")) return textStringParts[2].concat(" : ".concat(textStringParts[1]));
		if ( elementText.toString().contains("ComponentDef :")) return textStringParts[2].concat(" : ".concat(textStringParts[1]));
		
		if ( elementText.toString().contains("BaseTemplate")) return textStringParts[2].concat(" : ".concat(textStringParts[1]));
		
		if ( elementText.toString().contains("TestcaseDef")) return textStringParts[1];
		
		return elementText;
	}
	
	public static Boolean shouldBeDisplayed(Object modelElement, String elementText) {
		// Sanity check, if we receive null-data, return true
		// Display information if not sure rather than hiding it.
		if(modelElement == null || elementText == null) {
			return true;    		
    	}
		
		// Group all textPart arrays in one 2-dimensional array
		// to be able to later use a single for loop to iterate through them
		String[][] validElementTextParts = {defaultTypes, typeTypes, templateTypes, testcaseTypes};
		
		// Iterate through items in arrays in validElementTextParts
		// If we find a match (the received elementText containing the textPart)
		// we know this element should be displayed
		for(String[] textPartArray : validElementTextParts) {
			for(String textPart : textPartArray) {
				if(elementText.contains(textPart)) return true;
			}
		}
		
		return false;
		
	}
}
