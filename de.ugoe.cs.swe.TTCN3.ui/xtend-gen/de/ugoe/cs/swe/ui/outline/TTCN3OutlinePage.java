package de.ugoe.cs.swe.ui.outline;

import org.eclipse.xtext.ui.editor.ISourceViewerAware;
import org.eclipse.xtext.ui.editor.outline.impl.OutlinePage;

/**
 * Customization of the default outline structure.
 * see http://www.eclipse.org/Xtext/documentation.html#outline
 */
@SuppressWarnings("all")
public class TTCN3OutlinePage extends OutlinePage implements ISourceViewerAware {
  @Override
  protected int getDefaultExpansionLevel() {
    return Integer.MAX_VALUE;
  }
}
