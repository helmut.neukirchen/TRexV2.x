# How to contribute to TRex2.0

You must have Eclipse installed, at version Oxygen or higher, on a computer with a minimum of 1.6 GB of RAM. The current download site is located [here](http://www.eclipse.org/downloads/eclipse-packages/).
 

You must also have Xtext installed as an Eclipse plugin. The following section walks you through the install, with the assumption that you already have Eclipse up and running.

## Setting up Xtext for Eclipse

1.  In the Eclipse menu bar, select "Help" -> "Install new software...", then click the "Add" button in the top right. A new menu called "Add repository" will pop up.
2.  In the "Name" field you can write whatever you like - we suggest something descriptive like "Xtext releases update site" - while in the "Location" field you should enter the http://download.eclipse.org/modeling/tmf/xtext/updates/composite/releases/ link. Then press the "OK" button. A list of possible downloads will appear. Please note that this step and the next may take a while; if you're waiting for Eclipse to respond, look for a small green progress bar in the bottom right-hand corner of the program to see how far along you are in the process.
3.   One of those downloads is called "Xtext". Press the '>' character next to its checkbox, check the subitem called "Xtext Complete SDK" and press the "Next" button, then "Accept License", and finally "Finish". Please note that if asked, you need to confirm that you trust the certificate.

(Optional) If you wish to familiarize yourself with Xtext, we suggest starting with the "5 minute tutorial" found at https://www.eclipse.org/Xtext/documentation/101_five_minutes.html

Now you need to test Xtext is correctly set up for Eclipse. To do so, please follow the steps listed in the "15 minute tutorial" at https://www.eclipse.org/Xtext/documentation/102_domainmodelwalkthrough.html up to 
the "Run the Generated Eclipse Plug-in" section. 

Once you've completed the "Run as" -> "Eclipse Application", you may be asked to select a launch configuration; if so, choose "Eclipse Application". However, please note that this configuration may not enable the correct plug-ins for the second Eclipse instance you just ran. To ensure that it's enabled, do the following:
1.  Choose "Run As" -> "Run configurations..."
2.  Select your "Eclipse Application" configuration and click the "Plug-ins" tab
3.  From the "Launch with" drop-down menu, choose the "plug-ins selected below only" option.
4.  Click the "Select all" button on the right, then *deselect* from your Workspace plug-in tree (not your Target Platform tree) all plugins that are not from <pre>org.example.domainmodel.*</pre>

## Contributing to TRex2.0

**When committing code to the TRex2.0 codebase, please respect the following coding rules:**

## Style Guide

- [Code Conventions for the Java Programming Language](http://www.oracle.com/technetwork/java/javase/documentation/codeconvtoc-136057.html). There is a warning that these guidelines may not be valid since they have not been changed since 1999, but the general rules are still very valid.
- There are also some Eclipse specific resources available, like the [Development Conventions and guidelines](https://wiki.eclipse.org/Development_Conventions_and_Guidelines).
- Format your code using Eclipse's default code formatting options (they may violate Java's Code Conventions. In this case, Eclipse's default code formatting have precedence).
- Do not hesitate to use Java 8 language features.
- Use Javadoc comments, relevent tags only. See [How to Write Doc Comments for the Javadoc Tool](http://www.oracle.com/technetwork/java/javase/tech/index-137868.html), and also a helpful video on YouTube: [Writing Javadoc Comments in Eclipse](https://www.youtube.com/watch?v=6XoVf4x-tag) (Skip to 4:00 for a quick tip).


## Clean code

- You may use Eclipes's Source -&gt; Clean up functionality with default settings. (Even though the default settings do not clean up very much.)
- Minimise compiler warnings. 
- For ANTLR-generated code, we have disabled warnings; but for code which is under our control, we do not want to see any warnings.


## Unit Tests

- Add JUnit tests for the non-UI components where possible.

## Issues

Open issues can be found under Issues in GitLab. If you plan on working on an issue:
1. Open it from the list and assign yourself to it in the top of the panel to the right
1. Create a branch for the issue, each branch should be limited to the resolution of one issue. The naming convention of branches is: [Issue ID]-[Issue description]
1. Git commit messages for the branch should contain #[Issue ID], along with some short explanitory text. (Do not describe changes on the syntactical level: the syntax of your changes can be obtained by using the Compare functionality).
1. When the issue is resolved add "Resolved" label to it in the issue tracker in GitLab and assign it to the project manager(Helmut). Do not close the issue, the project manager will take care of that.

How to create a new issue:
1. Under the issues list click "Create New Issue"
1. The title should include a short description of the problem
1. Under description write a more detailed description, but keep it simple. You may also include any information on how to solve the issue.
1. Leave the issue unassigned or assign it to yourself
1. Add relevant label and weight
1. Submit the new issue
1. Follow the rules for working on issues as described above.


## Merge Requests

Use merge requests when merging feature branch to master:

1. In GitLab create new merge request
1. If not already selected choose feature branch as source and master branch as target
1. To submit your work in progress for review start title with WIP:
1. As Assignee select project manager (Helmut)
1. Select to submit your merge request
